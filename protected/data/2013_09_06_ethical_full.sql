/*
SQLyog Enterprise - MySQL GUI v7.14 
MySQL - 5.5.27 : Database - ethics_storage
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`ethics_storage` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `ethics_storage`;

/*Table structure for table `administrator` */

DROP TABLE IF EXISTS `administrator`;

CREATE TABLE `administrator` (
  `admin_id` int(15) NOT NULL,
  PRIMARY KEY (`admin_id`),
  CONSTRAINT `administrator_ibfk_1` FOREIGN KEY (`admin_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `administrator` */

insert  into `administrator`(`admin_id`) values (25);

/*Table structure for table `application` */

DROP TABLE IF EXISTS `application`;

CREATE TABLE `application` (
  `stage` tinyint(1) NOT NULL,
  `project_id` int(15) NOT NULL AUTO_INCREMENT,
  `user_id` int(20) NOT NULL,
  `status` varchar(20) DEFAULT 'not_submited',
  `create_time` time NOT NULL,
  `form_id` int(15) DEFAULT NULL,
  `application_type` varchar(20) NOT NULL,
  PRIMARY KEY (`project_id`),
  UNIQUE KEY `form_id` (`form_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `application_ibfk_22` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=latin1;

/*Data for the table `application` */

insert  into `application`(`stage`,`project_id`,`user_id`,`status`,`create_time`,`form_id`,`application_type`) values (0,36,31,'not_submited','22:24:11',NULL,'form1'),(0,43,31,'not_submited','23:02:06',NULL,'Low Risk Research'),(0,44,31,'not_submited','23:14:20',NULL,'Low Risk Research'),(0,45,31,'not_submited','23:15:10',NULL,'Low Risk Research'),(0,46,31,'not_submited','23:22:19',NULL,'Low Risk Research'),(0,47,31,'not_submited','23:23:21',NULL,'Low Risk Research'),(0,48,31,'not_submited','23:25:14',NULL,'Low Risk Research'),(0,49,31,'not_submited','23:44:01',NULL,'form1'),(0,50,31,'not_submited','23:52:42',NULL,'form1'),(0,53,31,'not_submited','23:55:08',NULL,'Multicentre Research'),(0,54,31,'not_submited','23:58:57',NULL,'Multicentre Research'),(0,55,31,'not_submited','00:02:30',NULL,'Research involving R'),(0,56,31,'not_submited','00:31:02',NULL,'Low Risk Research'),(0,57,31,'not_submited','00:40:36',NULL,'Low Risk Research'),(0,58,31,'not_submited','00:46:46',NULL,'Low Risk Research'),(0,59,31,'not_submited','01:00:26',NULL,'Low Risk Research'),(0,60,31,'not_submited','01:03:30',NULL,'Low Risk Research'),(0,61,31,'not_submited','22:39:09',NULL,'Research involving R'),(0,62,31,'not_submited','22:46:28',NULL,'Low Risk Research'),(0,63,25,'not_submited','22:52:10',NULL,'Multicentre Research'),(0,64,25,'not_submited','23:05:10',NULL,'form1'),(0,67,25,'not_submited','00:40:49',NULL,'Low Risk Research');

/*Table structure for table `application_form1` */

DROP TABLE IF EXISTS `application_form1`;

CREATE TABLE `application_form1` (
  `stage` tinyint(1) NOT NULL,
  `project_id` int(15) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `status` varchar(20) NOT NULL,
  `create_time` datetime NOT NULL,
  `form_id` int(15) NOT NULL,
  `application_type` varchar(20) NOT NULL,
  PRIMARY KEY (`project_id`),
  KEY `project_id` (`project_id`,`user_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `application_form1` */

/*Table structure for table `application_low_risk` */

DROP TABLE IF EXISTS `application_low_risk`;

CREATE TABLE `application_low_risk` (
  `stage` tinyint(1) NOT NULL,
  `project_id` int(15) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `status` varchar(20) NOT NULL,
  `create_time` datetime NOT NULL,
  `form_id` int(15) NOT NULL,
  `application_type` varchar(20) NOT NULL,
  PRIMARY KEY (`project_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `application_low_risk` */

/*Table structure for table `application_multicentre` */

DROP TABLE IF EXISTS `application_multicentre`;

CREATE TABLE `application_multicentre` (
  `stage` tinyint(1) NOT NULL,
  `project_id` int(15) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `status` varchar(20) NOT NULL,
  `create_time` datetime NOT NULL,
  `form_id` int(15) NOT NULL,
  `application_type` varchar(20) NOT NULL,
  PRIMARY KEY (`project_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `application_multicentre` */

/*Table structure for table `application_radiation` */

DROP TABLE IF EXISTS `application_radiation`;

CREATE TABLE `application_radiation` (
  `stage` tinyint(1) NOT NULL,
  `project_id` int(15) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `status` varchar(20) NOT NULL,
  `create_time` datetime NOT NULL,
  `form_id` int(15) NOT NULL,
  `application_type` varchar(20) NOT NULL,
  PRIMARY KEY (`project_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `application_radiation` */

/*Table structure for table `compliance_privacy_legislation` */

DROP TABLE IF EXISTS `compliance_privacy_legislation`;

CREATE TABLE `compliance_privacy_legislation` (
  `section_id` int(10) NOT NULL AUTO_INCREMENT,
  `collecting` varchar(100) NOT NULL,
  `answer` varchar(100) NOT NULL,
  `other_explain` varchar(100) NOT NULL,
  `researchers_access_data` varchar(100) NOT NULL,
  `data_retained` varchar(100) NOT NULL,
  `victorian_privacy` varchar(100) NOT NULL,
  `information_handled` varchar(100) NOT NULL,
  `descibe_procedures` varchar(100) NOT NULL,
  `collection_of_information` varchar(100) NOT NULL,
  `information_collected` varchar(100) NOT NULL,
  `sensi_infor` varchar(100) NOT NULL,
  `health_infor` varchar(100) NOT NULL,
  `identity_of_organisation` varchar(100) NOT NULL,
  `purpose` varchar(100) NOT NULL,
  `periods_for_records` varchar(100) NOT NULL,
  `steps_taken` varchar(100) NOT NULL,
  `types` varchar(100) NOT NULL,
  `privacy_protected` varchar(100) NOT NULL,
  `access_information` varchar(100) NOT NULL,
  `law_required` varchar(100) NOT NULL,
  `consequences` varchar(100) NOT NULL,
  UNIQUE KEY `id_6` (`section_id`),
  KEY `id` (`section_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `compliance_privacy_legislation` */

insert  into `compliance_privacy_legislation`(`section_id`,`collecting`,`answer`,`other_explain`,`researchers_access_data`,`data_retained`,`victorian_privacy`,`information_handled`,`descibe_procedures`,`collection_of_information`,`information_collected`,`sensi_infor`,`health_infor`,`identity_of_organisation`,`purpose`,`periods_for_records`,`steps_taken`,`types`,`privacy_protected`,`access_information`,`law_required`,`consequences`) values (1,'0','0','sfa','1','1','1','6.3','6.4','1','1','0','0','1','1','1','1','1','1','1','0','0'),(2,'0','0','132','0','0','1','21312','313','0','1','0','0','1','1','1','1','1','1','1','0','0'),(3,'1','0','123','1','1','1','123','123','1','0','0','1','1','1','1','1','1','1','1','0','0');

/*Table structure for table `configurations` */

DROP TABLE IF EXISTS `configurations`;

CREATE TABLE `configurations` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `value` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `configurations` */

insert  into `configurations`(`id`,`name`,`value`) values (1,'ADMIN_EMAIL','ethicgroup6@gmail.com');

/*Table structure for table `data_collection` */

DROP TABLE IF EXISTS `data_collection`;

CREATE TABLE `data_collection` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `questionnaire` varchar(35) NOT NULL,
  `interviews` varchar(35) NOT NULL,
  `interview_audio_recording` varchar(35) NOT NULL,
  `interview_video_recording` varchar(35) NOT NULL,
  `focus_groups` varchar(35) NOT NULL,
  `observations` varchar(35) NOT NULL,
  `observations_with_kp` varchar(35) NOT NULL,
  `observations_without_kp` varchar(35) NOT NULL,
  `photo_video_graph_with_kp` varchar(35) NOT NULL,
  `photo_video_graph_without_kp` varchar(35) NOT NULL,
  `photo_video_photography` varchar(35) NOT NULL,
  `photo_video_graph_video` varchar(35) NOT NULL,
  `collection_of_HT` varchar(35) NOT NULL,
  `collection_of_details` text NOT NULL,
  `response_to_tss` varchar(35) NOT NULL,
  `response_to_tss_details` text NOT NULL,
  `epidemiological_research` varchar(35) NOT NULL,
  `epidemiological_research_details` text NOT NULL,
  `psychology_inventories` varchar(35) NOT NULL,
  `psychology_inventories_details` text NOT NULL,
  `online_survey` varchar(35) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `data_collection` */

insert  into `data_collection`(`id`,`questionnaire`,`interviews`,`interview_audio_recording`,`interview_video_recording`,`focus_groups`,`observations`,`observations_with_kp`,`observations_without_kp`,`photo_video_graph_with_kp`,`photo_video_graph_without_kp`,`photo_video_photography`,`photo_video_graph_video`,`collection_of_HT`,`collection_of_details`,`response_to_tss`,`response_to_tss_details`,`epidemiological_research`,`epidemiological_research_details`,`psychology_inventories`,`psychology_inventories_details`,`online_survey`) values (1,'Fully identifiable (name on it)','In_depth','1','1','1','1','Audio recording','Audio recording','Audio recording','Audio recording','','1','1','','1','','1','','1','','1'),(2,'Fully identifiable (name on it)','In_depth','1','1','1','1','Video recording','Audio recording','Audio recording','Audio recording','','0','1','','1','','1','','1','','1'),(3,'Potentially identifiable (coded)','Semi-structured','0','1','1','1','Audio recording','Audio recording','Audio recording','Audio recording','','1','1','','1','','1','','1','','1');

/*Table structure for table `data_collection_cont` */

DROP TABLE IF EXISTS `data_collection_cont`;

CREATE TABLE `data_collection_cont` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `admin_of_substance` varchar(35) NOT NULL,
  `admin_of_substance_details` text NOT NULL,
  `other` varchar(35) NOT NULL,
  `other_description` text NOT NULL,
  `data_collection_location` text NOT NULL,
  `data_collection_location_sug` text NOT NULL,
  `person_to_collect_data` varchar(35) NOT NULL,
  `translator_required` varchar(35) NOT NULL,
  `translator_required_desc` text NOT NULL,
  `research_involve_children` varchar(35) NOT NULL,
  `research_involve_children_ys` varchar(35) NOT NULL,
  `research_involve_children_no` text NOT NULL,
  `dependent_or_unequal_rlshp` varchar(35) NOT NULL,
  `dependent_or_unequal_rlshp_details` text NOT NULL,
  `admin_involvement` varchar(35) NOT NULL,
  `admin_involvement_details` text NOT NULL,
  `procedures_use_diagonistic_or_indicative` varchar(35) NOT NULL,
  `criteria_to_assess_results` varchar(35) NOT NULL,
  `duty_of_care_to_participants` varchar(35) NOT NULL,
  `acquired_necessary_competence` varchar(35) NOT NULL,
  `indicate_procedure_proposed_to_PP` varchar(35) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `data_collection_cont` */

insert  into `data_collection_cont`(`id`,`admin_of_substance`,`admin_of_substance_details`,`other`,`other_description`,`data_collection_location`,`data_collection_location_sug`,`person_to_collect_data`,`translator_required`,`translator_required_desc`,`research_involve_children`,`research_involve_children_ys`,`research_involve_children_no`,`dependent_or_unequal_rlshp`,`dependent_or_unequal_rlshp_details`,`admin_involvement`,`admin_involvement_details`,`procedures_use_diagonistic_or_indicative`,`criteria_to_assess_results`,`duty_of_care_to_participants`,`acquired_necessary_competence`,`indicate_procedure_proposed_to_PP`) values (1,'1','','1','','12321','12312','12321','1','','1','0','','1','','1','','1','','5.9','1','1'),(2,'1','','1','','132','123','213','1','','1','0','','1','','1','','1','','132131','1','1'),(3,'1','','1','','13','123','123','1','','1','0','','1','','1','','1','','123','1','1');

/*Table structure for table `data_materials_procedures` */

DROP TABLE IF EXISTS `data_materials_procedures`;

CREATE TABLE `data_materials_procedures` (
  `section_id` int(10) NOT NULL AUTO_INCREMENT,
  `data_set` text NOT NULL,
  `data_collected` text NOT NULL,
  `ethics_approval` varchar(100) NOT NULL,
  `primary_purpose` text NOT NULL,
  `information_given` varchar(100) NOT NULL,
  `organisation_informed` varchar(100) NOT NULL,
  `procedure_description` varchar(100) DEFAULT NULL,
  `method_used` varchar(100) NOT NULL,
  `participants_consented` varchar(100) NOT NULL,
  `access_NCIS` varchar(100) NOT NULL,
  `access_level` varchar(100) NOT NULL,
  `usually_access` varchar(100) NOT NULL,
  `describe_role` varchar(100) DEFAULT NULL,
  `coronial_records` varchar(100) NOT NULL,
  `access_aproved` varchar(100) NOT NULL,
  UNIQUE KEY `id_4` (`section_id`),
  KEY `id` (`section_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `data_materials_procedures` */

insert  into `data_materials_procedures`(`section_id`,`data_set`,`data_collected`,`ethics_approval`,`primary_purpose`,`information_given`,`organisation_informed`,`procedure_description`,`method_used`,`participants_consented`,`access_NCIS`,`access_level`,`usually_access`,`describe_role`,`coronial_records`,`access_aproved`) values (1,'234','2342','23424','234','234234','0','','Implied consent','0','0','Level 1','1','','1','Pending'),(2,'123','12321','12321','123','123','0','','Implied consent','0','0','Level 1','1','','1','Pending'),(3,'13213','123','213','123','123','0','','Implied consent','1','1','Level 1','1','','1','Pending');

/*Table structure for table `datacollection` */

DROP TABLE IF EXISTS `datacollection`;

CREATE TABLE `datacollection` (
  `section_id` int(12) NOT NULL AUTO_INCREMENT,
  `location_other_than_Australia` varchar(250) NOT NULL,
  `if_yes_location_explain` text,
  `name_of_organization_agency` varchar(250) NOT NULL,
  `award` varchar(250) NOT NULL,
  `name_of_institution` varchar(250) NOT NULL,
  `role_of_monash` varchar(250) NOT NULL,
  `monash_staff` varchar(250) NOT NULL,
  `how_many_people` int(4) NOT NULL,
  `groups_involved` varchar(250) NOT NULL,
  `age_range` varchar(250) NOT NULL,
  `recruitment_of_participants` varchar(250) NOT NULL,
  `reimbursement_to_participants` varchar(250) NOT NULL,
  `if_yes_how_much` text,
  `dependent_or_unequal_relationship` varchar(250) NOT NULL,
  `if_yes_relationship` text,
  `consent_form_attached` varchar(250) NOT NULL,
  `if_yes_please_attach` varchar(250) NOT NULL,
  `involve_the_collection` varchar(250) NOT NULL,
  `arrangements_in_place` varchar(250) NOT NULL,
  PRIMARY KEY (`section_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `datacollection` */

insert  into `datacollection`(`section_id`,`location_other_than_Australia`,`if_yes_location_explain`,`name_of_organization_agency`,`award`,`name_of_institution`,`role_of_monash`,`monash_staff`,`how_many_people`,`groups_involved`,`age_range`,`recruitment_of_participants`,`reimbursement_to_participants`,`if_yes_how_much`,`dependent_or_unequal_relationship`,`if_yes_relationship`,`consent_form_attached`,`if_yes_please_attach`,`involve_the_collection`,`arrangements_in_place`) values (1,'1','','1312','12312','1231','1231','1232',1231,'12312','31 - 40','12312','1','','1','','1','','1','1232131'),(2,'1','','123','123','123','123','123',123,'123','1 - 10','123','1','','1','','1','','1','123');

/*Table structure for table `debriefing_procedures` */

DROP TABLE IF EXISTS `debriefing_procedures`;

CREATE TABLE `debriefing_procedures` (
  `section_id` int(10) NOT NULL AUTO_INCREMENT,
  `publish_form` varchar(250) NOT NULL,
  `communication_form` varchar(250) NOT NULL,
  `participants_results` varchar(250) NOT NULL,
  `results_provided` varchar(25) NOT NULL,
  `results_provided_others` varchar(250) NOT NULL,
  `debriefing_form_required` text NOT NULL,
  `results_infor_communication` text NOT NULL,
  `collectivity_community_research` text NOT NULL,
  PRIMARY KEY (`section_id`),
  UNIQUE KEY `id_10` (`section_id`),
  KEY `id` (`section_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `debriefing_procedures` */

insert  into `debriefing_procedures`(`section_id`,`publish_form`,`communication_form`,`participants_results`,`results_provided`,`results_provided_others`,`debriefing_form_required`,`results_infor_communication`,`collectivity_community_research`) values (1,'Thesis','Copy of journal article / book / chapter','Participants will be provided with the researchers’ contact details in the Explanatory Statement to request the results','0','In totally deidentified summary form in which no individual can be identified','1321','213','123'),(2,'Thesis','Copy of journal article / book / chapter','Participants will be provided with the researchers’ contact details in the Explanatory Statement to request the results','0','In identified form, or in a manner which may allow participants to be identified ','123213','12321','123213'),(3,'Conference presentation','On-line web based','Participants will be provided with the researchers’ contact details in the Explanatory Statement to request the results','1','In totally deidentified summary form in which no individual can be identified','123','123','123');

/*Table structure for table `detail_multi_centre` */

DROP TABLE IF EXISTS `detail_multi_centre`;

CREATE TABLE `detail_multi_centre` (
  `section_id` int(12) NOT NULL AUTO_INCREMENT,
  `aim_research_project` varchar(250) NOT NULL,
  `details_of_procedures` text NOT NULL,
  `research_ethics_committee` text NOT NULL,
  `attach_letter_of_approval` varchar(250) DEFAULT NULL,
  `primary_HREC` varchar(250) NOT NULL,
  `HREC1` varchar(250) DEFAULT NULL,
  `HREC1_yes` varchar(250) DEFAULT NULL,
  `HREC1_no` varchar(250) DEFAULT NULL,
  `HREC1_pending` varchar(250) DEFAULT NULL,
  `HREC2` varchar(250) DEFAULT NULL,
  `HREC2_yes` varchar(250) DEFAULT NULL,
  `HREC2_no` varchar(250) DEFAULT NULL,
  `HREC2_pending` varchar(250) DEFAULT NULL,
  `HREC3` varchar(250) DEFAULT NULL,
  `HREC3_yes` varchar(250) DEFAULT NULL,
  `HREC3_no` varchar(250) DEFAULT NULL,
  `HREC3_pending` varchar(250) DEFAULT NULL,
  `HREC4` varchar(250) DEFAULT NULL,
  `HREC4_yes` varchar(250) DEFAULT NULL,
  `HREC4_no` varchar(250) DEFAULT NULL,
  `HREC4_pending` varchar(250) DEFAULT NULL,
  `women_pregnant` varchar(250) NOT NULL,
  `children_young` varchar(250) NOT NULL,
  `intellectual_disability` varchar(250) NOT NULL,
  `forensic_patient` varchar(25) NOT NULL,
  `impaired_capacity` varchar(25) NOT NULL,
  `prisoners_parole` varchar(25) NOT NULL,
  `children_wards_of_the_state` varchar(25) NOT NULL,
  `dependent_on_medical_care` varchar(25) NOT NULL,
  `examining_potentially` varchar(25) NOT NULL,
  `seeking_disclosure` varchar(25) NOT NULL,
  `participation_in_illegal_activities` varchar(25) NOT NULL,
  `other_vulnerable_participants` varchar(25) NOT NULL,
  `if_yes_other_specify` text,
  `location_other_than_Australia` varchar(250) NOT NULL,
  `if_yes_location_explain` text,
  `name_of_organization_agency` varchar(250) NOT NULL,
  `award` varchar(250) NOT NULL,
  `name_of_institution` varchar(250) NOT NULL,
  `role_of_monash` varchar(250) NOT NULL,
  `monash_staff` varchar(250) NOT NULL,
  `how_many_people` int(4) NOT NULL,
  `groups_involved` varchar(250) NOT NULL,
  `age_range` varchar(250) NOT NULL,
  `recruitment_of_participants` varchar(250) NOT NULL,
  `reimbursement_to_participants` varchar(250) NOT NULL,
  `if_yes_how_much` text,
  `dependent_or_unequal_relationship` varchar(250) NOT NULL,
  `if_yes_relationship` text,
  `consent_form_attached` varchar(250) NOT NULL,
  `if_yes_please_attach` varchar(250) NOT NULL,
  `involve_the_collection` varchar(250) NOT NULL,
  `arrangements_in_place` varchar(250) NOT NULL,
  PRIMARY KEY (`section_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `detail_multi_centre` */

/*Table structure for table `details_of_project` */

DROP TABLE IF EXISTS `details_of_project`;

CREATE TABLE `details_of_project` (
  `section_id` int(12) NOT NULL AUTO_INCREMENT,
  `aim_research_project` varchar(250) NOT NULL,
  `details_of_procedures` text NOT NULL,
  `research_ethics_committee` text NOT NULL,
  `attach_letter_of_approval` varchar(250) DEFAULT NULL,
  `primary_HREC` varchar(250) NOT NULL,
  `HREC1` varchar(250) DEFAULT NULL,
  `HREC1_yes` varchar(250) DEFAULT NULL,
  `HREC1_no` varchar(250) DEFAULT NULL,
  `HREC1_pending` varchar(250) DEFAULT NULL,
  `HREC2` varchar(250) DEFAULT NULL,
  `HREC2_yes` varchar(250) DEFAULT NULL,
  `HREC2_no` varchar(250) DEFAULT NULL,
  `HREC2_pending` varchar(250) DEFAULT NULL,
  `HREC3` varchar(250) DEFAULT NULL,
  `HREC3_yes` varchar(250) DEFAULT NULL,
  `HREC3_no` varchar(250) DEFAULT NULL,
  `HREC3_pending` varchar(250) DEFAULT NULL,
  `HREC4` varchar(250) DEFAULT NULL,
  `HREC4_yes` varchar(250) DEFAULT NULL,
  `HREC4_no` varchar(250) DEFAULT NULL,
  `HREC4_pending` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`section_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `details_of_project` */

insert  into `details_of_project`(`section_id`,`aim_research_project`,`details_of_procedures`,`research_ethics_committee`,`attach_letter_of_approval`,`primary_HREC`,`HREC1`,`HREC1_yes`,`HREC1_no`,`HREC1_pending`,`HREC2`,`HREC2_yes`,`HREC2_no`,`HREC2_pending`,`HREC3`,`HREC3_yes`,`HREC3_no`,`HREC3_pending`,`HREC4`,`HREC4_yes`,`HREC4_no`,`HREC4_pending`) values (1,'123','1321','12321','','1','12321','1','0','0','12321','0','0','1','','0','0','0','','0','0','0'),(2,'12','123','123','','1','123','0','1','0','123','0','0','1','123','0','1','0','123','0','1','0');

/*Table structure for table `disclosure_of_information` */

DROP TABLE IF EXISTS `disclosure_of_information`;

CREATE TABLE `disclosure_of_information` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `information_type` varchar(100) NOT NULL,
  `sensi_information` varchar(100) NOT NULL,
  `heat_information` varchar(100) NOT NULL,
  `organisation_involved_health` varchar(100) NOT NULL,
  `private_sector` varchar(100) NOT NULL,
  `health_public_sector` varchar(100) NOT NULL,
  `other_orgar` varchar(100) NOT NULL,
  `organisation_involved_personal` varchar(100) NOT NULL,
  `victoria_private_sector` varchar(100) NOT NULL,
  `victoria_commonwealth` varchar(100) NOT NULL,
  `other_public_sector` varchar(100) NOT NULL,
  `organisation_involved_sensitive` varchar(100) NOT NULL,
  `vi_private_sector` varchar(100) NOT NULL,
  `vi_commonwealth` varchar(100) NOT NULL,
  `other_vi_public_sector` varchar(100) NOT NULL,
  `principle_health` varchar(100) NOT NULL,
  `hpp2_health` varchar(100) NOT NULL,
  `ipp11_health` varchar(100) NOT NULL,
  `npp2_other` varchar(100) NOT NULL,
  `principle_personal` varchar(100) NOT NULL,
  `npp2_personal` varchar(100) NOT NULL,
  `ipp11_personal` varchar(100) NOT NULL,
  `np2_personal` varchar(100) NOT NULL,
  `principle_sensitive` varchar(100) NOT NULL,
  `npp_sensitive` varchar(100) NOT NULL,
  `ipp_sensitive` varchar(100) NOT NULL,
  `npp2_sensitive` varchar(100) NOT NULL,
  `specific_purpose` varchar(100) NOT NULL,
  `secondary_purpose` text NOT NULL,
  `give_details` varchar(100) NOT NULL,
  `not_used_in_deidentified_form` varchar(100) NOT NULL,
  `public_interest` varchar(100) NOT NULL,
  `potentially_identifiable` varchar(100) NOT NULL,
  `info` varchar(100) NOT NULL,
  `sen_infor` varchar(100) NOT NULL,
  `hea_infor` varchar(100) NOT NULL,
  `organisation_health` varchar(100) NOT NULL,
  `pri_sector` varchar(100) NOT NULL,
  `pu_sector` varchar(100) NOT NULL,
  `other_victorian` varchar(100) NOT NULL,
  `organisation_personal` varchar(100) NOT NULL,
  `vate_sector` varchar(100) NOT NULL,
  `vate_commonwealth` varchar(100) NOT NULL,
  `other_vate` varchar(100) NOT NULL,
  `organisation_sensitive` varchar(100) NOT NULL,
  `prite_sector` varchar(100) NOT NULL,
  `commu_public_sector` varchar(100) NOT NULL,
  `other_wealth` varchar(100) NOT NULL,
  `privacy_health` varchar(100) NOT NULL,
  `h_health` varchar(100) NOT NULL,
  `ip_health` varchar(100) NOT NULL,
  `np_health` varchar(100) NOT NULL,
  `privacy_personal` varchar(100) NOT NULL,
  `npp_personal1` varchar(100) NOT NULL,
  `ipp_personal1` varchar(100) NOT NULL,
  `n_personal1` varchar(100) NOT NULL,
  `privacy_sensitive` varchar(100) NOT NULL,
  `nppp_sensitive1` varchar(100) NOT NULL,
  `ippp_sensitive1` varchar(100) NOT NULL,
  `nppp_senstive1` varchar(100) NOT NULL,
  `organisations_list` varchar(100) NOT NULL,
  `disclosed_by_researcher` varchar(100) NOT NULL,
  `information` varchar(100) NOT NULL,
  `se_information` varchar(100) NOT NULL,
  `he_information` varchar(100) NOT NULL,
  `orga_involved_health` varchar(100) NOT NULL,
  `victorian_private_sec` varchar(100) NOT NULL,
  `commonwealth_pub_sector` varchar(100) NOT NULL,
  `other_involved_health` varchar(100) NOT NULL,
  `orga_involved_personal` varchar(100) NOT NULL,
  `victorian_pvt` varchar(100) NOT NULL,
  `commonwealth_pvt` varchar(100) NOT NULL,
  `other_victorian_pvt` varchar(100) NOT NULL,
  `orga_involved_sensitive` varchar(100) NOT NULL,
  `victorian_pvt_sensitive` varchar(100) NOT NULL,
  `commonwealth_pvt_sensitive` varchar(100) NOT NULL,
  `other_victorian_sensitive_pvt` varchar(100) NOT NULL,
  `principles_health` varchar(100) NOT NULL,
  `pvt_hpp2_health` varchar(100) NOT NULL,
  `pvt_ipp11_health` varchar(100) NOT NULL,
  `pvt_npp2_health` varchar(100) NOT NULL,
  `principles_personal` varchar(100) NOT NULL,
  `pvt_hpp2_personal` varchar(100) NOT NULL,
  `pvt_ipp11_personal` varchar(100) NOT NULL,
  `pvt_npp2_personal` varchar(100) NOT NULL,
  `principles_sensitive` varchar(100) NOT NULL,
  `pvt_hpp2_sensitive` varchar(100) NOT NULL,
  `pvt_ipp11_sensitive` varchar(100) NOT NULL,
  `pvt_npp2_sensitive` varchar(100) NOT NULL,
  `info_disclosed` varchar(100) NOT NULL,
  `info_not_used` varchar(100) NOT NULL,
  `explain_consent` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `disclosure_of_information` */

insert  into `disclosure_of_information`(`id`,`information_type`,`sensi_information`,`heat_information`,`organisation_involved_health`,`private_sector`,`health_public_sector`,`other_orgar`,`organisation_involved_personal`,`victoria_private_sector`,`victoria_commonwealth`,`other_public_sector`,`organisation_involved_sensitive`,`vi_private_sector`,`vi_commonwealth`,`other_vi_public_sector`,`principle_health`,`hpp2_health`,`ipp11_health`,`npp2_other`,`principle_personal`,`npp2_personal`,`ipp11_personal`,`np2_personal`,`principle_sensitive`,`npp_sensitive`,`ipp_sensitive`,`npp2_sensitive`,`specific_purpose`,`secondary_purpose`,`give_details`,`not_used_in_deidentified_form`,`public_interest`,`potentially_identifiable`,`info`,`sen_infor`,`hea_infor`,`organisation_health`,`pri_sector`,`pu_sector`,`other_victorian`,`organisation_personal`,`vate_sector`,`vate_commonwealth`,`other_vate`,`organisation_sensitive`,`prite_sector`,`commu_public_sector`,`other_wealth`,`privacy_health`,`h_health`,`ip_health`,`np_health`,`privacy_personal`,`npp_personal1`,`ipp_personal1`,`n_personal1`,`privacy_sensitive`,`nppp_sensitive1`,`ippp_sensitive1`,`nppp_senstive1`,`organisations_list`,`disclosed_by_researcher`,`information`,`se_information`,`he_information`,`orga_involved_health`,`victorian_private_sec`,`commonwealth_pub_sector`,`other_involved_health`,`orga_involved_personal`,`victorian_pvt`,`commonwealth_pvt`,`other_victorian_pvt`,`orga_involved_sensitive`,`victorian_pvt_sensitive`,`commonwealth_pvt_sensitive`,`other_victorian_sensitive_pvt`,`principles_health`,`pvt_hpp2_health`,`pvt_ipp11_health`,`pvt_npp2_health`,`principles_personal`,`pvt_hpp2_personal`,`pvt_ipp11_personal`,`pvt_npp2_personal`,`principles_sensitive`,`pvt_hpp2_sensitive`,`pvt_ipp11_sensitive`,`pvt_npp2_sensitive`,`info_disclosed`,`info_not_used`,`explain_consent`) values (1,'1','0','0','1','0','0','0','1','0','0','0','1','0','0','0','0','1','0','0','0','0','1','0','0','1','0','0','13132','1','','123123','13','1','0','0','1','0','0','1','0','0','0','0','1','0','0','1','0','0','0','1','0','0','1','0','0','0','1','0','0','13123','1','0','0','1','0','0','1','0','0','1','0','0','0','0','1','0','0','0','1','0','0','1','0','0','0','0','1','0','13213','1313','123123'),(2,'1','0','0','1','0','0','0','0','0','1','0','1','0','0','0','0','1','0','0','1','0','0','0','1','0','0','0','132','1','','123','13','1','0','1','0','0','0','0','1','0','0','0','1','0','1','0','0','0','0','1','0','0','0','0','1','0','1','0','0','132','1','0','0','0','0','0','0','0','0','1','0','1','0','0','1','0','0','1','0','0','0','0','1','0','1','0','0','0','1231','131','123'),(3,'0','1','0','0','0','1','0','1','0','0','0','1','0','0','0','1','0','0','0','1','0','0','0','0','1','0','0','123','1','','123','123','1','1','0','0','0','1','0','0','0','1','0','0','0','1','0','0','1','0','0','0','0','1','0','0','0','0','1','0','123','1','0','0','1','0','0','0','1','0','1','0','0','1','0','0','0','0','1','0','0','0','1','0','0','0','1','0','0','123','123','123');

/*Table structure for table `ethical` */

DROP TABLE IF EXISTS `ethical`;

CREATE TABLE `ethical` (
  `section_id` int(12) NOT NULL AUTO_INCREMENT,
  `issues` varchar(250) NOT NULL,
  PRIMARY KEY (`section_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `ethical` */

insert  into `ethical`(`section_id`,`issues`) values (1,'1321'),(2,'234234'),(3,'123');

/*Table structure for table `exclusion_criteria` */

DROP TABLE IF EXISTS `exclusion_criteria`;

CREATE TABLE `exclusion_criteria` (
  `section_id` int(11) NOT NULL AUTO_INCREMENT,
  `children` varchar(20) NOT NULL,
  `adult` varchar(20) NOT NULL,
  `preg_women` varchar(20) NOT NULL,
  `breastfd_women` varchar(20) NOT NULL,
  `individuals` varchar(20) NOT NULL,
  `other` varchar(20) NOT NULL,
  `if_yes_specify` varchar(250) DEFAULT NULL,
  `justification` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`section_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `exclusion_criteria` */

insert  into `exclusion_criteria`(`section_id`,`children`,`adult`,`preg_women`,`breastfd_women`,`individuals`,`other`,`if_yes_specify`,`justification`) values (1,'1','0','0','0','0','0',NULL,'123123'),(2,'0','0','0','0','1','0',NULL,'');

/*Table structure for table `form1` */

DROP TABLE IF EXISTS `form1`;

CREATE TABLE `form1` (
  `form_id` int(10) NOT NULL AUTO_INCREMENT,
  `application_id` int(11) NOT NULL,
  `section1_id` int(10) DEFAULT NULL,
  `section2_id` int(10) DEFAULT NULL,
  `section3_id` int(10) DEFAULT NULL,
  `section4A_id` int(10) DEFAULT NULL,
  `section4B_id` int(10) DEFAULT NULL,
  `section5A_id` int(10) DEFAULT NULL,
  `section5B_id` int(10) DEFAULT NULL,
  `section6A_id` int(10) DEFAULT NULL,
  `section6B_id` int(10) DEFAULT NULL,
  `section6C_id` int(10) DEFAULT NULL,
  `section6D_id` int(10) DEFAULT NULL,
  `section7_id` int(10) DEFAULT NULL,
  `section9_id` int(10) DEFAULT NULL,
  `ethical_issues` text,
  `section10_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`form_id`),
  UNIQUE KEY `application_id` (`application_id`),
  UNIQUE KEY `section2_id` (`section2_id`),
  UNIQUE KEY `section5B_id` (`section5B_id`),
  UNIQUE KEY `section6C_id` (`section6C_id`),
  UNIQUE KEY `section6D_id` (`section6D_id`),
  KEY `section1_id` (`section1_id`),
  KEY `section3_id` (`section3_id`),
  KEY `section4A_id` (`section4A_id`),
  KEY `section5A_id` (`section5A_id`),
  KEY `section4B_id` (`section4B_id`),
  KEY `section6A_id` (`section6A_id`),
  KEY `section6B_id` (`section6B_id`),
  KEY `section7_id` (`section7_id`),
  KEY `section9_id` (`section9_id`),
  KEY `section10_id` (`section10_id`),
  CONSTRAINT `form1_ibfk_63` FOREIGN KEY (`application_id`) REFERENCES `application` (`project_id`) ON DELETE CASCADE,
  CONSTRAINT `form1_ibfk_64` FOREIGN KEY (`section1_id`) REFERENCES `project_details` (`section_id`) ON DELETE CASCADE,
  CONSTRAINT `form1_ibfk_66` FOREIGN KEY (`section3_id`) REFERENCES `risks_procedures` (`section_id`) ON DELETE CASCADE,
  CONSTRAINT `form1_ibfk_67` FOREIGN KEY (`section4A_id`) REFERENCES `gaining_informed_consent` (`section_id`) ON DELETE CASCADE,
  CONSTRAINT `form1_ibfk_68` FOREIGN KEY (`section4B_id`) REFERENCES `data_materials_procedures` (`section_id`) ON DELETE CASCADE,
  CONSTRAINT `form1_ibfk_69` FOREIGN KEY (`section5A_id`) REFERENCES `data_collection` (`id`) ON DELETE CASCADE,
  CONSTRAINT `form1_ibfk_71` FOREIGN KEY (`section6A_id`) REFERENCES `compliance_privacy_legislation` (`section_id`) ON DELETE CASCADE,
  CONSTRAINT `form1_ibfk_74` FOREIGN KEY (`section5B_id`) REFERENCES `data_collection_cont` (`id`) ON DELETE CASCADE,
  CONSTRAINT `form1_ibfk_75` FOREIGN KEY (`section6B_id`) REFERENCES `organisation_involved_info` (`section_id`) ON DELETE CASCADE,
  CONSTRAINT `form1_ibfk_76` FOREIGN KEY (`section7_id`) REFERENCES `debriefing_procedures` (`section_id`) ON DELETE CASCADE,
  CONSTRAINT `form1_ibfk_77` FOREIGN KEY (`section2_id`) REFERENCES `research_group` (`section_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `form1` */

insert  into `form1`(`form_id`,`application_id`,`section1_id`,`section2_id`,`section3_id`,`section4A_id`,`section4B_id`,`section5A_id`,`section5B_id`,`section6A_id`,`section6B_id`,`section6C_id`,`section6D_id`,`section7_id`,`section9_id`,`ethical_issues`,`section10_id`) values (1,36,2,1,1,1,1,1,1,1,1,1,1,1,1,NULL,1),(3,49,4,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,50,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,64,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `funder` */

DROP TABLE IF EXISTS `funder`;

CREATE TABLE `funder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agency` varchar(20) NOT NULL,
  `funding_approved` tinyint(1) NOT NULL,
  `summary_attached` varchar(20) NOT NULL,
  `fm1_section1` int(11) DEFAULT NULL,
  `application_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `fm1_section1` (`fm1_section1`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `funder` */

/*Table structure for table `gaining_informed_consent` */

DROP TABLE IF EXISTS `gaining_informed_consent`;

CREATE TABLE `gaining_informed_consent` (
  `section_id` int(10) NOT NULL AUTO_INCREMENT,
  `explanatory_statement` varchar(100) NOT NULL,
  `exp_2_participants` varchar(100) DEFAULT NULL COMMENT 'if no explanations 2 part if yes attach statement',
  `research_nature` varchar(100) NOT NULL,
  `describe_procedure` varchar(100) DEFAULT NULL COMMENT 'if no en describe procedure',
  `obtain_consent` varchar(100) NOT NULL,
  `participants_informed` varchar(100) NOT NULL,
  `other_specify` varchar(100) NOT NULL,
  `obtain_informed_consent` varchar(100) NOT NULL,
  `witness_necessity` varchar(100) NOT NULL,
  `independent_witness_necessity` varchar(100) DEFAULT NULL,
  UNIQUE KEY `id_4` (`section_id`),
  KEY `id` (`section_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `gaining_informed_consent` */

insert  into `gaining_informed_consent`(`section_id`,`explanatory_statement`,`exp_2_participants`,`research_nature`,`describe_procedure`,`obtain_consent`,`participants_informed`,`other_specify`,`obtain_informed_consent`,`witness_necessity`,`independent_witness_necessity`) values (1,'0','','0','','Implied consent','1123','12312','123123','1',''),(2,'0','','0','','Implied consent','1321','13','13','1',''),(3,'1','132','1','123','Implied consent','123','123','123','1','');

/*Table structure for table `general_issues` */

DROP TABLE IF EXISTS `general_issues`;

CREATE TABLE `general_issues` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `number_of_records` varchar(100) NOT NULL,
  `type_of_records` varchar(100) NOT NULL,
  `adoption_unique_identifiers` varchar(100) NOT NULL,
  `details_of_unique_identifiers` varchar(100) NOT NULL,
  `involve_transborder` varchar(100) NOT NULL,
  `details_of_transborder` varchar(100) NOT NULL,
  `period_of_time` varchar(100) NOT NULL,
  `security_arrangements` varchar(100) NOT NULL,
  `privacy_of_individuals` varchar(100) NOT NULL,
  `procedures_in_place` varchar(100) NOT NULL,
  `ethical_issues` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `general_issues` */

insert  into `general_issues`(`id`,`number_of_records`,`type_of_records`,`adoption_unique_identifiers`,`details_of_unique_identifiers`,`involve_transborder`,`details_of_transborder`,`period_of_time`,`security_arrangements`,`privacy_of_individuals`,`procedures_in_place`,`ethical_issues`) values (1,'12321','123123','1','','1','','6.11','2','3','23','23'),(2,'1312','12313','1','','1','','1323','12312','12321','123','123'),(3,'123','1231','1','','1','','123','213','123','123','123');

/*Table structure for table `group_identification` */

DROP TABLE IF EXISTS `group_identification`;

CREATE TABLE `group_identification` (
  `section_id` int(11) NOT NULL AUTO_INCREMENT,
  `children` varchar(20) NOT NULL,
  `adult` varchar(20) NOT NULL,
  `preg_women` varchar(20) NOT NULL,
  `breastfd_women` varchar(20) NOT NULL,
  `individuals` varchar(20) NOT NULL,
  `justification` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`section_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `group_identification` */

insert  into `group_identification`(`section_id`,`children`,`adult`,`preg_women`,`breastfd_women`,`individuals`,`justification`) values (1,'1','0','0','0','0','1231323'),(2,'0','0','1','0','0','');

/*Table structure for table `low_risk_research` */

DROP TABLE IF EXISTS `low_risk_research`;

CREATE TABLE `low_risk_research` (
  `form_id` int(10) NOT NULL AUTO_INCREMENT,
  `application_id` int(11) NOT NULL,
  `section1_id` int(10) DEFAULT NULL,
  `section2_id` int(10) DEFAULT NULL,
  `section3_id` int(10) DEFAULT NULL,
  `section4_id` int(10) DEFAULT NULL,
  `section5_id` int(10) DEFAULT NULL,
  `section6_id` int(10) DEFAULT NULL,
  `section7A_id` int(10) DEFAULT NULL,
  `section7B_id` int(10) DEFAULT NULL,
  `section8A_id` int(10) DEFAULT NULL,
  `section8B_id` int(10) DEFAULT NULL,
  `section9A_id` int(10) DEFAULT NULL,
  `section9B_id` int(10) DEFAULT NULL,
  `section9C_id` int(10) DEFAULT NULL,
  `section9D_id` int(10) DEFAULT NULL,
  `section10_id` int(10) DEFAULT NULL,
  `section11_id` int(10) DEFAULT NULL,
  `section12_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`form_id`),
  KEY `application_id` (`application_id`),
  KEY `section1_id` (`section1_id`),
  KEY `section2_id` (`section2_id`),
  KEY `section3_id` (`section3_id`),
  KEY `section4_id` (`section4_id`),
  KEY `section5_id` (`section5_id`),
  KEY `section6_id` (`section6_id`),
  KEY `section7A_id` (`section7A_id`),
  KEY `section7B_id` (`section7B_id`),
  KEY `section8A_id` (`section8A_id`),
  KEY `section8B_id` (`section8B_id`),
  KEY `section9A_id` (`section9A_id`),
  KEY `section9B_id` (`section9B_id`),
  KEY `section9C_id` (`section9C_id`),
  KEY `section9D_id` (`section9D_id`),
  KEY `section10_id` (`section10_id`),
  KEY `section11_id` (`section11_id`),
  KEY `section12_id` (`section12_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

/*Data for the table `low_risk_research` */

insert  into `low_risk_research`(`form_id`,`application_id`,`section1_id`,`section2_id`,`section3_id`,`section4_id`,`section5_id`,`section6_id`,`section7A_id`,`section7B_id`,`section8A_id`,`section8B_id`,`section9A_id`,`section9B_id`,`section9C_id`,`section9D_id`,`section10_id`,`section11_id`,`section12_id`) values (22,67,4,4,4,5,3,3,3,3,3,3,3,3,3,3,3,3,3);

/*Table structure for table `low_risk_research_bk` */

DROP TABLE IF EXISTS `low_risk_research_bk`;

CREATE TABLE `low_risk_research_bk` (
  `form_id` int(10) NOT NULL AUTO_INCREMENT,
  `application_id` int(11) NOT NULL,
  `section1_id` int(10) DEFAULT NULL,
  `section2_id` int(10) DEFAULT NULL,
  `section3_id` int(10) DEFAULT NULL,
  `section4A_id` int(10) DEFAULT NULL,
  `section4B_id` int(10) DEFAULT NULL,
  `section5A_id` int(10) DEFAULT NULL,
  `section5B_id` int(10) DEFAULT NULL,
  `section6A_id` int(10) DEFAULT NULL,
  `section6B_id` int(10) DEFAULT NULL,
  `section6C_id` int(10) DEFAULT NULL,
  `section6D_id` int(10) DEFAULT NULL,
  `section7_id` int(10) DEFAULT NULL,
  `section8_id` int(10) DEFAULT NULL,
  `section9_id` int(10) DEFAULT NULL,
  `section10_id` int(10) DEFAULT NULL,
  `section11_id` int(10) DEFAULT NULL,
  `section12_id` int(10) DEFAULT NULL,
  `section13_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`form_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Data for the table `low_risk_research_bk` */

insert  into `low_risk_research_bk`(`form_id`,`application_id`,`section1_id`,`section2_id`,`section3_id`,`section4A_id`,`section4B_id`,`section5A_id`,`section5B_id`,`section6A_id`,`section6B_id`,`section6C_id`,`section6D_id`,`section7_id`,`section8_id`,`section9_id`,`section10_id`,`section11_id`,`section12_id`,`section13_id`) values (6,43,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,1),(7,44,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(8,45,3,2,2,2,2,2,2,2,2,2,2,2,NULL,2,2,2,2,2),(9,46,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10,47,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(11,48,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(12,56,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,3,3,3),(13,57,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(14,58,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `multi_centre_research` */

DROP TABLE IF EXISTS `multi_centre_research`;

CREATE TABLE `multi_centre_research` (
  `form_id` int(10) NOT NULL AUTO_INCREMENT,
  `application_id` int(11) NOT NULL,
  `section1_id` int(10) DEFAULT NULL,
  `section2_id` int(10) DEFAULT NULL,
  `section3_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`form_id`),
  UNIQUE KEY `application_id` (`application_id`),
  UNIQUE KEY `section1_id` (`section1_id`),
  UNIQUE KEY `section2_id` (`section2_id`),
  UNIQUE KEY `section3_id` (`section3_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `multi_centre_research` */

insert  into `multi_centre_research`(`form_id`,`application_id`,`section1_id`,`section2_id`,`section3_id`) values (3,53,1,1,1),(4,54,NULL,NULL,NULL),(5,63,2,2,2);

/*Table structure for table `occupy` */

DROP TABLE IF EXISTS `occupy`;

CREATE TABLE `occupy` (
  `staff_id` int(15) NOT NULL,
  `position_id` int(8) NOT NULL,
  PRIMARY KEY (`staff_id`,`position_id`),
  KEY `position_id` (`position_id`),
  CONSTRAINT `occupy_ibfk_1` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`) ON DELETE CASCADE,
  CONSTRAINT `occupy_ibfk_2` FOREIGN KEY (`position_id`) REFERENCES `position` (`position_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `occupy` */

insert  into `occupy`(`staff_id`,`position_id`) values (14,2),(15,3),(23,3),(24,3),(22,31),(18,36),(15,38),(31,39);

/*Table structure for table `organisation` */

DROP TABLE IF EXISTS `organisation`;

CREATE TABLE `organisation` (
  `name` varchar(35) NOT NULL,
  `person_granting_approval` varchar(35) NOT NULL,
  `role_in_organisation` varchar(35) NOT NULL,
  `permission_granted` varchar(100) NOT NULL,
  `project_id` int(10) NOT NULL,
  PRIMARY KEY (`name`),
  KEY `project_id` (`project_id`),
  CONSTRAINT `organisation_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `application` (`project_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `organisation` */

/*Table structure for table `organisation_involved_info` */

DROP TABLE IF EXISTS `organisation_involved_info`;

CREATE TABLE `organisation_involved_info` (
  `section_id` int(10) NOT NULL AUTO_INCREMENT,
  `involve_collection` varchar(100) NOT NULL,
  `collection_without_consent` varchar(100) NOT NULL,
  `collection_identifiable` varchar(100) NOT NULL,
  `identifiable_information` varchar(100) NOT NULL,
  `disclosure_of_identifiable_information` varchar(100) NOT NULL,
  `collect_info_from_third_party` varchar(100) NOT NULL,
  `victorian_private` varchar(100) NOT NULL,
  `organisation_other` varchar(100) NOT NULL,
  `a_data_set` varchar(100) NOT NULL,
  `victorian_govt_depart` varchar(100) NOT NULL,
  `victorian_source` varchar(100) NOT NULL,
  `commonwealth_agency` varchar(100) NOT NULL,
  `agency_from_another_state` varchar(100) NOT NULL,
  `organization_defined` varchar(100) NOT NULL,
  `individual_carer` varchar(100) NOT NULL,
  `other_infor` varchar(100) NOT NULL,
  `category` varchar(100) NOT NULL,
  `information_of_records` varchar(100) NOT NULL,
  `agree_to_provide_information` varchar(100) NOT NULL,
  `explain_if_no` varchar(100) NOT NULL,
  `evidence` varchar(100) NOT NULL,
  `seperate_HREC_approval` varchar(100) NOT NULL,
  `routinely_access_info` varchar(100) NOT NULL,
  `type_of_info` varchar(100) NOT NULL,
  `sensitive_information` varchar(100) NOT NULL,
  `health_information` varchar(100) NOT NULL,
  `type_of_organisation_health` varchar(100) NOT NULL,
  `victorian_private_sector` varchar(100) NOT NULL,
  `commonwealth` varchar(100) NOT NULL,
  `other_organisation` varchar(100) NOT NULL,
  `type_of_organisation_personal` varchar(100) NOT NULL,
  `victor_private_sector` varchar(100) NOT NULL,
  `common_public_sector` varchar(100) NOT NULL,
  `other_personal` varchar(100) NOT NULL,
  `type_of_organisation_sensitive` varchar(100) NOT NULL,
  `vic_private_sector` varchar(100) NOT NULL,
  `com_public_sector` varchar(100) NOT NULL,
  `other_senstive` varchar(100) NOT NULL,
  `privacy_principle_health` varchar(100) NOT NULL,
  `HPP1_health` varchar(100) NOT NULL,
  `IPP11_health` varchar(100) NOT NULL,
  `NPP1_health` varchar(100) NOT NULL,
  `privacy_principle_personal` varchar(100) NOT NULL,
  `npp1_personal` varchar(100) NOT NULL,
  `ipp11_personal` varchar(100) NOT NULL,
  `npp10_personal` varchar(100) NOT NULL,
  `privacy_principle_sensitive` varchar(100) NOT NULL,
  `npp0_sensitive` varchar(100) NOT NULL,
  `ipp11_senstive` varchar(100) NOT NULL,
  `npp10_sensitive` varchar(100) NOT NULL,
  `de_intefied_form` varchar(100) NOT NULL,
  `not_obtain_consent` varchar(100) NOT NULL,
  `reasons` varchar(100) NOT NULL,
  UNIQUE KEY `id_7` (`section_id`),
  KEY `id` (`section_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `organisation_involved_info` */

insert  into `organisation_involved_info`(`section_id`,`involve_collection`,`collection_without_consent`,`collection_identifiable`,`identifiable_information`,`disclosure_of_identifiable_information`,`collect_info_from_third_party`,`victorian_private`,`organisation_other`,`a_data_set`,`victorian_govt_depart`,`victorian_source`,`commonwealth_agency`,`agency_from_another_state`,`organization_defined`,`individual_carer`,`other_infor`,`category`,`information_of_records`,`agree_to_provide_information`,`explain_if_no`,`evidence`,`seperate_HREC_approval`,`routinely_access_info`,`type_of_info`,`sensitive_information`,`health_information`,`type_of_organisation_health`,`victorian_private_sector`,`commonwealth`,`other_organisation`,`type_of_organisation_personal`,`victor_private_sector`,`common_public_sector`,`other_personal`,`type_of_organisation_sensitive`,`vic_private_sector`,`com_public_sector`,`other_senstive`,`privacy_principle_health`,`HPP1_health`,`IPP11_health`,`NPP1_health`,`privacy_principle_personal`,`npp1_personal`,`ipp11_personal`,`npp10_personal`,`privacy_principle_sensitive`,`npp0_sensitive`,`ipp11_senstive`,`npp10_sensitive`,`de_intefied_form`,`not_obtain_consent`,`reasons`) values (1,'1','1','0','0','0','1','0','0','0','0','0','0','0','0','0','0','1312','3123','1','1321','','1','1','0','0','1','0','1','0','0','0','0','0','1','0','1','0','0','1','0','0','0','0','1','0','0','0','1','0','0','6.8','2342','2342'),(2,'1','1','1','1','1','0','0','0','0','1','0','0','0','0','0','0','13','213','1','1321','','1','1','0','1','0','0','1','0','0','0','1','0','0','1','0','0','0','1','0','0','0','0','1','0','0','1','0','0','0','1323','1231','123'),(3,'1','1','1','1','1','1','0','0','0','0','0','0','0','0','0','0','123','123123','1','123','','1','1','0','1','0','0','0','0','1','0','1','0','0','0','0','1','0','0','0','1','0','0','1','0','0','1','0','0','0','12312','123','123');

/*Table structure for table `participants_desc` */

DROP TABLE IF EXISTS `participants_desc`;

CREATE TABLE `participants_desc` (
  `section_id` int(11) NOT NULL AUTO_INCREMENT,
  `pple_num` int(20) NOT NULL,
  `pple_involved` varchar(40) NOT NULL,
  `age_range` varchar(60) NOT NULL,
  PRIMARY KEY (`section_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `participants_desc` */

insert  into `participants_desc`(`section_id`,`pple_num`,`pple_involved`,`age_range`) values (1,123,'123','1 - 10');

/*Table structure for table `position` */

DROP TABLE IF EXISTS `position`;

CREATE TABLE `position` (
  `position_id` int(8) NOT NULL AUTO_INCREMENT,
  `position_name` varchar(35) NOT NULL,
  `position_level` int(2) NOT NULL,
  `department_name` varchar(35) NOT NULL,
  PRIMARY KEY (`position_id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

/*Data for the table `position` */

insert  into `position`(`position_id`,`position_name`,`position_level`,`department_name`) values (2,'accountant',1,'Finance'),(3,'lecture',2,'Ademic'),(22,'accountant',2,'Adminstration'),(31,'op',2,'Adminstration'),(32,'clerk',3,'Adminstration'),(36,'admin clerk',1,'Adminstration'),(38,'programmer',1,'Information Technology'),(39,'1321',12,'Research');

/*Table structure for table `position_indv` */

DROP TABLE IF EXISTS `position_indv`;

CREATE TABLE `position_indv` (
  `section_id` int(11) NOT NULL AUTO_INCREMENT,
  `indv_name` varchar(20) NOT NULL,
  `indv_pos` varchar(20) NOT NULL,
  `org` varchar(20) NOT NULL,
  PRIMARY KEY (`section_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `position_indv` */

insert  into `position_indv`(`section_id`,`indv_name`,`indv_pos`,`org`) values (1,'123','123','132');

/*Table structure for table `potentially_vulnerable_participants` */

DROP TABLE IF EXISTS `potentially_vulnerable_participants`;

CREATE TABLE `potentially_vulnerable_participants` (
  `section_id` int(12) NOT NULL AUTO_INCREMENT,
  `women_pregnant` varchar(250) NOT NULL,
  `children_young` varchar(250) NOT NULL,
  `intellectual_disability` varchar(250) NOT NULL,
  `forensic_patient` varchar(25) NOT NULL,
  `impaired_capacity` varchar(25) NOT NULL,
  `prisoners_parole` varchar(25) NOT NULL,
  `children_wards_of_the_state` varchar(25) NOT NULL,
  `dependent_on_medical_care` varchar(25) NOT NULL,
  `examining_potentially` varchar(25) NOT NULL,
  `seeking_disclosure` varchar(25) NOT NULL,
  `participation_in_illegal_activities` varchar(25) NOT NULL,
  `other_vulnerable_participants` varchar(25) NOT NULL,
  `if_yes_other_specify` text,
  PRIMARY KEY (`section_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `potentially_vulnerable_participants` */

insert  into `potentially_vulnerable_participants`(`section_id`,`women_pregnant`,`children_young`,`intellectual_disability`,`forensic_patient`,`impaired_capacity`,`prisoners_parole`,`children_wards_of_the_state`,`dependent_on_medical_care`,`examining_potentially`,`seeking_disclosure`,`participation_in_illegal_activities`,`other_vulnerable_participants`,`if_yes_other_specify`) values (1,'1','1','1','1','1','1','1','1','1','1','1','1',''),(2,'1','1','1','1','1','1','1','1','1','1','1','1','');

/*Table structure for table `procedures_desc` */

DROP TABLE IF EXISTS `procedures_desc`;

CREATE TABLE `procedures_desc` (
  `section_id` int(11) NOT NULL AUTO_INCREMENT,
  `grp_1` varchar(400) NOT NULL,
  `grp_2` varchar(400) NOT NULL,
  `grp_3` varchar(400) NOT NULL,
  `grp_4` varchar(400) NOT NULL,
  PRIMARY KEY (`section_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `procedures_desc` */

insert  into `procedures_desc`(`section_id`,`grp_1`,`grp_2`,`grp_3`,`grp_4`) values (1,'12','123','123','123'),(2,'123','123','123','123'),(3,'1','1','1','1'),(4,'123','123','123','123'),(5,'123','123','131','13'),(6,'123','123','123','123'),(7,'132','123','123','12133');

/*Table structure for table `profile` */

DROP TABLE IF EXISTS `profile`;

CREATE TABLE `profile` (
  `user_name` varchar(20) NOT NULL,
  `first_name` varchar(20) NOT NULL,
  `title` varchar(8) NOT NULL,
  `surname` varchar(20) NOT NULL,
  `gender` varchar(6) NOT NULL,
  `contact_number` varchar(11) NOT NULL,
  `email` varchar(65) NOT NULL,
  `contact_number2` int(35) DEFAULT NULL,
  `fax` int(35) DEFAULT NULL,
  `designation` varchar(35) NOT NULL,
  `profile_picture` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`user_name`),
  KEY `user_name` (`user_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `profile` */

insert  into `profile`(`user_name`,`first_name`,`title`,`surname`,`gender`,`contact_number`,`email`,`contact_number2`,`fax`,`designation`,`profile_picture`) values ('admin','Neyo','Mr','Anderson','Male','47852265','admin@localhost',NULL,NULL,'Staff','default.jpg'),('alba1','Alex','Mr','Blake','Male','021445780','alex@gmail.com',NULL,NULL,'Staff','default.jpg'),('asd23','asd2','Mr','whale','Male','0557898','slol@lolgmail.com',NULL,NULL,'Staff','default.jpg'),('cool12','micheal','Mr','lastword','Male','0478951','cool12@gmail.edu',NULL,NULL,'Student','default.jpg'),('emag6','Eddie','Mr','Magara','Male','0711394806','highedd@gmail.com',NULL,NULL,'Student','7793-Bicycle.jpg'),('emy12','jhonathan','Mr','Thiel','Male','0844094293','jhon@gmail.com',NULL,NULL,'Student','default.jpg'),('hasd','iohkjg','Mr','asdhiuh','Male','458798952','lololol@gmail.com',NULL,NULL,'Student','default.jpg'),('Isal5','imraan','Mr','Salie','Male','0844789566','isal6@gmail.com',NULL,NULL,'Staff','301-av-83.gif'),('Jskur1','Jarod','Mr','Kurland','Male','0844094293','jskur1@gmail.com',NULL,NULL,'Staff','default.jpg'),('lol23','hapoon','Mr','Williams','Male','0844094293','s@lolgmail.com',NULL,NULL,'Student','default.jpg'),('lol90','jhon','Mr','Wilknson','Male','02155478','lol90@gmail.com',NULL,NULL,'Staff','default.jpg'),('man12','manly','Mr','manhood','Male','0417889895','man@gmail.com',NULL,NULL,'Student','default.jpg'),('mod13','micheal','Mr','kean','Male','0717888558','mod13@gmail.com',NULL,NULL,'Student','default.jpg'),('ngtho2','Nick','Mr','Thorby','Male','071852963','nick@gmail.com',NULL,NULL,'Student','default.jpg'),('pctin1609','Tin','Miss','Pham','Male','0977409352','pctin1609@gmail.com',NULL,NULL,'Staff','default.jpg'),('swil60','Karabo','Mr','Williams','Male','084494293','sizwewill@gmail.com',NULL,NULL,'Student','default.jpg'),('tyl3r','tylor','Mr','Durden','Male','0844094293','tylor@gmail.com',NULL,NULL,'Staff','3292-Tyler-Durden.jpg'),('user','User','Mr','Us','Male','0977409352','chanhtincntt@gmail.com',NULL,NULL,'Student','default.jpg'),('wil12','Wilson','Mr','Mark','Male','041456123','will@gmail.com',NULL,NULL,'Student','default.jpg'),('wl12','Wilson','Mr','Rodney','Male','0844098989','wl12@gmail.com',NULL,NULL,'Student','default.jpg');

/*Table structure for table `project_details` */

DROP TABLE IF EXISTS `project_details`;

CREATE TABLE `project_details` (
  `section_id` int(12) NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL,
  `aim` text NOT NULL,
  `type1_research` varchar(250) NOT NULL,
  `type2_research` varchar(250) NOT NULL,
  `researcher_finance_involvement` varchar(250) NOT NULL,
  `research_funding` varchar(25) NOT NULL,
  PRIMARY KEY (`section_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `project_details` */

insert  into `project_details`(`section_id`,`description`,`aim`,`type1_research`,`type2_research`,`researcher_finance_involvement`,`research_funding`) values (2,'1a','1b','1c','Action research','123',''),(3,'1','1','1','Action research','1',''),(4,'2432','24234','234234','Action research','234',''),(5,'1323','1321','123123','Action research','123','');

/*Table structure for table `radiation` */

DROP TABLE IF EXISTS `radiation`;

CREATE TABLE `radiation` (
  `form_id` int(10) NOT NULL AUTO_INCREMENT,
  `application_id` int(11) NOT NULL,
  `section1_id` int(10) DEFAULT NULL,
  `section2_id` int(10) DEFAULT NULL,
  `section3_id` int(10) DEFAULT NULL,
  `section4_id` int(10) DEFAULT NULL,
  `section5_id` int(10) DEFAULT NULL,
  `section6_id` int(10) DEFAULT NULL,
  `section7_id` int(10) DEFAULT NULL,
  `section8_id` int(10) DEFAULT NULL,
  `section9_id` int(10) DEFAULT NULL,
  `section10_id` int(10) DEFAULT NULL,
  `section11_id` int(10) DEFAULT NULL,
  `section12_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`form_id`),
  UNIQUE KEY `application_id` (`application_id`),
  UNIQUE KEY `section1_id` (`section1_id`),
  UNIQUE KEY `section2_id` (`section2_id`),
  UNIQUE KEY `section3_id` (`section3_id`),
  UNIQUE KEY `section4_id` (`section4_id`),
  UNIQUE KEY `section5_id` (`section5_id`),
  UNIQUE KEY `section6_id` (`section6_id`),
  UNIQUE KEY `section7_id` (`section7_id`),
  UNIQUE KEY `section8_id` (`section8_id`),
  UNIQUE KEY `section9_id` (`section9_id`),
  UNIQUE KEY `section10_id` (`section10_id`),
  UNIQUE KEY `section11_id` (`section11_id`),
  UNIQUE KEY `section12_id` (`section12_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `radiation` */

insert  into `radiation`(`form_id`,`application_id`,`section1_id`,`section2_id`,`section3_id`,`section4_id`,`section5_id`,`section6_id`,`section7_id`,`section8_id`,`section9_id`,`section10_id`,`section11_id`,`section12_id`) values (1,55,1,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,61,2,2,2,1,1,2,1,3,4,5,6,7);

/*Table structure for table `research_group` */

DROP TABLE IF EXISTS `research_group`;

CREATE TABLE `research_group` (
  `section_id` int(10) NOT NULL AUTO_INCREMENT,
  `number_of_people` int(4) DEFAULT NULL,
  `group_desc` varchar(45) DEFAULT NULL,
  `age_range` varchar(20) NOT NULL,
  `criteria_procedure` varchar(20) NOT NULL,
  `work_time` varchar(45) NOT NULL,
  `human_participants_involved` varchar(20) DEFAULT NULL,
  `incentive` varchar(20) DEFAULT NULL,
  `MBBS_attatchment` varchar(45) DEFAULT NULL,
  `reimbursement_desc` varchar(45) NOT NULL,
  `using_UPSPP` varchar(45) DEFAULT NULL,
  `supporting_paragraph_for_using_UPSPP` text,
  `using_monash_MBBS` varchar(45) DEFAULT NULL,
  `include_exclude_status` varchar(45) DEFAULT NULL,
  `exclusion_desc` varchar(45) DEFAULT NULL,
  `participant_recruitment_detail` text,
  `selection_method` varchar(20) NOT NULL,
  `method_of_obtaining_participants_contacts` varchar(400) DEFAULT NULL,
  `using_previous_datasets` varchar(400) DEFAULT NULL,
  `previous_ethical_approved` varchar(400) DEFAULT NULL,
  `using_HT_HSC` varchar(400) DEFAULT NULL,
  PRIMARY KEY (`section_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `research_group` */

insert  into `research_group`(`section_id`,`number_of_people`,`group_desc`,`age_range`,`criteria_procedure`,`work_time`,`human_participants_involved`,`incentive`,`MBBS_attatchment`,`reimbursement_desc`,`using_UPSPP`,`supporting_paragraph_for_using_UPSPP`,`using_monash_MBBS`,`include_exclude_status`,`exclusion_desc`,`participant_recruitment_detail`,`selection_method`,`method_of_obtaining_participants_contacts`,`using_previous_datasets`,`previous_ethical_approved`,`using_HT_HSC`) values (1,2,'1','1 - 10','2.3','2.4','1','1','','','1','','1','1','','2.6','2.6','2.6','0','0','1'),(2,123,'1312','1 - 10','13211','21313','1','1','','','1','','','1','','123','13','','0','0','1'),(3,24324,'234','21 - 30','234324','2342','1','1','','','1','','1','1','','234','23423','234','1','0','0');

/*Table structure for table `research_project` */

DROP TABLE IF EXISTS `research_project`;

CREATE TABLE `research_project` (
  `section_id` int(10) NOT NULL AUTO_INCREMENT,
  `reimbursement` varchar(25) NOT NULL,
  `financial_involvement` varchar(25) NOT NULL,
  `conflict_of_interest` varchar(25) NOT NULL,
  `use_of_NCIS` varchar(25) NOT NULL,
  PRIMARY KEY (`section_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `research_project` */

insert  into `research_project`(`section_id`,`reimbursement`,`financial_involvement`,`conflict_of_interest`,`use_of_NCIS`) values (1,'1','1','1','1'),(2,'1','1','1','1'),(3,'1','1','1','0'),(4,'1','1','1','1');

/*Table structure for table `researcher` */

DROP TABLE IF EXISTS `researcher`;

CREATE TABLE `researcher` (
  `researcher_id` int(20) NOT NULL,
  `degree` varchar(60) NOT NULL,
  `student_number` int(10) NOT NULL,
  PRIMARY KEY (`researcher_id`),
  CONSTRAINT `researcher_ibfk_1` FOREIGN KEY (`researcher_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `researcher` */

insert  into `researcher`(`researcher_id`,`degree`,`student_number`) values (13,'Honours in Bachelor of Computer and Social Science',23444851),(16,'undergraduate',23212799),(17,'Honours in Bachelor of Business Sci',456123),(19,'Honours in Bachelor of Business Science',235854),(20,'Honours in Bachelor of Business Science',4587),(21,'Honours in Bachelor of Business Science',1),(26,'Honours in Bachelor of Business Science',7894522),(27,'Honours in Bachelor of Business Science',44586232),(28,'Honours in Bachelor of Business Science',789562),(29,'Honours in Bachelor of Business Science',7895581),(30,'Honours in Bachelor of Business Science',2344851),(32,'Honours in Bachelor of Business Science',13231);

/*Table structure for table `risk_management_plan` */

DROP TABLE IF EXISTS `risk_management_plan`;

CREATE TABLE `risk_management_plan` (
  `section_id` int(12) NOT NULL AUTO_INCREMENT,
  `assessment` varchar(25) NOT NULL,
  PRIMARY KEY (`section_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `risk_management_plan` */

insert  into `risk_management_plan`(`section_id`,`assessment`) values (1,'Not Yet'),(2,'Yes'),(3,'Yes');

/*Table structure for table `risks_procedures` */

DROP TABLE IF EXISTS `risks_procedures`;

CREATE TABLE `risks_procedures` (
  `section_id` int(11) NOT NULL AUTO_INCREMENT,
  `participation_risk_involved` text NOT NULL,
  `participation_risk_description` text,
  `risk_described_in_consent_form` varchar(60) NOT NULL,
  `risk_not_outlined_reason` text,
  `risk_mitigation_plans` text NOT NULL,
  `emergency_procedures` text NOT NULL,
  `researcher_risk_mitigation_plans` text NOT NULL,
  `report_discoveries` text NOT NULL,
  UNIQUE KEY `id_2` (`section_id`),
  KEY `id` (`section_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `risks_procedures` */

insert  into `risks_procedures`(`section_id`,`participation_risk_involved`,`participation_risk_description`,`risk_described_in_consent_form`,`risk_not_outlined_reason`,`risk_mitigation_plans`,`emergency_procedures`,`researcher_risk_mitigation_plans`,`report_discoveries`) values (1,'1','','0','','?','ádf','ádf','ádf'),(2,'1','','0','','123','12321','123213','123123'),(3,'1','','1','123','123','12312','12312','123');

/*Table structure for table `staff` */

DROP TABLE IF EXISTS `staff`;

CREATE TABLE `staff` (
  `staff_id` int(15) NOT NULL,
  `staff_id_number` int(35) NOT NULL,
  `validated` tinyint(1) NOT NULL,
  `available` tinyint(1) NOT NULL,
  `unavailable_start_time` date NOT NULL,
  `unavailable_end_time` date NOT NULL,
  PRIMARY KEY (`staff_id`),
  CONSTRAINT `staff_ibfk_1` FOREIGN KEY (`staff_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `staff` */

insert  into `staff`(`staff_id`,`staff_id_number`,`validated`,`available`,`unavailable_start_time`,`unavailable_end_time`) values (14,456456,0,1,'2013-06-02','2013-09-10'),(15,457896,0,1,'2013-06-03','2013-09-11'),(18,123456,0,1,'2013-06-02','2013-06-05'),(22,4785,0,1,'2013-06-01','2013-06-03'),(23,45678920,0,1,'2013-06-07','2013-06-08'),(24,456788,0,1,'2013-06-18','2013-06-19'),(25,963254,0,1,'2013-06-05','2013-06-06'),(31,1231,0,1,'0000-00-00','0000-00-00');

/*Table structure for table `status_approval` */

DROP TABLE IF EXISTS `status_approval`;

CREATE TABLE `status_approval` (
  `section_id` int(11) NOT NULL AUTO_INCREMENT,
  `approval_from` varchar(20) NOT NULL,
  `attach_letter` varchar(250) DEFAULT NULL,
  `seek_approval` varchar(20) NOT NULL,
  `rac_approval` varchar(20) NOT NULL,
  `other` varchar(20) NOT NULL,
  `if_yes_specify` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`section_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `status_approval` */

insert  into `status_approval`(`section_id`,`approval_from`,`attach_letter`,`seek_approval`,`rac_approval`,`other`,`if_yes_specify`) values (1,'1','','1','1','1',''),(2,'1','','1','1','1','');

/*Table structure for table `supervise` */

DROP TABLE IF EXISTS `supervise`;

CREATE TABLE `supervise` (
  `project_id` int(15) NOT NULL DEFAULT '0',
  `staff_id` int(15) NOT NULL DEFAULT '0',
  KEY `staff_id` (`staff_id`),
  CONSTRAINT `supervise_ibfk_1` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `supervise` */

insert  into `supervise`(`project_id`,`staff_id`) values (22,14),(22,15),(22,18),(23,14),(23,15),(23,18),(24,14),(24,22),(24,15),(25,25),(25,14),(25,15),(26,25),(26,14),(26,15),(27,25),(27,14),(27,15),(28,25),(28,14),(28,24),(29,25),(29,15),(29,24),(30,25),(31,25),(1,22),(1,25),(32,14),(33,24),(34,25),(35,25),(36,25),(37,25),(38,22),(39,25),(40,25),(41,25),(42,14),(43,25),(44,25),(44,31),(45,14),(46,22),(47,31),(48,22),(49,14),(50,15),(51,24),(52,24),(53,24),(54,31),(55,24),(56,15),(57,23),(58,31),(59,25),(60,25),(61,22),(62,25),(63,31),(64,25),(65,14),(66,14),(67,25);

/*Table structure for table `types_of_activities` */

DROP TABLE IF EXISTS `types_of_activities`;

CREATE TABLE `types_of_activities` (
  `section_id` int(10) NOT NULL AUTO_INCREMENT,
  `toxins` varchar(25) NOT NULL,
  `use_without_consent` varchar(25) NOT NULL,
  `causes_discomfort` varchar(25) NOT NULL,
  `covert_observation` varchar(25) NOT NULL,
  `sensitive_issues` varchar(25) NOT NULL,
  `disclosure_of_info` varchar(25) NOT NULL,
  `intrusive_techniques` varchar(25) NOT NULL,
  `illegal_activities` varchar(25) NOT NULL,
  `genetic_research` varchar(25) NOT NULL,
  `xenotransplantation` varchar(25) NOT NULL,
  `clinical_trials` varchar(25) NOT NULL,
  `random_trials` varchar(25) NOT NULL,
  `psychology_inventories` varchar(25) NOT NULL,
  `radioactive_substances` varchar(25) NOT NULL,
  `reproductive_technology` varchar(25) NOT NULL,
  `innovative_therapy` varchar(25) NOT NULL,
  `human_tissue` varchar(25) NOT NULL,
  `human_stem_cells` varchar(25) NOT NULL,
  `recombitant_DNA` varchar(25) NOT NULL,
  PRIMARY KEY (`section_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `types_of_activities` */

insert  into `types_of_activities`(`section_id`,`toxins`,`use_without_consent`,`causes_discomfort`,`covert_observation`,`sensitive_issues`,`disclosure_of_info`,`intrusive_techniques`,`illegal_activities`,`genetic_research`,`xenotransplantation`,`clinical_trials`,`random_trials`,`psychology_inventories`,`radioactive_substances`,`reproductive_technology`,`innovative_therapy`,`human_tissue`,`human_stem_cells`,`recombitant_DNA`) values (1,'1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1'),(2,'1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1'),(3,'1','1','1','1','1','1','1','1','0','0','0','0','1','1','0','0','1','1','1'),(4,'1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `user_id` int(20) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(20) NOT NULL,
  `password` varchar(256) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `application_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_name` (`user_name`),
  UNIQUE KEY `application` (`application_id`),
  CONSTRAINT `user_ibfk_5` FOREIGN KEY (`user_name`) REFERENCES `profile` (`user_name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_ibfk_6` FOREIGN KEY (`application_id`) REFERENCES `application` (`project_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

/*Data for the table `user` */

insert  into `user`(`user_id`,`user_name`,`password`,`last_login`,`application_id`) values (13,'swil60','a37a3f2f9eeecb73e5a7c51e64c5be96','2013-06-14 00:07:05',NULL),(14,'alba1','5f4dcc3b5aa765d61d8327deb882cf99','2013-05-29 20:59:20',NULL),(15,'Isal5','84badf32aa2071f84e7bded91df3e025','2013-05-29 21:00:44',NULL),(16,'emag6','34aa2f3ab8f26c9b170ef3dd339fef94','2013-05-31 10:42:25',NULL),(17,'wil12','5f4dcc3b5aa765d61d8327deb882cf99','2013-06-01 12:01:17',NULL),(18,'Jskur1','5f4dcc3b5aa765d61d8327deb882cf99','2013-06-01 12:12:11',NULL),(19,'ngtho2','5f4dcc3b5aa765d61d8327deb882cf99','2013-06-02 15:10:28',NULL),(20,'wl12','5f4dcc3b5aa765d61d8327deb882cf99','2013-06-02 15:25:50',NULL),(21,'lol23','c4ca4238a0b923820dcc509a6f75849b','2013-06-02 15:31:46',NULL),(22,'asd23','c20ad4d76fe97759aa27a0c99bff6710','2013-06-02 15:48:09',NULL),(23,'lol90','c20ad4d76fe97759aa27a0c99bff6710','2013-06-02 17:45:41',NULL),(24,'tyl3r','a37a3f2f9eeecb73e5a7c51e64c5be96','2013-06-02 17:50:58',NULL),(25,'admin','e10adc3949ba59abbe56e057f20f883e','2013-09-05 22:48:58',NULL),(26,'man12','5f4dcc3b5aa765d61d8327deb882cf99','2013-06-03 06:39:57',NULL),(27,'emy12','5f4dcc3b5aa765d61d8327deb882cf99','2013-06-03 08:24:53',NULL),(28,'hasd','a37a3f2f9eeecb73e5a7c51e64c5be96','2013-06-03 08:31:06',NULL),(29,'mod13','5f4dcc3b5aa765d61d8327deb882cf99','2013-06-03 09:45:45',NULL),(30,'cool12','5f4dcc3b5aa765d61d8327deb882cf99','2013-06-03 10:27:57',NULL),(31,'pctin1609','e10adc3949ba59abbe56e057f20f883e','2013-09-05 22:38:47',NULL),(32,'user','e10adc3949ba59abbe56e057f20f883e','2013-09-03 23:36:17',NULL);

/*Table structure for table `vulnerable_participants` */

DROP TABLE IF EXISTS `vulnerable_participants`;

CREATE TABLE `vulnerable_participants` (
  `section_id` int(12) NOT NULL AUTO_INCREMENT,
  `women_pregnant` varchar(250) NOT NULL,
  `children_young` varchar(250) NOT NULL,
  `intellectual_disability` varchar(250) NOT NULL,
  `forensic_patient` varchar(25) NOT NULL,
  `impaired_capacity` varchar(25) NOT NULL,
  `prisoners_parole` varchar(25) NOT NULL,
  `children_wards_of_the_state` varchar(25) NOT NULL,
  `dependent_on_medical_care` varchar(25) NOT NULL,
  `examining_potentially` varchar(25) NOT NULL,
  `seeking_disclosure` varchar(25) NOT NULL,
  `participation_in_illegal_activities` varchar(25) NOT NULL,
  `other_vulnerable_participants` varchar(25) NOT NULL,
  `if_yes_other_specify` text,
  `working_in_formalized_institutions` varchar(25) NOT NULL,
  `collectives` varchar(25) NOT NULL,
  `aboriginal` varchar(25) NOT NULL,
  PRIMARY KEY (`section_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `vulnerable_participants` */

insert  into `vulnerable_participants`(`section_id`,`women_pregnant`,`children_young`,`intellectual_disability`,`forensic_patient`,`impaired_capacity`,`prisoners_parole`,`children_wards_of_the_state`,`dependent_on_medical_care`,`examining_potentially`,`seeking_disclosure`,`participation_in_illegal_activities`,`other_vulnerable_participants`,`if_yes_other_specify`,`working_in_formalized_institutions`,`collectives`,`aboriginal`) values (1,'1','1','1','','1','1','1','1','','','','1',NULL,'1','1','1'),(2,'1','1','1','','1','1','1','1','','','','1',NULL,'1','1','1'),(3,'0','1','0','','1','1','1','1','','','','0',NULL,'1','0','0'),(4,'1','1','1','1','1','1','1','1','1','1','','1',NULL,'1','0','1');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
