-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 14, 2013 at 03:55 PM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ethics_storage`
--

-- --------------------------------------------------------

--
-- Table structure for table `administrator`
--

CREATE TABLE IF NOT EXISTS `administrator` (
  `admin_id` int(15) NOT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------
INSERT INTO `administrator` (`admin_id`) VALUES (1);
--
-- Table structure for table `application`
--

CREATE TABLE IF NOT EXISTS `application` (
  `stage` tinyint(1) NOT NULL,
  `project_id` int(15) NOT NULL AUTO_INCREMENT,
  `user_id` int(20) NOT NULL,
  `status` varchar(20) DEFAULT 'not_submited',
  `create_time` time NOT NULL,
  `form_id` int(15) DEFAULT NULL,
  `application_type` varchar(20) NOT NULL,
  PRIMARY KEY (`project_id`),
  UNIQUE KEY `form_id` (`form_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `application`
--

INSERT INTO `application` (`stage`, `project_id`, `user_id`, `status`, `create_time`, `form_id`, `application_type`) VALUES
(0, 22, 15, 'not_submited', '18:22:31', NULL, 'form1'),
(0, 23, 13, 'not_submited', '18:30:03', NULL, 'form1'),
(0, 24, 24, 'not_submited', '17:52:44', NULL, 'form1'),
(0, 25, 18, 'not_submited', '18:12:05', NULL, 'form1'),
(0, 26, 25, 'not_submited', '18:41:04', NULL, 'form1'),
(0, 27, 14, 'not_submited', '05:40:32', NULL, 'form1'),
(0, 28, 27, 'not_submited', '08:26:30', NULL, 'form1'),
(0, 29, 30, 'not_submited', '10:29:39', NULL, 'form1');

-- --------------------------------------------------------

--
-- Table structure for table `compliance_privacy_legislation`
--

CREATE TABLE IF NOT EXISTS `compliance_privacy_legislation` (
  `section_id` int(10) NOT NULL AUTO_INCREMENT,
  `collecting` varchar(100) NOT NULL,
  `answer` varchar(100) NOT NULL,
  `other_explain` varchar(100) NOT NULL,
  `researchers_access_data` varchar(100) NOT NULL,
  `data_retained` varchar(100) NOT NULL,
  `victorian_privacy` varchar(100) NOT NULL,
  `information_handled` varchar(100) NOT NULL,
  `descibe_procedures` varchar(100) NOT NULL,
  `collection_of_information` varchar(100) NOT NULL,
  `information_collected` varchar(100) NOT NULL,
  `identity_of_organisation` varchar(100) NOT NULL,
  `purpose` varchar(100) NOT NULL,
  `periods_for_records` varchar(100) NOT NULL,
  `steps_taken` varchar(100) NOT NULL,
  `types` varchar(100) NOT NULL,
  `privacy_protected` varchar(100) NOT NULL,
  `access_information` varchar(100) NOT NULL,
  `law_required` varchar(100) NOT NULL,
  `consequences` varchar(100) NOT NULL,
  UNIQUE KEY `id_6` (`section_id`),
  KEY `id` (`section_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `compliance_privacy_legislation`
--

INSERT INTO `compliance_privacy_legislation` (`section_id`, `collecting`, `answer`, `other_explain`, `researchers_access_data`, `data_retained`, `victorian_privacy`, `information_handled`, `descibe_procedures`, `collection_of_information`, `information_collected`, `identity_of_organisation`, `purpose`, `periods_for_records`, `steps_taken`, `types`, `privacy_protected`, `access_information`, `law_required`, `consequences`) VALUES
(6, '1', '0', 'asd/bkj', '0', '1', '0', 'asd;oih', 'asd;oug', '0', '0', '0', '1', '0', '1', '0', '0', '1', '0', '0'),
(7, '0', '0', ',;l,;l,', '1', '1', '1', ';lm;lml', 'l;m;lm', '1', '1', '0', '1', '0', '1', '1', '1', '0', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `data_collection`
--

CREATE TABLE IF NOT EXISTS `data_collection` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `questionnaire` varchar(35) NOT NULL,
  `interviews` varchar(35) NOT NULL,
  `interview_audio_recording` varchar(35) NOT NULL,
  `interview_video_recording` varchar(35) NOT NULL,
  `focus_groups` varchar(35) NOT NULL,
  `observations` varchar(35) NOT NULL,
  `observations_with_kp` varchar(35) NOT NULL,
  `observations_without_kp` varchar(35) NOT NULL,
  `photo_video_graph_with_kp` varchar(35) NOT NULL,
  `photo_video_graph_without_kp` varchar(35) NOT NULL,
  `photo_video_photography` varchar(35) NOT NULL,
  `photo_video_graph_video` varchar(35) NOT NULL,
  `collection_of_HT` varchar(35) NOT NULL,
  `collection_of_details` text NOT NULL,
  `response_to_tss` varchar(35) NOT NULL,
  `response_to_tss_details` text NOT NULL,
  `epidemiological_research` varchar(35) NOT NULL,
  `epidemiological_research_details` text NOT NULL,
  `psychology_inventories` varchar(35) NOT NULL,
  `psychology_inventories_details` text NOT NULL,
  `online_survey` varchar(35) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `data_collection`
--

INSERT INTO `data_collection` (`id`, `questionnaire`, `interviews`, `interview_audio_recording`, `interview_video_recording`, `focus_groups`, `observations`, `observations_with_kp`, `observations_without_kp`, `photo_video_graph_with_kp`, `photo_video_graph_without_kp`, `photo_video_photography`, `photo_video_graph_video`, `collection_of_HT`, `collection_of_details`, `response_to_tss`, `response_to_tss_details`, `epidemiological_research`, `epidemiological_research_details`, `psychology_inventories`, `psychology_inventories_details`, `online_survey`) VALUES
(19, 'Fully identifiable (name on it)', 'In_depth', 'No', 'Yes', 'NO', 'YES', '1', '1', '1', '1', '', 'NO', 'NO', 'asdasd', 'NO', 'asd;oh', 'NO', 'asd;uoh', 'NO', 'asd.iho', 'NO'),
(20, 'Fully identifiable (name on it)', 'In_depth', 'Yes', 'No', 'YES', 'YES', '1', '1', '1', '1', '', 'NO', 'YES', ';lm;lml', 'YES', ';lm;lm', 'NO', ';lm;lm', 'NO', 'lklnklnl', 'NO');

-- --------------------------------------------------------

--
-- Table structure for table `data_collection_cont`
--

CREATE TABLE IF NOT EXISTS `data_collection_cont` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `admin_of_substance` varchar(35) NOT NULL,
  `admin_of_substance_details` text NOT NULL,
  `other` varchar(35) NOT NULL,
  `other_description` text NOT NULL,
  `data_collection_location` text NOT NULL,
  `data_collection_location_sug` text NOT NULL,
  `person_to_collect_data` varchar(35) NOT NULL,
  `translator_required` varchar(35) NOT NULL,
  `translator_required_desc` text NOT NULL,
  `research_involve_children` varchar(35) NOT NULL,
  `research_involve_children_ys` varchar(35) NOT NULL,
  `research_involve_children_no` text NOT NULL,
  `dependent_or_unequal_rlshp` varchar(35) NOT NULL,
  `dependent_or_unequal_rlshp_details` text NOT NULL,
  `admin_involvement` varchar(35) NOT NULL,
  `admin_involvement_details` text NOT NULL,
  `procedures_use_diagonistic_or_indicative` varchar(35) NOT NULL,
  `criteria_to_assess_results` varchar(35) NOT NULL,
  `duty_of_care_to_participants` varchar(35) NOT NULL,
  `acquired_necessary_competence` varchar(35) NOT NULL,
  `indicate_procedure_proposed_to_PP` varchar(35) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `data_collection_cont`
--

INSERT INTO `data_collection_cont` (`id`, `admin_of_substance`, `admin_of_substance_details`, `other`, `other_description`, `data_collection_location`, `data_collection_location_sug`, `person_to_collect_data`, `translator_required`, `translator_required_desc`, `research_involve_children`, `research_involve_children_ys`, `research_involve_children_no`, `dependent_or_unequal_rlshp`, `dependent_or_unequal_rlshp_details`, `admin_involvement`, `admin_involvement_details`, `procedures_use_diagonistic_or_indicative`, `criteria_to_assess_results`, `duty_of_care_to_participants`, `acquired_necessary_competence`, `indicate_procedure_proposed_to_PP`) VALUES
(4, 'YES', 'asdasd', 'NO', 'asd', 'NO', 'asd', 'asdasd', 'No', 'asd', 'No', 'Yes', 'asd;oih', 'Yes', 'asd;oih', 'No', 'asdasd', 'Yes', 'asdklj', 'asd;ojh', 'Yes', 'Yes'),
(5, 'NO', 'l;m;lm', 'NO', ';lm;lm', 'NO', ';;lm;l', ';lm;lm', 'Yes', ';,'';,'';,''', 'Yes', 'No', ';lkm;lm', 'No', ';lm;lm', 'No', 'm;n;', 'No', ';l;lml', ';lm;l', 'No', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `data_materials_procedures`
--

CREATE TABLE IF NOT EXISTS `data_materials_procedures` (
  `section_id` int(10) NOT NULL AUTO_INCREMENT,
  `data_set` text NOT NULL,
  `data_collected` text NOT NULL,
  `ethics_approval` varchar(100) NOT NULL,
  `primary_purpose` text NOT NULL,
  `information_given` varchar(100) NOT NULL,
  `statement_used` varchar(100) NOT NULL COMMENT 'attachment required',
  `organisation_informed` varchar(100) NOT NULL,
  `procedure_description` varchar(100) NOT NULL,
  `method_used` varchar(100) NOT NULL,
  `participants_consented` varchar(100) NOT NULL,
  `access_NCIS` varchar(100) NOT NULL,
  `access_level` varchar(100) NOT NULL,
  `usually_access` varchar(100) NOT NULL,
  `describe_role` varchar(100) NOT NULL,
  `coronial_records` varchar(100) NOT NULL,
  `access_aproved` varchar(100) NOT NULL,
  UNIQUE KEY `id_4` (`section_id`),
  KEY `id` (`section_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `data_materials_procedures`
--

INSERT INTO `data_materials_procedures` (`section_id`, `data_set`, `data_collected`, `ethics_approval`, `primary_purpose`, `information_given`, `statement_used`, `organisation_informed`, `procedure_description`, `method_used`, `participants_consented`, `access_NCIS`, `access_level`, `usually_access`, `describe_role`, `coronial_records`, `access_aproved`) VALUES
(3, 'adsliug', 'asdipgu', 'asd;iug', 'asdliu', 'asdip9', 'Tyler-Durden.jpg', 'Yes', 'asdliug', 'Implied consent', 'No', 'Yes', 'Level 1', 'No', 'asdasd', 'No', 'Pending'),
(4, 'klnkln', 'lnkln', 'lkblknl', 'm;lm;l', 'lnklln', 'Lessons Learnt_Sizwe Williams.docx', 'Yes', ';lm;lm;l', 'Implied consent', 'Yes', 'No', 'Level 1', 'Yes', 'm;lm;lm', 'No', 'Pending');

-- --------------------------------------------------------

--
-- Table structure for table `debriefing_procedures`
--

CREATE TABLE IF NOT EXISTS `debriefing_procedures` (
  `section_id` int(10) NOT NULL AUTO_INCREMENT,
  `publish_form` varchar(250) NOT NULL,
  `communication_form` varchar(250) NOT NULL,
  `participants_results` varchar(250) NOT NULL,
  `results_provided` varchar(25) NOT NULL,
  `results_provided_others` varchar(250) NOT NULL,
  `debriefing_form_required` text NOT NULL,
  `results_infor_communication` text NOT NULL,
  `collectivity_community_research` text NOT NULL,
  PRIMARY KEY (`section_id`),
  UNIQUE KEY `id_10` (`section_id`),
  KEY `id` (`section_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `debriefing_procedures`
--

INSERT INTO `debriefing_procedures` (`section_id`, `publish_form`, `communication_form`, `participants_results`, `results_provided`, `results_provided_others`, `debriefing_form_required`, `results_infor_communication`, `collectivity_community_research`) VALUES
(4, 'Thesis', 'Copy of journal article / book / chapter', 'Participants will be provided with the researchers’ contact details in the Explanatory Statement to request the results', 'No', 'In totally deidentified summary form in which no individual can be identified', 'asd;oh', 'asdljg', 'asd;oih'),
(5, 'Thesis', 'Copy of journal article / book / chapter', 'Participants will be provided with the researchers’ contact details in the Explanatory Statement to request the results', 'Yes', 'In totally deidentified summary form in which no individual can be identified', 'klnklnknl', 'lknlknkln', '[pm[k[pk');

-- --------------------------------------------------------

--
-- Table structure for table `form1`
--

CREATE TABLE IF NOT EXISTS `form1` (
  `form_id` int(10) NOT NULL AUTO_INCREMENT,
  `application_id` int(11) NOT NULL,
  `section1_id` int(10) DEFAULT NULL,
  `section2_id` int(10) DEFAULT NULL,
  `section3_id` int(10) DEFAULT NULL,
  `section4A_id` int(10) DEFAULT NULL,
  `section4B_id` int(10) DEFAULT NULL,
  `section5A_id` int(10) DEFAULT NULL,
  `section5B_id` int(10) DEFAULT NULL,
  `section6A_id` int(10) DEFAULT NULL,
  `section6B_id` int(10) DEFAULT NULL,
  `section6C_id` int(10) DEFAULT NULL,
  `section6D_id` int(10) DEFAULT NULL,
  `section7_id` int(10) DEFAULT NULL,
  `ethical_issues` text,
  PRIMARY KEY (`form_id`),
  UNIQUE KEY `application_id` (`application_id`),
  UNIQUE KEY `section2_id` (`section2_id`),
  UNIQUE KEY `section5B_id` (`section5B_id`),
  KEY `section1_id` (`section1_id`),
  KEY `section3_id` (`section3_id`),
  KEY `section4A_id` (`section4A_id`),
  KEY `section5A_id` (`section5A_id`),
  KEY `section4B_id` (`section4B_id`),
  KEY `section6A_id` (`section6A_id`),
  KEY `section6B_id` (`section6B_id`),
  KEY `section7_id` (`section7_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `form1`
--

INSERT INTO `form1` (`form_id`, `application_id`, `section1_id`, `section2_id`, `section3_id`, `section4A_id`, `section4B_id`, `section5A_id`, `section5B_id`, `section6A_id`, `section6B_id`, `section6C_id`, `section6D_id`, `section7_id`, `ethical_issues`) VALUES
(11, 22, 25, 4, 4, 4, 3, 19, 4, 6, 3, NULL, NULL, 4, NULL),
(12, 23, 26, 5, 5, 5, 4, 20, 5, 7, 4, NULL, NULL, 5, NULL),
(13, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, 25, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 26, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, 27, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, 28, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(18, 29, 27, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `funder`
--

CREATE TABLE IF NOT EXISTS `funder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agency` varchar(20) NOT NULL,
  `funding_approved` tinyint(1) NOT NULL,
  `summary_attached` varchar(20) NOT NULL,
  `fm1_section1` int(11) DEFAULT NULL,
  `application_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `fm1_section1` (`fm1_section1`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `gaining_informed_consent`
--

CREATE TABLE IF NOT EXISTS `gaining_informed_consent` (
  `section_id` int(10) NOT NULL AUTO_INCREMENT,
  `explanatory_statement` varchar(100) NOT NULL,
  `exp_2_participants` varchar(100) DEFAULT NULL COMMENT 'if no explanations 2 part if yes attach statement',
  `research_nature` varchar(100) NOT NULL,
  `describe_procedure` varchar(100) DEFAULT NULL COMMENT 'if no en describe procedure',
  `obtain_consent` varchar(100) NOT NULL,
  `participants_informed` varchar(100) NOT NULL,
  `other_specify` varchar(100) NOT NULL,
  `obtain_informed_consent` varchar(100) NOT NULL,
  `witness_necessity` varchar(100) NOT NULL,
  `independent_witness_necessity` varchar(100) DEFAULT NULL,
  UNIQUE KEY `id_4` (`section_id`),
  KEY `id` (`section_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `gaining_informed_consent`
--

INSERT INTO `gaining_informed_consent` (`section_id`, `explanatory_statement`, `exp_2_participants`, `research_nature`, `describe_procedure`, `obtain_consent`, `participants_informed`, `other_specify`, `obtain_informed_consent`, `witness_necessity`, `independent_witness_necessity`) VALUES
(4, 'No', 'asd', 'No', 'asd', 'Implied consent', 'asdasd', 'asdasd', 'asdasd', 'No', 'asd'),
(5, 'No', ';lm;lm', 'Yes', 'lmlknkl', 'Implied consent', ';lm;lml', 'l;;k;l', ';lm;m', 'Yes', 'lnlkn');

-- --------------------------------------------------------

--
-- Table structure for table `occupy`
--

CREATE TABLE IF NOT EXISTS `occupy` (
  `staff_id` int(15) NOT NULL,
  `position_id` int(8) NOT NULL,
  PRIMARY KEY (`staff_id`,`position_id`),
  KEY `position_id` (`position_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `occupy`
--

INSERT INTO `occupy` (`staff_id`, `position_id`) VALUES
(14, 2),
(15, 3),
(23, 3),
(24, 3),
(22, 31),
(18, 36),
(15, 38);

-- --------------------------------------------------------

--
-- Table structure for table `organisation`
--

CREATE TABLE IF NOT EXISTS `organisation` (
  `name` varchar(35) NOT NULL,
  `person_granting_approval` varchar(35) NOT NULL,
  `role_in_organisation` varchar(35) NOT NULL,
  `permission_granted` varchar(100) NOT NULL,
  `project_id` int(10) NOT NULL,
  PRIMARY KEY (`name`),
  KEY `project_id` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `organisation_involved_info`
--

CREATE TABLE IF NOT EXISTS `organisation_involved_info` (
  `section_id` int(10) NOT NULL AUTO_INCREMENT,
  `involve_collection` varchar(100) NOT NULL,
  `collection_without_consent` varchar(100) NOT NULL,
  `collection_identifiable` varchar(100) NOT NULL,
  `identifiable_information` varchar(100) NOT NULL,
  `disclosure_of_identifiable_information` varchar(100) NOT NULL,
  `collect_info_from_third_party` varchar(100) NOT NULL,
  `category` varchar(100) NOT NULL,
  `information_of_records` varchar(100) NOT NULL,
  `agree_to_provide_information` varchar(100) NOT NULL,
  `explain_if_no` varchar(100) NOT NULL,
  `evidence` varchar(100) NOT NULL,
  `seperate_HREC_approval` varchar(100) NOT NULL,
  `routinely_access_info` varchar(100) NOT NULL,
  `type_of_info` varchar(100) NOT NULL,
  `type_of_organisation_health` varchar(100) NOT NULL,
  `type_of_organisation_personal` varchar(100) NOT NULL,
  `type_of_organisation_sensitive` varchar(100) NOT NULL,
  `privacy_principle_health` varchar(100) NOT NULL,
  `privacy_principle_personal` varchar(100) NOT NULL,
  `privacy_principle_sensitive` varchar(100) NOT NULL,
  `de_intefied_form` varchar(100) NOT NULL,
  `not_obtain_consent` varchar(100) NOT NULL,
  `reasons` varchar(100) NOT NULL,
  UNIQUE KEY `id_7` (`section_id`),
  KEY `id` (`section_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `organisation_involved_info`
--

INSERT INTO `organisation_involved_info` (`section_id`, `involve_collection`, `collection_without_consent`, `collection_identifiable`, `identifiable_information`, `disclosure_of_identifiable_information`, `collect_info_from_third_party`, `category`, `information_of_records`, `agree_to_provide_information`, `explain_if_no`, `evidence`, `seperate_HREC_approval`, `routinely_access_info`, `type_of_info`, `type_of_organisation_health`, `type_of_organisation_personal`, `type_of_organisation_sensitive`, `privacy_principle_health`, `privacy_principle_personal`, `privacy_principle_sensitive`, `de_intefied_form`, `not_obtain_consent`, `reasons`) VALUES
(3, 'No', 'Yes', 'No', 'Yes', 'No', '1', 'asd', 'asd', 'No', 'asd', 'asd', 'No', 'No', '0', '0', '0', '0', '0', '0', '0', 'asd', 'asd', 'asd'),
(4, 'No', 'No', 'Yes', 'Yes', 'Yes', '1', 'klnkln', 'k.bkjb', 'Yes', 'lblb', '/lknlkn', 'No', 'Yes', '1', '1', '0', '0', '0', '0', '0', 'k/lnlkn', 'l;knkln', ';onion');

-- --------------------------------------------------------

--
-- Table structure for table `position`
--

CREATE TABLE IF NOT EXISTS `position` (
  `position_id` int(8) NOT NULL AUTO_INCREMENT,
  `position_name` varchar(35) NOT NULL,
  `position_level` int(2) NOT NULL,
  `department_name` varchar(35) NOT NULL,
  PRIMARY KEY (`position_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=39 ;

--
-- Dumping data for table `position`
--

INSERT INTO `position` (`position_id`, `position_name`, `position_level`, `department_name`) VALUES
(2, 'accountant', 1, 'Finance'),
(3, 'lecture', 2, 'Ademic'),
(22, 'accountant', 2, 'Adminstration'),
(31, 'op', 2, 'Adminstration'),
(32, 'clerk', 3, 'Adminstration'),
(36, 'admin clerk', 1, 'Adminstration'),
(38, 'programmer', 1, 'Information Technology');

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE IF NOT EXISTS `profile` (
  `user_name` varchar(20) NOT NULL,
  `first_name` varchar(20) NOT NULL,
  `title` varchar(8) NOT NULL,
  `surname` varchar(20) NOT NULL,
  `gender` varchar(6) NOT NULL,
  `contact_number` varchar(11) NOT NULL,
  `email` varchar(65) NOT NULL,
  `contact_number2` int(35) DEFAULT NULL,
  `fax` int(35) DEFAULT NULL,
  `designation` varchar(35) NOT NULL,
  `profile_picture` varchar(75) DEFAULT NULL,
  PRIMARY KEY (`user_name`),
  KEY `user_name` (`user_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`user_name`, `first_name`, `title`, `surname`, `gender`, `contact_number`, `email`, `contact_number2`, `fax`, `designation`, `profile_picture`) VALUES
('admin', 'Neyo', 'Mr', 'Anderson', 'Male', '47852265', 'admin@localhost', NULL, NULL, 'Staff', 'default.jpg'),
('alba1', 'Alex', 'Mr', 'Blake', 'Male', '021445780', 'alex@gmail.com', NULL, NULL, 'Staff', 'default.jpg'),
('asd23', 'asd2', 'Mr', 'whale', 'Male', '0557898', 'slol@lolgmail.com', NULL, NULL, 'Staff', 'default.jpg'),
('cool12', 'micheal', 'Mr', 'lastword', 'Male', '0478951', 'cool12@gmail.edu', NULL, NULL, 'Student', 'default.jpg'),
('emag6', 'Eddie', 'Mr', 'Magara', 'Male', '0711394806', 'highedd@gmail.com', NULL, NULL, 'Student', '7793-Bicycle.jpg'),
('emy12', 'jhonathan', 'Mr', 'Thiel', 'Male', '0844094293', 'jhon@gmail.com', NULL, NULL, 'Student', 'default.jpg'),
('hasd', 'iohkjg', 'Mr', 'asdhiuh', 'Male', '458798952', 'lololol@gmail.com', NULL, NULL, 'Student', 'default.jpg'),
('Isal5', 'imraan', 'Mr', 'Salie', 'Male', '0844789566', 'isal6@gmail.com', NULL, NULL, 'Staff', '301-av-83.gif'),
('Jskur1', 'Jarod', 'Mr', 'Kurland', 'Male', '0844094293', 'jskur1@gmail.com', NULL, NULL, 'Staff', 'default.jpg'),
('lol23', 'hapoon', 'Mr', 'Williams', 'Male', '0844094293', 's@lolgmail.com', NULL, NULL, 'Student', 'default.jpg'),
('lol90', 'jhon', 'Mr', 'Wilknson', 'Male', '02155478', 'lol90@gmail.com', NULL, NULL, 'Staff', 'default.jpg'),
('man12', 'manly', 'Mr', 'manhood', 'Male', '0417889895', 'man@gmail.com', NULL, NULL, 'Student', 'default.jpg'),
('mod13', 'micheal', 'Mr', 'kean', 'Male', '0717888558', 'mod13@gmail.com', NULL, NULL, 'Student', 'default.jpg'),
('ngtho2', 'Nick', 'Mr', 'Thorby', 'Male', '071852963', 'nick@gmail.com', NULL, NULL, 'Student', 'default.jpg'),
('swil60', 'Karabo', 'Mr', 'Williams', 'Male', '084494293', 'sizwewill@gmail.com', NULL, NULL, 'Student', 'default.jpg'),
('tyl3r', 'tylor', 'Mr', 'Durden', 'Male', '0844094293', 'tylor@gmail.com', NULL, NULL, 'Staff', '3292-Tyler-Durden.jpg'),
('wil12', 'Wilson', 'Mr', 'Mark', 'Male', '041456123', 'will@gmail.com', NULL, NULL, 'Student', 'default.jpg'),
('wl12', 'Wilson', 'Mr', 'Rodney', 'Male', '0844098989', 'wl12@gmail.com', NULL, NULL, 'Student', 'default.jpg'),
 ('admin1', 'Admin', 'Mr', 'Admin', 'Female', '0976342837', 'admin@localhost.com', NULL, NULL, 'administrator', 'default.jpg');
-- --------------------------------------------------------

--
-- Table structure for table `project_details`
--

CREATE TABLE IF NOT EXISTS `project_details` (
  `section_id` int(12) NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL,
  `aim` text NOT NULL,
  `type1_research` varchar(250) NOT NULL,
  `type2_research` varchar(250) NOT NULL,
  `researcher_finance_involvement` varchar(250) NOT NULL,
  `research_funding` varchar(25) NOT NULL,
  PRIMARY KEY (`section_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `project_details`
--

INSERT INTO `project_details` (`section_id`, `description`, `aim`, `type1_research`, `type2_research`, `researcher_finance_involvement`, `research_funding`) VALUES
(25, 'ailug', 'asd', '1', 'Action research', '2', ''),
(26, 'iuh', 'asd', 'asd', 'Action research', 'joj', ''),
(27, 'oyo', 'poj', 'oih', 'Action research', 'ioh', '');

-- --------------------------------------------------------

--
-- Table structure for table `researcher`
--

CREATE TABLE IF NOT EXISTS `researcher` (
  `researcher_id` int(20) NOT NULL,
  `degree` varchar(60) NOT NULL,
  `student_number` int(10) NOT NULL,
  PRIMARY KEY (`researcher_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `researcher`
--

INSERT INTO `researcher` (`researcher_id`, `degree`, `student_number`) VALUES
(13, 'Honours in Bachelor of Computer and Social Science', 23444851),
(16, 'undergraduate', 23212799),
(17, 'Honours in Bachelor of Business Sci', 456123),
(19, 'Honours in Bachelor of Business Science', 235854),
(20, 'Honours in Bachelor of Business Science', 4587),
(21, 'Honours in Bachelor of Business Science', 1),
(26, 'Honours in Bachelor of Business Science', 7894522),
(27, 'Honours in Bachelor of Business Science', 44586232),
(28, 'Honours in Bachelor of Business Science', 789562),
(29, 'Honours in Bachelor of Business Science', 7895581),
(30, 'Honours in Bachelor of Business Science', 2344851);

-- --------------------------------------------------------

--
-- Table structure for table `research_group`
--

CREATE TABLE IF NOT EXISTS `research_group` (
  `section_id` int(10) NOT NULL AUTO_INCREMENT,
  `number_of_people` int(4) DEFAULT NULL,
  `group_desc` varchar(45) DEFAULT NULL,
  `age_range` varchar(20) NOT NULL,
  `criteria_procedure` varchar(20) NOT NULL,
  `work_time` varchar(45) NOT NULL,
  `human_participants_involved` varchar(20) DEFAULT NULL,
  `incentive` varchar(20) DEFAULT NULL,
  `MBBS_attatchment` varchar(45) DEFAULT NULL,
  `reimbursement_desc` varchar(45) NOT NULL,
  `using_UPSPP` varchar(45) DEFAULT NULL,
  `supporting_paragraph_for_using_UPSPP` text,
  `using_monash_MBBS` varchar(45) DEFAULT NULL,
  `include_exclude_status` varchar(45) DEFAULT NULL,
  `exclusion_desc` varchar(45) DEFAULT NULL,
  `participant_recruitment_detail` text,
  `selection_method` varchar(20) NOT NULL,
  `method_of_obtaining_participants_contacts` varchar(400) DEFAULT NULL,
  `using_previous_datasets` varchar(400) DEFAULT NULL,
  `previous_ethical_approved` varchar(400) DEFAULT NULL,
  `using_HT_HSC` varchar(400) DEFAULT NULL,
  PRIMARY KEY (`section_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `research_group`
--

INSERT INTO `research_group` (`section_id`, `number_of_people`, `group_desc`, `age_range`, `criteria_procedure`, `work_time`, `human_participants_involved`, `incentive`, `MBBS_attatchment`, `reimbursement_desc`, `using_UPSPP`, `supporting_paragraph_for_using_UPSPP`, `using_monash_MBBS`, `include_exclude_status`, `exclusion_desc`, `participant_recruitment_detail`, `selection_method`, `method_of_obtaining_participants_contacts`, `using_previous_datasets`, `previous_ethical_approved`, `using_HT_HSC`) VALUES
(4, 2, '', '1 - 10', 'asdkl', 'asd', 'No', '', '', '', '', '', '1', '', '', ';j;jh', 'asdilug', 'kljlh', '0', '1', '1'),
(5, NULL, '', '1 - 10', 'lm;l', 'asd', 'No', '1', '', 'asd', '1', 'asd', '0', '1', 'asd;oi', 'asdpoi', 'asd', 'jnoij', '1', '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `risks_procedures`
--

CREATE TABLE IF NOT EXISTS `risks_procedures` (
  `section_id` int(11) NOT NULL AUTO_INCREMENT,
  `participation_risk_involved` text NOT NULL,
  `participation_risk_description` text,
  `risk_described_in_consent_form` varchar(60) NOT NULL,
  `risk_not_outlined_reason` text,
  `risk_mitigation_plans` text NOT NULL,
  `emergency_procedures` text NOT NULL,
  `researcher_risk_mitigation_plans` text NOT NULL,
  `report_discoveries` text NOT NULL,
  UNIQUE KEY `id_2` (`section_id`),
  KEY `id` (`section_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `risks_procedures`
--

INSERT INTO `risks_procedures` (`section_id`, `participation_risk_involved`, `participation_risk_description`, `risk_described_in_consent_form`, `risk_not_outlined_reason`, `risk_mitigation_plans`, `emergency_procedures`, `researcher_risk_mitigation_plans`, `report_discoveries`) VALUES
(4, '1', 'asd', '0', 'asd', 'asdilu', 'asd', 'asdf', 'asd'),
(5, '1', 'iulgilf', '0', 'klnkl', ';lk;k;l', 'jpojp', 'jjl', 'knkl');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE IF NOT EXISTS `staff` (
  `staff_id` int(15) NOT NULL,
  `staff_id_number` int(35) NOT NULL,
  `validated` tinyint(1) NOT NULL,
  `available` tinyint(1) NOT NULL,
  `unavailable_start_time` date DEFAULT NULL,
  `unavailable_end_time` date DEFAULT NULL,
  PRIMARY KEY (`staff_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`staff_id`, `staff_id_number`, `validated`, `available`, `unavailable_start_time`, `unavailable_end_time`) VALUES
(14, 456456, 0, 1, NULL, NULL),
(15, 457896, 0, 1, '2013-06-03', '2013-09-11'),
(18, 123456, 0, 1, '2013-06-02', '2013-06-05'),
(22, 4785, 0, 1, NULL, NULL),
(23, 45678920, 0, 1, NULL, NULL),
(24, 456788, 0, 1, NULL, NULL),
(25, 963254, 0, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `supervise`
--

CREATE TABLE IF NOT EXISTS `supervise` (
  `project_id` int(15) NOT NULL DEFAULT '0',
  `staff_id` int(15) NOT NULL DEFAULT '0',
  KEY `staff_id` (`staff_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supervise`
--

INSERT INTO `supervise` (`project_id`, `staff_id`) VALUES
(22, 14),
(22, 15),
(22, 18),
(23, 14),
(23, 15),
(23, 18),
(24, 14),
(24, 22),
(24, 15),
(25, 25),
(25, 14),
(25, 15),
(26, 25),
(26, 14),
(26, 15),
(27, 25),
(27, 14),
(27, 15),
(28, 25),
(28, 14),
(28, 24),
(29, 25),
(29, 15),
(29, 24);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(20) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(20) NOT NULL,
  `password` varchar(256) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `application_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_name` (`user_name`),
  UNIQUE KEY `application` (`application_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_name`, `password`, `last_login`, `application_id`) VALUES
(13, 'swil60', 'a37a3f2f9eeecb73e5a7c51e64c5be96', '2013-06-14 00:07:05', NULL),
(14, 'alba1', '5f4dcc3b5aa765d61d8327deb882cf99', '2013-05-29 20:59:20', NULL),
(15, 'Isal5', '84badf32aa2071f84e7bded91df3e025', '2013-05-29 21:00:44', NULL),
(16, 'emag6', '34aa2f3ab8f26c9b170ef3dd339fef94', '2013-05-31 10:42:25', NULL),
(17, 'wil12', '5f4dcc3b5aa765d61d8327deb882cf99', '2013-06-01 12:01:17', NULL),
(18, 'Jskur1', '5f4dcc3b5aa765d61d8327deb882cf99', '2013-06-01 12:12:11', NULL),
(19, 'ngtho2', '5f4dcc3b5aa765d61d8327deb882cf99', '2013-06-02 15:10:28', NULL),
(20, 'wl12', '5f4dcc3b5aa765d61d8327deb882cf99', '2013-06-02 15:25:50', NULL),
(21, 'lol23', 'c4ca4238a0b923820dcc509a6f75849b', '2013-06-02 15:31:46', NULL),
(22, 'asd23', 'c20ad4d76fe97759aa27a0c99bff6710', '2013-06-02 15:48:09', NULL),
(23, 'lol90', 'c20ad4d76fe97759aa27a0c99bff6710', '2013-06-02 17:45:41', NULL),
(24, 'tyl3r', 'a37a3f2f9eeecb73e5a7c51e64c5be96', '2013-06-02 17:50:58', NULL),
(25, 'admin', 'a37a3f2f9eeecb73e5a7c51e64c5be96', '2013-06-02 18:00:35', NULL),
(26, 'man12', '5f4dcc3b5aa765d61d8327deb882cf99', '2013-06-03 06:39:57', NULL),
(27, 'emy12', '5f4dcc3b5aa765d61d8327deb882cf99', '2013-06-03 08:24:53', NULL),
(28, 'hasd', 'a37a3f2f9eeecb73e5a7c51e64c5be96', '2013-06-03 08:31:06', NULL),
(29, 'mod13', '5f4dcc3b5aa765d61d8327deb882cf99', '2013-06-03 09:45:45', NULL),
(30, 'cool12', '5f4dcc3b5aa765d61d8327deb882cf99', '2013-06-03 10:27:57', NULL),
(1, 'admin1', 'admin1', NULL, NULL);
--
-- Constraints for dumped tables
--

--
-- Constraints for table `administrator`
--
ALTER TABLE `administrator`
  ADD CONSTRAINT `administrator_ibfk_1` FOREIGN KEY (`admin_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE;

--
-- Constraints for table `application`
--
ALTER TABLE `application`
  ADD CONSTRAINT `application_ibfk_22` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

--
-- Constraints for table `form1`
--
ALTER TABLE `form1`
  ADD CONSTRAINT `form1_ibfk_63` FOREIGN KEY (`application_id`) REFERENCES `application` (`project_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `form1_ibfk_64` FOREIGN KEY (`section1_id`) REFERENCES `project_details` (`section_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `form1_ibfk_66` FOREIGN KEY (`section3_id`) REFERENCES `risks_procedures` (`section_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `form1_ibfk_67` FOREIGN KEY (`section4A_id`) REFERENCES `gaining_informed_consent` (`section_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `form1_ibfk_68` FOREIGN KEY (`section4B_id`) REFERENCES `data_materials_procedures` (`section_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `form1_ibfk_69` FOREIGN KEY (`section5A_id`) REFERENCES `data_collection` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `form1_ibfk_71` FOREIGN KEY (`section6A_id`) REFERENCES `compliance_privacy_legislation` (`section_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `form1_ibfk_74` FOREIGN KEY (`section5B_id`) REFERENCES `data_collection_cont` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `form1_ibfk_75` FOREIGN KEY (`section6B_id`) REFERENCES `organisation_involved_info` (`section_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `form1_ibfk_76` FOREIGN KEY (`section7_id`) REFERENCES `debriefing_procedures` (`section_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `form1_ibfk_77` FOREIGN KEY (`section2_id`) REFERENCES `research_group` (`section_id`) ON DELETE CASCADE;

--
-- Constraints for table `occupy`
--
ALTER TABLE `occupy`
  ADD CONSTRAINT `occupy_ibfk_1` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `occupy_ibfk_2` FOREIGN KEY (`position_id`) REFERENCES `position` (`position_id`) ON DELETE CASCADE;

--
-- Constraints for table `organisation`
--
ALTER TABLE `organisation`
  ADD CONSTRAINT `organisation_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `application` (`project_id`) ON DELETE CASCADE;

--
-- Constraints for table `researcher`
--
ALTER TABLE `researcher`
  ADD CONSTRAINT `researcher_ibfk_1` FOREIGN KEY (`researcher_id`) REFERENCES `user` (`user_id`);

--
-- Constraints for table `staff`
--
ALTER TABLE `staff`
  ADD CONSTRAINT `staff_ibfk_1` FOREIGN KEY (`staff_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE;

--
-- Constraints for table `supervise`
--
ALTER TABLE `supervise`
  ADD CONSTRAINT `supervise_ibfk_1` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`staff_id`) ON DELETE CASCADE;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_5` FOREIGN KEY (`user_name`) REFERENCES `profile` (`user_name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_ibfk_6` FOREIGN KEY (`application_id`) REFERENCES `application` (`project_id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
