<?php

class ApplicationController extends Controller
{
    public function beforeAction($action)
    {
        $config = array(
            'steps' => array(
                ' 1' => 'ProjectDetails', 
                ' 2' => 'ResearchGroup', 
                ' 3' => 'RisksProcedures', 
                ' 4a' => 'GainingInformedConsent', 
                ' 4b' => 'DataMaterialsProcedures', 
                ' 5a' => 'DataCollection', 
                ' 5b' => 'DataCollectionCont', 
                ' 6a' => 'CompliancePrivacyLegislation', 
                ' 6b' => 'OrganisationInvolvedInfo', 
                ' 6c' => 'DisclosureOfInformation', 
                ' 6d' => 'GeneralIssues', 
                ' 7' => 'DebriefingProcedures', 
                ' 8' => 'Ethical', 
                ' 9' => 'RiskManagementPlan'
            //data_collection
            ),
            'events' => array(
                'onStart' => 'wizardStart',
                'onProcessStep' => 'applicationWizardProcessStep',
                'onFinished' => 'wizardFinished',
                'onInvalidStep' => 'wizardInvalidStep',
            ),
            'menuLastItem' => ''
        );

        if (!empty($config)) {
            $config['class'] = 'application.components.WizardBehavior';
            $this->attachBehavior('wizard', $config);
        }
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionSelectSuperVisor($id)
    {
        $application = new Application();

        $model = new Profile('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Profile']))
            $model->attributes = $_GET['Profile'];

        /*
         * SUPERVISOR
         */
        if (isset($_POST['supervisors'])) {
            $staffUsernames = $_POST['supervisors'];
            $supervisors = new ArrayObject();
            $staffUsers = new ArrayObject();

            $application->application_type = $this->getFormTypes($id);
            for ($index = 0; $index < count($staffUsernames); $index++) {
                if ($index < 3) {//need to only get the 1st 3 elements of supervisor names::notify user limit
                    $supervisors[$index] = User::model()->findByAttributes(array('user_name' => $staffUsernames[$index]));
                    //print_r($supervisors[$index]->user_name);
                    $staffUsers[$index] = User::model()->findByAttributes(array('user_name' => $staffUsernames[$index]));
                }
            }

            $application->create_time = new CDbExpression('NOW()');
            $application->user_id = Yii::app()->user->id;
            $application->save(false);
            //finally saving the supervisors
            foreach ($staffUsers as $staffuser) {
                $supervision = new Supervise;
                $supervision->project_id = $application->project_id;
                $supervision->staff_id = $staffuser->user_id;

                $supervision->save();
            }

            if ($id == 0) {
                $sForm = new Form1('create');
                $sForm->form_id = null;
                $sForm->application_id = $application->project_id;
                $sForm->save();
                $this->setFormId($sForm->form_id);

                $this->redirect(array('application/form1'));
            } elseif ($id == 1) {
                $sForm = new LowRiskResearch('create');
                $sForm->form_id = null;
                $sForm->application_id = $application->project_id;
                $sForm->save();
                $this->redirect(array('application/lowRiskResearch'));
            } elseif ($id == 2) {
                $sForm = new MulticentreResearch('create');
                $sForm->form_id = null;
                $sForm->application_id = $application->project_id;
                $sForm->save();
                $this->redirect(array
                    ('application/multiCentreResearch'));
            } elseif ($id == 3) {
                $sForm = new Radiation('create');
                $sForm->form_id = null;
                $sForm->application_id = $application->project_id;
                $sForm->save();
                $this->redirect(array('application/radiation'));
            }

        }

        $this->render('selectSupervisor', array(
            'model' => $model, 'formType' => $id, 'application' => $application,
        ));
    }

    public function loadApplication()
    {
        if ($this->getFormId() == null) {
            $application = Application::model()->findBypk($this->getFormId());
        } else {
            $application = Application::model()->findByAttributes(array('user_id' => Yii::app()->user->id));
        }

        return $application;
    }

    public function actionResumeApplication($id)
    {
        yii::app()->session['appId'] = $id;
        $id = $this->loadApplication();
        $this->redirect(array('application/form1'));
    }

    public function setFormId($id)
    {
        yii::app()->session['appId'] = $id;
    }

    public function getFormId()
    {
        $prod = yii::app()->session['appId'];
        return $prod;
    }

    public function loadForm($applicationId)
    {
        $sform = Form1::model()->findByAttributes(array('application_id' => $applicationId));

        return $sform;
    }

    public function accessRules()
    {
        return array(
            array('deny',
                'actions' => array('index,section1,selectSupervisor', 'loadApplication', 'applicationWizardProcessStep', 'Form1', 'Funder', 'downloads'),
                'users' => array('?'),
            ),
            array('allow',
                'actions' => array('index,section1,selectSupervisor', 'loadApplication', 'applicationWizardProcessStep', 'Form1', 'Funder', 'downloads'),
                'users' => array('*'),
            ),
        );
    }

    public function actionForm1($step = null)
    {
        $this->pageTitle = $step;
        $this->process($step);
    }

    /**
     * Process wizard steps.
     * The event handler must set $event->handled=true for the wizard to continue
     * @param WizardEvent The event
     */
    public function applicationWizardProcessStep($event)
    {
        $modelName = ucfirst($event->step);
        $model = new $modelName();
        $sectionName = null;

        switch ($modelName) {
            case "ProjectDetails":
                $sectionName = "Section1";
                break;
            case "ResearchGroup":
                $sectionName = "Section2";
                break;
            case "RisksProcedures":
                $sectionName = "Section3";
                break;
            case "GainingInformedConsent":
                $sectionName = "Section4A";
                break;
            case "DataMaterialsProcedures":
                $sectionName = "Section4B";
                break;
            case "CompliancePrivacyLegislation":
                $sectionName = "Section6A";
                break;
            case "DataCollection":
                $sectionName = "Section5A";
                break;
            case "DataCollectionCont":
                $sectionName = "Section5B";
                break;
            case "OrganisationInvolvedInfo":
                $sectionName = "Section6B";
                break;
            case "DisclosureOfInformation":
                $sectionName = "Section6C";
                break;
            case "GeneralIssues":
                $sectionName = "Section6D";
                break;
            case "DebriefingProcedures"://do setion 7
                $sectionName = "Section7";
                break;
            case "Ethical":
                $sectionName = "Section10";
                break;
            case "RiskManagementPlan":
                $sectionName = "Section9";
                break;
        }
        if ($sectionName == null)
            throw new CHttpException(404, 'The requested page does not exist.');

        $model->attributes = $this->loadSection($event, $sectionName);

        if ($model->attributes == null)
            $model->attributes = $event->data;

        if (isset($_POST[$modelName])) {
            $model->attributes = $_POST[$modelName];
            if ($model->validate()) {

                $event->sender->save($model->attributes);
                $event->handled = true;
                $this->saveForm1($event, $sectionName, $model);

                if (isset($_POST["save"])) {

                    Yii::app()->user->setFlash('save', 'You have successfully saved the information in this section');
                    $this->refresh();
                }
            } else if ($model->validate() == false) {//on the same step and handling validation needing input once more
                $this->render('form1', array('event' => $event, 'model' => $model, 'sectionName' => $sectionName));
            }
        }
        else
        //on the next step
            $this->render('form1', array('event' => $event, 'model' => $model, 'sectionName' => $sectionName));
    }

    // Wizard Behavior Event Handlers
    /**
     * Raised when the wizard starts; before any steps are processed.
     * MUST set $event->handled=true for the wizard to continue.
     * Leaving $event->handled===false causes the onFinished event to be raised.
     * @param WizardEvent The event
     */
    public function wizardStart($event)
    {
        $event->handled = true;
    }

    /**
     * Raised when the wizard detects an invalid step
     * @param WizardEvent The event
     */
    public function wizardInvalidStep($event)
    {
        Yii::app()->getUser()->setFlash('notice', $event->step . ' is not a valid section in in this application');
    }

    public function actionDownloads()
    {
        $consent = new EFileHandler(Yii::getPathOfAlias('application.downloads.forms'), 'application/ConsentDownload');
        $this->render('downloads', array(
            'files' => $consent->getFiles(),
            'links' => $consent->getFileLinks()
        ));
    }

    public function actionConsentDownload($file)
    {
        Yii::app()->getRequest()->sendFile($file, file_get_contents(Yii::getPathOfAlias('application.downloads.forms') . '/' . $file));
    }

    public function actionFunder()
    {
        $models = array();
        $models[0] = new Funder();

        if (isset($_POST['Funder'])) {
            for ($index = 0; $index < count($_POST['Funder']); $index++) {
                if (isset($_POST['Funder'][$index])) {
                    $models[$index] = $_POST['Funder'][$index];
                    $models[$index] = new Funder();
                    $models[$index]->attributes = $_POST['Funder'][$index];
                    $models[$index]->validate();
                }
            }
        }


        $this->render('Funder', array('models' => $models));
    }

    public function actionMyapplications()
    {
        $model = new Application('search');

        $this->render('listApplication', array(
            'model' => $model,
        ));
    }

    public function actionSuperviseApplications()
    {
        $model = new Application('search');

        $this->render('superviseApplications', array(
            'model' => $model,
        ));
    }

    public function actionFunderRow($index)
    {
        $this->renderPartial('rowFunder', array('index' => $index));
    }

    /**
     * The wizard has finished; use $event->step to find out why.
     * Normally on successful completion ($event->step===true) data would be saved
     * to permanent storage; the demo just displays it
     * @param WizardEvent The event
     */
    public function wizardFinished($event)
    {
        //if ($event->step === true)
        $this->render('completed', compact('event'));
        //else
        //  $this->render('finished', compact('event'));
        //$event->sender->reset();
        Yii::app()->end();
    }

    public function saveForm1($event, $sectionName, $newInfo)
    {

        $application = $this->loadApplication();
        $sForm1 = $this->loadForm($application->project_id);

        $sectionName = lcfirst($sectionName . '_id');

        $modelName = ucfirst($event->step);

        if ($sForm1->$sectionName == null) {
            $newInfo->save();
            $sForm1->$sectionName = $newInfo->primaryKey;
            //print_r($newInfo->primaryKey);

            $sForm1->save();
        } else {
            $model = $modelName::model()->findByPk($sForm1->$sectionName);
            $model->attributes = $newInfo->attributes;
            $model->save();
        }
    }

    public function loadSection($event, $sectionName)
    {
        $application = $this->loadApplication();
        $sForm1 = $this->loadForm($application->project_id);
        $sectionName = lcfirst($sectionName . '_id');
        $returnVal = null;
        if ($sForm1->$sectionName != null) {
            $modelName = ucfirst($event->step);
            $model = $modelName::model()->findByPk($sForm1->$sectionName);
            $returnVal = $model->attributes;
        }
        return $returnVal;
    }

}
