<?php

class ApplicationMultiCentreController extends Controller
{
    public function beforeAction($action)
    {
        $config = array(
            'steps' => array(
                ' 1' => 'DetailsOfProject', 
                ' 2' => 'PotentiallyVulnerableParticipants', 
                ' 3' => 'DataLocation'
            //data_collection
            ),
            'events' => array(
                'onStart' => 'wizardStart',
                'onProcessStep' => 'applicationWizardProcessStep',
                'onFinished' => 'wizardFinished',
                'onInvalidStep' => 'wizardInvalidStep',
            ),
            'menuLastItem' => ''
        );

        if (!empty($config)) {
            $config['class'] = 'application.components.WizardBehavior';
            $this->attachBehavior('wizard', $config);
        }
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionResumeApplication($id)
    {
        yii::app()->session['appId'] = $id;
        $id = $this->loadApplication();
        $this->redirect(array('applicationMultiCentre/multiCentreResearch'));
    }

    public function loadForm($applicationId)
    {
        $sform = MultiCentreResearch::model()->findByAttributes(array('application_id' => $applicationId));

        return $sform;
    }

    public function accessRules()
    {
        return array(
            array('deny',
                'actions' => array('index,section1,selectSupervisor', 'loadApplication', 'applicationWizardProcessStep', 'MultiCentreResearch', 'Funder', 'downloads'),
                'users' => array('?'),
            ),
            array('allow',
                'actions' => array('index,section1,selectSupervisor', 'loadApplication', 'applicationWizardProcessStep', 'MultiCentreResearch', 'Funder', 'downloads'),
                'users' => array('*'),
            ),
        );
    }

    public function actionMultiCentreResearch($step = null)
    {
        $this->pageTitle = $step;
        $this->process($step);
    }

    /**
     * Process wizard steps.
     * The event handler must set $event->handled=true for the wizard to continue
     * @param WizardEvent The event
     */
    public function applicationWizardProcessStep($event)
    {
        $modelName = ucfirst($event->step);
        $model = new $modelName();
        $sectionName = null;

        switch ($modelName) {
            case "DetailsOfProject":
                $sectionName = "Section1";
                break;
            case "PotentiallyVulnerableParticipants":
                $sectionName = "Section2";
                break;
            case "DataLocation":
                $sectionName = "Section3";
                break;
        }
        
        if ($sectionName == null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }

        $model->attributes = $this->loadSection($event, $sectionName);

        if ($model->attributes == null) {
            $model->attributes = $event->data;
        }

        if (isset($_POST[$modelName])) {
            $model->attributes = $_POST[$modelName];
            if ($model->validate()) {
                $event->sender->save($model->attributes);
                $event->handled = true;
                $this->saveMultiCentreResearch($event, $sectionName, $model);

                if (isset($_POST["save"])) {
                    Yii::app()->user->setFlash('save', 'You have successfully saved the information in this section');
                    $this->refresh();
                }
            } else if ($model->validate() == false) {//on the same step and handling validation needing input once more
                $this->render('multiCentreResearch', array('event' => $event, 'model' => $model, 'sectionName' => $sectionName));
            }
        }
        else
        //on the next step
            $this->render('multiCentreResearch', array('event' => $event, 'model' => $model, 'sectionName' => $sectionName));
    }

    // Wizard Behavior Event Handlers
    /**
     * Raised when the wizard starts; before any steps are processed.
     * MUST set $event->handled=true for the wizard to continue.
     * Leaving $event->handled===false causes the onFinished event to be raised.
     * @param WizardEvent The event
     */
    public function wizardStart($event)
    {
        $event->handled = true;
    }

    /**
     * Raised when the wizard detects an invalid step
     * @param WizardEvent The event
     */
    public function wizardInvalidStep($event)
    {
        Yii::app()->getUser()->setFlash('notice', $event->step . ' is not a valid section in in this application');
    }

    public function actionDownloads()
    {
        $consent = new EFileHandler(Yii::getPathOfAlias('application.downloads.forms'), 'application/ConsentDownload');
        $this->render('downloads', array(
            'files' => $consent->getFiles(),
            'links' => $consent->getFileLinks()
        ));
    }

    public function actionConsentDownload($file)
    {
        Yii::app()->getRequest()->sendFile($file, file_get_contents(Yii::getPathOfAlias('application.downloads.forms') . '/' . $file));
    }

    public function actionFunder()
    {
        $models = array();
        $models[0] = new Funder();

        if (isset($_POST['Funder'])) {
            for ($index = 0; $index < count($_POST['Funder']); $index++) {
                if (isset($_POST['Funder'][$index])) {
                    $models[$index] = $_POST['Funder'][$index];
                    $models[$index] = new Funder();
                    $models[$index]->attributes = $_POST['Funder'][$index];
                    $models[$index]->validate();
                }
            }
        }

        $this->render('Funder', array('models' => $models));
    }

    public function actionMyapplications()
    {
        $model = new Application('search');

        $this->render('listApplication', array(
            'model' => $model,
        ));
    }

    public function actionSuperviseApplications()
    {
        $model = new Application('search');

        $this->render('superviseApplications', array(
            'model' => $model,
        ));
    }

    public function actionFunderRow($index)
    {
        $this->renderPartial('rowFunder', array('index' => $index));
    }

    /**
     * The wizard has finished; use $event->step to find out why.
     * Normally on successful completion ($event->step===true) data would be saved
     * to permanent storage; the demo just displays it
     * @param WizardEvent The event
     */
    public function wizardFinished($event)
    {
        //if ($event->step === true)
        $this->render('completed', compact('event'));
        //else
        //  $this->render('finished', compact('event'));
        //$event->sender->reset();
        Yii::app()->end();
    }

    public function saveMultiCentreResearch($event, $sectionName, $newInfo)
    {

        $application = $this->loadApplication();
        $sForm1 = $this->loadForm($application->project_id);

        $sectionName = lcfirst($sectionName . '_id');

        $modelName = ucfirst($event->step);

        if ($sForm1->$sectionName == null) {
            $newInfo->save();
            $sForm1->$sectionName = $newInfo->primaryKey;
            //print_r($newInfo->primaryKey);

            $sForm1->save();
        } else {
            $model = $modelName::model()->findByPk($sForm1->$sectionName);
            $model->attributes = $newInfo->attributes;
            $model->save();
        }
    }

    public function loadSection($event, $sectionName)
    {
        $application = $this->loadApplication();
        $sMultiCetreResearch = $this->loadForm($application->project_id);
        $sectionName = lcfirst($sectionName . '_id');
        $returnVal = null;
        if ($sMultiCetreResearch->$sectionName != null) {
            $modelName = ucfirst($event->step);
            $model = $modelName::model()->findByPk($sMultiCetreResearch->$sectionName);
            $returnVal = $model->attributes;
        }
        return $returnVal;
    }

}
