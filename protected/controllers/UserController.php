<?php

class UserController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    public function beforeAction($action) {
        $ref = parent::beforeAction($action);

        $profile = $this->loadProfile();
        if ($profile->designation == 'Student')
            $this->menu = array(
                array('label' => 'View my Profile', 'url' => array('user/index')),
                array('label' => 'Update my Profile', 'url' => array('user/update')),
                array('label' => 'Change Password', 'url' => array('user/changePassword')),
            );
        else
            $this->menu = array(
                array('label' => 'View my Profile', 'url' => array('user/index')),
                array('label' => 'Update my Profile', 'url' => array('user/update')),
                array('label' => 'Change Password', 'url' => array('user/changePassword')),
                array('label' => 'Set Availability', 'url' => array('user/setAvailability')),
                array('label' => 'View Availability', 'url' => array('user/availability')),
                array('label' => 'Supervise Applications', 'url' => array('user/superviseApplications')),
            );

        return $ref;
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

//    public function accessRules() {
//        return array(
//            array('allow', // allow authenticated user to perform 'create' and 'update' actions
//                'actions' => array('index,update'),
//                'users' => array('@'),
//            ),
//            array('allow', // allow admin user to perform 'admin' and 'delete' actions
//                'actions' => array('admin'),
//                'users' => array('admin'),
//            ),
//            array('deny', // deny all users
//                'users' => array('*'),
//            ),
//        );
//    }


    public function actionIndex() {
        $model = $this->loadUserModel();
        $profile = $this->loadProfile();
        if ($profile->designation == 'Student') {
            $this->render('student', array(
                'model' => $model, 'profile' => $profile
            ));
        } elseif ($profile->designation == 'Staff') {
            $this->render('staff', array(
                'model' => $model, 'profile' => $profile
            ));
        }
    }

    public function loadStaffPositions() {
        $staff = Staff::model()->findByPk(yii::app()->user->id);


        echo '<div  class="list-view" >';
        foreach ($staff->positions as $position) {
            echo '<div class="view">';
            echo'<class=view>';
            echo '<b>Position Name: </b>' . $position->position_name . '</br>';
            echo '<b>Position Level: </b>' . $position->position_level . '</br>';
            echo '<b>Department Name:</b>' . $position->department_name . '</br>';
            echo '</div>';
        }
        echo '</div>';
        //to stop the use of looping through database upddate the position dataprovider and render as requred


        /**
          $positionDataProvider = new CActiveDataProvider('position', array(
          'criteria' => array(
          'with' => array(
          'staffs' => array(
          //'condition'=>'staff_id='.$staff->staff_id,
          'together' => true,
          'joinType' => 'INNER JOIN',)
          )

          ),
          )
          );
         */
        //return $positionDataProvider;
    }

    public function loadUserModel() {
        $model = User::model()->findByPk(yii::app()->user->id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function loadProfile() {
        $model = User::model()->findByPk(yii::app()->user->id);
        $retrievedProfile = Profile::model()->findByPk($model->user_name);
        if ($retrievedProfile === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $retrievedProfile;
    }

    public function actionUpdate() {
        $profile = $this->loadProfile();
        if ($profile->designation == "Student") {
            $researcher = Researcher::model()->findByPk(yii::app()->user->id);
        } else {
            $staff = Staff::model()->findByPk(yii::app()->user->id);
            $positions = $staff->positions;
        }



        if (isset($_POST['Profile'])) {

            $uploadedFile = CUploadedFile::getInstance($profile, 'profile_picture');
            $profile->attributes = $_POST['Profile'];
            $rnd = rand(0, 9999);
            if ($uploadedFile != null) {
                $fileName = "{$rnd}-{$uploadedFile}";
                $profile->profile_picture = $fileName;
                $uploadedFile->saveAs(Yii::app()->basePath . '/../images/profilePics/' . $fileName);
            }
            if (isset($_POST['Researcher'])) {
                $researcher->attributes = $_POST['Researcher'];
                $researcher->save();
            }

            if ((isset($_POST['Position']))) {
                $pos = new Position();
                $pos->attributes = $_POST['Position'];
                if ($pos->isPositionExisting($pos) == false) {
                    $pos->save();
                    $occupyModel = new Occupy();
                    $occupyModel->position_id = $pos->position_id;
                    $occupyModel->staff_id = $staff->staff_id;
                    $occupyModel->save();
                }
            }


            if ($profile->save())
                $this->redirect(array('user/index'));
        }
        if ($profile->designation == 'Student') {
            $this->render('update/student', array(
                'profile' => $profile, 'researcher' => $researcher
            ));
        } elseif ($profile->designation == 'Staff') {
            $this->render('update/staff', array(
                'profile' => $profile, 'positions' => $positions
            ));
        }
    }

    public function actionPostionRow($index) {
        $this->renderPartial('update/rowPosition', array('index' => $index));
    }

    public function actionSetavailability() {
        $model = Staff::model()->findByPk(yii::app()->user->id);
        if (isset($_POST['Staff'])) {
            $model->attributes = $_POST['Staff'];
            $model->save();
            Yii::app()->user->setFlash('availability', 'You have set times in which you will not receive application forms to validate');
            $this->refresh();
        }

        $this->render('setAvailability', array('model' => $model));
    }

    public function actionAvailability() {
        $model = Staff::model()->findByPk(yii::app()->user->id);
        $date = date("Y-m-d");


        $avilability = true;
        if ($model->unavailable_start_time <= $date && $model->unavailable_end_time > $date) {
            $avilability = false;
        }

        $this->render('availability', array('model' => $model, 'availability' => $avilability));
    }

    public function actionChangePassword() {
        $model = new ChangePasswordForm();
        $user = User::model()->findByPk(yii::app()->user->id);

        if (isset($_POST['ChangePasswordForm'])) {
            $model->attributes = $_POST['ChangePasswordForm'];
            if ($model->validate()) {
                if (md5($model->currentPassword) == $user->password) {
                    $user->password = md5($model->newPassword);
                    if ($user->save(false)) {
                        Yii::app()->user->setFlash('success', 'Change password successfully.');
                        $this->refresh();
                    }
                } else
                    $model->addError('currentPassword', 'Current Password does not match');
            }
        }

        $this->render('password', array('model' => $model));
    }

}
