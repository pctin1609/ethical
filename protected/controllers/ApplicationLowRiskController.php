<?php

class ApplicationLowRiskController extends Controller
{
    public function beforeAction($action)
    {
        $config = array(
            'steps' => array(
                ' 1' => 'VulnerableParticipants', 
                ' 2' => 'TypeOfActivities', 
                ' 3' => 'ResearchProject', 
                ' 4' => 'ProjectDetailsLow', 
                ' 5' => 'ResearchGroupLow', 
                ' 6' => 'RisksProceduresLow', 
                ' 7a' => 'GainingInformedConsentLow', 
                ' 7b' => 'DataMaterialsProceduresLow', 
                ' 8a' => 'DataCollectionLow', 
                ' 8b' => 'DataCollectionContLow', 
                ' 9a' => 'CompliancePrivacyLegislationLow', 
                ' 9b' => 'OrganisationInvolvedInfoLow', 
                ' 9c' => 'DisclosureOfInformationLow', 
                ' 9d' => 'GeneralIssuesLow', 
                ' 10' => 'DebriefingProceduresLow', 
                ' 11' => 'EthicalLow', 
                ' 12' => 'RiskManagementPlanLow'
            //data_collection
            ),
            'events' => array(
                'onStart' => 'wizardStart',
                'onProcessStep' => 'applicationWizardProcessStep',
                'onFinished' => 'wizardFinished',
                'onInvalidStep' => 'wizardInvalidStep',
            ),
            'menuLastItem' => ''
        );

        if (!empty($config)) {
            $config['class'] = 'application.components.WizardBehavior';
            $this->attachBehavior('wizard', $config);
        }
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionResumeApplication($id)
    {
        Yii::app()->session['appId'] = $id;
        $id = $this->loadApplication();
        $this->redirect(array('applicationLowRisk/lowRiskResearch'));
    }

    public function loadForm($applicationId)
    {
        $sform = LowRiskResearch::model()->findByAttributes(array('application_id' => $applicationId));

        return $sform;
    }

    public function getFormTypes($index)
    {
        if ($index >= 0 && $index <= 3) {
            $formTypes = array(
                "form1", 
                "Low Risk Research",
                "Multicentre Research", 
                "Research involving Radiation", 
                "Retrospective Research and Research involving Existing Data",
                "Research involving Human Tissue", 
                "Expected Review", 
                "Research that may be exempt from Ethical Review"
            );

            return $formTypes[$index];
        } else
            throw new CHttpException(404, 'The requested page does not exist.');
    }

    public function accessRules()
    {
        return array(
            array('deny',
                'actions' => array('index,section1,selectSupervisor', 'loadApplication', 'applicationWizardProcessStep', 'LowRiskResearch', 'Funder', 'downloads'),
                'users' => array('?'),
            ),
            array('allow',
                'actions' => array('index,section1,selectSupervisor', 'loadApplication', 'applicationWizardProcessStep', 'LowRiskResearch', 'Funder', 'downloads'),
                'users' => array('*'),
            ),
        );
    }

    public function actionLowRiskResearch($step = null)
    {
        $this->pageTitle = $step;
        $this->process($step);
    }

    /**
     * Process wizard steps.
     * The event handler must set $event->handled=true for the wizard to continue
     * @param WizardEvent The event
     */
    public function applicationWizardProcessStep($event)
    {
        $modelName = ucfirst($event->step);
        $model = new $modelName();
        $sectionName = null;

        switch ($modelName) {
            case "VulnerableParticipants":
                $sectionName = "Section1";
                break;
            case "TypeOfActivities":
                $sectionName = "Section2";
                break;
            case "ResearchProject":
                $sectionName = "Section3";
                break;
			case "ProjectDetailsLow":
                $sectionName = "Section4";
                break;
            case "ResearchGroupLow":
                $sectionName = "Section5";
                break;
            case "RisksProceduresLow":
                $sectionName = "Section6";
                break;
            case "GainingInformedConsentLow":
                $sectionName = "Section7A";
                break;
            case "DataMaterialsProceduresLow":
                $sectionName = "Section7B";
                break;
            case "CompliancePrivacyLegislationLow":
                $sectionName = "Section9A";
                break;
            case "DataCollectionLow":
                $sectionName = "Section8A";
                break;
            case "DataCollectionContLow":
                $sectionName = "Section8B";
                break;
            case "OrganisationInvolvedInfoLow":
                $sectionName = "Section9B";
                break;
            case "DisclosureOfInformationLow":
                $sectionName = "Section9C";
                break;
            case "GeneralIssuesLow":
                $sectionName = "Section9D";
                break;
            case "DebriefingProceduresLow"://do setion 7
                $sectionName = "Section10";
                break;
            case "EthicalLow":
                $sectionName = "Section11";
                break;
            case "RiskManagementPlanLow":
                $sectionName = "Section12";
                break;
            
        }
        
        if ($sectionName == null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }

        $model->attributes = $this->loadSection($event, $sectionName);

        if ($model->attributes == null)
            $model->attributes = $event->data;

        if (isset($_POST[$modelName])) {
            $model->attributes = $_POST[$modelName];
            if ($model->validate()) {

                $event->sender->save($model->attributes);
                $event->handled = true;
                $this->saveLowRisk($event, $sectionName, $model);

                if (isset($_POST["save"])) {
                    Yii::app()->user->setFlash('save', 'You have successfully saved the information in this section');
                    $this->refresh();
                }
            } else if ($model->validate() == false) {//on the same step and handling validation needing input once more
                $this->render('lowRiskResearch', array('event' => $event, 'model' => $model, 'sectionName' => $sectionName));
            }
        }
        else
        //on the next step
            $this->render('lowRiskResearch', array('event' => $event, 'model' => $model, 'sectionName' => $sectionName));
    }

    // Wizard Behavior Event Handlers
    /**
     * Raised when the wizard starts; before any steps are processed.
     * MUST set $event->handled=true for the wizard to continue.
     * Leaving $event->handled===false causes the onFinished event to be raised.
     * @param WizardEvent The event
     */
    public function wizardStart($event)
    {
        $event->handled = true;
    }

    /**
     * Raised when the wizard detects an invalid step
     * @param WizardEvent The event
     */
    public function wizardInvalidStep($event)
    {
        Yii::app()->getUser()->setFlash('notice', $event->step . ' is not a valid section in in this application');
    }

    public function actionDownloads()
    {
        $consent = new EFileHandler(Yii::getPathOfAlias('application.downloads.forms'), 'application/ConsentDownload');
        $this->render('downloads', array(
            'files' => $consent->getFiles(),
            'links' => $consent->getFileLinks()
        ));
    }

    public function actionConsentDownload($file)
    {
        Yii::app()->getRequest()->sendFile($file, file_get_contents(Yii::getPathOfAlias('application.downloads.forms') . '/' . $file));
    }

    public function actionFunder()
    {
        $models = array();
        $models[0] = new Funder();

        if (isset($_POST['Funder'])) {
            for ($index = 0; $index < count($_POST['Funder']); $index++) {
                if (isset($_POST['Funder'][$index])) {
                    $models[$index] = $_POST['Funder'][$index];
                    $models[$index] = new Funder();
                    $models[$index]->attributes = $_POST['Funder'][$index];
                    $models[$index]->validate();
                }
            }
        }

        $this->render('Funder', array('models' => $models));
    }

    public function actionMyapplications()
    {
        $model = new Application('search');

        $this->render('listApplication', array(
            'model' => $model,
        ));
    }

    public function actionSuperviseApplications()
    {
        $model = new Application('search');

        $this->render('superviseApplications', array(
            'model' => $model,
        ));
    }

    public function actionFunderRow($index)
    {
        $this->renderPartial('rowFunder', array('index' => $index));
    }

    /**
     * The wizard has finished; use $event->step to find out why.
     * Normally on successful completion ($event->step===true) data would be saved
     * to permanent storage; the demo just displays it
     * @param WizardEvent The event
     */
    public function wizardFinished($event)
    {
        //if ($event->step === true)
        $this->render('completed', compact('event'));
        //else
        //  $this->render('finished', compact('event'));
        //$event->sender->reset();
        Yii::app()->end();
    }

    public function saveLowRisk($event, $sectionName, $newInfo)
    {
        $application = $this->loadApplication();
        $sForm1 = $this->loadForm($application->project_id);

        $sectionName = lcfirst($sectionName . '_id');
        $modelName = ucfirst($event->step);

        if ($sForm1->$sectionName == null) {
            $newInfo->save();
            $sForm1->$sectionName = $newInfo->primaryKey;
            //print_r($newInfo->primaryKey);

            $sForm1->save();
        } else {
            $model = $modelName::model()->findByPk($sForm1->$sectionName);
            $model->attributes = $newInfo->attributes;
            $model->save();
        }
    }

    public function loadSection($event, $sectionName)
    {
        $application = $this->loadApplication();
        $sForm1 = $this->loadForm($application->project_id);
        $sectionName = lcfirst($sectionName . '_id');
        $returnVal = null;
        if ($sForm1->$sectionName != null) {
            $modelName = ucfirst($event->step);
            $model = $modelName::model()->findByPk($sForm1->$sectionName);
            $returnVal = $model->attributes;
        }
        return $returnVal;
    }

}
