<?php

class Html extends CHtml {
    
	/**
	 * @return string Html string of required tag.
	 */
	public static function requiredTag() {
		return self::tag("span", array("class" => "required"), "*");
	}
	
    /**
     * Makes the given URL relative to the /image directory
     */
    public static function imageUrl($url) {
        return Hp::baseUrl() . '/img/' . $url;
    }
    
    /**
     * Makes the given URL relative to the /css directory
     */
    public static function cssUrl($url) {
        return Hp::baseUrl() . '/css/' . $url;
    }
    
    /**
     * Makes the given URL relative to the /js directory
     */
    public static function jsUrl($url) {
        return Hp::baseUrl() . '/js/' . $url;
    }	
}