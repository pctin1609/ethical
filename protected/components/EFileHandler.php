<?php
class EFileHandler {
	
	private $directory=null;
	private $path=null;
	private $files=array();
	private $fileLinks=array();
	private $downloadAction;
	
	public function __construct($path, $downloadAction) {
		$this->path = $path;
		$this->downloadAction = $downloadAction;
		if(is_dir($this->path))
			$this->directory = dir($this->path);
		else $this->directory = false;
		
		$this->setFiles();
	}
	
	public function setFiles() {
		$this->files=array();
		
		if(is_dir($this->path)) {
			while(false !== ($entry = readdir($this->directory->handle))) {
				if($entry !== '.' && $entry !== '..') {
					array_push($this->files, array(
						'name'=>$entry,
						'size'=>round(filesize($this->path.DIRECTORY_SEPARATOR.$entry)/1048576,2,PHP_ROUND_HALF_EVEN),
						'type'=>$this->extension($entry)
					));
					array_push($this->fileLinks, Yii::app()->createUrl($this->downloadAction,array('file'=>$entry)));
				}
			}
		}
	}
	
	public function getFiles() {
		return $this->files;
	}
	
	public function getFileLinks() {
		return $this->fileLinks;
	}
	
	private function extension($path) {
		$path = explode('.', $path);
		return $path[count($path)-1];
	}
}


?>