<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
    public $formid;
    public $sform;
    public $userData; //Holds an ActiveRecord with the current user, null if guest

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl'
        );
    }
    
    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = array();

    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = array();

    public function init()
    {
        $uri = Yii::app()->baseUrl . '/themes/monash/css/applications.css';
        Yii::app()->clientScript->registerCssFile($uri, 'screen, projection');
        return parent::init();
    }

    public function loadApplication()
    {
        if ($this->getFormId() == null) {
            $application = Application::model()->findByAttributes(array('user_id' => Yii::app()->user->id));
        } else {
            $application = Application::model()->findByPk($this->getFormId());
        }

        return $application;
    }
    
    public function actionSelectSuperVisor($id)
    {
        $application = new Application();

        $model = new Profile('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Profile']))
            $model->attributes = $_GET['Profile'];

        /*
         * SUPERVISOR
         */
        if (isset($_POST['supervisors'])) {
            $staffUsernames = $_POST['supervisors'];
            $supervisors = new ArrayObject();
            $staffUsers = new ArrayObject();

            $application->application_type = $this->getFormTypes($id);
            for ($index = 0; $index < count($staffUsernames); $index++) {
                if ($index < 3) {//need to only get the 1st 3 elements of supervisor names::notify user limit
                    $supervisors[$index] = User::model()->findByAttributes(array('user_name' => $staffUsernames[$index]));
                    //print_r($supervisors[$index]->user_name);
                    $staffUsers[$index] = User::model()->findByAttributes(array('user_name' => $staffUsernames[$index]));
                }
            }

            $application->create_time = new CDbExpression('NOW()');
            $application->user_id = Yii::app()->user->id;
            $application->save(false);
            //finally saving the supervisors
            foreach ($staffUsers as $staffuser) {
                $supervision = new Supervise;
                $supervision->project_id = $application->project_id;
                $supervision->staff_id = $staffuser->user_id;

                $supervision->save();
            }
            
            $this->setFormId($application->project_id);

            if ($id == 0) {
                $sForm = new Form1('create');
                $sForm->form_id = null;
                $sForm->application_id = $application->project_id;
                $sForm->save();

                $this->redirect(array('applicationForm1/form1'));
            } elseif ($id == 1) {
                $sForm = new LowRiskResearch('create');
                $sForm->form_id = null;
                $sForm->application_id = $application->project_id;
                $sForm->save();
                
                $this->redirect(array('applicationLowRisk/lowRiskResearch'));
            } elseif ($id == 2) {
                $sForm = new MulticentreResearch('create');
                $sForm->form_id = null;
                $sForm->application_id = $application->project_id;
                $sForm->save();
                
                $this->redirect(array('applicationMultiCentre/multiCentreResearch'));
            } else {
                $sForm = new Radiation('create');
                $sForm->form_id = null;
                $sForm->application_id = $application->project_id;
                $sForm->save();
                
                $this->redirect(array('applicationRadiation/radiationForm'));
            }
        }

        $this->render('selectSupervisor', array(
            'model' => $model, 'formType' => $id, 'application' => $application,
        ));
    }
    
    public function setFormId($id)
    {
        Yii::app()->session['appId'] = $id;
    }

    public function getFormId()
    {
        $prod = Yii::app()->session['appId'];
        return $prod;
    }
    
    public function getFormTypes($index)
    {
        if ($index >= 0 && $index <= 3) {
            $formTypes = array(
                "form1",
                "Low Risk Research",
                "Multicentre Research",
                "Research involving Radiation",
                "Retrospective Research and Research involving Existing Data",
                "Research involving Human Tissue",
                "Expected Review",
                "Research that may be exempt from Ethical Review"
            );

            return $formTypes[$index];
        } else
            throw new CHttpException(404, 'The requested page does not exist.');
    }
}