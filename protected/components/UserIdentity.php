<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity {

    private $_id;
    private $user;

    public function authenticate() {
        $user = User::model()->findByAttributes(array('user_name' => $this->username));
        if ($user === null)
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        else
        if ($user->password != $user->encrypt($this->password))
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        else {
            $this->_id = $user->user_id;
            if (null === $user->last_login) {
                $lastLogin = time();
            }   else
            {
          $lastLogin = strtotime($user->last_login);
        }
            $adminUser = Administrator::model()->findByAttributes(array('admin_id' => $user->user_id));
            $this->setState('profileDetails', $this->toString());
            $this->setState('email', $this->getEmail());
            $this->setState('designation', $this->designation());
            $this->setState('isAdmin', !is_null($adminUser));
            $this->errorCode = self::ERROR_NONE;
        }
        return !$this->errorCode;
    }

    public function getId() {
        return $this->_id;
    }

    //to ensure you can get details of the user which are in profile
    private function toString() {
        $profile = Profile::model()->findByAttributes(array('user_name' => $this->username));

        return $profile->title . ' ' . $profile->first_name . ' ' . $profile->surname;
    }

    public Function designation() {
        $profile = Profile::model()->findByAttributes(array('user_name' => $this->username));

        $designation = $profile->designation;
        return $designation;
    }

    public Function getEmail() {
        $profile = Profile::model()->findByAttributes(array('user_name' => $this->username));

        $email = $profile->email;
        return $email;
    }

}