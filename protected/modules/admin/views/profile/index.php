<div id ="square">
<?php
/* @var $this ProfileController */
/* @var $model Profile */

$this->breadcrumbs = array(
    'Profiles' => array('index'),
    'Manage',
);

$this->menu = array(
    array('label' => 'View all Users', 'url' => array('index')),
    array('label' => 'List Of Researchers', 'url' => array('researcher')),
    array('label' => 'List Of Staff Members', 'url' => array('ManageStaff')),
 array('label' => 'Information', 'url' => array('displayForms/index')),
	);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('profile-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>View All Users</h1>



<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button'));  ?>
<div class="search-form" style="display:none">
    <?php
    $this->renderPartial('_search', array(
        'model' => $model,
    ));
    ?>
</div><!-- search-form -->

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'profile-grid',
    'dataProvider' => $model->search(),
    //'filter'=>$model,
    'columns' => array(
        array(
            // 'htmlOptions'=>array('width'=>'90px'),


            'name' => 'profile picture',
            'type' => 'raw',
            'value' => 'CHtml::image(Yii::app()->baseUrl."/images/profilePics/$data->profile_picture","profile picture",array("width"=>"90px","height"=>"90"))',
        ),
        'user_name',
        'title',
        'first_name',
        'surname',
        'gender',
        'contact_number',
        'designation',
        'users.user_id'
    /*
      'email',
      'contact_number2',
      'fax',
      'designation',
      'profile_picture',
     */
//		array(
//			'class'=>'CButtonColumn',
//		),
    ),
));
?>
</div>