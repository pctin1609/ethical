<div id ="square">
<?php
$this->menu = array(
    array('label' => 'View all Users', 'url' => array('index')),
    array('label' => 'List of  Researchers', 'url' => array('researcher')),
    array('label' => 'List Of Staff Members', 'url' => array('ManageStaff')),
);
?>

<h1>List Of Staff Members</h1>



<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'profile-grid',
    'dataProvider' => $model->searchStaff(),
    //'filter'=>$model,
    'columns' => array(
          array(
            // 'htmlOptions'=>array('width'=>'90px'),


            'name' => 'profile picture',
            'type' => 'raw',
            'value' => 'CHtml::image(Yii::app()->baseUrl."/images/profilePics/$data->profile_picture","profile picture",array("width"=>"90px","height"=>"90"))',
        ),
        'user_name',
        'title',
        'first_name',
        
        'surname',
        'gender',
        'contact_number',
      
    //'users.user_id'
    /*
      'email',
      'contact_number2',
      'fax',
      'designation',
      'profile_picture',
     */
//		array(
//			'class'=>'CButtonColumn',
//		),
    ),
));
?>

</div>

