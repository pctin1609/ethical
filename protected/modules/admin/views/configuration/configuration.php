
<div class="form">
    <?php if (Yii::app()->user->hasFlash("success")): ?>
        <div class="flash-success">
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
    <?php endif; ?>
    
    <h1><?php echo $titleConfigPage; ?></h1>
    <p class="note">All fields are mandatory.</p>
    
    <form method="post" action="">
        <?php foreach ($models as $i => $model) { ?>
            <div class="row">
                <?php echo "<label class='required'>" . $configFields[$model->name]['title'] . "</label>"; ?>
                <?php 
                    //define some input text class default
                    $inputTextClass = array('textfield');
                    switch ($configFields[$model->name]['type']) {
                        case 'select':
                            echo Html::activeDropDownList($model, "[$i]value", $configFields[$model->name]['data']);
                            break;
                        case 'password':
                            echo Html::activePasswordField($model, "[$i]value", array('class' => implode(' ', $inputTextClass)));
                            break;
                        case 'text':
                        default:
                            echo Html::activeTextField($model, "[$i]value", array('class' => implode(' ', $inputTextClass)));
                            break;
                    }
                    if (isset($configFields[$model->name]['desc']))
                        echo "<br/><small>" . $configFields[$model->name]['desc'] . "</small>";
                ?>
                <?php echo Html::error($model, "[$i]value");  ?>
            </div>
        <?php } ?>
        
        <div class="row buttons">
            <?php echo CHtml::submitButton('Save'); ?>
        </div>
    </form>
</div>