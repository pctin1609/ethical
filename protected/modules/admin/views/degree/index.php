<?php

$this->pageTitle = 'Ethical Storage :: Admin :: Degree';
?>

<?php if (app()->user->hasFlash('success')) { ?>
    <div class="flash-success">
        <?php echo app()->user->getFlash('success');?>
    </div>
<?php } ?>

<h3>Management Of  Degrees</h3>

<?php echo CHtml::link('Add Degree', url('/admin/degree/add'), array('style' => 'float: right; margin-top: -35px;'))?>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'application-grid',
    'dataProvider' => $dataProvider,
    'columns' => array(
        'name',
        array(
            'name' => 'created_date',
            'value' => 'date("M d, Y h:i:s")',
        ),
        array(
            'class'=>'CButtonColumn',
            'template' => '{update} {delete}',
        ),
    ),
));
?>