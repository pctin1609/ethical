
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'degree-form',
	'enableClientValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>
    <h1><?php echo ($model->isNewRecord ? 'Add' : 'Update')?> Degree</h1>
    
    <p class="note">All fields are mandatory.</p>
    
    <div class="row">
        <?php echo $form->labelEx($model, 'name'); ?>
        <?php echo $form->textField($model, 'name', array('style' => 'width: 400px')); ?>
        <?php echo $form->error($model, 'name'); ?>
    </div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Submit'); ?>
        <?php echo CHtml::button('Cancel', array('onclick' => 'window.location="' . url('/admin/degree') . '"')); ?>
	</div>
<?php $this->endWidget(); ?>
</div><!-- form -->