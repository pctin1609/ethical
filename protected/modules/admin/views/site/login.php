<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - login';
$this->breadcrumbs=array(
	'login',
);
?>

<!--<h1>Welcome to <i><?php echo CHtml::encode(Yii::app()->name); ?></i></h1>-->


<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>


	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username'); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password'); ?>
		<?php echo $form->error($model,'password'); ?>
		<p class="hint">
			<p>Not a user yet? <?php echo CHtml::link("Register here",array('site/register')); ?></p>
		</p>
	</div>

	<div class="row rememberMe">
		<?php echo $form->checkBox($model,'rememberMe'); ?>
		<?php echo $form->label($model,'rememberMe'); ?>
		<?php echo $form->error($model,'rememberMe'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Login'); ?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->



<?php
        $this->widget('ext.slider.slider', array(   
            'container'=>'slideshow',
            'width'=>600, 
            'height'=>280, 
            'timeout'=>4000,
            'infos'=>true,
            'constrainImage'=>true,
         'images'=>array('easy.jpg','lolo.png','res.jpg','pop.jpg'),
		 
            'alts'=>array('Easy to fill in application forms','','Apply for Ethical Research Clearance','Select a supervisor that will guide you'),
            )
        );
        ?>
