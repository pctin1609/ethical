<?php
/* @var $this SiteController */

$this->pageTitle = Yii::app()->name;
?>
<p></p>
<p><?php echo 'Welcome ' . yii::app()->user->profileDetails; ?></p>

<!--<p> We are pleased to see you connected to the Eagle South Africa Ethical Clearance Site </p>-->

<h3>My applications for Form1</h3>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'application-grid',
    'dataProvider' => $model->getUserApplications(yii::app()->user->id, 'form1'),
    'columns' => array(
        'application_type',
        'create_time',
        'status',
        array(
            'class' => 'CLinkColumn',
            'label' => 'Resume Application',
            'urlExpression' => 'Yii::app()->createUrl("applicationForm1/resumeApplication",array("id"=>$data->project_id))',
            'header' => 'Resume'
        ),
    ),
));
?>


<h3>My applications for Low Risk</h3>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'application-grid',
    'dataProvider' => $model->getUserApplications(yii::app()->user->id, 'Low Risk Research'),
    'columns' => array(
        'application_type',
        'create_time',
        'status',
        array(
            'class' => 'CLinkColumn',
            'label' => 'Resume Application',
            'urlExpression' => 'Yii::app()->createUrl("applicationLowRisk/resumeApplication",array("id"=>$data->project_id))',
            'header' => 'Resume'
        ),
    ),
));
?>




<h3>My applications for Multi Centre Research</h3>
<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'application-grid',
    'dataProvider'=>$model->getUserApplications(yii::app()->user->id, 'Multicentre Research'),
    'columns'=>array(
        'application_type',
        'create_time',        
        'status',
         array(
        'class'=>'CLinkColumn',
        'label'=>'Resume Application',
        'urlExpression'=>'Yii::app()->createUrl("applicationMultiCentre/resumeApplication",array("id"=>$data->project_id))',
        'header'=>'Resume'
      ),
       
    ),
    
)); ?>
<h3>My applications for Research Involving Radiation</h3>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'application-grid',
    'dataProvider' => $model->getUserApplications(yii::app()->user->id, 'Research involving Radiation'),
    'columns' => array(
        'application_type',
        'create_time',
        'status',
        array(
            'class' => 'CLinkColumn',
            'label' => 'Resume Application',
            'urlExpression' => 'Yii::app()->createUrl("applicationRadiation/resumeApplication",array("id"=>$data->project_id))',
            'header' => 'Resume'
        ),
    ),
));
?>