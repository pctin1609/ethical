<?php if (Yii::app()->user->hasFlash('register')): ?>
    <div class="flash-success">
        <?php echo Yii::app()->user->getFlash('register');?>
    </div>

<?php else: ?>
    <script>
        window.addEventListener('load',displayDate);
        function displayDate()
        {
            var ddl = document.getElementById("Profile_designation");
            var selectedText = ddl.options[ddl.selectedIndex].value;

            if (selectedText == 'Staff')
            {
                document.getElementById('Staff').style.display = "block";
                document.getElementById('Student').style.display = "none";
            } else if(selectedText == 'Student')
            {
                document.getElementById('Student').style.display = "block";
                document.getElementById('Staff').style.display = "none";
            
            } else{
                document.getElementById('Student').style.display = "none";
                document.getElementById('Staff').style.display = "none";
            }

        }
    </script>

    <?php
    /* @var $this ProfileController */
    /* @var $model Profile */
    /* @var $form CActiveForm */
    $this->pageTitle = Yii::app()->name . ' - register';
    $this->breadcrumbs = array(
        'register',);
    ?>

    <div class="form">

        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'profile-register-form',
            'enableAjaxValidation' => false,
            'htmlOptions' => array('enctype' => 'multipart/form-data')
                ));
        ?>
        <h1>Register a new account</h1>
        <p class="note">Fields with * are required.</p>


        <div class="row">
            <?php echo $form->labelEx($model, 'user_name'); ?>
            <?php echo $form->textField($model, 'user_name', array('size' => 20, 'maxlength' => 20)); ?>
            <?php echo $form->error($model, 'user_name'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model, 'title'); ?>
            <?php echo $form->DropDownList($model, 'title', $model->getTitle()); ?>
            <?php echo $form->error($model, 'title'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model, 'first_name'); ?>
            <?php echo $form->textField($model, 'first_name', array('size' => 20, 'maxlength' => 20)); ?>
            <?php echo $form->error($model, 'first_name'); ?>
        </div>


        <div class="row">
            <?php echo $form->labelEx($model, 'surname'); ?>
            <?php echo $form->textField($model, 'surname', array('size' => 20, 'maxlength' => 20)); ?>
            <?php echo $form->error($model, 'surname'); ?>
        </div>


        <div class="row">
            <?php echo $form->labelEx($model, 'gender'); ?>
            <?php echo $form->DropDownList($model, 'gender', $model->getSex()); ?>
            <?php echo $form->error($model, 'gender'); ?>
        </div>

        <div class='row'>
            <?php echo $form->labelEx($model, 'profile_picture'); ?>
            <?php echo CHtml::activeFileField($model, 'profile_picture'); ?>
            <?php echo $form->error($model, 'profile_picture'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model, 'contact_number'); ?>
            <?php echo $form->textField($model, 'contact_number', array('size' => 20, 'maxlength' => 20)); ?>
            <?php echo $form->error($model, 'contact_number'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model, 'email'); ?>
            <?php echo $form->textField($model, 'email', array('size' => 60, 'maxlength' => 65)); ?>
            <?php echo $form->error($model, 'email'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model, 'Confirm_Email'); ?>
            <?php echo $form->textField($model, 'email_repeat', array('size' => 60, 'maxlength' => 65)); ?>
            <?php echo $form->error($model, 'email_repeat'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model, 'password'); ?>
            <?php echo $form->passwordField($model, 'password', array('size' => 60, 'maxlength' => 65)); ?>
            <?php echo $form->error($model, 'password'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model, 'password_repeat'); ?>
            <?php echo $form->passwordField($model, 'password_repeat', array('size' => 60, 'maxlength' => 65)); ?>
            <?php echo $form->error($model, 'password_repeat'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model, 'designation'); ?>
            <?php echo $form->DropDownList($model, 'designation', $model->getDesignation(), array('onChange' => 'displayDate()')); ?>

            <?php echo $form->error($model, 'designation'); ?>
        </div>

        <div id="Staff" class="row" style="display:none">

            <div class="row">
                <?php echo $form->labelEx($modelStaff, 'staff_id_number'); ?>
                <?php echo $form->textField($modelStaff, 'staff_id_number'); ?>
                <?php echo $form->error($modelStaff, 'staff_id_number'); ?>
            </div>


            <div class="row">
                <?php echo $form->labelEx($modelPosition, 'position_name'); ?>
                <?php echo $form->textField($modelPosition, 'position_name'); ?>
                <?php echo $form->error($modelPosition, 'position_name'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($modelPosition, 'position_level'); ?>
                <?php echo $form->textField($modelPosition, 'position_level', array('size' => 2, 'maxlength' => 2)); ?>
                <?php echo $form->error($modelPosition, 'position_level'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($modelPosition, 'department_name'); ?>
                <?php echo $form->dropDownlist($modelPosition, 'department_name', $modelPosition->getDepartment()); ?>
                <?php echo $form->error($modelPosition, 'department_name'); ?>
            </div>



        </div>
        <div id="Student" class="row" style="display:none">

            <div class="row">
                <?php echo $form->labelEx($modelReseacher, 'degree_id'); ?>
                <?php echo $form->dropDownlist($modelReseacher, 'degree_id', CHtml::listData(Degree::model()->findAll(), 'id', 'name')); ?>
                <?php echo $form->error($modelReseacher, 'degree_id'); ?>
            </div>


            <div class="row">
                <?php echo $form->labelEx($modelReseacher, 'student_number'); ?>
                <?php echo $form->textField($modelReseacher, 'student_number'); ?>
                <?php echo $form->error($modelReseacher, 'student_number'); ?>

            </div>

        </div>


        <?php if (CCaptcha::checkRequirements() && Yii::app()->user->isGuest): ?>
            <div class="row">
                <?php echo $form->labelEx($model, 'verifyCode'); ?>
                <div>
                    <?php $this->widget('CCaptcha'); ?>
                    <?php echo $form->textField($model, 'verifyCode'); ?>
                </div>
                <div class="hint">Please enter the letters as they are shown in the image above.
                    <br/>Letters are not case-sensitive.</div>
                <?php echo $form->error($model, 'verifyCode'); ?>
            </div>
        <?php endif; ?>


        <p class="tip"> Please note once you have created an account you will be required to login</p>




        <div class="row buttons">
            <?php echo CHtml::submitButton('Register'); ?>
        </div>


        <?php $this->endWidget(); ?>

    </div><!-- form -->
<?php endif; ?>