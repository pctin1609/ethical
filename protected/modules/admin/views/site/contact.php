<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form CActiveForm */

$this->pageTitle = Yii::app()->name . ' - Contact Us';
$this->breadcrumbs = array(
    'Contact',
);
?>

<h1>Contact Us</h1>

<?php if (Yii::app()->user->hasFlash('contact')): ?>

    <div class="flash-success">
        <?php echo Yii::app()->user->getFlash('contact'); ?>
    </div>

<?php endif; ?>

    <p>
        If you have business inquiries or other questions, please fill out the following form to contact us.or alternatively contact us using the following information.
    </p>
       
        <div class="contact form">

            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'contact-form',
                'enableClientValidation' => FALSE,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                ),
            ));
            ?>
            <h2>Online inquiry</h2>
            <p class="note">All fields are mandatory.</p>
<?php         if (Yii::app()->user->getId() === null): ?>


            <div class="row">
                <?php echo $form->labelEx($model, 'name'); ?>
                <?php echo $form->textField($model, 'name'); ?>
                <?php echo $form->error($model, 'name'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model, 'email'); ?>
                <?php echo $form->textField($model, 'email'); ?>
                <?php echo $form->error($model, 'email'); ?>
            </div>
 <?php           endif;?>

            <div class="row">
                <?php echo $form->labelEx($model, 'subject'); ?>
                <?php echo $form->textField($model, 'subject', array('size' => 60, 'maxlength' => 128)); ?>
                <?php echo $form->error($model, 'subject'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model, 'body'); ?>
                <?php echo $form->textArea($model, 'body', array('rows' => 6, 'cols' => 50)); ?>
                <?php echo $form->error($model, 'body'); ?>
            </div>
<?php         if (Yii::app()->user->getId() === null): ?>

            <?php if (CCaptcha::checkRequirements()): ?>
                <div class="row">
                    <?php echo $form->labelEx($model, 'verifyCode'); ?>
                    <div>
                        <?php $this->widget('CCaptcha'); ?>
                        <?php echo $form->textField($model, 'verifyCode'); ?>
                    </div>
                    <div class="hint">Please enter the letters as they are shown in the image above.
                        <br/>Letters are not case-sensitive.</div>
                    <?php echo $form->error($model, 'verifyCode'); ?>
                </div>
            <?php endif; ?>
 <?php endif; ?>


            <div class="row buttons">
                <?php echo CHtml::submitButton('Send Mail'); ?>
            </div>

            <?php $this->endWidget(); ?>

        </div><!-- form -->
         <div class="contact" style="padding-left:2%;margin-left:10px;border-left: 1px dashed black;">
            <h2>Postal address</h2>
            <p>
                Private Bag X60<br />
                Roodepoort 1725<br />
                South Africa
            </p>

            <h2>Street address</h2>
            <p>
                144 Peter Road<br />
                Ruimsig<br />
                Johannesburg<br />
                South Africa
            </p>
            <h2>General inquiries</h2>
            <p>
                <?php $adminEmail = Utils::getConfigValue('ADMIN_EMAIL') ?>
                Email: <a href="mailto:<?php echo $adminEmail ?>"><?php echo $adminEmail ?></a><br />
                Telephone: <strong>+27 11 950 4000</strong>
            </p>
        </div>


