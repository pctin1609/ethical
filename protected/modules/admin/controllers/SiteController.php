<?php

class SiteController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    //public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl'
        );
    }

    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex()
    {

        if (Yii::app()->user->getId() != null)
            $this->redirect(array('site/home'));

        // display the login form
        $this->render('index');
    }

    public function actionAdmin()
    {
        if (Yii::app()->user->getId() === 32) {
            $this->redirect(array('admin/profile/index'));
        }
    }

    public function actionHome()
    {
        if (Yii::app()->user->getId() === null)
            $this->redirect(array('site/login'));

        $model = new Application('search');

        $this->render('home', array('model' => $model));
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', array('error' => $error));
        }
    }

    /**
     * Displays the contact page
     */
    public function actionContact()
    {
        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
                $subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
                $headers = "From: $name <{$model->email}>\r\n" .
                        "Reply-To: {$model->email}\r\n" .
                        "MIME-Version: 1.0\r\n" .
                        "Content-type: text/plain; charset=UTF-8";
                // email is being sent here
                mail(Utils::getConfigValue('ADMIN_EMAIL'), $subject, $model->body, $headers);
                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact', array('model' => $model));
    }

    /**
     * Displays the contact page
     */
    public function actionDevelopment()
    {
        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
                $subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
                $headers = "From: $name <{$model->email}>\r\n" .
                        "Reply-To: {$model->email}\r\n" .
                        "MIME-Version: 1.0\r\n" .
                        "Content-type: text/plain; charset=UTF-8";

                mail(Yii::app()->params['adminEmail'], $subject, $model->body, $headers);
                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('development', array('model' => $model));
    }

    /**
     * Displays the login page
     */
    public function actionLogin()
    {
        if (Yii::app()->user->getId() != null)
            $this->redirect(array('site/home'));
        $model = new LoginForm;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login())
            //$this->render('error', $error);
            //$this->redirect(Yii::app()->user->returnUrl);
               if (user()->isAdmin)
					$this->redirect(array('/admin/profile/index'));
                else $this->redirect(array('site/home'));
        }
        // display the login form
        $this->render('login', array('model' => $model));
    }

    public function actionRegister()
    {
        if (Yii::app()->user->getId() != null)
            $this->redirect(array('site/home'));

        $model = new Profile('register');
        $modelResearcher = new Researcher('register');
        $modelStaff = new Staff('register');
        $modelUser = new User();
        $modelPosition = new Position();

        $validate;

        if (isset($_POST['Profile'])) {
            $rnd = rand(0, 9999); //this will ensure that it has a unique
            $model->attributes = $_POST['Profile'];
            $uploadedFile = CUploadedFile::getInstance($model, 'profile_picture');
            if ($uploadedFile != null) {
                $fileName = "{$rnd}-{$uploadedFile}";
                $model->profile_picture = $fileName;
                $uploadedFile->saveAs(Yii::app()->basePath . '/../images/profilePics/' . $fileName);
            } else {
                $model->profile_picture = 'default.jpg';
            }
            $modelUser->user_id = null;
            $modelUser->user_name = $model->user_name;
            $modelUser->last_login = new CDbExpression('NOW()');
            $modelUser->password = $model->password;

            if ($model->designation == 'Staff') {
                $modelStaff = new Staff();
                $modelStaff->attributes = $_POST['Staff'];
                $modelStaff->available = 1;
                $modelStaff->validated = 0;
                $modelStaff->staff_id = 0;
                $validate = $modelStaff->validate();

                $modelPosition->attributes = $_POST['Position'];
                $modelPosition->position_id = 9;
                $validate = $modelPosition->validate() && $validate;
            } else if ($model->designation == 'Student') {
                $modelResearcher->attributes = $_POST['Researcher'];
                $validate = $modelResearcher->validate();
            }

            $validate = $model->validate() && $validate;

            if ($validate) {
                $this->SaveToModels($model, $modelUser, $modelStaff, $modelResearcher, $modelPosition);
                Yii::app()->user->setFlash('register', 'You have successfully created an acoount may you please proceed to ' . CHtml::link('Log in', array('site/login')));
                $this->refresh();
            }
        }

        $this->render('register', array('model' => $model, 'modelStaff' => $modelStaff, 'modelReseacher' => $modelResearcher, 'modelPosition' => $modelPosition));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'actions' => array(),
                'users' => array('*'),
            ),
            array('allow',
                'actions' => array('home'),
                'users' => array('@'),
            ),
            array('allow',
                'actions' => array(),
                'users' => array('admin'),
            ),
        );
    }

    public function SaveToModels($model, $modelUser, $modelStaff, $modelResearcher, $modelPosition)
    {

        if ($model->designation == 'Student') {
            $model->save(false);
            $modelUser->save();
            $modelResearcher->researcher_id = $modelUser->user_id;
            $modelResearcher->save(false);
        } elseif ($model->designation == 'Staff') {
            $model->save(false);
            $modelUser->save();
            $modelStaff->staff_id = $modelUser->user_id;
            $modelStaff->save(false);
            if ($modelPosition->isPositionExisting($modelPosition) == false) {
                $modelPosition->position_id = null;
                $modelPosition->save(false);
                $occupyModel = new Occupy();
                $occupyModel->position_id = $modelPosition->position_id;
                $occupyModel->staff_id = $modelStaff->staff_id;
                $occupyModel->save(false);
            } else {
                $occupyModel = new Occupy();
                $modelPosition->position_id = null;

                $modelPosition = Position::model()->findByAttributes(array('position_name' => $modelPosition->position_name, 'department_name' => $modelPosition->department_name,
                    'position_level' => $modelPosition->position_level));
                $occupyModel->position_id = $modelPosition->position_id;
                $occupyModel->staff_id = $modelStaff->staff_id;


                $occupyModel->save();
                //$user=User::model()->findByAttributes(array('username'=>$this->username));
            }

            $occupyModel->save();
        }
    }

}