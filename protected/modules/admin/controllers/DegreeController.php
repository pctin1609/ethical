<?php

class DegreeController extends Controller
{
    public function filters() 
    {
        return array(
            'accessControl' // perform access control for CRUD operations
        );
    }

    /**
     * @desc AccessRules
     * @return type 
     */
    public function accessRules() 
    {
        return array(
            array(
                'allow',
                'users' => array('@'),
                'expression' => 'Yii::app()->user->isAdmin'
            ),
            array(
                'deny',
                'users' => array('*')
            ),
        );
    }
    
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $criteria->order = 'created_date DESC';
        
        $dataProvider = new CActiveDataProvider('Degree', array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => param('pageSize'),
            )
        ));
            
        $this->render('index', array('dataProvider' => $dataProvider));
    }
    
    public function actionAdd()
    {
        $model  = new Degree();
        
        if (isset($_POST['Degree'])) {
            $model->attributes = $_POST['Degree'];
            if ($model->save()) {
                app()->user->setFlash('success', 'Degree added successfully!');
                $this->redirect(url('/admin/degree'));
            }
        }
        
        $this->render('add', array('model' => $model));
    }
    
    public function actionUpdate($id)
    {
        $model  = Degree::model()->findByPk($id);
        
        if (is_null($model))
            throw new CHttpException(404, 'Degree was not found.');

        if (isset($_POST['Degree'])) {
            $model->attributes = $_POST['Degree'];
            if ($model->save()) {
                app()->user->setFlash('success', 'Added degree successfully');
                $this->redirect(url('/admin/degree'));
            }
        }
        
        $this->render('update', array('model' => $model));
    }
    
    public function actionDelete($id)
    {
        if (app()->request->isAjaxRequest) {
            $model  = Degree::model()->findByPk($id);
            if ($model && $model->numResearcher == 0) {
                $model->delete();
                app()->end();
            }
        }
    }
}
