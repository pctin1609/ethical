<?php

class ConfigurationController extends Controller
{
    public $configPageRender = 'configuration';
    public $configFields = array();
    public $titleConfigPage = 'General Configurations';
    
    public function filters() 
    {
        return array(
            'accessControl' // perform access control for CRUD operations
        );
    }

    /**
     * @desc AccessRules
     * @return type 
     */
    public function accessRules() 
    {
        return array(
            array(
                'allow',
                'users' => array('@'),
                'expression' => 'Yii::app()->user->isAdmin'
            ),
            array(
                'deny',
                'users' => array('*')
            ),
        );
    }
    
    
    /**
     * @desc action configuration 
     */
    public function actionAdminEmail()
    {
        $this->pageTitle = 'Admin :: Admin Email';
        $this->configFields = array(
            'ADMIN_EMAIL' => array(
                'title' => 'Admin Email',
                'type' => 'text',
                'default' => '',
                'desc' => '',
            ),
        );
        $this->titleConfigPage = 'Admin Email';

        $this->doProccessConfigPage();
    }

    /**
     * @desc update configuration 
     */
    public function doProccessConfigPage()
    {
        $models = array();
        foreach ($this->configFields as $key => $configParams) {
            $model = Configuration::model()->findByAttributes(array('name' => $key));
            if (!$model) {
                $model = new Configuration;
                $model->name = $key;
                $model->value = isset($configParams['default']) ? $configParams['default'] : '';
            }

            $model->scenario = isset($configParams['scenario']) ? $configParams['scenario'] : '';
            $models[] = $model;
        }

        if (isset($_POST['Configuration'])) {
            $valid = true;
            foreach ($models as $i => $model) {
                if (isset($_POST['Configuration'][$i]))
                    $model->attributes = $_POST['Configuration'][$i];
                $valid = $model->validate() && $valid;
            }

            if ($valid) {
                foreach ($models as $i => $model) {
                    $model->attributes = $_POST['Configuration'][$i];
                    $model->save(false);
                }

                user()->setFlash('success', 'Email changed successfully!');
            }
        }

        $this->render($this->configPageRender, array(
            'models' => $models,
            'configFields' => $this->configFields,
            'titleConfigPage' => $this->titleConfigPage,
        ));
    }

}