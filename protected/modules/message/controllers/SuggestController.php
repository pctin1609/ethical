<?php

class SuggestController extends Controller {

    public function filters()
        {
                return CMap::mergeArray(parent::filters(),array(
                        'accessControl', // perform access control for CRUD operations
                ));
        }
        /**
         * Specifies the access control rules.
         * This method is used by the 'accessControl' filter.
         * @return array access control rules
         */
        public function accessRules()
        {
                return array(
                        array('allow',  // allow all users to perform 'index' and 'view' actions
                                'actions'=>array('user'),
                                'users'=>array('@'),
                        ),
                        array('deny',  // deny all users
                                'users'=>array('*'),
                        ),
                );
        }

	
    public function actionUser() {
                $res =array();

                if (isset($_GET['term'])) {
                        $q = Yii::app()->request->getParam('term');
                        $userModels = (array) call_user_func(array(
                                CActiveRecord::model(Yii::app()->getModule('message')->userModel),
                                Yii::app()->getModule('message')->getSuggestMethod
                        ), $q);                        
                        
                        if ($userModels) {
                            foreach ($userModels as $userModel) {
                                $fullName=call_user_func(array(
                                        $userModel, $this->getModule()->getNameMethod
                                ));
                                $users[] = array(
                                    'label' => $fullName,
                                    'value' => $fullName,
                                    'id' => $userModel->getPrimaryKey(),
                                );
                            }
                        }
                }

                header('Cache-Control: no-store');
                header('Pragma: no-store');
                header("Content-type: application/json");
                echo CJSON::encode($users);
                Yii::app()->end();
        }

}
