<?php
/* @var $this Fm1_Section7Controller */
/* @var $model Fm1_Section7 */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'fm1--section7-section7-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo " <h2>Section 7 - Feedback and debriefing procedures</h2>" ?>

<p class="note">All fields are mandatory.</p>
	<div class="row">
		<?php echo $form->labelEx($model,'publish_form'); ?>
		<?php echo $form->dropdownlist($model,'publish_form',$model->getFormTypes()); ?>
		<?php echo $form->error($model,'publish_form'); ?>
	</div></br>

	<div class="row">
		<?php echo $form->labelEx($model,'communication_form'); ?>
		<?php echo $form->dropdownlist($model,'communication_form' ,$model->getFormResults()); ?>
		<?php echo $form->error($model,'communication_form'); ?>
	</div></br>

	<div class="row">
		<?php echo $form->labelEx($model,'participants_results'); ?>
		<?php echo $form->dropdownlist($model,'participants_results',$model->getParticipantsResults()); ?>
		<?php echo $form->error($model,'participants_results'); ?>
	</div></br>

	<div class="row">
		<?php echo $form->labelEx($model,'results_provided'); ?>
		<div class = 'radiobuttons'>
		<?php echo $form->radiobuttonlist($model,'results_provided',$model->getValues()); ?>
		</div>
		<?php echo $form->error($model,'results_provided'); ?>
	</div></br>

	<div class="row">
		<?php echo $form->labelEx($model,'results_provided_others'); ?>
		<?php echo $form->dropdownlist($model,'results_provided_others',$model->getOthersResults()); ?>
		<?php echo $form->error($model,'results_provided_others'); ?>
	</div></br>

	<div class="row">
		<?php echo $form->labelEx($model,'debriefing_form_required'); ?>
		<?php echo $form->textArea($model,'debriefing_form_required',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'debriefing_form_required'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'results_infor_communication'); ?>
		<?php echo $form->textarea($model,'results_infor_communication',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'results_infor_communication'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'collectivity_community_research'); ?>
		<?php echo $form->textarea($model,'collectivity_community_research',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'collectivity_community_research'); ?>
	</div>

    <div class="row buttons">
		<?php echo CHtml::submitButton('Previous',array('name'=>'previous')); ?>
        <?php echo CHtml::submitButton('Save',array('name'=>'save')); ?>
        <?php echo CHtml::submitButton('Next'); ?>
	</div>
<?php $this->endWidget(); ?>

</div><!-- form -->