<?php
/* @var $this Fm1_section6dController */
/* @var $model Fm1_section6d */
/* @var $form CActiveForm */
?>

<div class="form">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'fm1-section6d-Section6d-form',
        'enableAjaxValidation' => false,
            ));
    ?>

    <p class="note"></p>
    <?php echo " <h2>Section 6d - General Issues</h2>" ?>

    <h7>6.11: General Issues</h7><br>
	<h7>6.11a How many records will be collected, used or disclosed? Specify the information that will be collected, used or disclosed (e.g. date of birth, medical history, number of convictions, etc) Complete this question only if you are seeking privacy exemption from this HREC</h7>
	<div class="row">
        <?php echo $form->labelEx($model, 'number_of_records'); ?>
        <?php echo $form->textArea($model, 'number_of_records', array('size' => '100%', 'maxlength' => 250, 'style' => 'width: 100%')); ?>
        <?php echo $form->error($model, 'number_of_records'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'type_of_records'); ?>
        <?php echo $form->textArea($model, 'type_of_records', array('size' => '100%', 'maxlength' => 250, 'style' => 'width: 100%')); ?>
        <?php echo $form->error($model, 'type_of_records'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'adoption_unique_identifiers'); ?>
        <div class='radiobuttons'>
            <?php echo $form->radioButtonList($model, 'adoption_unique_identifiers', $model->getValues(), array('onchange' => 'menuTypeChange(this.value,"uni",vis="0");')); ?>
        </div>
        <?php echo $form->error($model, 'adoption_unique_identifiers'); ?>
    </div>

    <div class="row" id="uni" style="display: none">
        <?php echo $form->labelEx($model, 'details_of_unique_identifiers'); ?>
        <?php echo $form->textArea($model, 'details_of_unique_identifiers', array('size' => '100%', 'maxlength' => 250, 'style' => 'width: 100%')); ?>
        <?php echo $form->error($model, 'details_of_unique_identifiers'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'involve_transborder'); ?>
        <div class='radiobuttons'>
            <?php echo $form->radioButtonList($model, 'involve_transborder', $model->getValues(), array('onchange' => 'menuTypeChange(this.value,"involve",vis="0");')); ?>
        </div>
        <?php echo $form->error($model, 'involve_transborder'); ?>
    </div>

    <div class="row" id="involve" style="display: none">
        <?php echo $form->labelEx($model, 'details_of_transborder'); ?>
        <?php echo $form->textArea($model, 'details_of_transborder', array('size' => '100%', 'maxlength' => 250, 'style' => 'width: 100%')); ?>
        <?php echo $form->error($model, 'details_of_transborder'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'period_of_time'); ?>
        <?php echo $form->textArea($model, 'period_of_time', array('size' => '100%', 'maxlength' => 250, 'style' => 'width: 100%')); ?>
        <?php echo $form->error($model, 'period_of_time'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'security_arrangements'); ?>
        <?php echo $form->textArea($model, 'security_arrangements', array('size' => '100%', 'maxlength' => 250, 'style' => 'width: 100%')); ?>
        <?php echo $form->error($model, 'security_arrangements'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'privacy_of_individuals'); ?>
        <?php echo $form->textArea($model, 'privacy_of_individuals', array('size' => '100%', 'maxlength' => 250, 'style' => 'width: 100%')); ?>
        <?php echo $form->error($model, 'privacy_of_individuals'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'procedures_in_place'); ?>
        <?php echo $form->textArea($model, 'procedures_in_place', array('size' => '100%', 'maxlength' => 250, 'style' => 'width: 100%')); ?>
        <?php echo $form->error($model, 'procedures_in_place'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'ethical_issues'); ?>
        <?php echo $form->textArea($model, 'ethical_issues', array('size' => '100%', 'maxlength' => 250, 'style' => 'width: 100%')); ?>
        <?php echo $form->error($model, 'ethical_issues'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Previous', array('name' => 'previous')); ?>
        <?php echo CHtml::submitButton('Save', array('name' => 'save')); ?>
        <?php echo CHtml::submitButton('Next'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->