<script>
        window.addEventListener('load',displayAge);
        function displayAge()
        {
            var ddl = document.getElementById("ResearchGroup_age_range");
            var selectedText = ddl.options[ddl.selectedIndex].value;

            if (selectedText == 'Other')
            {
                document.getElementById('Other').style.display = "block";
                document.getElementById('1- 10').style.display = "none";
            } else if(selectedText == '1- 10')
            {
                document.getElementById('Other').style.display = "block";
                document.getElementById('1 - 10').style.display = "none";
            
            } else{
                document.getElementById('Other').style.display = "none";
                document.getElementById('1 - 10').style.display = "none";
            }

        }
    </script>
<?php
/* @var $this form1_section_2Controller */
/* @var $model form1_section_2 */
/* @var $form CActiveForm */
?>

<div class="form">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'fm1-section2-section2-form',
        'enableAjaxValidation' => false,
            ));
    ?>
    <?php echo " <h2>Section 2 - Details about the participants of the proposed research project </h2>" ?>
     <p class="note">Fields with * are required</p>
    <div class="row">
        <?php echo $form->labelEx($model, 'human_participants_involved'); ?>
        <div class='radiobuttons'>
            <?php echo $form->radioButtonList($model, 'human_participants_involved', $model->getValues()); ?>
        </div>
        <?php echo $form->error($model, 'human_participants_involved'); ?>
    </div> 

    <h7> If NO:</h7>
    <div class="checkboxes">
        <?php echo $form->checkBox($model, 'using_previous_datasets', array('value' => 1, 'uncheckValue' => 0)); ?>
        <?php echo $form->labelEx($model, 'using_previous_datasets'); ?>
    </div>
    <?php echo $form->error($model, 'using_previous_datasets'); ?>

    <div class="checkboxes">
        <?php echo $form->checkBox($model, 'previous_ethical_approved', array('value' => 1, 'uncheckValue' => 0)); ?>
        <?php echo $form->labelEx($model, 'previous_ethical_approved'); ?>
    </div>
    <?php echo $form->error($model, 'previous_ethical_approved'); ?>

    <div class="checkboxes" >
        <?php echo $form->checkBox($model, 'using_HT_HSC', array('value' => 1, 'uncheckValue' => 0)); ?>             
        <?php echo $form->labelEx($model, ' using_HT_HSC '); ?>
    </div>                        
    <?php echo $form->error($model, 'using_HT_HSC'); ?> <br>
	
 <h7> 2.2	Please describe the participants (in groups) involved in your research project</h7>
    
	<div class="row">
        <?php echo $form->labelEx($model, 'number_of_people'); ?>
        <?php echo $form->textArea($model, 'number_of_people', array('size' => '100%', 'maxlength' => 250, 'style' => 'width: 100%')); ?>
        <?php echo $form->error($model, 'number_of_people'); ?>    
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'group_desc'); ?>
        <?php echo $form->textArea($model, 'group_desc', array('size' => '100%', 'maxlength' => 250, 'style' => 'width: 100%')); ?>
        <?php echo $form->error($model, 'group_desc'); ?>
    </div>	

    <div class="row">
        <?php echo $form->labelEx($model, 'age_range'); ?>
        <?php echo $form->dropDownList($model, 'age_range', $model->getInformation(),array('onchange'=>'displayAge()'));?> 
        <?php echo $form->error($model, 'age_range'); ?>
        
    </div>
 
 <div id="Other" class="row" style="display:none">

           <div class="row" >
        <?php echo $form->labelEx($model, 'age_range2'); ?>
        <?php echo $form->textArea($model, 'age_range2', array('size' => '100%', 'maxlength' => 250, 'style' => 'width: 100%')); ?>
        <?php echo $form->error($model, 'age_range2'); ?>
          
           </div>

  </div>

    <div class="row" >
        <?php echo $form->labelEx($model, 'include_exclude_status'); ?>
        <div class='radiobuttons'>
            <?php echo $form->radioButtonList($model, 'include_exclude_status', $model->getValues(), array('onchange' => 'menuTypeChange(this.value,"include",vis="0");')); ?>
        </div>
        <?php echo $form->error($model, 'include_exclude_status'); ?>
    </div>

    <div class="row" id="include" style="display: none">
        <?php echo $form->labelEx($model, 'exclusion_desc'); ?><br>
        <?php echo $form->textArea($model, 'exclusion_desc', array('size' => '100%', 'maxlength' => 250, 'style' => 'width: 100%')); ?>
        <?php echo $form->error($model, 'exclusion_desc'); ?>
    </div>	

    <div class="row">
        <?php echo $form->labelEx($model, 'criteria_procedure'); ?>
        <?php echo $form->textArea($model, 'criteria_procedure', array('size' => '100%', 'maxlength' => 250, 'style' => 'width: 100%')); ?>
        <?php echo $form->error($model, 'criteria_procedure'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'work_time'); ?>
        <?php echo $form->textArea($model, 'work_time', array('size' => '100%', 'maxlength' => 250, 'style' => 'width: 100%')); ?>
        <?php echo $form->error($model, 'work_time'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model, 'incentive'); ?>
        <div class='radiobuttons'>
            <?php echo $form->radioButtonList($model, 'incentive', $model->getValues(), array('onchange' => 'menuTypeChange(this.value,"incent",vis="0");')); ?>
        </div>
        <?php echo $form->error($model, 'incentive'); ?>
    </div>
    
    <div class="row" id ="incent" style="display: none">
        <?php echo $form->labelEx($model, 'reimbursement_desc'); ?><br>
        <?php echo $form->textArea($model, 'reimbursement_desc', array('size' => '100%', 'maxlength' => 250, 'style' => 'width: 100%')); ?>
        <?php echo $form->error($model, 'reimbursement or incentive description'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'using_UPSPP'); ?>
        <div class='radiobuttons'>
            <?php echo $form->radioButtonList($model, 'using_UPSPP', $model->getValues(), array('onchange' => 'menuTypeChange(this.value,"supporting",vis="0");')); ?>
        </div>
        <?php echo $form->error($model, 'using_UPSPP'); ?>
    </div>

    <div class="row" id= "supporting" style="display: none">				
        <?php echo $form->labelEx($model, 'supporting_paragraph_for_using_UPSPP'); ?>
        <?php echo $form->textArea($model, 'supporting_paragraph_for_using_UPSPP', array('rows' => 3, 'cols' => 100)); ?>
        <?php echo $form->error($model, 'Supporting paragraph for using UPSPP'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'using_monash_MBBS'); ?>
        <div class='radiobuttons'>
            <?php echo $form->radioButtonList($model, 'using_monash_MBBS', $model->getValues(), array('onchange' => 'menuTypeChange(this.value,"monash",vis="0");')); ?>
        </div>
        <?php echo $form->error($model, 'using_monash_MBBS'); ?>
    </div>       

    <div class="row" id ="monash" style="display: none">
        <?php echo $form->labelEx($model, 'MBBS_attatchment'); ?><br>
        <?php echo $form->fileField($model, 'MBBS_attatchment', array('size' => '100%', 'maxlength' => 250, 'style' => 'width: 100%')); ?>
        <?php echo $form->error($model, 'Monash MBBS attachment'); ?>
    </div>

    <?php echo $form->labelEx($model, 'selection_method'); ?>
    <?php echo $form->textArea($model, 'selection_method', array('size' => '100%', 'maxlength' => 250, 'style' => 'width: 100%')); ?>
    <?php echo $form->error($model, 'selection_method'); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'participant_recruitment_detail'); ?>
        <?php echo $form->textArea($model, 'participant_recruitment_detail', array('size' => '100%', 'maxlength' => 250, 'style' => 'width: 100%')); ?>					
        <?php echo $form->error($model, 'participant_recruitment_detail'); ?>          
    </div> <br>

	<h7>2.6e Please explain in details how you will obtain the contact details of participants </h7><br>
    <h7>If form a public domain source-please identify the source.</h7><br>
	 <h7>If from a previously approved database-please confirm that consent has been obtained;</h7>
	<div class="row">
        <?php echo $form->labelEx($model, 'method_of_obtaining_participants_contacts'); ?>
        <?php echo $form->textArea($model, 'method_of_obtaining_participants_contacts', array('size' => '100%', 'maxlength' => 250, 'style' => 'width: 100%')); ?>
        <?php echo $form->error($model, 'method of obtaining participants contacts description'); ?>
    </div>





    <div class="row buttons">
        <?php echo CHtml::submitButton('Previous', array('name' => 'previous')); ?>
        <?php echo CHtml::submitButton('Save', array('name' => 'save')); ?>
        <?php echo CHtml::submitButton('Next'); ?>

    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->