<?php
/* @var $this RiskManagementPlanController */
/* @var $model RiskManagementPlan */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'risk-management-plan-Section9-form',
	'enableAjaxValidation'=>false,
)); ?>
    <?php echo " <h2>Section 9 - Risk Assessment and Risk Management Plan</h2>" ?>
	<p class="note"></p>

	<div class="row">
		<?php echo $form->labelEx($model,'assessment'); ?>
		<?php echo $form->dropDownList($model,'assessment',$model->getRiskAssessment()); ?>
        <?php echo $form->error($model,'assessment'); ?>
	</div>

    <div class="row buttons">
		<?php echo CHtml::submitButton('Previous',array('name'=>'previous')); ?>
        <?php echo CHtml::submitButton('Submit Form'); ?>
	</div>
<?php $this->endWidget(); ?>

</div><!-- form -->