<?php
//
/* @var $this ApplicationController */

$this->breadcrumbs = array(
    'ApplicationForm1',
);
?>
<script>
    function showHide() {
        var ele = document.getElementById("showHideDiv");
        if(ele.style.display == "block") {
            ele.style.display = "none";
        }
        else {
            ele.style.display = "block";
        }
    }
    
</script>
<p></p>
<div id="formlist">
    <p><input type="button" value="Form 1" style="width:100%;" onclick="return showHide();" /></p>

    <div id="showHideDiv" class = "show">
        <strong>Instructions to complete Form 1 </strong>
        <p><strong>Complete this form if your research involves the following:</strong></p>
        <ul>
            <li>Interventions & Therapies </li>
            <li>Human genetics & or stem cells </li>
            <li>Women who are pregnant & the Human foetus </li>
            <li>People highly dependent on medical care who may be unable to give consent</li>
            <li>People with a cognitive impairment,an intellectual disability, or mentall illness</li>
            <li>Aboriginal & Torres Straut Islanders</li>
            <li>Use of collected human data for a new purpose </li>    
        </ul><br/>
        <p class="form_link"><?php echo CHtml::link('Proceed >>', array('applicationForm1/selectSupervisor&id=0')); ?></p>
    </div>
</div>

