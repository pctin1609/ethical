<?php
/* @var $this DetailsOfProjectController */
/* @var $model DetailsOfProject */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'details-of-project-detailsOfProject-form',
	'enableAjaxValidation'=>false,
)); ?>

	 <?php echo " <h2>Section1</h2>" ?>
	<p class="note">Fields with * are required.</p>

	<div class="row">
		<?php echo $form->labelEx($model,'aim_research_project'); ?>
		<?php echo $form->textArea($model,'aim_research_project',array('size' => '100%', 'maxlength' => 250, 'style' => 'width: 100%')); ?>
		<?php echo $form->error($model,'aim_research_project'); ?>
		
	</div>

	<div class="row">
           <?php echo $form->labelEx($model,'details_of_procedures'); ?>
		<?php echo $form->textArea($model,'details_of_procedures',array('size' => '100%', 'maxlength' => 250, 'style' => 'width: 100%')); ?>
		<?php echo $form->error($model,'details_of_procedures'); ?>
		
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'research_ethics_committee'); ?>
		<?php echo $form->textArea($model,'research_ethics_committee', array('size' => '100%', 'maxlength' => 250, 'style' => 'width: 100%')); ?>
		<?php echo $form->error($model,'research_ethics_committee'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'primary_HREC'); ?>
		<div class= 'radiobuttons'>
		<?php echo $form->radioButtonList($model,'primary_HREC',$model->getValues());?>
		</div>
		
		<?php echo $form->error($model,'primary_HREC'); ?>
	</div>

	

	
	
	<div class="row">
		<?php echo $form->labelEx($model,'attach_letter_of_approval'); ?>
		<?php echo $form->fileField($model,'attach_letter_of_approval',array('rows'=>3,'cols'=>100)); ?>
		<?php echo $form->error($model,'attach_letter_of_approval'); ?>
	</div>

	<h7>3 If the project has been approved at multiple sites, list other HRECs where you have submitted a human ethics application.<br> </br> PLEASE NOTE (If your research involves Southern Health, Peninsula Health, Alfred Health, Cabrini Health or the Department of Justice (Vic) stop 
			completing this form and see our website for the appropriate expedited process.</h7> 
	
	<div class="row">
		<?php echo $form->labelEx($model,'HREC1'); ?>
		<?php echo $form->textArea($model,'HREC1', array('size' => '100%', 'maxlength' => 250, 'style' => 'width: 100%')); ?>
		<?php echo $form->error($model,'HREC1'); ?>
	</div>
<h7> Is approval granted and letter attached?<br>
Place x in appropriate box
</h7>
	<div class="checkboxes">
		<?php echo $form->checkBox($model,'HREC1_yes',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'HREC1_yes'); ?>
		<?php echo $form->error($model,'HREC1_yes'); ?>
	</div>
  <div class="checkboxes">
		<?php echo $form->checkBox($model,'HREC1_no',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'HREC1_no'); ?>
		<?php echo $form->error($model,'HREC1_no'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'HREC1_pending',array('value'=>1, 'uncheckValue'=>0) ); ?>
		<?php echo $form->labelEx($model,'HREC1_pending'); ?>
		<?php echo $form->error($model,'HREC1_pending'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'HREC2'); ?>
		<?php echo $form->textArea($model,'HREC2', array('size' => '100%', 'maxlength' => 250, 'style' => 'width: 100%')); ?>
		<?php echo $form->error($model,'HREC2'); ?>
	</div>
<h7> Is approval granted and letter attached?<br>
Place x in appropriate box
</h7>
	<div class="checkboxes">
		<?php echo $form->checkBox($model,'HREC2_yes',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'HREC2_yes'); ?>
		<?php echo $form->error($model,'HREC2_yes'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'HREC2_no',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'HREC2_no'); ?>
		<?php echo $form->error($model,'HREC2_no'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'HREC2_pending',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'HREC2_pending'); ?>
		<?php echo $form->error($model,'HREC2_pending'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'HREC3'); ?>
		<?php echo $form->textArea($model,'HREC3',array('size' => '100%', 'maxlength' => 250, 'style' => 'width: 100%')); ?>
		<?php echo $form->error($model,'HREC3'); ?>
	</div>
<h7> Is approval granted and letter attached?<br>
Place x in appropriate box
</h7>
	<div class="checkboxes">
		<?php echo $form->checkBox($model,'HREC3_yes',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'HREC3_yes'); ?>
		<?php echo $form->error($model,'HREC3_yes'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'HREC3_no',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'HREC3_no'); ?>
		<?php echo $form->error($model,'HREC3_no'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'HREC3_pending',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'HREC3_pending'); ?>
		<?php echo $form->error($model,'HREC3_pending'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'HREC4'); ?>
		<?php echo $form->textArea($model,'HREC4', array('size' => '100%', 'maxlength' => 250, 'style' => 'width: 100%')); ?>
		<?php echo $form->error($model,'HREC4'); ?>
	</div>
<h7> Is approval granted and letter attached?<br>
Place x in appropriate box
</h7>
	<div class="checkboxes">
		<?php echo $form->checkBox($model,'HREC4_yes',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'HREC4_yes'); ?>
		<?php echo $form->error($model,'HREC4_yes'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'HREC4_no',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'HREC4_no'); ?>
		<?php echo $form->error($model,'HREC4_no'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'HREC4_pending',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'HREC4_pending'); ?>
		<?php echo $form->error($model,'HREC4_pending'); ?>
	</div>

	


	<div class="row buttons">
		<?php echo CHtml::submitButton('Save', array('name' => 'save')); ?>
        <?php echo CHtml::submitButton('Next'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->