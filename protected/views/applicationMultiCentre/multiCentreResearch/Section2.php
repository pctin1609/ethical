<?php
/* @var $this PotentiallyVulnerableParticipantsController */
/* @var $model PotentiallyVulnerableParticipants */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'potentially-vulnerable-participants-potentiallyVulnerableParticipants-form',
	'enableAjaxValidation'=>false,
)); ?>
		 <?php echo " <h2>Section2 </h2>" ?>
	
<h7> Please identify if you are using potentially vulnerable participants as listed below.</h7><br>
 </h7>Please identify which group </h7><br>
	
<div class="row">
		<?php echo $form->labelEx($model,'women_pregnant'); ?>
		<div class='radiobuttons'>
		<?php echo $form->radioButtonList($model,'women_pregnant',$model->getValues()); ?>
		 </div>
		<?php echo $form->error($model,'women_pregnant'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'children_young'); ?>
		<div class='radiobuttons'>
		<?php echo $form->radioButtonList($model,'children_young',$model->getValues()); ?>
		</div>
		<?php echo $form->error($model,'children_young'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'intellectual_disability'); ?>
		<div class='radiobuttons'>
		<?php echo $form->radioButtonList($model,'intellectual_disability',$model->getValues()); ?>
		</div>
		<?php echo $form->error($model,'intellectual_disability'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'forensic_patient'); ?>
		<div class='radiobuttons'>
		<?php echo $form->radioButtonList($model,'forensic_patient',$model->getValues()); ?>
		</div>
		<?php echo $form->error($model,'forensic_patient'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'impaired_capacity'); ?>
			<div class='radiobuttons'>
		<?php echo $form->radioButtonList($model,'impaired_capacity',$model->getValues()); ?>
		</div>
		<?php echo $form->error($model,'impaired_capacity'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'prisoners_parole'); ?>
		<div class='radiobuttons'>
		<?php echo $form->radioButtonList($model,'prisoners_parole' ,$model->getValues()); ?>
		</div>
		<?php echo $form->error($model,'prisoners_parole'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'children_wards_of_the_state'); ?>
		<div class='radiobuttons'>
		<?php echo $form->radioButtonList($model,'children_wards_of_the_state',$model->getValues()); ?>
		</div>
		<?php echo $form->error($model,'children_wards_of_the_state'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dependent_on_medical_care'); ?>
		<div class='radiobuttons'>
		<?php echo $form->radioButtonList($model,'dependent_on_medical_care',$model->getValues()); ?>
		</div>
		<?php echo $form->error($model,'dependent_on_medical_care'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'examining_potentially'); ?>
		<div class='radiobuttons'>
		<?php echo $form->radioButtonList($model,'examining_potentially',$model->getValues()); ?>
		</div>
		<?php echo $form->error($model,'examining_potentially'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'seeking_disclosure'); ?>
		<div class='radiobuttons'>
		<?php echo $form->radioButtonList($model,'seeking_disclosure',$model->getValues()); ?>
		</div>
		<?php echo $form->error($model,'seeking_disclosure'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'participation_in_illegal_activities'); ?>
		<div class='radiobuttons'>
		<?php echo $form->radioButtonList($model,'participation_in_illegal_activities',$model->getValues()); ?>
		</div>
		<?php echo $form->error($model,'participation_in_illegal_activities'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'other_vulnerable_participants'); ?>
		<div class='radiobuttons'>
		<?php echo $form->radioButtonList($model,'other_vulnerable_participants',$model->getValues(), array('onchange' => 'menuTypeChange(this.value,"vulnerable",vis="0");')); ?>
		</div>
		<?php echo $form->error($model,'other_vulnerable_participants'); ?>
	</div>

	<div class="row" id="vulnerable" style="display: none">
		<?php echo $form->labelEx($model,'if_yes_other_specify'); ?>
		
		<?php echo $form->textArea($model,'if_yes_other_specify',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 70%')); ?>
		
		<?php echo $form->error($model,'if_yes_other_specify'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton('Previous', array('name' => 'previous')); ?>
        <?php echo CHtml::submitButton('Save', array('name' => 'save')); ?>
        <?php echo CHtml::submitButton('Next'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->