<?php
/* @var $this DataLocationController */
/* @var $model DataLocation */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'data-location-dataLocation-form',
	'enableAjaxValidation'=>false,
)); ?>
 <?php echo " <h2>Section3 </h2>" ?>
	
    <div class="row">
		<?php echo $form->labelEx($model,'location_other_than_Australia'); ?>
		<div class='radiobuttons'>
		<?php echo $form->radioButtonList($model,'location_other_than_Australia',$model->getValues(), array('onchange' => 'menuTypeChange(this.value,"location",vis="0");')); ?>
		 </div>
		<?php echo $form->error($model,'location_other_than_Australia'); ?>
	</div>
    
    <div class="row"id="location" style="display: none">
		<?php echo $form->labelEx($model,'if_yes_location_explain'); ?>
		<?php echo $form->textArea($model,'if_yes_location_explain',array('size' => '100%', 'maxlength' => 250, 'style' => 'width: 100%')); ?>
		<?php echo $form->error($model,'if_yes_location_explain'); ?>
	</div>
	<h7>6 Funding of your research project - please complete the following: </h7> 
	<div class="row" >
		<?php echo $form->labelEx($model,'name_of_organization_agency'); ?>
		<?php echo $form->textArea($model,'name_of_organization_agency', array('size' => '100%', 'maxlength' => 250, 'style' => 'width: 100%')); ?>
		<?php echo $form->error($model,'name_of_organization_agency'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'award'); ?>
		<?php echo $form->textArea($model,'award', array('size' => '100%', 'maxlength' => 250, 'style' => 'width: 100%')); ?>
		<?php echo $form->error($model,'award'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'name_of_institution'); ?>
		<?php echo $form->textArea($model,'name_of_institution', array('size' => '100%', 'maxlength' => 250, 'style' => 'width: 100%')); ?>
		<?php echo $form->error($model,'name_of_institution'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'role_of_monash'); ?>
		<?php echo $form->textArea($model,'role_of_monash',array('size' => '100%', 'maxlength' => 250, 'style' => 'width: 100%')); ?>
		<?php echo $form->error($model,'role_of_monash'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'monash_staff'); ?>
		<?php echo $form->textArea($model,'monash_staff', array('size' => '100%', 'maxlength' => 250, 'style' => 'width: 100%')); ?>
		<?php echo $form->error($model,'monash_staff'); ?>
	</div>
<h7>11 Please describe the participants involved in your research project </h7><br> 
	<div class="row">
		<?php echo $form->labelEx($model,'how_many_people'); ?>
		<?php echo $form->textArea($model,'how_many_people', array('size' => '100%', 'maxlength' => 4, 'style' => 'width: 100%')); ?>
		<?php echo $form->error($model,'how_many_people'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'groups_involved'); ?>
		<?php echo $form->textArea($model,'groups_involved', array('size' => '100%', 'maxlength' => 250, 'style' => 'width: 100%')); ?>
		<?php echo $form->error($model,'groups_involved'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'age_range'); ?>
		<?php echo $form->dropDownList($model,'age_range',$model->getInfor()); ?>
		<?php echo $form->error($model,'age_range'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'recruitment_of_participants'); ?>
		<?php echo $form->textArea($model,'recruitment_of_participants', array('size' => '100%', 'maxlength' => 250, 'style' => 'width: 100%')); ?>
		<?php echo $form->error($model,'recruitment_of_participants'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'reimbursement_to_participants'); ?>
		<div class='radiobuttons'>
		<?php echo $form->radioButtonList($model,'reimbursement_to_participants',$model->getValues(), array('onchange' => 'menuTypeChange(this.value,"part",vis="0");')); ?>
		 </div>
		<?php echo $form->error($model,'reimbursement_to_participants'); ?>
	</div>
 	
	<div class="row" id="part" style="display: none">
		<?php echo $form->labelEx($model,'if_yes_how_much'); ?>
		<?php echo $form->textArea($model,'if_yes_how_much',array('size' => '100%', 'maxlength' => 250, 'style' => 'width: 100%')); ?>
		<?php echo $form->error($model,'if_yes_how_much'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'dependent_or_unequal_relationship'); ?>
		<div class='radiobuttons'>
		<?php echo $form->radioButtonList($model,'dependent_or_unequal_relationship' ,$model->getValues(), array('onchange' => 'menuTypeChange(this.value,"unequal",vis="0");')); ?>
		 </div>
		<?php echo $form->error($model,'dependent_or_unequal_relationship'); ?>
	</div>

	<div class="row" id="unequal" style="display: none">
		<?php echo $form->labelEx($model,'if_yes_relationship'); ?>
		<?php echo $form->textArea($model,'if_yes_relationship' ,array('size' => '100%', 'maxlength' => 250, 'style' => 'width: 100%')); ?>
		<?php echo $form->error($model,'if_yes_relationship'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'consent_form_attached'); ?>
		<div class='radiobuttons'>
		<?php echo $form->radioButtonList($model,'consent_form_attached',$model->getValues(), array('onchange' => 'menuTypeChange(this.value,"attach",vis="0");')); ?>
        </div>
		<?php echo $form->error($model,'consent_form_attached'); ?>
	</div>

	<div class="row" id="attach" style="display: none">
		<?php echo $form->labelEx($model,'if_yes_please_attach'); ?>
		<?php echo $form->fileField($model,'if_yes_please_attach',array('size' => '100%', 'maxlength' => 250, 'style' => 'width: 100%')); ?>
		<?php echo $form->error($model,'if_yes_please_attach'); ?>
	</div>
   
	<div class="row">
		<?php echo $form->labelEx($model,'involve_the_collection'); ?>
		<div class='radiobuttons'>
		<?php echo $form->radioButtonList($model,'involve_the_collection',$model->getValues() ); ?>
		</div>
		<?php echo $form->error($model,'involve_the_collection'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'arrangements_in_place'); ?>
		<?php echo $form->textArea($model,'arrangements_in_place',array('size' => '100%', 'maxlength' => 250, 'style' => 'width: 100%')); ?>
		<?php echo $form->error($model,'arrangements_in_place'); ?>
	</div>

	<div class="row buttons">
        <?php echo CHtml::submitButton('Previous', array('name' => 'previous')); ?>
        
		<?php echo CHtml::submitButton('Submit'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->