<?php
/* @var $this fm1_section1Controller */
/* @var $model fm1_section1 */
/* @var $form CActiveForm */
?>

<script language="javascript">
    function menuTypeChange(value, div,vis)
    {
        if (value === vis) {
            document.getElementById(div).style.display = 'block';
        } else
        {
            document.getElementById(div).style.display = 'none';
            $('#' + div).find('textArea').val('');
        }
    }
</script>

<?php
echo '<h1>' . "MultiCentre Research" . '</h1>';

echo $event->sender->menu->run();
echo 'Step ' . $event->sender->currentStep . ' of ' . $event->sender->stepCount;
?>
<?php if (Yii::app()->user->hasFlash('save')): ?>

    <div class="flash-success">
        <?php echo Yii::app()->user->getFlash('save'); ?>
    </div>

<?php endif ?>
<?php
echo CHtml::tag('div', array('class' => 'form'), $this->renderPartial('multiCentreResearch/' . $sectionName, array('model' => $model, 'event' => $event)));
?>

