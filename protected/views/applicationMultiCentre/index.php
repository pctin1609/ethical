<?php
/* @var $this ApplicationMultiCentreController */

$this->breadcrumbs = array(
    'Application Multi Centre',
);
?>
<script>
    function showHide() {
        var ele = document.getElementById("showHideDiv");
        if(ele.style.display == "block") {
            ele.style.display = "none";
        }
        else {
            ele.style.display = "block";
        }
    }

</script>
<p></p>
<p></p>
<div id="formlist">
    <p><input type="button" value="MultiCentre Research" style="width:100%;" onclick="return showHide();" /></p>

    <div id="showHideDiv" class = "show"><strong>Instructions to complete Multicentre Research  </strong><p>Multicentre research includes research projects:</p>
        <ul>
            <ul>
                <li> Undertaken at more than one institution; </li>
                <li> Conducted by researchers affiliated with more than one institution; </li>
                <li> Conducted by Monash University staff at hospitals; </li>
                <li> Conducted by Monash University researchers in a wide range of disciplines, 
                    involving collaborations with other Universities and corporations;  </li>
                <li> Undertaken by staff new to Monash who have transferred with existing research projects. </li>   
            </ul>
            <br/>
            <p class="form_link"><?php echo CHtml::link('Proceed >>', array('applicationMultiCentre/selectSupervisor&id=2')); ?></p>
    </div>
</div>