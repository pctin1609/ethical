<?php
/* @var $this fm1_section1Controller */
/* @var $model fm1_section1 */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'fm1-section1-view-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textField($model,'description'); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'aim'); ?>
		<?php echo $form->textField($model,'aim'); ?>
		<?php echo $form->error($model,'aim'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'type1_research'); ?>
		<?php echo $form->textField($model,'type1_research'); ?>
		<?php echo $form->error($model,'type1_research'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'type2_research'); ?>
		<?php echo $form->textField($model,'type2_research'); ?>
		<?php echo $form->error($model,'type2_research'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'researcher_finance_involvement'); ?>
		<?php echo $form->textField($model,'researcher_finance_involvement'); ?>
		<?php echo $form->error($model,'researcher_finance_involvement'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton('Submit'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->