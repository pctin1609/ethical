
<?php
/* @var $this DisplayFormsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Display Forms',
);

$this->menu=array(
	array('label'=>'Create displayForms', 'url'=>array('create')),
	array('label'=>'Manage displayForms', 'url'=>array('admin')),
);
?>

<h1><p align = 'center'>Display Forms</p></h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
