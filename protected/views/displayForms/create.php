<div id = "square">
<?php
/* @var $this DisplayFormsController */
/* @var $model displayForms */

$this->breadcrumbs=array(
	'Display Forms'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List displayForms', 'url'=>array('index')),
	array('label'=>'Manage displayForms', 'url'=>array('admin')),
);
?>

<h1>Create displayForms</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
</div>