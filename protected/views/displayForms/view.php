<div id = "square">
<?php
/* @var $this DisplayFormsController */
/* @var $model displayForms */

$this->breadcrumbs=array(
	'Display Forms'=>array('index'),
	$model->project_id,
);

$this->menu=array(
	array('label'=>'List displayForms', 'url'=>array('index')),
	array('label'=>'Create displayForms', 'url'=>array('create')),
	array('label'=>'Update displayForms', 'url'=>array('update', 'id'=>$model->project_id)),
	array('label'=>'Delete displayForms', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->project_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage displayForms', 'url'=>array('admin')),
);
?>

<h1>View displayForms #<?php echo $model->project_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'stage',
		'project_id',
		'user_id',
		'status',
		'create_time',
		'form_id',
		'application_type',
	),
)); ?>
</div>