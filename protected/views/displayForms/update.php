<div id = "square">
<?php
/* @var $this DisplayFormsController */
/* @var $model displayForms */

$this->breadcrumbs=array(
	'Display Forms'=>array('index'),
	$model->project_id=>array('view','id'=>$model->project_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List displayForms', 'url'=>array('index')),
	array('label'=>'Create displayForms', 'url'=>array('create')),
	array('label'=>'View displayForms', 'url'=>array('view', 'id'=>$model->project_id)),
	array('label'=>'Manage displayForms', 'url'=>array('admin')),
);
?>

<h1>Update displayForms <?php echo $model->project_id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
</div>