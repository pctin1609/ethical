<div id = "square">
<?php
/* @var $this DisplayFormsController */
/* @var $model displayForms */

$this->breadcrumbs=array(
	'Display Forms'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List displayForms', 'url'=>array('index')),
	array('label'=>'Create displayForms', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#display-forms-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Display Forms</h1>



<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'display-forms-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'stage',
		'project_id',
		'user_id',
		'status',
		'create_time',
		'form_id',
		/*
		'application_type',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
</div>