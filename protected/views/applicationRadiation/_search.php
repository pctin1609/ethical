<?php
/* @var $this ProfileController */
/* @var $model Profile */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	
<div class="row">
		<?php echo 'Search' ?>
		<?php echo $form->textField($model,'user_name',array('size'=>20,'maxlength'=>20)); ?>
	
		<?php echo CHtml::submitButton('Search'); ?>
                    <?php echo '<br/>(Search by Firstname,Surname,E-mail or Username)';?>

	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->