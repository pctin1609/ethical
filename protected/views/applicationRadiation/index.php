<?php
/* @var $this ApplicationRadiationController */

$this->breadcrumbs = array(
    'Application Radiation',
);
?>
<script>
    function showHide() {
        var ele = document.getElementById("showHideDiv");
        if(ele.style.display == "block") {
            ele.style.display = "none";
        }
        else {
            ele.style.display = "block";
        }
    }
    
</script>
<p></p>
<div id="formlist">
    <p><input type="button" value="Research Involving Radiation" style="width:100%;" onclick="return showHide();" /></p>
    <div id="showHideDiv" class = "show">
        <ul>	   
            <strong>Instructions to complete  Research involving Radiation </strong><br/>
            <p><strong>Make sure you have read and understood the National Statement on Ethical Conduct in Research Involving Humans Chapter 10</strong></p><br/>
            <p><strong>Make sure you have read and understood the Monash University Radiation Module Guidelines</strong></p><br/>
            <p><strong>Make sure you have read and understood the Code of Practice for the Exposure of Humans to Ionising Radiation for research Purposes (2005).Read now <a href="/ethicalClearance/index.php?r=application/ConsentDownload&file=code-of-practice.pdf" class="bl">Code Of Practice</a></strong></p><br/>
		</ul>
        <br/>
        <p class="form_link"><?php echo CHtml::link('Proceed >>', array('applicationRadiation/selectSupervisor&id=3')); ?></p>
    </div>
</div>


