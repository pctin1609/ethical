<?php
/* @var $this ProceduresDesc9Controller */
/* @var $model ProceduresDesc9 */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'procedures-desc9-section9-form',
	'enableAjaxValidation'=>false,
)); ?>

    <?php echo "<h2>Section 9</h2> ";?>
	
	<?php echo "<h7>Specify the radiation dose for each group of participants and the risks associated with this 
dose of radiation. Please attach evidence of the dose calculations by an independent 
medical physicist.</h7>";?>
	
	<?php echo "Where the radiation dose exceeds the dose specified in Table 1(Code of Practice: Exposure of Humans to 
Ionizing Radiation for Research Purposes), your application will also need to be sent to the Department of 
Human Services Radiations Advisory committee for review.<br><br>";?>

	<div class="row">
		<?php echo $form->labelEx($model,'grp_1'); ?>
		<?php echo $form->textArea($model,'grp_1',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'grp_1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'grp_2'); ?>
		<?php echo $form->textArea($model,'grp_2',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'grp_2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'grp_3'); ?>
		<?php echo $form->textArea($model,'grp_3',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'grp_3'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'grp_4'); ?>
		<?php echo $form->textArea($model,'grp_4',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'grp_4'); ?>
	</div>


    <div class="row buttons">
        <?php echo CHtml::submitButton('Previous', array('name' => 'previous')); ?>
        <?php echo CHtml::submitButton('Save', array('name' => 'save')); ?>
        <?php echo CHtml::submitButton('Next'); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->