<?php
/* @var $this StatusApprovalController */
/* @var $model StatusApproval */
/* @var $form CActiveForm */
?>

<div class="form">

<?php
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'status-approval-section1-form',
	'enableAjaxValidation'=>false,
)); ?>

	    <?php echo "<h2>Section 1</h2>";?>

	
	
	<?php echo "<h7>Specify the status of approval of the application:</h7>";?>

	

	<div class="row">
		<?php echo $form->labelEx($model,'approval_from'); ?>
		<div class='radiobuttons'>
		<?php echo $form->radioButtonList($model,'approval_from',$model->getValues(),array('onchange' => 'menuTypeChange(this.value,"attach",vis="0");')); ?>
		</div>
		<?php echo $form->error($model,'approval_from'); ?>
	</div>

	
	<div class="row"id="attach" style="display: none">
		<?php echo $form->labelEx($model,'attach_letter'); ?>
		<?php echo $form->fileField($model,'attach_letter',array('rows'=>3, 'cols'=>100)); ?>
		<?php echo $form->error($model,'attach_letter'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'seek_approval'); ?>
		<div class='radiobuttons'>
		<?php echo $form->radioButtonList($model,'seek_approval',$model->getValues()); ?>
		</div>
		<?php echo $form->error($model,'seek_approval'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'rac_approval'); ?>
		<div class='radiobuttons'>
		<?php echo $form->radioButtonList($model,'rac_approval',$model->getValues()); ?>
		</div>
		<?php echo $form->error($model,'rac_approval'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'other'); ?>
		<div class='radiobuttons'>
		<?php echo $form->radioButtonList($model,'other',$model->getValues(),array('onchange' => 'menuTypeChange(this.value,"other",vis="0");')); ?>
		</div>
		<?php echo $form->error($model,'other'); ?>
	</div>



	<div class="row"id="other" style="display: none">
		<?php echo $form->labelEx($model,'if_yes_specify'); ?>
		<?php echo $form->textArea($model,'if_yes_specify',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'if_yes_specify'); ?>
	</div>



	<div class="row buttons">
		
                <?php echo CHtml::submitButton('Save',array('name'=>'save')); ?>
                <?php echo CHtml::submitButton('Next'); ?>


	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->