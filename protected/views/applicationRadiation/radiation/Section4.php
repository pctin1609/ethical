<?php
/* @var $this ParticipantsDescController */
/* @var $model ParticipantsDesc */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'participants-desc-section4-form',
	'enableAjaxValidation'=>false,
)); ?>

		 <?php echo "<h2>Section 4</h2> ";?>
	

	 <?php echo "<h7>Describe the participants (in groups) exposed to radiation in your research project.</h7>";?>

	 <?php echo "<b>GROUP 1</b>";?>
	<div class="row">
	    
		<?php echo $form->labelEx($model,'pple_num'); ?>
		<?php echo $form->textArea($model,'pple_num',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'pple_num'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pple_involved'); ?>
		<?php echo $form->textArea($model,'pple_involved',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'pple_involved'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'age_range'); ?>
		<?php echo $form->dropdownlist($model,'age_range',$model->getInfor()); ?>
		<?php echo $form->error($model,'age_range'); ?>
	</div><br><br>
	
	
	 <?php echo "<b>GROUP 2</b>";?>
	<div class="row">
	    
		<?php echo $form->labelEx($model,'pple_num'); ?>
		<?php echo $form->textArea($model,'pple_num',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'pple_num'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pple_involved'); ?>
		<?php echo $form->textArea($model,'pple_involved',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'pple_involved'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'age_range'); ?>
		<?php echo $form->dropdownlist($model,'age_range',$model->getInfor()); ?>
		<?php echo $form->error($model,'age_range'); ?>
	</div><br><br>
	
		 <?php echo "<b>GROUP 3</b>";?>
	<div class="row">
	    
		<?php echo $form->labelEx($model,'pple_num'); ?>
		<?php echo $form->textArea($model,'pple_num',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'pple_num'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pple_involved'); ?>
		<?php echo $form->textArea($model,'pple_involved',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'pple_involved'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'age_range'); ?>
		<?php echo $form->dropdownlist($model,'age_range',$model->getInfor()); ?>
		<?php echo $form->error($model,'age_range'); ?>
	</div><br><br>
	
	
		 <?php echo "<b>GROUP 4</b>";?>
	<div class="row">
	    
		<?php echo $form->labelEx($model,'pple_num'); ?>
		<?php echo $form->textArea($model,'pple_num',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'pple_num'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pple_involved'); ?>
		<?php echo $form->textArea($model,'pple_involved',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'pple_involved'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'age_range'); ?>
		<?php echo $form->dropdownlist($model,'age_range',$model->getInfor()); ?>
		<?php echo $form->error($model,'age_range'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Previous',array('name'=>'previous')); ?>
                <?php echo CHtml::submitButton('Save',array('name'=>'save')); ?>
                  <?php echo CHtml::submitButton('Next'); ?>


	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->