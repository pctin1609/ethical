<?php
/* @var $this PositionIndvController */
/* @var $model PositionIndv */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'position-indv-section7-form',
	'enableAjaxValidation'=>false,
)); ?>

			 <?php echo "<h2>Section 7</h2> ";?>
	
	<?php echo "<h7>Name the individual and the position of the individual and the organisation they are employed by who will give the radiation dose to each group.</h7>";?>

	
    <?php echo "(Where it is the same for each group – repeat the information.<br>
     Where there is more than 1 individual for each group, name all individuals)<br><br>";?>
	
	<?php echo "<b>GROUP 1</b>";?>
	<div class="row">
		<?php echo $form->labelEx($model,'indv_name'); ?>
		<?php echo $form->textArea($model,'indv_name',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'indv_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'indv_pos'); ?>
		<?php echo $form->textArea($model,'indv_pos',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'indv_pos'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'org'); ?>
		<?php echo $form->textArea($model,'org',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'org'); ?>
	</div><br><br>
	
		<?php echo "<b>GROUP 2</b>";?>
	<div class="row">
		<?php echo $form->labelEx($model,'indv_name'); ?>
		<?php echo $form->textArea($model,'indv_name',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'indv_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'indv_pos'); ?>
		<?php echo $form->textArea($model,'indv_pos',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'indv_pos'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'org'); ?>
		<?php echo $form->textArea($model,'org',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'org'); ?>
	</div><br><br>
	
		<?php echo "<b>GROUP 3</b>";?>
	<div class="row">
		<?php echo $form->labelEx($model,'indv_name'); ?>
		<?php echo $form->textArea($model,'indv_name',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'indv_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'indv_pos'); ?>
		<?php echo $form->textArea($model,'indv_pos',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'indv_pos'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'org'); ?>
		<?php echo $form->textArea($model,'org',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'org'); ?>
	</div><br><br>
	
		<?php echo "<b>GROUP 4</b>";?>
	<div class="row">
		<?php echo $form->labelEx($model,'indv_name'); ?>
		<?php echo $form->textArea($model,'indv_name',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'indv_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'indv_pos'); ?>
		<?php echo $form->textArea($model,'indv_pos',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'indv_pos'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'org'); ?>
		<?php echo $form->textArea($model,'org',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'org'); ?>
	</div><br><br>


	<div class="row buttons">
		<?php echo CHtml::submitButton('Previous',array('name'=>'previous')); ?>
                <?php echo CHtml::submitButton('Save',array('name'=>'save')); ?>
                    <?php echo CHtml::submitButton('Next'); ?>


	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->