<?php
/* @var $this ProceduresDesc8Controller */
/* @var $model ProceduresDesc8 */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'procedures-desc8-section8-form',
	'enableAjaxValidation'=>false,
)); ?>

    <?php echo "<h2>Section 8</h2> ";?>
	
	<?php echo "<h7>State the name of the site and address where the radiation dose will be given.</h7>";?>

	<div class="row">
		<?php echo $form->labelEx($model,'grp_1'); ?>
		<?php echo $form->textArea($model,'grp_1',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'grp_1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'grp_2'); ?>
		<?php echo $form->textArea($model,'grp_2',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'grp_2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'grp_3'); ?>
		<?php echo $form->textArea($model,'grp_3',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'grp_3'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'grp_4'); ?>
		<?php echo $form->textArea($model,'grp_4',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'grp_4'); ?>
	</div>


    <div class="row buttons">
        <?php echo CHtml::submitButton('Previous', array('name' => 'previous')); ?>
        <?php echo CHtml::submitButton('Save', array('name' => 'save')); ?>
        <?php echo CHtml::submitButton('Next'); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->