<?php
/* @var $this GroupIdentificationController */
/* @var $model GroupIdentification */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'group-identification-section2-form',
	'enableAjaxValidation'=>false,
)); ?>

    


    <?php echo "<h2>Section 2</h2>";?>

    <?php echo "<h7>Identify whether this research involves any of the following groups:</h7>";?>
	

<?php echo "Please identify which group";?>
	<div class="checkboxes">
		<?php echo $form->checkbox($model,'children'); ?>
		<?php echo $form->labelEx($model,'children'); ?>
		<?php echo $form->error($model,'children'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkbox($model,'adult'); ?>
		<?php echo $form->labelEx($model,'adult'); ?>
		<?php echo $form->error($model,'adult'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkbox($model,'preg_women'); ?>
		<?php echo $form->labelEx($model,'preg_women'); ?>
		<?php echo $form->error($model,'preg_women'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkbox($model,'breastfd_women'); ?>
		<?php echo $form->labelEx($model,'breastfd_women'); ?>
		<?php echo $form->error($model,'breastfd_women'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkbox($model,'individuals'); ?>
		<?php echo $form->labelEx($model,'individuals'); ?>
		<?php echo $form->error($model,'individuals'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'justification'); ?>
		<?php echo $form->textArea($model,'justification',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'justification'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton('Previous',array('name'=>'previous')); ?>
                <?php echo CHtml::submitButton('Save',array('name'=>'save')); ?>
                <?php echo CHtml::submitButton('Next'); ?>


	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->