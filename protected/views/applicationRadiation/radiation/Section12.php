<?php
/* @var $this ProceduresDesc12Controller */
/* @var $model ProceduresDesc12 */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'procedures-desc12-section12-form',
	'enableAjaxValidation'=>false,
)); ?>

	 <?php echo "<h2>Section 12</h2> ";?>


	<?php echo "<h7>Explain the precautions taken to ensure radiation exposure to others will be kept to a 
           minimum.</h7>";?>
	

	<div class="row">
		<?php echo $form->labelEx($model,'grp_1'); ?>
		<?php echo $form->textArea($model,'grp_1',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'grp_1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'grp_2'); ?>
		<?php echo $form->textArea($model,'grp_2',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'grp_2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'grp_3'); ?>
		<?php echo $form->textArea($model,'grp_3',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'grp_3'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'grp_4'); ?>
		<?php echo $form->textArea($model,'grp_4',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'grp_4'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton('Previous',array('name'=>'previous')); ?>
                
                <?php echo CHtml::submitButton('Submit'); ?>


	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->