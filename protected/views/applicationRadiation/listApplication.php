<?php
/* @var $this ApplicationController */
/* @var $model Application */

$this->breadcrumbs=array(
    'Applications'=>array('index'),
    'Manage',
);

$this->menu=array(
    array('label'=>'List Application', 'url'=>array('index')),
    array('label'=>'Create Application', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').toggle();
    return false;
});
$('.search-form form').submit(function(){
    $.fn.yiiGridView.update('application-grid', {
        data: $(this).serialize()
    });
    return false;
});
");
?>

<h1>My Application(s)</h1>



<?php
   $id= yii::app()->user->id;
  

// echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">

</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'application-grid',
    'dataProvider'=>$model->getUserApplications(yii::app()->user->id),
    'columns'=>array(
        'application_type',
        'create_time',        
        'status',
         array(
        'class'=>'CLinkColumn',
        'label'=>'Resume Application',
        'urlExpression'=>'Yii::app()->createUrl("application/resumeApplication",array("id"=>$data->project_id))',
        'header'=>'Resume'
      ),
       
    ),
    
)); ?>