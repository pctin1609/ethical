<?php
//
/* @var $this ApplicationController */

$this->breadcrumbs = array(
    'Application',
);
?>

<script>
    function showHide() {
        var ele = document.getElementById("showHideDiv");
        if(ele.style.display == "block") {
            ele.style.display = "none";
        }
        else {
            ele.style.display = "block";
        }
    }

    function showHide2() {
        var ele = document.getElementById("showHideDiv2");
        if(ele.style.display == "block") {
            ele.style.display = "none";
        }
        else {
            ele.style.display = "block";
        }
    }

    function showHide3() {
        var ele = document.getElementById("showHideDiv3");
        if(ele.style.display == "block") {
            ele.style.display = "none";
        }
        else {
            ele.style.display = "block";
        }
    }

    function showHide4() {
        var ele = document.getElementById("showHideDiv4");
        if(ele.style.display == "block") {
            ele.style.display = "none";
        }
        else {
            ele.style.display = "block";
        }
    }

    

    
</script>

<div id="formlist">
    <p><input type="button" value="Form 1" style="width:100%;" onclick="return showHide();" /></p>

    <div id="showHideDiv" class = "show" style="display:none;"><strong>Instructions to complete Form 1 </strong><p><strong>Complete this form if your research involves the following:</strong></p>
        <ul>
            <li>Interventions & Therapies </li>
            <li>Human genetics & or stem cells </li>
            <li>Women who are pregnant & the Human foetus </li>
            <li>People highly dependent on medical care who may be unable to give consent</li>
            <li>People with a cognitive impairment,an intellectual disability, or mentall illness</li>
            <li>Aboriginal & Torres Straut Islanders</li>
            <li>Use of collected human data for a new purpose </li>    
        </ul><br/><p class="form_link"><?php echo CHtml::link('Proceed �', array('application/selectSupervisor&id=0')); ?></p></div></form>

<form method="post" action="">
    <p><input type="button" value="Low Risk Research" style="width:100%;" onclick="return showHide2();" /></p>

    <div id="showHideDiv2" class = "show" style="display:none;"><strong>Instructions to complete Low Risk Research </strong>
        <p>This form is intended to be used for all research where the foreseeable risk to participants
            is no more than one of discomfort for example:</p>
        <ul>
            <li> The questionnaires and/or survey; </li>
            <li> Anonymous;  </li>
            <li> Online; </li>
            <li> Involving non-sensitive topics and observations;  </li>
            <li> With or without consent. </li>
        </ul><br/><p class="form_link"><?php echo CHtml::link('Proceed �', array('application/selectSupervisor&id=1')); ?></p></div></form>

<form method="post" action="">
    <p><input type="button" value="Multicentre Research"style="width:100%;" onclick="return showHide3();" /></p>

    <div id="showHideDiv3" class = "show" style="display:none;"><strong>Instructions to complete Multicentre Research </strong>
        <p> Multicentre research includes research projects:</p>
        <ul>
            <li> Undertaken at more than one institution; </li>
            <li> Conducted by researchers affiliated with more than one institution; </li>
            <li> Conducted by Monash University staff at hospitals; </li>
            <li> Conducted by Monash University researchers in a wide range of disciplines, 
                involving collaborations with other Universities and corporations;  </li>
            <li> Undertaken by staff new to Monash who have transferred with existing research projects. </li>
        </ul><br/><p class="form_link"><?php echo CHtml::link('Proceed �', array('application/selectSupervisor&id=2')); ?></p></div></form>




<form method="post" action="">
    <p><input type="button" value="Research involving Radiation" style="width:100%;" onclick="return showHide4();" /></p>

    <div id="showHideDiv4" class = "show" style="display:none;">
        <strong>Instructions to complete  Research involving Radiation </strong><br/>
        <p><strong>Make sure you have read and understood the National Statement on Ethical Conduct in Research Involving Humans Chapter 10</strong></p><br/>
        <p><strong>Make sure you have read and understood the Monash University Radiation Module Guidelines</strong></p><br/>
		<p><strong>Make sure you have read and understood the Code of Practice for the Exposure of Humans to Ionising Radiation for research Purposes (2005).</strong></p><br/>
		<p class="form_link"><?php echo CHtml::link('Proceed �', array('application/selectSupervisor&id=3')); ?></p></div></form>
</div>