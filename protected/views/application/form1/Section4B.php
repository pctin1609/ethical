<?php
/* @var $this Fm1Section4bController */
/* @var $model Fm1Section4b */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'fm1-section4b-fm1_section4b-form',
	'enableAjaxValidation'=>false,
)); ?>

	
<?php echo " <h2> Section 4b - Data materials and procedures</h2>" ?>
	<script language="javascript">
        function menuTypeChange(value, div,vis)
        {
            if (value === vis) {
                document.getElementById(div).style.display = 'block';
            } else
            {
                document.getElementById(div).style.display = 'none';

            }
        }
    </script>
	<p class="note">All fields are mandatory.</p>

	<div class="row">
		<?php echo $form->labelEx($model,'data_set'); ?>
		<?php echo $form->textArea($model,'data_set',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'data_set'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'data_collected'); ?>
		<?php echo $form->textArea($model,'data_collected',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'data_collected'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ethics_approval'); ?>
		<?php echo $form->textArea($model,'ethics_approval',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'ethics_approval'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'primary_purpose'); ?>
		<?php echo $form->textArea($model,'primary_purpose',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'primary_purpose'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'information_given'); ?>
		<?php echo $form->textArea($model,'information_given',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'information_given'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'organisation_informed'); ?>
                        <div class='radiobuttons'>

		<?php echo $form->radioButtonList($model,'organisation_informed',$model->getValues() , array('onchange' => 'menuTypeChange(this.value,"inform",vis="1");')); ?>
                             </div>
		<?php echo $form->error($model,'organisation_informed'); ?>
	</div>

	

	<div class="row" id ="inform" style="display: none">
		<?php echo $form->labelEx($model,'procedure_description'); ?>
		<?php echo $form->textArea($model,'procedure_description',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'procedure_description'); ?>
	</div>
 

	<div class="row">
		<?php echo $form->labelEx($model,'method_used'); ?>
		<?php echo $form->dropDownList($model,'method_used',$model->getInformedConsentTypes()); ?>
		<?php echo $form->error($model,'method_used'); ?>
	</div>
<div class="row">
		<?php echo $form->labelEx($model,'participants_consented'); ?>
                        <div class='radiobuttons'>

		<?php echo $form->radioButtonList($model,'participants_consented',$model->getValues()); ?>
                             </div>
		<?php echo $form->error($model,'participants_consented'); ?>
	</div>
	
<div class="row">
		<?php echo $form->labelEx($model,'access_NCIS'); ?>
                        <div class='radiobuttons'>

		<?php echo $form->radioButtonList($model,'access_NCIS',$model->getValues()); ?>
                             </div>
		<?php echo $form->error($model,'access_NCIS'); ?>
	</div>
	

	<div class="row">
		<?php echo $form->labelEx($model,'access_level'); ?>
		<?php echo $form->dropDownList($model,'access_level',$model->getAccessLevel()); ?>
		<?php echo $form->error($model,'access_level'); ?>
	</div>
<div class="row">
		<?php echo $form->labelEx($model,'usually_access'); ?>
                        <div class='radiobuttons'>

		<?php echo $form->radioButtonList($model,'usually_access',$model->getValues(), array('onchange' => 'menuTypeChange(this.value,"usually",vis="0");')); ?>
                             </div>
		<?php echo $form->error($model,'usually_access'); ?>
	</div>


	<div class="row" id = "usually" style="display: none">
		<?php echo $form->labelEx($model,'describe_role'); ?>
		<?php echo $form->textArea($model,'describe_role',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'describe_role'); ?>
	</div>
<div class="row">
		<?php echo $form->labelEx($model,'coronial_records'); ?>
                        <div class='radiobuttons'>

		<?php echo $form->radioButtonList($model,'coronial_records',$model->getValues() , array('onchange' => 'menuTypeChange(this.value,"records",vis="0");')); ?>
                             </div>
		<?php echo $form->error($model,'coronial_records'); ?>
	</div>
	

	<div class="row" id = "records" style= "display: none">
		<?php echo $form->labelEx($model,'access_aproved'); ?>
		<?php echo $form->dropDownList($model,'access_aproved',$model->getAccessApproved()); ?>
		<?php echo $form->error($model,'access_aproved'); ?>
	</div>

	

	<div class="row buttons">
                <?php echo CHtml::submitButton('Previous',array('name'=>'previous')); ?>
		<?php echo CHtml::submitButton('Save',array('name'=>'save')); ?>
                <?php echo CHtml::submitButton('Next',array('name'=>'next')) ?>

	</div>
<?php $this->endWidget(); ?>

</div><!-- form -->