<?php
/* @var $this form1_section_8Controller */
/* @var $model form1_section_8 */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'fm1-section8-section8-form',
	'enableAjaxValidation'=>false,
)); ?>


    <h2> Section 8 - Other Ethical Issues</h2>
    
 	<div class="row">
         <?php echo $form->labelEx($model,'ethical_issues'); ?>
        <?php echo $form->textArea($model,'ethical_issues',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
	<?php echo $form->error($model,'ethical_issues'); ?>   
            
            </div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Previous',array('name'=>'previous')); ?>
                <?php echo CHtml::submitButton('Save',array('name'=>'save')); ?>
                <?php echo CHtml::submitButton('Next'); ?>


	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->