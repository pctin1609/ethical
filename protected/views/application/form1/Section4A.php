<?php
/* @var $this Fm1Section4aController */
/* @var $model Fm1Section4a */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'fm1-section4a-fm1_section4a-form',
	'enableAjaxValidation'=>false,
)); ?>

	

	<?php echo " <h2> Section 4a -  Procedures for explanation and gaining informed consent</h2>" ?>
<script language="javascript">
        function menuTypeChange(value, div,vis)
        {
            if (value === vis) {
                document.getElementById(div).style.display = 'block';
            } else
            {
                document.getElementById(div).style.display = 'none';

            }
        }
    </script>
	<p class="note">All fields are mandatory.</p>
	
<div class ="row">
  <?php echo $form -> labelEx($model,'explanatory_statement');?>
    <div class = 'radiobuttons'>
	<?php echo $form -> radioButtonList ($model, 'explanatory_statement', $model->getValues() , array('onchange' => 'menuTypeChange(this.value,"stat",vis="1");'));?>
	 </div>
	 <?php echo $form -> error ($model, 'explanatory_statement');?>
	 </div>
	<div class="row" id = "stat" style="display: none">
		<?php echo $form->labelEx($model,'exp_2_participants'); ?>
		<?php echo $form->textarea($model,'exp_2_participants',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'exp_2_participants'); ?>
	</div>
<div class="row">
		<?php echo $form->labelEx($model,'research_nature'); ?>
                        <div class='radiobuttons'>

		<?php echo $form->radioButtonList($model,'research_nature',$model->getValues(), array('onchange' => 'menuTypeChange(this.value,"nature",vis="1");')); ?>
                             </div>
		<?php echo $form->error($model,'research_nature'); ?>
	</div>
	

	<div class="row" id ="nature" style="display: none">
		<?php echo $form->labelEx($model,'describe_procedure'); ?>
		<?php echo $form->textarea($model,'describe_procedure',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		
		<?php echo $form->error($model,'describe_procedure'); ?>
	</div>
<div class="row">
		<?php echo $form->labelEx($model,'obtain_consent'); ?>
		<?php echo $form->dropDownList($model,'obtain_consent',$model->getInformedConsentTypes()); ?>
		<?php echo $form->error($model,'obtain_consent'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'participants_informed'); ?>
		<?php echo $form->textarea($model,'participants_informed',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'participants_informed'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'other_specify'); ?>
		<?php echo $form->textarea($model,'other_specify',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'other_specify'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'obtain_informed_consent'); ?>
		<?php echo $form->textarea($model,'obtain_informed_consent',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'obtain_informed_consent'); ?>
	</div>
<div class="row">
		<?php echo $form->labelEx($model,'witness_necessity'); ?>
            <div class='radiobuttons'>
		<?php echo $form->radioButtonList($model,'witness_necessity',$model->getValues(), array('onchange' => 'menuTypeChange(this.value,"witness",vis="0");')); ?>
                </div>
		<?php echo $form->error($model,'witness_necessity'); ?>
	</div>
	

	<div class="row" id ="witness" style="display: none">
		<?php echo $form->labelEx($model,'independent_witness_necessity'); ?>
        <?php echo $form->textarea($model,'independent_witness_necessity',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'independent_witness_necessity'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton('Previous',array('name'=>'previous')); ?>
                <?php echo CHtml::submitButton('Save',array('name'=>'save')); ?>
                <?php echo CHtml::submitButton('Next'); ?>


	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->