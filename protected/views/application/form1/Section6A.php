<?php
/* @var $this CompliancePrivacyLegislationController */
/* @var $model CompliancePrivacyLegislation */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'compliance-privacy-legislation-Section6A-form',
	'enableAjaxValidation'=>false,
)); ?>
<?php echo " <h2>Section 6a - Compliance with privacy legislation : Research involving collection, use and disclosure of information</h2>" ?>

	<script language="javascript">
        function menuTypeChange(value, div,vis)
        {
            if (value === vis) {
                document.getElementById(div).style.display = 'block';
            } else
            {
                document.getElementById(div).style.display = 'none';

            }
        }
		</script>
	<p class="note">All fields are mandatory.</p>

<div class="row">
		<?php echo $form->labelEx($model,'collecting'); ?>
                        <div class='radiobuttons'>

		<?php echo $form->radioButtonList($model,'collecting',$model->getValues(), array('onchange' => 'menuTypeChange(this.value,"collec",vis="1");')); ?>
                             </div>
		<?php echo $form->error($model,'collecting'); ?>
	</div>
	

	<div class="row"id="collec" style="display: none">
		<?php echo $form->labelEx($model,'answer'); ?>
		<?php echo $form->dropDownList($model,'answer',$model->getReason()); ?>
		<?php echo $form->error($model,'answer'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'other_explain'); ?>
		<?php echo $form->textArea($model,'other_explain',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'other_explain'); ?>
	</div>

<div class="row">
	<div class="row">
		<?php echo $form->labelEx($model,'researchers_access_data'); ?>
                        <div class='radiobuttons'>

		<?php echo $form->radioButtonList($model,'researchers_access_data',$model->getValues()); ?>
                             </div>
		<?php echo $form->error($model,'researchers_access_data'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'data_retained'); ?>
                        <div class='radiobuttons'>

		<?php echo $form->radioButtonList($model,'data_retained',$model->getValues()); ?>
                             </div>
		<?php echo $form->error($model,'data_retained'); ?>
	</div>
<div class="row">
		<?php echo $form->labelEx($model,'victorian_privacy'); ?>
                        <div class='radiobuttons'>

		<?php echo $form->radioButtonList($model,'victorian_privacy',$model->getValues()); ?>
                             </div>
		<?php echo $form->error($model,'victorian_privacy'); ?>
	</div>
	

	<div class="row">
		<?php echo $form->labelEx($model,'information_handled'); ?>
		<?php echo $form->textArea($model,'information_handled',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'information_handled'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'descibe_procedures'); ?>
		<?php echo $form->textArea($model,'descibe_procedures',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'descibe_procedures'); ?>
	</div>
  
  

  
  <div class="row">
		<?php echo $form->labelEx($model,'collection_of_information'); ?>
                        <div class='radiobuttons'>

		<?php echo $form->radioButtonList($model,'collection_of_information',$model->getValues()); ?>
                             </div>
		<?php echo $form->error($model,'collection_of_information'); ?>
	</div>

	
	<h7> 6.5b What type of information will be collected? </h7>
	<div class="checkboxes">
		<?php echo $form->checkBox($model,'information_collected',array('value'=>1, 'uncheckValue'=>0) ); ?>
		<?php echo $form->labelEx($model,'information_collected'); ?>
		<?php echo $form->error($model,'information_collected'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'sensi_infor' ,array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'sensi_infor'); ?>
		<?php echo $form->error($model,'sensi_infor'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'health_infor',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'health_infor'); ?>
		<?php echo $form->error($model,'health_infor'); ?>
	</div>
<div class="row">
		<?php echo $form->labelEx($model,'identity_of_organisation'); ?>
                        <div class='radiobuttons'>

		<?php echo $form->radioButtonList($model,'identity_of_organisation',$model->getValues()); ?>
                             </div>
		<?php echo $form->error($model,'identity_of_organisation'); ?>
	</div>
	

	<div class="row">
		<?php echo $form->labelEx($model,'purpose'); ?>
                        <div class='radiobuttons'>

		<?php echo $form->radioButtonList($model,'purpose',$model->getValues()); ?>
                             </div>
		<?php echo $form->error($model,'purpose'); ?>
	</div>
	
	
<div class="row">
		<?php echo $form->labelEx($model,'periods_for_records'); ?>
                        <div class='radiobuttons'>

		<?php echo $form->radioButtonList($model,'periods_for_records',$model->getValues()); ?>
                             </div>
		<?php echo $form->error($model,'periods_for_records'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'steps_taken'); ?>
                        <div class='radiobuttons'>

		<?php echo $form->radioButtonList($model,'steps_taken',$model->getValues()); ?>
                             </div>
		<?php echo $form->error($model,'steps_taken'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'types'); ?>
                        <div class='radiobuttons'>

		<?php echo $form->radioButtonList($model,'types',$model->getValues()); ?>
                             </div>
		<?php echo $form->error($model,'types'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'privacy_protected'); ?>
                        <div class='radiobuttons'>

		<?php echo $form->radioButtonList($model,'privacy_protected',$model->getValues()); ?>
                             </div>
		<?php echo $form->error($model,'privacy_protected'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'access_information'); ?>
                        <div class='radiobuttons'>

		<?php echo $form->radioButtonList($model,'access_information',$model->getValues()); ?>
                             </div>
		<?php echo $form->error($model,'access_information'); ?>
	</div>

	
<div class="row">
		<?php echo $form->labelEx($model,'law_required'); ?>
		<?php echo $form->dropDownList($model,'law_required',$model->getAnswer()); ?>
		
		<?php echo $form->error($model,'law_required'); ?>
	</div>
	
<div class="row">
		<?php echo $form->labelEx($model,'consequences'); ?>
		<?php echo $form->dropDownList($model,'consequences',$model->getConsequences()); ?>
		
		<?php echo $form->error($model,'consequences'); ?>
	</div>
	

<div class="row buttons">
		<?php echo CHtml::submitButton('Previous',array('name'=>'previous')); ?>
                <?php echo CHtml::submitButton('Save',array('name'=>'save')); ?>
                <?php echo CHtml::submitButton('Next'); ?>


	

<?php $this->endWidget(); ?>

</div><!-- form -->