<?php
/* @var $this Fm1_Section5aController */
/* @var $model Fm1_Section5a */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'fm1--section5a-section5a-form',
        'enableAjaxValidation' => false,
    ));
    ?>

  
    <?php echo " <h2> Section 5a - Collection of data materials and procedures</h2>" ?>
     <script language="javascript">
        function menuTypeChange(value, div,vis)
        {
            if (value === vis) {
                document.getElementById(div).style.display = 'block';
            } else
            {
                document.getElementById(div).style.display = 'none';

            }
        }
    </script>
	 <p class="note">All fields are mandatory.</p>
	
       	

    <div class="row">
        <?php echo $form->labelEx($model, 'questionnaire'); ?>
<?php echo $form->dropdownlist($model, 'questionnaire', $model->getQuestionnaireTypes()); ?>
<?php echo $form->error($model, 'questionnaire'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'Interviews'); ?>
<?php echo $form->dropdownlist($model, 'interviews', $model->getInterviewTypes()); ?>
<?php echo $form->error($model, 'interviews'); ?>
    </div>

    <div class="row">

            <?php echo $form->labelEx($model, 'Audio recording'); ?>
        <div class='radiobuttons'> 
        <?php echo $form->radiobuttonlist($model, 'interview_audio_recording', $model->getValues()); ?>
        </div>
<?php echo $form->error($model, 'interview_audio_recording'); ?>
    </div>

    <div class="row">
            <?php echo $form->labelEx($model, 'Video recording'); ?>
        <div class='radiobuttons'> 
        <?php echo $form->radiobuttonlist($model, 'interview_video_recording', $model->getValues()); ?>
        </div>
<?php echo $form->error($model, 'interview_video_recording'); ?>
    </div>

    
   <div class="row">
		<?php echo $form->labelEx($model,'focus_groups'); ?>
            <div class='radiobuttons'>
		<?php echo $form->radioButtonList($model,'focus_groups',$model->getValues()); ?>
                </div>
		<?php echo $form->error($model,'focus_groups'); ?>
	</div>
    
<div class="row">
		<?php echo $form->labelEx($model,'observations'); ?>
            <div class='radiobuttons'>
		<?php echo $form->radioButtonList($model,'observations',$model->getValues()); ?>
                </div>
		<?php echo $form->error($model,'observations'); ?>
	</div>
 

    <div class="row">
        <?php echo $form->labelEx($model, 'observations_with_kp'); ?>
<?php echo $form->dropdownlist($model, 'observations_with_kp',$model->getInfor()); ?>
<?php echo $form->error($model, 'observations_with_kp'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'observations_without_kp'); ?>
<?php echo $form->dropdownlist($model, 'observations_without_kp',$model->getInfor()); ?>
<?php echo $form->error($model, 'observations_without_kp'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'photo_video_graph_with_kp'); ?>
<?php echo $form->dropdownlist($model, 'photo_video_graph_with_kp', $model->getInfor()); ?>
        <?php echo $form->error($model, 'photo_video_graph_with_kp'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'photo_video_graph_without_kp'); ?>
<?php echo $form->dropdownlist($model, 'photo_video_graph_without_kp',$model->getInfor()); ?>
<?php echo $form->error($model, 'photo_video_graph_without_kp'); ?>
    </div>

<div class="row">
		<?php echo $form->labelEx($model,'photo_video_graph_video'); ?>
            <div class='radiobuttons'>
		<?php echo $form->radioButtonList($model,'photo_video_graph_video',$model->getValues()); ?>
                </div>
		<?php echo $form->error($model,'photo_video_graph_video'); ?>
	</div>
	
   <div class="row">
		<?php echo $form->labelEx($model,'collection_of_HT'); ?>
            <div class='radiobuttons'>
		<?php echo $form->radioButtonList($model,'collection_of_HT',$model->getValues(), array('onchange' => 'menuTypeChange(this.value,"collect",vis="0");')); ?>
                </div>
		<?php echo $form->error($model,'collection_of_HT'); ?>
	</div>
	
<div class="row" id ="collect" style="display: none">
        <?php echo $form->labelEx($model, 'collection_of_details'); ?>
<?php echo $form->textArea($model, 'collection_of_details',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
<?php echo $form->error($model, 'collection_of_details'); ?>
    </div>

   <div class="row">
		<?php echo $form->labelEx($model,'response_to_tss'); ?>
            <div class='radiobuttons'>
		<?php echo $form->radioButtonList($model,'response_to_tss',$model->getValues(), array('onchange' => 'menuTypeChange(this.value,"respond",vis="0");')); ?>
                </div>
		<?php echo $form->error($model,'response_to_tss'); ?>
	</div>
	
<div class="row" id ="respond" style="display: none">
        <?php echo $form->labelEx($model, 'response_to_tss_details'); ?>
<?php echo $form->textArea($model, 'response_to_tss_details',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
<?php echo $form->error($model, 'response_to_tss_details'); ?>
    </div>
   
  
    <div class="row">
		<?php echo $form->labelEx($model,'epidemiological_research'); ?>
            <div class='radiobuttons'>
		<?php echo $form->radioButtonList($model,'epidemiological_research',$model->getValues(), array('onchange' => 'menuTypeChange(this.value,"epidermic",vis="0");')); ?>
                </div>
		<?php echo $form->error($model,'epidemiological_research'); ?>
	</div>
	
<div class="row" id ="epidermic" style="display: none">
        <?php echo $form->labelEx($model, 'epidemiological_research_details'); ?>
<?php echo $form->textArea($model, 'epidemiological_research_details',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
<?php echo $form->error($model, 'epidemiological_research_details'); ?>
    </div>
	
	
	<div class="row">
		<?php echo $form->labelEx($model,'psychology_inventories'); ?>
            <div class='radiobuttons'>
		<?php echo $form->radioButtonList($model,'psychology_inventories',$model->getValues(), array('onchange' => 'menuTypeChange(this.value,"psyc",vis="0");')); ?>
                </div>
		<?php echo $form->error($model,'psychology_inventories'); ?>
	</div>
	
<div class="row" id ="psyc" style="display: none">
        <?php echo $form->labelEx($model, 'psychology_inventories_details'); ?>
<?php echo $form->textArea($model, 'psychology_inventories_details',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
<?php echo $form->error($model, 'psychology_inventories_details'); ?>
    </div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'online_survey'); ?>
            <div class='radiobuttons'>
		<?php echo $form->radioButtonList($model,'online_survey',$model->getValues()); ?>
                </div>
		<?php echo $form->error($model,'online_survey'); ?>
	</div>
    

 <div class="row buttons">
        <?php echo CHtml::submitButton('Previous', array('name' => 'previous')); ?>
        <?php echo CHtml::submitButton('Save', array('name' => 'save')); ?>
        <?php echo CHtml::submitButton('Next'); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->