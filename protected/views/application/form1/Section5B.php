<?php
/* @var $this Fm1_Section5bController */
/* @var $model Fm1_Section5b */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'fm1--section5b-section5b-form',
	'enableAjaxValidation'=>false,
)); ?>



	
	<?php echo " <h2>Section 5b - Collection of data materials and procedures</h2>" ?>
     <script language="javascript">
        function menuTypeChange(value, div,vis)
        {
            if (value === vis) {
                document.getElementById(div).style.display = 'block';
            } else
            {
                document.getElementById(div).style.display = 'none';

            }
        }
    </script>
	<p class="note">All fields are mandatory.</p>
	<h7> How will the data be collected?</h7> 
	<div class="row">
		<?php echo $form->labelEx($model,'admin_of_substance'); ?>
            <div class='radiobuttons'>
		<?php echo $form->radioButtonList($model,'admin_of_substance',$model->getValues(), array('onchange' => 'menuTypeChange(this.value,"admin",vis="0");')); ?>
                </div>
		<?php echo $form->error($model,'admin_of_substance'); ?>
	</div>
	
<div class="row" id ="admin" style="display: none">
        <?php echo $form->labelEx($model, 'admin_of_substance_details'); ?>
<?php echo $form->textArea($model, 'admin_of_substance_details',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
<?php echo $form->error($model, 'admin_of_substance_details'); ?>
    </div>

	<div class="row">
		<?php echo $form->labelEx($model,'other'); ?>
            <div class='radiobuttons'>
		<?php echo $form->radioButtonList($model,'other',$model->getValues(), array('onchange' => 'menuTypeChange(this.value,"other",vis="0");')); ?>
                </div>
		<?php echo $form->error($model,'other'); ?>
	</div>
	
<div class="row" id ="other" style="display: none">
        <?php echo $form->labelEx($model, 'other_description'); ?>
<?php echo $form->textArea($model, 'other_description',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
<?php echo $form->error($model, 'other_description'); ?>
    </div>

<div class="row">
		
	
		<?php echo $form->labelEx($model,'data_collection_location'); ?>
			<?php echo $form->textArea($model,'data_collection_location',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
	   <?php echo $form->error($model,'data_collection_location'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'data_collection_location_sug'); ?>
		<?php echo $form->textArea($model,'data_collection_location_sug',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'data_collection_location'); ?>
	</div>


	<div class="row">
		<?php echo $form->labelEx($model,'person_to_collect_data'); ?>
		<?php echo $form->textArea($model,'person_to_collect_data',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'person_to_collect_data'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'translator_required'); ?>
		  <div class='radiobuttons'>
		<?php echo $form->radioButtonList($model,'translator_required',$model->getValues() , array('onchange' => 'menuTypeChange(this.value,"trans",vis="0");')); ?>
		  </div>
		<?php echo $form->error($model,'translator_required'); ?>
		
	</div>

    <div class="row" id ="trans" style="display: none">
		<?php echo $form->labelEx ($model,'translator_required_desc');?>
		<?php echo $form->textArea($model,'translator_required_desc',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'translator_required'); ?>
    </div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'research_involve_children'); ?>
				  <div class='radiobuttons'>
		<?php echo $form->radioButtonList($model,'research_involve_children',$model->getValues()); ?>
		         </div>
		<?php echo $form->error($model,'research_involve_children'); ?>
	</div>
	
	<div class="row">
	  <?php echo $form->labelEx($model,'research_involve_children_ys'); ?>
     				  <div class='radiobuttons'>
		<?php echo $form->radioButtonList($model,'research_involve_children_ys',$model->getValues(), array('onchange' => 'menuTypeChange(this.value,"research",vis="1");')); ?>
		         </div>
	  <?php echo $form->error($model,'research_involve_children'); ?>
	</div>
	
	<div class="row" id = "research" style= "display: none">
	  <?php echo $form->labelEx($model,'research_involve_children_no'); ?>
      <?php echo $form->textarea($model,'research_involve_children_no',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
	  <?php echo $form->error($model,'research_involve_children_no'); ?>
	</div>
	  
	<div class="row">
		<?php echo $form->labelEx($model,'dependent_or_unequal_rlshp'); ?>
						  <div class='radiobuttons'>
		<?php echo $form->radioButtonList($model,'dependent_or_unequal_rlshp',$model->getValues(), array('onchange' => 'menuTypeChange(this.value,"dependent",vis="0");')); ?>
		         </div>
		<?php echo $form->error($model,'dependent_or_unequal_rlshp'); ?>
	</div>
	
	

	<div class="row" id = "dependent" style= "display: none">
		<?php echo $form->labelEx($model,'dependent_or_unequal_rlshp_details'); ?>
		<?php echo $form->textarea($model,'dependent_or_unequal_rlshp_details',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'dependent_or_unequal_rlshp_details'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'admin_involvement'); ?>
					  <div class='radiobuttons'>
		<?php echo $form->radioButtonList($model,'admin_involvement',$model->getValues(), array('onchange' => 'menuTypeChange(this.value,"details",vis="0");')); ?>
		         </div>
		<?php echo $form->error($model,'admin_involvement'); ?>
	</div>

	<div class="row" id = "details" style= "display: none">
		<?php echo $form->labelEx($model,'admin_involvement_details');?>
		<?php echo $form->textArea($model,'admin_involvement_details',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'admin_involvement_details');?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'procedures_use_diagonistic_or_indicative'); ?>
				  <div class='radiobuttons'>
		<?php echo $form->radioButtonList($model,'procedures_use_diagonistic_or_indicative',$model->getValues() , array('onchange' => 'menuTypeChange(this.value,"use",vis="0");')); ?>
		         </div>
		<?php echo $form->error($model,'procedures_use_diagonistic_or_indicative'); ?></br>
	</div>
	

	<div class="row" id ="use" style="display: none">
	    <?php echo $form->labelEx($model,'criteria_to_assess_results'); ?>
		<?php echo $form->textarea($model,'criteria_to_assess_results',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'criteria_to_assess_results'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'duty_of_care_to_participants'); ?>
		<?php echo $form->textarea($model,'duty_of_care_to_participants' ,array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'duty_of_care_to_participants'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'acquired_necessary_competence'); ?>
			     <div class='radiobuttons'>
		<?php echo $form->radioButtonList($model,'acquired_necessary_competence',$model->getValues()); ?>
		         </div>
		<?php echo $form->error($model,'acquired_necessary_competence'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'indicate_procedure_proposed_to_PP'); ?>
			     <div class='radiobuttons'>
		<?php echo $form->radioButtonList($model,'indicate_procedure_proposed_to_PP',$model->getValues()); ?>
		         </div>
		<?php echo $form->error($model,'indicate_procedure_proposed_to_PP'); ?>
	</div>


    <div class="row buttons">
		<?php echo CHtml::submitButton('Previous',array('name'=>'previous')); ?>
        <?php echo CHtml::submitButton('Save',array('name'=>'save')); ?>
        <?php echo CHtml::submitButton('Next'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->