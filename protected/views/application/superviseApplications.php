<?php
/* @var $this ApplicationController */
/* @var $model Application */

$this->breadcrumbs=array(
    'Applications'=>array('index'),
    'Manage',
);

 
    $this->menu = array(
        array('label' => 'View my Profile', 'url' => array('user/index')),
        array('label' => 'Update my Profile', 'url' => array('user/update')),
        array('label' => 'Set Availability', 'url' => array('user/setAvailability')),
        array('label' => 'View Availability', 'url' => array('user/availability')),
        array('label' => 'Supervise Applications', 'url' => array('user/superviseApplications')),
    );
    

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').toggle();
    return false;
});
$('.search-form form').submit(function(){
    $.fn.yiiGridView.update('application-grid', {
        data: $(this).serialize()
    });
    return false;
});
");
?>

<h1>Supervise Applications</h1>



<?php
   $id= yii::app()->user->user_id;
  

// echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">

</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'project_id'=>'application-grid',
    'dataProvider'=>$model->getSuperviseApplications(yii::app()->user->user_id),
    'columns'=>array(
        
        'staff_id',
        'project_id',        
        
        
       
    ),
    
)); ?>
