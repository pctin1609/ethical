<?php
/* @var $this ApplicationLowRiskController */

$this->breadcrumbs = array(
    'Application Low Risk',
);
?>
<script>
    function showHide() {
        var ele = document.getElementById("showHideDiv");
        if(ele.style.display == "block") {
            ele.style.display = "none";
        }
        else {
            ele.style.display = "block";
        }
    }
</script>
<p></p>
<div id="formlist">
    <p><input type="button" value="Low Risk Research" style="width:100%;" onclick="return showHide();" /></p>

    <div id="showHideDiv" class = "show"><strong>Instructions to complete Low Risk Research </strong><p>This form is intended to be used for all research where the foreseeable risk to participants
            is no more than one of discomfort for example:</p>
        <ul>
            <li>The questionnaires and/or survey; </li>
            <li> Anonymous;  </li>
            <li> Online; </li>
            <li> Involving non-sensitive topics and observations;  </li>
            <li> With or without consent. </li>
        </ul><br/>
        <p class="form_link"><?php echo CHtml::link('Proceed >>', array('applicationLowRisk/selectSupervisor&id=1')); ?></p>
    </div>
</div>
