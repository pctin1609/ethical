<?php
/* @var $this FunderController */
/* @var $model Funder */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'funder-Funder-form',
        'enableAjaxValidation' => false,
    ));

    yii::app()->clientScript->registerScript(" ", "
    var index = 0;
    $('#addFunder').click(function(){
        $.ajax({
            url: '{$this->createUrl('application/funderrow')}&index='+(++index),
        }).done(function(data) {
            $('#tbl_funder').append(data);

            
        $('.remove').click(function (){
        $('#'+$(this).data('id')).detach();
    
    })
        })
        
        
    });

    $('.remove').click(function (){
    $('#'+$(this).data('id')).detach();
    
    });






    ");
    ?>

    <p class="note">all fields are mandatory unless stated optional</p>
    <table id='tbl_funder'>
        <tr class="funder">
            <th>Name of Organisation / funding agency</th>
            <th>Application ID (if applicable)</th>
            <th>funding proposal attachment </th>
            <th>Is funding approved?</th>
            <th>remove funder</th>


        </tr>
        <?php for ($index = 0; $index < count($models); $index++): ?>
            <?php $this->renderPartial('rowFunder', array('index' => $index,'form'=>$form,'model'=>$models[$index],'forml'=>$form)); ?>
        <?php endfor; ?>



    </table>
    <div class="row buttons">
    <td><input class="remove" type="button" value="remove" data-id="tbl_row<?php echo $index;?>" ></td>
    </div>


    <div class="row buttons">
        <?php echo CHtml::submitButton('Submit'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->