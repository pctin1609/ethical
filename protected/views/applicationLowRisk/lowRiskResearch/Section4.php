<?php
/* @var $this fm1_section1Controller */
/* @var $model fm1_section1 */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'fm1-section1-section1-form',
        'enableAjaxValidation' => false,
            ));
    ?>
    <?php echo " <h2>Section4 - Project details </h2>" ?>
    <p class="note">Fields with * are required.</p>

    <div class="row">
        <p>Plain language descriptions .</p>

        <?php echo $form->labelEx($model, 'description'); ?>
        <?php echo $form->textArea($model, 'description', array('size' => '100%', 'maxlength' => 250, 'style' => 'width: 100%')); ?>
        <?php echo $form->error($model, 'description'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'aim'); ?>
        <?php echo $form->textArea($model, 'aim', array('size' => '100%', 'maxlength' => 250, 'style' => 'width: 100%')); ?>
        <?php echo $form->error($model, 'aim'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'type1_research'); ?>
        <?php echo $form->textArea($model, 'type1_research', array('size' => '100%', 'maxlength' => 250, 'style' => 'width: 100%')); ?>
        <?php echo $form->error($model, 'type1_research'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'type2_research'); ?>
        <?php echo $form->dropDownList($model, 'type2_research', $model->getResearchTypes()); ?>
        <?php echo $form->error($model, 'type2_research'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model, 'research_funding'); ?>
        <?php echo $form->dropDownList($model, 'research_funding', $model->getFundingTypes()); ?>
        <?php echo $form->error($model, 'research_funding'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model, 'researcher_finance_involvement'); ?>
        <?php echo $form->textArea($model, 'researcher_finance_involvement', array('size' => '100%', 'maxlength' => 250, 'style' => 'width: 100%')); ?>
        <?php echo $form->error($model, 'researcher_finance_involvement'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Previous', array('name' => 'previous')); ?>
        <?php echo CHtml::submitButton('Save', array('name' => 'save')); ?>
        <?php echo CHtml::submitButton('Next', array('name' => 'next')) ?>

    </div>


    <?php $this->endWidget(); ?>

</div><!-- form -->