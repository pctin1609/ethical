<?php
/* @var $this LowRiskResearchController */
/* @var $model VulnerableParticipants */
/* @var $form CActiveForm */
?>

<div class="form">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'vulnerable-participants-VulnerableParticipants-form',
        'enableAjaxValidation' => false,
            ));
    ?>
     
	<?php echo " <h2>Section 1 - Vulnerable Participants </h2>" ?>
 <p class="note"></p>
     <h7>Does your research include any of the following groups of participants?</h7>
    <div class="row">
        <?php echo $form->labelEx($model, 'children_young'); ?>
        <div class='radiobuttons'>
            <?php echo $form->radioButtonList($model, 'children_young', $model->getValues()); ?>
        </div>
        <?php echo $form->error($model, 'children_young'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'impaired_capacity'); ?>
        <div class='radiobuttons'>
            <?php echo $form->radioButtonList($model, 'impaired_capacity', $model->getValues()); ?>
        </div>
        <?php echo $form->error($model, 'impaired_capacity'); ?>
    
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model, 'forensic_patient'); ?>
        <div class='radiobuttons'>
            <?php echo $form->radioButtonList($model, 'forensic_patient', $model->getValues()); ?>
        </div>
        <?php echo $form->error($model, 'forensic_patient'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model, 'prisoners_parole'); ?>
        <div class='radiobuttons'>
            <?php echo $form->radioButtonList($model, 'prisoners_parole', $model->getValues()); ?>
        </div>
        <?php echo $form->error($model, 'prisoners_parole'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'examining_potentially'); ?>
        <div class='radiobuttons'>
            <?php echo $form->radioButtonList($model, 'examining_potentially', $model->getValues()); ?>
        </div>
        <?php echo $form->error($model, 'examining_potentially'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'seeking_disclosure'); ?>
        <div class='radiobuttons'>
            <?php echo $form->radioButtonList($model, 'seeking_disclosure', $model->getValues()); ?>
        </div>
        <?php echo $form->error($model, 'seeking_disclosure'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model, 'children_wards_of_the_state'); ?>
        <div class='radiobuttons'>
            <?php echo $form->radioButtonList($model, 'children_wards_of_the_state', $model->getValues()); ?>
        </div>
        <?php echo $form->error($model, 'children_wards_of_the_state'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'dependent_on_medical_care'); ?>
        <div class='radiobuttons'>
            <?php echo $form->radioButtonList($model, 'dependent_on_medical_care', $model->getValues()); ?>
        </div>
        <?php echo $form->error($model, 'dependent_on_medical_care'); ?>
    </div>


    <div class="row">
        <?php echo $form->labelEx($model, 'working_in_formalized_institutions'); ?>
        <div class='radiobuttons'>
            <?php echo $form->radioButtonList($model, 'working_in_formalized_institutions', $model->getValues()); ?>
        </div>
        <?php echo $form->error($model, 'working_in_formalized_institutions'); ?>
    </div> 


    <div class="row">
        <?php echo $form->labelEx($model, 'other_vulnerable_participants'); ?>
        <div class='radiobuttons'>
            <?php echo $form->radioButtonList($model, 'other_vulnerable_participants', $model->getValues()); ?>
        </div>
        <?php echo $form->error($model, 'other_vulnerable_participants'); ?>
    </div>


    <div class="row">
        <?php echo $form->labelEx($model, 'collectives'); ?>
        <div class='radiobuttons'>
            <?php echo $form->radioButtonList($model, 'collectives', $model->getValues()); ?>
        </div>
        <?php echo $form->error($model, 'collectives'); ?>
    </div>


    <div class="row">
        <?php echo $form->labelEx($model, 'aboriginal'); ?>
        <div class='radiobuttons'>
            <?php echo $form->radioButtonList($model, 'aboriginal', $model->getValues()); ?>
        </div>
        <?php echo $form->error($model, 'aboriginal'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'women_pregnant'); ?>
        <div class='radiobuttons'>
            <?php echo $form->radioButtonList($model, 'women_pregnant', $model->getValues()); ?>
        </div>
        <?php echo $form->error($model, 'women_pregnant'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'intellectual_disability'); ?>
        <div class='radiobuttons'>
            <?php echo $form->radioButtonList($model, 'intellectual_disability', $model->getValues()); ?>
        </div>
        <?php echo $form->error($model, 'intellectual_disability'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Save', array('name' => 'save')); ?>
        <?php echo CHtml::submitButton('Next', array('name' => 'next')) ?>

    </div>
<?php $this->endWidget(); ?>

</div><!-- form -->