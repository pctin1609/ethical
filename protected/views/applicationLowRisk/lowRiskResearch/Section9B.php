<?php
/* @var $this OrganisationInvolvedInfoController */
/* @var $model OrganisationInvolvedInfo */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'organisation-involved-info-Section6B-form',
	'enableAjaxValidation'=>false,
)); ?>

	

<h2>Section 9b</h2>

	<div class="row">
		<?php echo $form->labelEx($model,'involve_collection'); ?>
                        <div class='radiobuttons'>

		<?php echo $form->radioButtonList($model,'involve_collection',$model->getValues()); ?>
                             </div>
		<?php echo $form->error($model,'involve_collection'); ?>
	</div>


	<div class="row">
		<?php echo $form->labelEx($model,'collection_without_consent'); ?>
                        <div class='radiobuttons'>

		<?php echo $form->radioButtonList($model,'collection_without_consent',$model->getValues()); ?>
                             </div>
		<?php echo $form->error($model,'collection_without_consent'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'collection_identifiable'); ?>
                        <div class='radiobuttons'>

		<?php echo $form->radioButtonList($model,'collection_identifiable',$model->getValues()); ?>
                             </div>
		<?php echo $form->error($model,'collection_identifiable'); ?>
	</div>
	

	<div class="row">
		<?php echo $form->labelEx($model,'identifiable_information'); ?>
                        <div class='radiobuttons'>

		<?php echo $form->radioButtonList($model,'identifiable_information',$model->getValues()); ?>
                             </div>
		<?php echo $form->error($model,'identifiable_information'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'disclosure_of_identifiable_information'); ?>
                        <div class='radiobuttons'>

		<?php echo $form->radioButtonList($model,'disclosure_of_identifiable_information',$model->getValues()); ?>
                             </div>
		<?php echo $form->error($model,'disclosure_of_identifiable_information'); ?>
	</div>

	<h7>9.8a From which of the following sources will information be collected? </h7>
	
	<div class="checkboxes">
		<?php echo $form->checkBox($model,'collect_info_from_third_party',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'collect_info_from_third_party'); ?>
		<?php echo $form->error($model,'collect_info_from_third_party'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'victorian_private',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'victorian_private'); ?>
		<?php echo $form->error($model,'victorian_private'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'organisation_other',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'organisation_other'); ?>
		<?php echo $form->error($model,'organisation_other'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'a_data_set',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'a_data_set'); ?>
		<?php echo $form->error($model,'a_data_set'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'victorian_govt_depart',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'victorian_govt_depart'); ?>
		<?php echo $form->error($model,'victorian_govt_depart'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'victorian_source',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'victorian_source'); ?>
		<?php echo $form->error($model,'victorian_source'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'commonwealth_agency',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'commonwealth_agency'); ?>
		<?php echo $form->error($model,'commonwealth_agency'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'agency_from_another_state',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'agency_from_another_state'); ?>
		<?php echo $form->error($model,'agency_from_another_state'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'organization_defined',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'organization_defined'); ?>
		<?php echo $form->error($model,'organization_defined'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'individual_carer',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'individual_carer'); ?>
		<?php echo $form->error($model,'individual_carer'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'other_infor',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'other_infor'); ?>
		<?php echo $form->error($model,'other_infor'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'category'); ?>
			<?php echo $form->textArea($model,'category',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		
		<?php echo $form->error($model,'category'); ?>
	</div>

	
	<div class="row">
		<?php echo $form->labelEx($model,'information_of_records'); ?>
			<?php echo $form->textArea($model,'information_of_records',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
	
		<?php echo $form->error($model,'information_of_records'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'agree_to_provide_information'); ?>
                        <div class='radiobuttons'>

		<?php echo $form->radioButtonList($model,'agree_to_provide_information',$model->getValues(),array('onchange' => 'menuTypeChange(this.value,"agree",vis="0");')); ?>
                             </div>
		<?php echo $form->error($model,'agree_to_provide_information'); ?>
	</div>
<div class="row"id ="agree" style="display: none">
		<?php echo $form->labelEx($model,'evidence'); ?>
		<?php echo $form->textArea($model,'evidence',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'evidence'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'explain_if_no'); ?>
		<?php echo $form->textArea($model,'explain_if_no',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'explain_if_no'); ?>
	</div>

	
	

	<div class="row">
		<?php echo $form->labelEx($model,'seperate_HREC_approval'); ?>
                        <div class='radiobuttons'>

		<?php echo $form->radioButtonList($model,'seperate_HREC_approval',$model->getValues()); ?>
                             </div>
		<?php echo $form->error($model,'seperate_HREC_approval'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'routinely_access_info'); ?>
                        <div class='radiobuttons'>

		<?php echo $form->radioButtonList($model,'routinely_access_info',$model->getValues()); ?>
                             </div>
		<?php echo $form->error($model,'routinely_access_info'); ?>
	</div>

<h7>9.8f What information will be collected? (Tick all boxes that apply)</h7>
	<div class="checkboxes">
		<?php echo $form->checkBox($model,'type_of_info',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'type_of_info'); ?>
		<?php echo $form->error($model,'type_of_info'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'sensitive_information',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'sensitive_information'); ?>
		<?php echo $form->error($model,'sensitive_information'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'health_information',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'health_information'); ?>
		<?php echo $form->error($model,'health_information'); ?>
	</div>
<h7>Type of organisation(s) involved :Health</h7>
	<div class="checkboxes">
		<?php echo $form->checkBox($model,'type_of_organisation_health',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'type_of_organisation_health'); ?>
		<?php echo $form->error($model,'type_of_organisation_health'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'victorian_private_sector',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'victorian_private_sector'); ?>
		<?php echo $form->error($model,'victorian_private_sector'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'commonwealth',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'commonwealth'); ?>
		<?php echo $form->error($model,'commonwealth'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'other_organisation',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'other_organisation'); ?>
		<?php echo $form->error($model,'other_organisation'); ?>
	</div>
<h7>Type of organisation(s) involved :Personal</h7>
	<div class="checkboxes">
		<?php echo $form->checkBox($model,'type_of_organisation_personal',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'type_of_organisation_personal'); ?>
		<?php echo $form->error($model,'type_of_organisation_personal'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'victor_private_sector',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'victor_private_sector'); ?>
		<?php echo $form->error($model,'victor_private_sector'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'common_public_sector',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'common_public_sector'); ?>
		<?php echo $form->error($model,'common_public_sector'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'other_personal',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'other_personal'); ?>
		<?php echo $form->error($model,'other_personal'); ?>
	</div>
<h7>Type of organisation(s) involved :Sensitive</h7>
	<div class="checkboxes">
		<?php echo $form->checkBox($model,'type_of_organisation_sensitive',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'type_of_organisation_sensitive'); ?>
		<?php echo $form->error($model,'type_of_organisation_sensitive'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'vic_private_sector',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'vic_private_sector'); ?>
		<?php echo $form->error($model,'vic_private_sector'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'com_public_sector',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'com_public_sector'); ?>
		<?php echo $form->error($model,'com_public_sector'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'other_senstive',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'other_senstive'); ?>
		<?php echo $form->error($model,'other_senstive'); ?>
	</div>
<h7>Privacy Principle(s):Health</h7>
	<div class="checkboxes">
		<?php echo $form->checkBox($model,'privacy_principle_health',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'privacy_principle_health'); ?>
		<?php echo $form->error($model,'privacy_principle_health'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'HPP1_health',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'HPP1_health'); ?>
		<?php echo $form->error($model,'HPP1_health'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'IPP11_health',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'IPP11_health'); ?>
		<?php echo $form->error($model,'IPP11_health'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'NPP1_health',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'NPP1_health'); ?>
		<?php echo $form->error($model,'NPP1_health'); ?>
	</div>
<h7>Privacy Principle(s):Personal</h7>
	<div class="checkboxes">
		<?php echo $form->checkBox($model,'privacy_principle_personal',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'privacy_principle_personal'); ?>
		<?php echo $form->error($model,'privacy_principle_personal'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'npp1_personal',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'npp1_personal'); ?>
		<?php echo $form->error($model,'npp1_personal'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'ipp11_personal',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'ipp11_personal'); ?>
		<?php echo $form->error($model,'ipp11_personal'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'npp10_personal',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'npp10_personal'); ?>
		<?php echo $form->error($model,'npp10_personal'); ?>
	</div>
<h7>Privacy Principle(s):Sensitive</h7>
	<div class="checkboxes">
		<?php echo $form->checkBox($model,'privacy_principle_sensitive',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'privacy_principle_sensitive'); ?>
		<?php echo $form->error($model,'privacy_principle_sensitive'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'npp0_sensitive',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'npp0_sensitive'); ?>
		<?php echo $form->error($model,'npp0_sensitive'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'ipp11_senstive',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'ipp11_senstive'); ?>
		<?php echo $form->error($model,'ipp11_senstive'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'npp10_sensitive',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'npp10_sensitive'); ?>
		<?php echo $form->error($model,'npp10_sensitive'); ?>
	</div>
<div class="row">
		<?php echo $form->labelEx($model,'de_intefied_form'); ?>
		<?php echo $form->textArea($model,'de_intefied_form',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'de_intefied_form'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'not_obtain_consent'); ?>
		<?php echo $form->textArea($model,'not_obtain_consent',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		
		<?php echo $form->error($model,'not_obtain_consent'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'reasons'); ?>
		<?php echo $form->textArea($model,'reasons',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		
		
		<?php echo $form->error($model,'reasons'); ?>
	</div>
	
	<div class="row buttons">
		<?php echo CHtml::submitButton('Previous',array('name'=>'previous')); ?>
                <?php echo CHtml::submitButton('Save',array('name'=>'save')); ?>
                <?php echo CHtml::submitButton('Next'); ?>


	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->