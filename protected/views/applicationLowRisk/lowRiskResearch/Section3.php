<?php
/* @var $this ResearchProjectController */
/* @var $model ResearchProject */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'research-project-ResearchProject-form',
	'enableAjaxValidation'=>false,
)); ?>
	
	 <?php echo " <h2>Section 3</h2>" ?>
	<p class="note"></p>
	<h7>Do any of the following apply to your research project?</h7>
	<div class="row">
		<?php echo $form->labelEx($model,'reimbursement'); ?>
		<div class='radiobuttons'>
		<?php echo $form->radioButtonList($model,'reimbursement',$model->getValues()); ?>
		</div>
		<?php echo $form->error($model,'reimbursement'); ?>
	</div>
	
        <div class="row">
		<?php echo $form->labelEx($model,'financial_involvement'); ?>
		<div class='radiobuttons'>
		<?php echo $form->radioButtonList($model,'financial_involvement',$model->getValues()); ?>
		</div>
		<?php echo $form->error($model,'financial_involvement'); ?>
	</div>

        <div class="row">
		<?php echo $form->labelEx($model,'conflict_of_interest'); ?>
		<div class='radiobuttons'>
		<?php echo $form->radioButtonList($model,'conflict_of_interest',$model->getValues()); ?>
		</div>
		<?php echo $form->error($model,'conflict_of_interest'); ?>
	</div>

	
    <div class="row">
		<?php echo $form->labelEx($model,'use_of_NCIS'); ?>
		<div class='radiobuttons'>
		<?php echo $form->radioButtonList($model,'use_of_NCIS',$model->getValues()); ?>
		</div>
		<?php echo $form->error($model,'use_of_NCIS'); ?>
	</div>

    <div class="row buttons">
    <?php echo CHtml::submitButton('Previous', array('name' => 'previous')); ?>
    <?php echo CHtml::submitButton('Save', array('name' => 'save')); ?>
    <?php echo CHtml::submitButton('Next', array('name' => 'next')) ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->