<?php
/* @var $this fm1_section3Controller */
/* @var $model fm1_section3 */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'fm1-section3-section3-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <?php echo " <h2>Section 6 - Collection of data: risks and procedures </h2>" ?>

	

    <div class="row">
        
        <?php echo $form->labelEx($model, 'participation_risk_involved'); ?>
        <div class='radiobuttons'>

            <?php echo $form->radioButtonList($model, 'participation_risk_involved', $model->getValues(), array('onchange' => 'menuTypeChange(this.value,"risk",vis="0");')); ?>
        </div>
        <?php echo $form->error($model, 'participation_risk_involved'); ?>
    </div>

    <div class="row" id="risk" id="consent" style="display: none" >
        <?php echo $form->labelEx($model, 'participation_risk_description'); ?>
        <?php echo $form->textarea($model, 'participation_risk_description', array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
        <?php echo $form->error($model, 'participation_risk_description'); ?>
    </div>

    <div class="row" >
     <?php echo $form->labelEx($model, 'risk_described_in_consent_form'); ?>

        <div class='radiobuttons'>
      <?php echo $form->radioButtonList($model, 'risk_described_in_consent_form',$model->getValues(), array('onchange' => 'menuTypeChange(this.value,"consent",vis="1");')); ?>
        </div>
        <?php echo $form->error($model, 'risk_described_in_consent_form'); ?>
    </div>

    <div class="row" id="consent" style="display: none">
        <?php echo $form->labelEx($model, 'risk_not_outlined_reason'); ?>
        <?php echo $form->textArea($model, 'risk_not_outlined_reason', array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
        <?php echo $form->error($model, 'risk_not_outlined_reason'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'risk_mitigation_plans'); ?>
        <?php echo $form->textArea($model, 'risk_mitigation_plans',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
        <?php echo $form->error($model, 'risk_mitigation_plans'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'emergency_procedures'); ?>
        <?php echo $form->textArea($model, 'emergency_procedures', array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
        <?php echo $form->error($model, 'emergency_procedures'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'researcher_risk_mitigation_plans'); ?>
        <?php echo $form->textArea($model, 'researcher_risk_mitigation_plans', array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
        <?php echo $form->error($model, 'researcher_risk_mitigation_plans'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'report_discoveries'); ?>
        <?php echo $form->textArea($model, 'report_discoveries', array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
        <?php echo $form->error($model, 'report_discoveries'); ?>
    </div>


    <div class="row buttons">
        <?php echo CHtml::submitButton('Previous', array('name' => 'previous')); ?>
        <?php echo CHtml::submitButton('Save', array('name' => 'save')); ?>
        <?php echo CHtml::submitButton('Next'); ?>


    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->