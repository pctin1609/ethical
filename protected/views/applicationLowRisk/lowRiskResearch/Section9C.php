<?php
/* @var $this DisclosureOfInformationController */
/* @var $model DisclosureOfInformation */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'disclosure-of-information-Section6C-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note"></p>

	<h2>Section 9c</h2>

<h7>9.9 Use of Information (Only answer this question if the project involves the use of identified (or potentially identifiable) information without the consent of the individual whose information it is (or their legal guardian).
What information will be used? (Tick all boxes that apply). <br>9.9a What information will be collected:</br> <br> Type of information</br></h7>
	<div class="checkboxes">
		<?php echo $form->checkBox($model,'information_type',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'information_type'); ?>
		<?php echo $form->error($model,'information_type'); ?>
	</div>

    <div class="checkboxes">
		<?php echo $form->checkBox($model,'sensi_information',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'sensi_information'); ?>
		<?php echo $form->error($model,'sensi_information'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'heat_information',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'heat_information'); ?>
		<?php echo $form->error($model,'heat_information'); ?>
	</div>
<h7>Type of organisation(s) involved: Health Information</h7>
	<div class="checkboxes">
		<?php echo $form->checkBox($model,'organisation_involved_health',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'organisation_involved_health'); ?>
		<?php echo $form->error($model,'organisation_involved_health'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'private_sector',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'private_sector'); ?>
		<?php echo $form->error($model,'private_sector'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'health_public_sector',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'health_public_sector'); ?>
		<?php echo $form->error($model,'health_public_sector'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'other_orgar',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'other_orgar'); ?>
		<?php echo $form->error($model,'other_orgar'); ?>
	</div>
<h7>Type of organisation(s) involved: Personal Information</h7>
	<div class="checkboxes">
		<?php echo $form->checkBox($model,'organisation_involved_personal',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'organisation_involved_personal'); ?>
		<?php echo $form->error($model,'organisation_involved_personal'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'victoria_private_sector',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'victoria_private_sector'); ?>
		<?php echo $form->error($model,'victoria_private_sector'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'victoria_commonwealth',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'victoria_commonwealth'); ?>
		<?php echo $form->error($model,'victoria_commonwealth'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'other_public_sector',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'other_public_sector'); ?>
		<?php echo $form->error($model,'other_public_sector'); ?>
	</div>
<h7>Type of organisation(s) involved: Sensitive Information</h7>
	<div class="checkboxes">
		<?php echo $form->checkBox($model,'organisation_involved_sensitive',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'organisation_involved_sensitive'); ?>
		<?php echo $form->error($model,'organisation_involved_sensitive'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'vi_private_sector',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'vi_private_sector'); ?>
		<?php echo $form->error($model,'vi_private_sector'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'vi_commonwealth',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'vi_commonwealth'); ?>
		<?php echo $form->error($model,'vi_commonwealth'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'other_vi_public_sector',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'other_vi_public_sector'); ?>
		<?php echo $form->error($model,'other_vi_public_sector'); ?>
	</div>
<h7>Privacy Principle(s):Health Information</h7>
	<div class="checkboxes">
		<?php echo $form->checkBox($model,'principle_health',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'principle_health'); ?>
		<?php echo $form->error($model,'principle_health'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'hpp2_health',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'hpp2_health'); ?>
		<?php echo $form->error($model,'hpp2_health'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'ipp11_health',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'ipp11_health'); ?>
		<?php echo $form->error($model,'ipp11_health'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'npp2_other',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'npp2_other'); ?>
		<?php echo $form->error($model,'npp2_other'); ?>
	</div>
<h7>Privacy Principle(s):Personal Information</h7>
	<div class="checkboxes">
		<?php echo $form->checkBox($model,'principle_personal',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'principle_personal'); ?>
		<?php echo $form->error($model,'principle_personal'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'npp2_personal',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'npp2_personal'); ?>
		<?php echo $form->error($model,'npp2_personal'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'ipp11_personal',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'ipp11_personal'); ?>
		<?php echo $form->error($model,'ipp11_personal'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'np2_personal',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'np2_personal'); ?>
		<?php echo $form->error($model,'np2_personal'); ?>
	</div>
<h7>Privacy Principle(s):Senstive Information</h7>
	<div class="checkboxes">
		<?php echo $form->checkBox($model,'principle_sensitive',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'principle_sensitive'); ?>
		<?php echo $form->error($model,'principle_sensitive'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'npp_sensitive',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'npp_sensitive'); ?>
		<?php echo $form->error($model,'npp_sensitive'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'ipp_sensitive',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'ipp_sensitive'); ?>
		<?php echo $form->error($model,'ipp_sensitive'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'npp2_sensitive',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'npp2_sensitive'); ?>
		<?php echo $form->error($model,'npp2_sensitive'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'specific_purpose'); ?>
			<?php echo $form->textArea($model,'specific_purpose',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'specific_purpose'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'secondary_purpose'); ?>
                        <div class='radiobuttons'>

		<?php echo $form->radioButtonList($model,'secondary_purpose',$model->getValues(), array('onchange' => 'menuTypeChange(this.value,"sec",vis="0");')); ?>
                             </div>
		<?php echo $form->error($model,'secondary_purpose'); ?>
	</div>
	
	<div class="row" id ="sec" style="display: none">
		<?php echo $form->labelEx($model,'give_details'); ?>
		<?php echo $form->textArea($model,'give_details',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		
		<?php echo $form->error($model,'give_details'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'not_used_in_deidentified_form'); ?>
		<?php echo $form->textArea($model,'not_used_in_deidentified_form',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
	
		<?php echo $form->error($model,'not_used_in_deidentified_form'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'public_interest'); ?>
		<?php echo $form->textArea($model,'public_interest',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'public_interest'); ?>
	</div>
<h7>9.10 DISCLOSURE OF INFORMATION Only answer this question if the project involves the disclosure of identified (or potentially identifiable) information without the consent of the individual whose information it is (or their legal guardian).</h7><br>
	<div class="row">
		<?php echo $form->labelEx($model,'potentially_identifiable'); ?>
                        <div class='radiobuttons'>

		<?php echo $form->radioButtonList($model,'potentially_identifiable',$model->getValues()); ?>
                             </div>
		<?php echo $form->error($model,'potentially_identifiable'); ?>
	</div>
	

<h7>What information will be disclosed to the researchers? (Tick all boxes that apply)<br> Type of information:</h7>
	<div class="checkboxes">
		<?php echo $form->checkBox($model,'info',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'info'); ?>
		<?php echo $form->error($model,'info'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'sen_infor',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'sen_infor'); ?>
		<?php echo $form->error($model,'sen_infor'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'hea_infor',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'hea_infor'); ?>
		<?php echo $form->error($model,'hea_infor'); ?>
	</div>
<h7>Type of organisation(s) involved : Health Information</h7>
	<div class="checkboxes">
		<?php echo $form->checkBox($model,'organisation_health',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'organisation_health'); ?>
		<?php echo $form->error($model,'organisation_health'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'pri_sector',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'pri_sector'); ?>
		<?php echo $form->error($model,'pri_sector'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'pu_sector',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'pu_sector'); ?>
		<?php echo $form->error($model,'pu_sector'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'other_victorian',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'other_victorian'); ?>
		<?php echo $form->error($model,'other_victorian'); ?>
	</div>
<h7>Type of organisation(s) involved : Personal Information</h7>
	<div class="checkboxes">
		<?php echo $form->checkBox($model,'organisation_personal',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'organisation_personal'); ?>
		<?php echo $form->error($model,'organisation_personal'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'vate_sector',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'vate_sector'); ?>
		<?php echo $form->error($model,'vate_sector'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'vate_commonwealth',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'vate_commonwealth'); ?>
		<?php echo $form->error($model,'vate_commonwealth'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'other_vate',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'other_vate'); ?>
		<?php echo $form->error($model,'other_vate'); ?>
	</div>
<h7>Type of organisation(s) involved : Sensitive Information</h7>
	<div class="checkboxes">
		<?php echo $form->checkBox($model,'organisation_sensitive',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'organisation_sensitive'); ?>
		<?php echo $form->error($model,'organisation_sensitive'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'prite_sector',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'prite_sector'); ?>
		<?php echo $form->error($model,'prite_sector'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'commu_public_sector',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'commu_public_sector'); ?>
		<?php echo $form->error($model,'commu_public_sector'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'other_wealth',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'other_wealth'); ?>
		<?php echo $form->error($model,'other_wealth'); ?>
	</div>
<h7> Privacy Principle(s) : Health Information</h7>
	<div class="checkboxes">
		<?php echo $form->checkBox($model,'privacy_health',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'privacy_health'); ?>
		<?php echo $form->error($model,'privacy_health'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'h_health',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'h_health'); ?>
		<?php echo $form->error($model,'h_health'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'ip_health',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'ip_health'); ?>
		<?php echo $form->error($model,'ip_health'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'np_health',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'np_health'); ?>
		<?php echo $form->error($model,'np_health'); ?>
	</div>
<h7> Privacy Principle(s) : Personal Information</h7>
	<div class="checkboxes">
		<?php echo $form->checkBox($model,'privacy_personal',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'privacy_personal'); ?>
		<?php echo $form->error($model,'privacy_personal'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'npp_personal1',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'npp_personal1'); ?>
		<?php echo $form->error($model,'npp_personal1'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'ipp_personal1',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'ipp_personal1'); ?>
		<?php echo $form->error($model,'ipp_personal1'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'n_personal1',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'n_personal1'); ?>
		<?php echo $form->error($model,'n_personal1'); ?>
	</div>
<h7> Privacy Principle(s) : Sensitive Information</h7>
	<div class="checkboxes">
		<?php echo $form->checkBox($model,'privacy_sensitive',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'privacy_sensitive'); ?>
		<?php echo $form->error($model,'privacy_sensitive'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'nppp_sensitive1',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'nppp_sensitive1'); ?>
		<?php echo $form->error($model,'nppp_sensitive1'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'ippp_sensitive1',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'ippp_sensitive1'); ?>
		<?php echo $form->error($model,'ippp_sensitive1'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'nppp_senstive1',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'nppp_senstive1'); ?>
		<?php echo $form->error($model,'nppp_senstive1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'organisations_list'); ?>
		<?php echo $form->textArea($model,'organisations_list',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'organisations_list'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'disclosed_by_researcher'); ?>
                        <div class='radiobuttons'>

		<?php echo $form->radioButtonList($model,'disclosed_by_researcher',$model->getValues()); ?>
                             </div>
		<?php echo $form->error($model,'disclosed_by_researcher'); ?>
	</div>
<h7>Will identified (or potentially identifiable) information be disclosed by the researcher to other organisations? <br> What information will be disclosed by the researcher? (Tick all boxes that apply) </br> Type of Information:</h7>
	
	<div class="checkboxes">
		<?php echo $form->checkBox($model,'information',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'information'); ?>
		<?php echo $form->error($model,'information'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'se_information',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'se_information'); ?>
		<?php echo $form->error($model,'se_information'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'he_information',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'he_information'); ?>
		<?php echo $form->error($model,'he_information'); ?>
	</div>
<h7>Type of organisation(s) involved : Health Information</h7>
	<div class="checkboxes">
		<?php echo $form->checkBox($model,'orga_involved_health',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'orga_involved_health'); ?>
		<?php echo $form->error($model,'orga_involved_health'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'victorian_private_sec',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'victorian_private_sec'); ?>
		<?php echo $form->error($model,'victorian_private_sec'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'commonwealth_pub_sector',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'commonwealth_pub_sector'); ?>
		<?php echo $form->error($model,'commonwealth_pub_sector'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'other_involved_health',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'other_involved_health'); ?>
		<?php echo $form->error($model,'other_involved_health'); ?>
	</div>
<h7>Type of organisation(s) involved : Personal Information</h7>
	<div class="checkboxes">
		<?php echo $form->checkBox($model,'orga_involved_personal',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'orga_involved_personal'); ?>
		<?php echo $form->error($model,'orga_involved_personal'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'victorian_pvt',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'victorian_pvt'); ?>
		<?php echo $form->error($model,'victorian_pvt'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'commonwealth_pvt',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'commonwealth_pvt'); ?>
		<?php echo $form->error($model,'commonwealth_pvt'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'other_victorian_pvt',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'other_victorian_pvt'); ?>
		<?php echo $form->error($model,'other_victorian_pvt'); ?>
	</div>
<h7>Type of organisation(s) involved : Senstive Information</h7>
	<div class="checkboxes">
		<?php echo $form->checkBox($model,'orga_involved_sensitive',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'orga_involved_sensitive'); ?>
		<?php echo $form->error($model,'orga_involved_sensitive'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'victorian_pvt_sensitive',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'victorian_pvt_sensitive'); ?>
		<?php echo $form->error($model,'victorian_pvt_sensitive'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'commonwealth_pvt_sensitive',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'commonwealth_pvt_sensitive'); ?>
		<?php echo $form->error($model,'commonwealth_pvt_sensitive'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'other_victorian_sensitive_pvt',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'other_victorian_sensitive_pvt'); ?>
		<?php echo $form->error($model,'other_victorian_sensitive_pvt'); ?>
	</div>
<h7>Privacy Principle(s) : Health Information</h7>
	<div class="checkboxes">
		<?php echo $form->checkBox($model,'principles_health',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'principles_health'); ?>
		<?php echo $form->error($model,'principles_health'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'pvt_hpp2_health',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'pvt_hpp2_health'); ?>
		<?php echo $form->error($model,'pvt_hpp2_health'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'pvt_ipp11_health',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'pvt_ipp11_health'); ?>
		<?php echo $form->error($model,'pvt_ipp11_health'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'pvt_npp2_health',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'pvt_npp2_health'); ?>
		<?php echo $form->error($model,'pvt_npp2_health'); ?>
	</div>
<h7>Privacy Principle(s) : Personal Information</h7>
	<div class="checkboxes">
		<?php echo $form->checkBox($model,'principles_personal',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'principles_personal'); ?>
		<?php echo $form->error($model,'principles_personal'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'pvt_hpp2_personal',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'pvt_hpp2_personal'); ?>
		<?php echo $form->error($model,'pvt_hpp2_personal'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'pvt_ipp11_personal',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'pvt_ipp11_personal'); ?>
		<?php echo $form->error($model,'pvt_ipp11_personal'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'pvt_npp2_personal',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'pvt_npp2_personal'); ?>
		<?php echo $form->error($model,'pvt_npp2_personal'); ?>
	</div>
<h7>Privacy Principle(s) : Senstive Information</h7>
	<div class="checkboxes">
		<?php echo $form->checkBox($model,'principles_sensitive',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'principles_sensitive'); ?>
		<?php echo $form->error($model,'principles_sensitive'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'pvt_hpp2_sensitive',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'pvt_hpp2_sensitive'); ?>
		<?php echo $form->error($model,'pvt_hpp2_sensitive'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'pvt_ipp11_sensitive',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'pvt_ipp11_sensitive'); ?>
		<?php echo $form->error($model,'pvt_ipp11_sensitive'); ?>
	</div>

	<div class="checkboxes">
		<?php echo $form->checkBox($model,'pvt_npp2_sensitive',array('value'=>1, 'uncheckValue'=>0)); ?>
		<?php echo $form->labelEx($model,'pvt_npp2_sensitive'); ?>
		<?php echo $form->error($model,'pvt_npp2_sensitive'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'info_disclosed'); ?>
			<?php echo $form->textArea($model,'info_disclosed',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		
		<?php echo $form->error($model,'info_disclosed'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'info_not_used'); ?>
			<?php echo $form->textArea($model,'info_not_used',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		
		<?php echo $form->error($model,'info_not_used'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'explain_consent'); ?>
			<?php echo $form->textArea($model,'explain_consent',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		
		<?php echo $form->error($model,'explain_consent'); ?>
	</div>
	


	<div class="row buttons">
		<?php echo CHtml::submitButton('Previous',array('name'=>'previous')); ?>
                <?php echo CHtml::submitButton('Save',array('name'=>'save')); ?>
                <?php echo CHtml::submitButton('Next'); ?>


	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->