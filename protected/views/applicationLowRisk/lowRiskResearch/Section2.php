<?php
/* @var $this TypeOfActivitiesController */
/* @var $model TypeOfActivities */
/* @var $form CActiveForm */
?>

<div class="form">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'type-of-activities-TypeOfActivities-form',
        'enableAjaxValidation' => false,
            ));
    ?>
   <p class="note"></p>
  <?php echo " <h2>Section 2 - Types Of Activities</h2>" ?>
  <h7>Does your research involve any of the following types of activities? </h7><br>

    <div class="row">
        <?php echo $form->labelEx($model, 'toxins'); ?>
        <div class='radiobuttons'>
            <?php echo $form->radioButtonList($model, 'toxins', $model->getValues()); ?>
        </div>
        <?php echo $form->error($model, 'toxins'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'use_without_consent'); ?>
        <div class='radiobuttons'>
            <?php echo $form->radioButtonList($model, 'use_without_consent', $model->getValues()); ?>
        </div>
        <?php echo $form->error($model, 'use_without_consent'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'causes_discomfort'); ?>
        <div class='radiobuttons'>
            <?php echo $form->radioButtonList($model, 'causes_discomfort', $model->getValues()); ?>
        </div>
        <?php echo $form->error($model, 'causes_discomfort'); ?>
    </div>


    <div class="row">
        <?php echo $form->labelEx($model, 'covert_observation'); ?>
        <div class='radiobuttons'>
            <?php echo $form->radioButtonList($model, 'covert_observation', $model->getValues()); ?>
        </div>
        <?php echo $form->error($model, 'covert_observation'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'sensitive_issues'); ?>
        <div class='radiobuttons'>
            <?php echo $form->radioButtonList($model, 'sensitive_issues', $model->getValues()); ?>
        </div>
        <?php echo $form->error($model, 'sensitive_issues'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'disclosure_of_info'); ?>
        <div class='radiobuttons'>
            <?php echo $form->radioButtonList($model, 'disclosure_of_info', $model->getValues()); ?>
        </div>
        <?php echo $form->error($model, 'disclosure_of_info'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'intrusive_techniques'); ?>
        <div class='radiobuttons'>
            <?php echo $form->radioButtonList($model, 'intrusive_techniques', $model->getValues()); ?>
        </div>
        <?php echo $form->error($model, 'intrusive_techniques'); ?>
    </div>


    <div class="row">
        <?php echo $form->labelEx($model, 'illegal_activities'); ?>
        <div class='radiobuttons'>
            <?php echo $form->radioButtonList($model, 'illegal_activities', $model->getValues()); ?>
        </div>
        <?php echo $form->error($model, 'illegal_activities'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'genetic_research'); ?>
        <div class='radiobuttons'>
            <?php echo $form->radioButtonList($model, 'genetic_research', $model->getValues()); ?>
        </div>
        <?php echo $form->error($model, 'genetic_research'); ?>
    </div>


    <div class="row">
        <?php echo $form->labelEx($model, 'xenotransplantation'); ?>
        <div class='radiobuttons'>
            <?php echo $form->radioButtonList($model, 'xenotransplantation', $model->getValues()); ?>
        </div>
        <?php echo $form->error($model, 'xenotransplantation'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'clinical_trials'); ?>
        <div class='radiobuttons'>
            <?php echo $form->radioButtonList($model, 'clinical_trials', $model->getValues()); ?>
        </div>
        <?php echo $form->error($model, 'clinical_trials'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'random_trials'); ?>
        <div class='radiobuttons'>
            <?php echo $form->radioButtonList($model, 'random_trials', $model->getValues()); ?>
        </div>
        <?php echo $form->error($model, 'random_trials'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'psychology_inventories'); ?>
        <div class='radiobuttons'>
            <?php echo $form->radioButtonList($model, 'psychology_inventories', $model->getValues()); ?>
        </div>
        <?php echo $form->error($model, 'psychology_inventories'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'radioactive_substances'); ?>
        <div class='radiobuttons'>
            <?php echo $form->radioButtonList($model, 'radioactive_substances', $model->getValues()); ?>
        </div>
        <?php echo $form->error($model, 'radioactive_substances'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'reproductive_technology'); ?>
        <div class='radiobuttons'>
            <?php echo $form->radioButtonList($model, 'reproductive_technology', $model->getValues()); ?>
        </div>
        <?php echo $form->error($model, 'reproductive_technology'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'innovative_therapy'); ?>
        <div class='radiobuttons'>
            <?php echo $form->radioButtonList($model, 'innovative_therapy', $model->getValues()); ?>
        </div>
        <?php echo $form->error($model, 'innovative_therapy'); ?>
    </div>


    <div class="row">
        <?php echo $form->labelEx($model, 'human_stem_cells'); ?>
        <div class='radiobuttons'>
            <?php echo $form->radioButtonList($model, 'human_stem_cells', $model->getValues()); ?>
        </div>
        <?php echo $form->error($model, 'human_stem_cells'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'human_tissue'); ?>
        <div class='radiobuttons'>
            <?php echo $form->radioButtonList($model, 'human_tissue', $model->getValues()); ?>
        </div>
        <?php echo $form->error($model, 'human_tissue'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'recombitant_DNA'); ?>
        <div class='radiobuttons'>
            <?php echo $form->radioButtonList($model, 'recombitant_DNA', $model->getValues()); ?>
        </div>
        <?php echo $form->error($model, 'recombitant_DNA'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Previous', array('name' => 'previous')); ?>
        <?php echo CHtml::submitButton('Save', array('name' => 'save')); ?>
        <?php echo CHtml::submitButton('Next', array('name' => 'next')) ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->