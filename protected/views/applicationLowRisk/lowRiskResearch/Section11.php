<?php
/* @var $this EthicalController */
/* @var $model Ethical */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'ethical-Section10-form',
	'enableAjaxValidation'=>false,
)); ?>

    <h2> Section 11 - Other Ethical Issues</h2>
	<p class="note">All fields are mandatory.</p>

	<div class="row">
		<?php echo $form->labelEx($model,'issues'); ?>
		  <?php echo $form->textArea($model, 'issues',array('size'=>'100%','maxlength'=>250, 'style'=>'width: 100%')); ?>
		<?php echo $form->error($model,'issues'); ?>
	</div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Previous', array('name' => 'previous')); ?>
        <?php echo CHtml::submitButton('Save', array('name' => 'save')); ?>
        <?php echo CHtml::submitButton('Next'); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->