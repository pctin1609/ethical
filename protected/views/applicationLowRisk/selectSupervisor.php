<?php
/* @var $this ProfileController */
/* @var $model Profile */

$this->breadcrumbs=array(
	'applications'=>array('index'),
	'select supervisor',
);

$this->menu=array(
	array('label'=>'List Profile', 'url'=>array('index')),
	array('label'=>'Create Profile', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('profile-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>


<h1>Select a supervisor</h1>



<div class="search-form" style="display:block">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
<?php //echo "</br> you have chosen to do the following form: ".$formType ?>

<?php /* FORM */
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'supervisor-form',
    'enableAjaxValidation' => false,
 ));
?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'profile-grid',
        'dataProvider'=>$model->searchStaff(),
        //'filter'=>$model,
	'columns'=>array(
                'title',
		'first_name',
		'surname',
        	'user_name',
		'gender',
		'contact_number',
		'email',
              array(
                 // 'htmlOptions'=>array('width'=>'90px'),
                  

                  'name'=>'profile picture',
                    'type'=>'raw',
                    'value'=>'CHtml::image(Yii::app()->baseUrl."/images/profilePics/$data->profile_picture","profile picture",array("width"=>"90px","height"=>"90"))',
                ),	
      
       
              array(
           'class'=>'CCheckBoxColumn',
                  'selectableRows'=>'2',
           'id'=>'supervisors', // the columnID for getChecked
                              'header'=>'select as supervisor',

       ),
       
  
	),
)); 

         echo CHtml::submitButton('Next', array('id'=>'fSubmit'));
    
?>

<?php $this->endWidget(); ?>