<?php

/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - update';
$this->breadcrumbs=array(
	'update',
);
?>

<h1> Update profile </h1>

<h3>How to update study details:Steps</h3>

<ul>
 <li> Login your account. </li>
 <li> Click on profile tab, then update study details to the right of the page. </li>
 <li> You can update degree and current study. </li>
</ul>

