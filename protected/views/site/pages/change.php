<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Change';
$this->breadcrumbs=array(
	'Change',
);
?>

<h2>What Forms to Fill </h2>
<p><h5>This page describes which forms to fill.</h5></p>

<h4>Steps:</h4>

<ul>
 <li> Login your account. </li>
 <li> Hover over the Application Tab and select the Start New Application tab. </li>
 <li> Click on the title of the Form you wish to complete and read the instructions. </li>
 <li> Please Note.For Research involving Human Tissue and Research involving Radiation please fill in Form 1 first to avoid repetitive processes. </li>
 <li> Click on the proceed button to go to the select supervisor page. </li>
 <li> Click on the nex button to begin the Form. </li>
</u>