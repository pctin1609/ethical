<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - help';
$this->breadcrumbs=array(
	'help',
);
?>
<h1>Help</h1>
  <h3>How to register: Steps</h3>
 
  <ul>
   <li> Enter in your username (e.g demo).  </li>
   <li> Enter in your firstname (e.g Wayne). </li>
   <li> Enter in your surname (e.g Rooney). </li>
   <li> Choose your gender (e.g male). </li>
   <li> Enter in your contact number. </li>
   <li> Enter in your email address and confirm. </li>
   <li> Choose your password and When choosing your password it is a good idea to <br>
           Mix characters and numbers for example mu8go12ve and confirm. </li>
   <li> Choose whether you are staff or student. </li>
   <li> Enter the verification code to show if you are human.</li>
   <li> Finish the registration</b> by pressing the register button.</li>			
  </ul>
  
  
  <h3>Frequently Asked Questions</h3></center>
  
  <ul>
  <li><?php echo CHtml::link('Which Form Should Be Filled?', array('site/page&view=change')); ?></li>
  <li><?php echo CHtml::link('How Do I Update My Profile?', array('site/page&view=update')); ?></li>
 
  </ul>
