<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - About';
$this->breadcrumbs=array(
	'About',
);
?>
<h1>About us</h1>



<h2>Eagle Ethical Clearance</h2>
    
<p>
                                                 
As an educational and research institution, Monash recognises its responsibility to the community to 
ensure all research activities are conducted in accordance with the highest ethical standards and to 
ensure that staff and students are seen as positive role models amongst their peers and in the wider 
community. It is committed to protecting the rights, dignity, health, safety and privacy of the community, 
including research subjects, and to the welfare of animals and the integrity of the environment. 
While acknowledging all mandated requirements for secure data retention, protection of privacy and confidentiality,
 the University is also committed to promoting 
access to the benefits of research through collecting, storing and making research data accessible in such 
a way that it can be used in future by members of the research community and other interested parties.  By 
achieving the above, the University also aims to protect the health, safety, rights and academic freedom of 
researchers and the reputation of the University for excellence in research.  

Some subsets of research conducted at Monash will come under the purview of either an animal ethics committee
or the human research ethics committee while others will not. It is to be understood by all researchers that 
an approval by the appropriate ethics committee is a necessary but not sufficient assurance that the research
complies with ethical standards.  At all times all research will still require self reflection and review: 
while acknowledging that there are conflicting and contested interests, a researcher must choose where the 
greatest good or least harm may be done.
</p>
<h2>Eagle South Africa</h2>
<p>
    Eagle South Africa which was opened in 2001 is a campus under Eagle Australia.
 The South African campus has about a thousand students, private students and others 
 funded by government . Ethical clearance process for research at Eagle South Africa provides a research platform 
 for students who want to do research on human ethics. Staff and Students at Eagle South Africa 
 need an efficient business process for the ethical clearance application process. 
 An easy ethical clearance process should be designed from current complicated and static paper 
 forms into flexible, dynamic electronic forms. 
</p>



