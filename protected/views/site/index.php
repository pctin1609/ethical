<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;

?>

	
<!--<h1>Welcome to <i>Monash South Africa:Ethical Clearance</i></h1>-->


<h2>Ethical Clearance Process</h2>

<p>The Ethical Clearance Process is the process by which Researchers are vetted to find out if they are legible to conduct research with
 Monash University. This process is conducted by the MUHREC.</p>

<p>The Monash University Human Research Ethics Committee (MUHREC) reviews all research conducted by Monash University staff or students
 involving human participants&nbsp; and primarily considers issues that constitute integrity, respect for persons, beneficence, justice, 
 consent, research merit and safety. The primary role of MUHREC is to protect the welfare and the rights of participants
 in research and the primary responsibility of each member is to decide, independently, whether, in his or her opinion,
 the conduct of each research proposal will so protect participants.</p>


 





    