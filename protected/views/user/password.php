<?php
$this->pageTitle = Yii::app()->name . ' - Change Password';
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'profile-form',
        'enableAjaxValidation' => false, 'htmlOptions' => array('enctype' => 'multipart/form-data')
            ));
    ?>
    <?php if (app()->user->hasFlash('success')) { ?>
        <div class="flash-success">
            <?php echo app()->user->getFlash('success') ?>
        </div>
    <?php } ?>
    
    <h2>Change Password</h2>
    <p class="note">All fields are mandatory</p>

    <div class="row">
        <?php echo $form->labelEx($model, 'currentPassword'); ?>
        <?php echo $form->passwordField($model, 'currentPassword'); ?>
        <?php echo $form->error($model, 'currentPassword'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'newPassword'); ?>
        <?php echo $form->passwordField($model, 'newPassword', array('size' => 20, 'maxlength' => 20)); ?>
        <?php echo $form->error($model, 'newPassword'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'repeatPassword'); ?>
        <?php echo $form->passwordField($model, 'repeatPassword', array('size' => 20, 'maxlength' => 20)); ?>
        <?php echo $form->error($model, 'repeatPassword'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Save'); ?>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- form -->