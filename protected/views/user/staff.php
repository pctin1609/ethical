<div id = "square">
    <?php
    echo '<h2>Personal details</h2>',
    '<img src="' . Yii::app()->baseUrl . '/images/profilePics/' . $profile->profile_picture . ' "width="90" height="90"/>';


    $this->widget('zii.widgets.CDetailView', array(
        'cssFile' => Yii::app()->theme->baseUrl . '/css/detailedView.css',
        'data' => $profile,
        'attributes' => array(
            'user_name',
            'title',
            'first_name',
            'surname',
            'gender',
            'contact_number',
            'email',
            'designation',
        ),
    ));

    echo '<h2> Positions</h2>';
    $positions = $this->loadStaffPositions();
    ?>
</div>