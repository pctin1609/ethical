<div id = "square">
    <?php
    echo '<h2>Personal Details</h2>',
    '<img src="' . Yii::app()->baseUrl . '/images/profilePics/' . $profile->profile_picture . ' "width="90" height="90"/>';


    $this->widget('zii.widgets.CDetailView', array(
        'cssFile' => Yii::app()->theme->baseUrl . '/css/detailedView.css',
        'data' => $profile,
        'attributes' => array(
            'user_name',
            'title',
            'first_name',
            'surname',
            'gender',
            'contact_number',
            'email',
            'designation',
        ),
    ));



    $retrievedReseacher = Researcher::model()->findByPk($model->user_id);

    echo '<h2>Study Details</h2>';


    $this->widget('zii.widgets.CDetailView', array(
        'data' => $retrievedReseacher,
        'attributes' => array(
            array(
                'name' => 'degree_id',
                'value' => $retrievedReseacher->degree->name
            ),
            'student_number',
        ),
    ));
    ?>
</div>
