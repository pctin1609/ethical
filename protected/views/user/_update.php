
<h2>Update My profile</h2>

<?php


$this->pageTitle=Yii::app()->name . ' - update';

?>


<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'profile-form',
	'enableAjaxValidation'=>false,'htmlOptions' => array('enctype' => 'multipart/form-data')
            )); ?>

	<p class="note"> All fields are mandatory</p>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
			<?php echo $form->DropDownList($model,'title',$model->getTitle()); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>
        
        <div class="row">
		<?php echo $form->labelEx($model,'first_name'); ?>
		<?php echo $form->textField($model,'first_name',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'first_name'); ?>
	</div>

        
        
        <div class="row">
		<?php echo $form->labelEx($model,'surname'); ?>
		<?php echo $form->textField($model,'surname',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'surname'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'contact_number'); ?>
		<?php echo $form->textField($model,'contact_number'); ?>
		<?php echo $form->error($model,'contact_number'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>65)); ?>
		<?php echo $form->error($model,'email'); ?>
	
	</div>
        
            <div class='row'>
        <?php echo $form->labelEx($model, 'profile_picture'); ?>
        <?php echo CHtml::activeFileField($model, 'profile_picture'); ?>
        <?php echo $form->error($model, 'profile_picture'); ?>
             </div>
        
        
     
        

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
        </div>

<?php $this->endWidget(); ?>

</div><!-- form -->