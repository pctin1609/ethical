

<?php
$this->pageTitle = Yii::app()->name . ' - update';

yii::app()->clientScript->registerScript(" ", "
    var index = 100;
    $('#addFunder').click(function(){
        $.ajax({
            url: '{$this->createUrl('user/PostionRow')}&index='+(++index),
        }).done(function(data) {
            $('#tbl_funder').append(data);

            
        $('.remove').click(function (){

        $('#'+$(this).data('id')).detach();
    
    })
        })
        
        
    });

    $('.remove').click(function (){
    $('#'+$(this).data('id')).detach();
    
    });






    ");
?>





<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'profile-form',
        'enableAjaxValidation' => false, 'htmlOptions' => array('enctype' => 'multipart/form-data')
    ));
    ?>
    <h2>Update My profile</h2>

    <p class="note">All fields are mandatory</p>

    <div class="row">
        <?php echo $form->labelEx($profile, 'title'); ?>
        <?php echo $form->DropDownList($profile, 'title', $profile->getTitle()); ?>
        <?php echo $form->error($profile, 'title'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($profile, 'first_name'); ?>
        <?php echo $form->textField($profile, 'first_name', array('size' => 20, 'maxlength' => 20)); ?>
        <?php echo $form->error($profile, 'first_name'); ?>
    </div>



    <div class="row">
        <?php echo $form->labelEx($profile, 'surname'); ?>
        <?php echo $form->textField($profile, 'surname', array('size' => 20, 'maxlength' => 20)); ?>
        <?php echo $form->error($profile, 'surname'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($profile, 'contact_number'); ?>
        <?php echo $form->textField($profile, 'contact_number'); ?>
        <?php echo $form->error($profile, 'contact_number'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($profile, 'email'); ?>
        <?php echo $form->textField($profile, 'email', array('size' => 60, 'maxlength' => 65)); ?>
        <?php echo $form->error($profile, 'email'); ?>

    </div>

    <div class='row'>
        <?php echo $form->labelEx($profile, 'profile_picture'); ?>
        <?php echo CHtml::activeFileField($profile, 'profile_picture'); ?>
        <?php echo $form->error($profile, 'profile_picture'); ?>
    </div>
    <h2> Positions</h2>

    <table id='tbl_funder'>
        <tr class="funder">
            <th>Position Name</th>
            <th>Position level</th>
            <th>Department</th>
            <th>Remove position</th>


        </tr>
        <?php
        $index = 0;
        foreach ($positions as $position):
            ?>
            <tr id="tbl_row<?php echo $index; ?>"> 
                <?php echo'<td>' ?>
                <?php echo $form->textField($position, 'position_name'); ?>
                <?php echo $form->error($position, 'position_name'); ?>
                <?php echo'</td>' ?>

                <?php echo'<td>' ?>
                <?php echo $form->textField($position, 'position_level', array('size' => 2, 'maxlength' => 2)); ?>
                <?php echo $form->error($position, 'position_level'); ?>
                <?php echo'</td>' ?>

                <?php echo'<td>' ?>
                <?php echo $form->dropDownlist($position, 'department_name', $position->getDepartment()); ?>
                <?php echo $form->error($position, 'department_name'); ?>
                <?php echo'</td>' ?>

            <?php echo'<td>' ?>
            <input class="lol" type="button" value="Remove" data-id="tbl_row<?php echo $index; ?>" >
    <?php echo'</td>' ?>


    <?php $index++;
    echo '<tr>';
endforeach; ?>


    </table>

    <div class="row buttons">
    <?php echo CHtml::button('Add another Position', array('id' => 'addFunder')); ?>
    </div>

    <div class="row buttons">
    <?php echo CHtml::submitButton('Save'); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->