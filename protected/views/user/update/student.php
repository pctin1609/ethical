


<?php
$this->pageTitle = Yii::app()->name . ' - update';
?>


<div class="form">

<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'profile-form',
    'enableAjaxValidation' => false, 'htmlOptions' => array('enctype' => 'multipart/form-data')
        ));
?>
<h2>Update My profile</h2>
    <p class="note">All fields are mandatory</p>

    <div class="row">
        <?php echo $form->labelEx($profile, 'title'); ?>
        <?php echo $form->DropDownList($profile, 'title', $profile->getTitle()); ?>
        <?php echo $form->error($profile, 'title'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($profile, 'first_name'); ?>
        <?php echo $form->textField($profile, 'first_name', array('size' => 20, 'maxlength' => 20)); ?>
        <?php echo $form->error($profile, 'first_name'); ?>
    </div>



    <div class="row">
        <?php echo $form->labelEx($profile, 'surname'); ?>
        <?php echo $form->textField($profile, 'surname', array('size' => 20, 'maxlength' => 20)); ?>
        <?php echo $form->error($profile, 'surname'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($profile, 'contact_number'); ?>
        <?php echo $form->textField($profile, 'contact_number'); ?>
        <?php echo $form->error($profile, 'contact_number'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($profile, 'email'); ?>
        <?php echo $form->textField($profile, 'email', array('size' => 60, 'maxlength' => 65)); ?>
        <?php echo $form->error($profile, 'email'); ?>

    </div>

    <div class='row'>
        <?php echo $form->labelEx($profile, 'profile_picture'); ?>
        <?php echo CHtml::activeFileField($profile, 'profile_picture'); ?>
        <?php echo $form->error($profile, 'profile_picture'); ?>
    </div>
    
    <h2> Study Details </h2>
    <div class="row">
        <?php echo $form->labelEx($researcher, 'degree_id'); ?>
        <?php echo $form->dropDownlist($researcher, 'degree_id', CHtml::listData(Degree::model()->findAll(), 'id', 'name')); ?>
        <?php echo $form->error($researcher, 'degree_id'); ?>
    </div>


	<div class="row">
		<?php echo $form->labelEx($researcher,'student_number'); ?>
		<?php echo $form->textField($researcher,'student_number'); ?>
		<?php echo $form->error($researcher,'student_number'); ?>
	</div>




    <div class="row buttons">
        <?php echo CHtml::submitButton('Save'); ?>
    </div>

    <?php $this->endWidget(); ?>
    
</div><!-- form -->