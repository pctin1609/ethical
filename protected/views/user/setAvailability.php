<?php
/* @var $this StaffsetController */
/* @var $model Staffset */
/* @var $form CActiveForm */
?>
<?php if (Yii::app()->user->hasFlash('availability')): ?>

    <div class="flash-success">
        <?php echo Yii::app()->user->getFlash('availability'); ?>
  <?php else: ?>
<div class="form">

     
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'staffset-StaffAvailailability-form',
        'enableAjaxValidation' => false,
    ));
    ?>
    
    <h1> Set Unavailable Dates </h1>
    
   

    
    
    
    
    <p class="note">All fields are mandatory.</p>

    <div class="row">
        <?php echo $form->labelEx($model, 'unavailable_start_time'); ?>
        <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model' => $model,
            'attribute' => 'unavailable_start_time',
            'value' => $model->unavailable_start_time,
            'options' => array(
                'buttonText' => 'Select',
                'showAnim' => 'slide',
                'showButtonPanel' => true,
                'dateFormat' => 'yy-mm-dd',
            ),
        ));
        ?>
<?php echo $form->error($model, 'unavailablestarttime'); ?>

    </div>


    <div class="row">
        <?php echo $form->labelEx($model, 'unavailable_end_time'); ?>
        <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model' => $model,
            'attribute' => 'unavailable_end_time',
            'value' => $model->unavailable_end_time,
            'options' => array(
                'buttonImage' => Yii::app()->baseUrl . '/images/calendar.png',
                'buttomImageOnly' => true,
                'buttonText' => 'Select',
                'showAnim' => 'slide',
                'dateFormat' => 'yy-mm-dd',
            ),
        ));
        ?>
<?php echo $form->error($model, 'unavailable_end_time'); ?>

    </div>



    <div class="row buttons">
    <?php echo CHtml::submitButton('Save'); ?>

    </div>

    <?php $this->endWidget(); ?>
          <?php endif; ?>

</div><!-- form -->'
