
<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'researcher-form',
        'enableAjaxValidation' => false,
            ));
    ?>
        <div class="row">
            <?php echo $form->labelEx($model, 'degree_id'); ?>
            <?php echo $form->dropDownlist($model, 'degree_id', CHtml::listData(Degree::model()->findAll(), 'id', 'name')); ?>
            <?php echo $form->error($model, 'degree_id'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model, 'student_number'); ?>
            <?php echo $form->textField($model, 'student_number'); ?>
            <?php echo $form->error($model, 'student_number'); ?>
        </div>

        <div class="row buttons">
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
        </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->