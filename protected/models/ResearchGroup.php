
<?php
/**
 * This is the model class for table "research_group".
 *
 * The followings are the available columns in table 'research_group':
 * @property integer $section_id
 * @property integer $number_of_people
 * @property string $group_desc
 * @property string $age_range
 * @property string $criteria_procedure
 * @property string $work_time
 * @property string $human_participants_involved
 * @property string $incentive
 * @property string MBBS_attatchment
 * @property string $using_UPSPP
 * @property string $supporting_paragraph_for_using_UPSPP
 * @property string $using_monash_MBBS
 * @property string $include_exclude_status
 * @property string $participant_recruitment_detail
 * @property string $selection_method
 * @property string $method_of_obtaining_participants_contacts
 * @property string $using_previous_datasets
 * @property string $previous_ethical_approved
 * @property string $using_HT_HSC
 */
class ResearchGroup extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Researchgroup the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'research_group';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('age_range, criteria_procedure, work_time, selection_method', 'required','message'=>'this field can not be left blank'),
            array('number_of_people', 'numerical', 'integerOnly'=>true),
            array('group_desc, MBBS_attatchment,exclusion_desc, using_UPSPP, using_monash_MBBS, include_exclude_status', 'length', 'max'=>42),
            array('age_range,age_range2, criteria_procedure, human_participants_involved, incentive, selection_method', 'length', 'max'=>20),
            array('method_of_obtaining_participants_contacts, using_previous_datasets, previous_ethical_approved, using_HT_HSC', 'length', 'max'=>400),
            array('age_range2,reimbursement_desc,exclusion_desc,MBBS_attatchment ,supporting_paragraph_for_using_UPSPP, participant_recruitment_detail', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('section_id, number_of_people, group_desc, age_range,age_range2, criteria_procedure, work_time, human_participants_involved, incentive, MBBS_attatchment,exclusion_desc,using_UPSPP, supporting_paragraph_for_using_UPSPP, using_monash_MBBS, include_exclude_status, participant_recruitment_detail, selection_method, method_of_obtaining_participants_contacts, using_previous_datasets, previous_ethical_approved, using_HT_HSC', 'safe', 'on'=>'search'),
        );
    }
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'participants_group_id' => 'Participants Group',
			'number_of_people' => 'Number Of People',
			'group_desc' => 'Group Desc',
			'age_range' => 'Age Range',
			'age_range2' => 'If other add age range below:',
			'criteria_procedure' => '2.3a Give details of procedures involving participants.',
			'work_time' => '2.4 Describe how much time you are asking of participants in each group and when the time will be required.',
			'human_participants_involved' => '2.1 Does your research project involve the direct involvement or participation of human participants?',
			'incentive' => '2.5 Will you be offering reimbursement or any other incentives to participants?',
                        'MBBS_attatchment' =>'If Yes, please attach copy of the Monash MBBS Executive approval ',
                        'reimbursement_desc' => 'If Yes, how much and in what form will the reimbursement or incentive take',
			'exclusion_desc' => 'If Yes, please provide full details to explain each exclusion criteria for the group',
			'using_UPSPP' => '2.6a Will you be using Undergraduate Psychology Student participant Pool?This recruitment method is designed for low risk non-contentious research where the forseeable risk to participants is no more than discoumfort. If your research is higher risk or deals with contentious issues, you must justify the use of the poole research. Failure to provide a justification may delay the consideration of the project.(Note; this pool is only available to investigators/staff in the school of psychology and psychiatry) ',
			'supporting_paragraph_for_using_UPSPP' => 'If Yes, Write a paragraph supporting the use of this pool based on ethical grounds and what controls are in place to manage duty of care to participants and also describe why the research is low-risk and non contentious or provide a justification for the use of the pool',
			'using_monash_MBBS' => '2.6b Will your research involve the Monash MBBS Curriculum and its students?',
			'include_exclude_status' => 'In your research design, do you have any criteria for exclusion from your participants groups?',
			'participant_recruitment_detail' => '2.6d Please give in full step by step detail how you will recruit your participants and invite them to participate',
			'selection_method' => '2.6c Please explain how you will select participants in a group',
			'method_of_obtaining_participants_contacts' => ' Please specify if he research team is not obtaining participants contact details at any time during the research.',
			'project_id' => 'Project',
			'using_previous_datasets' => 'I am only using data collected for another purpose.',
			'previous_ethical_approved' => ' With previous ethics approval.Provide approval number if available Go to Section 4b',
			'using_HT_HSC' => 'Using Ht Hsc',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('participants_group_id',$this->participants_group_id,true);
		$criteria->compare('number_of_people',$this->number_of_people);
		$criteria->compare('group_desc',$this->group_desc,true);
		$criteria->compare('age_range',$this->age_range,true);
		$criteria->compare('age_range2',$this->age_range2,true);
		$criteria->compare('criteria_procedure',$this->criteria_procedure,true);
		$criteria->compare('work_time',$this->work_time,true);
		$criteria->compare('incentive',$this->incentive,true);
		$criteria->compare('reimbursement_desc',$this->reimbursement_desc,true);
		$criteria->compare('using_UPSPP',$this->using_UPSPP,true);
		$criteria->compare('supporting_paragraph_for_using_UPSPP',$this->supporting_paragraph_for_using_UPSPP,true);
		$criteria->compare('using_monash_MBBS',$this->using_monash_MBBS,true);
		$criteria->compare('include_exclude_status',$this->include_exclude_status,true);
                $criteria->compare('exclusion_desc',$this->exclusion_desc,true);
                $criteria->compare('MBBS_attatchment',$this->MBBS_attatchment,true);
		$criteria->compare('participant_recruitment_detail',$this->participant_recruitment_detail,true);
		$criteria->compare('selection_method',$this->selection_method,true);
		$criteria->compare('method_of_obtaining_participants_contacts',$this->method_of_obtaining_participants_contacts,true);
		$criteria->compare('project_id',$this->project_id);
		$criteria->compare('using_previous_datasets',$this->using_previous_datasets,true);
		$criteria->compare('previous_ethical_approved',$this->previous_ethical_approved,true);
		$criteria->compare('using_HT_HSC',$this->using_HT_HSC,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
public function getValues()
        {
            return array('Yes','No');
        }
	
 public function getInformation()
        {
            return array('1 - 10'=>'1 - 10','11 - 20'=>'11 - 20', '21 - 30'=>'21 - 30', '31 - 40'=>'31 - 40', '41 - 20'=>'41 - 20','21 - 60'=>'21 - 60','61 - 70'=>'61 - 70','71 - 80'=>'71 - 80', '80+'=>'80+','Other'=>'Other');
        }
 public function getAnswer()
        {
            return array('No, proceed to Q2.6c','yes');
        }
 public function getResponse()
        {
            return array('Yes'=>'Yes','No'=>'No');
       //, have you considered whether your research is Low Risk research and you could complete Form LR instead?
            }
 public function getData()
        {
            return array('with previous ethics approval - provide approval number if available Go to Section 4b','data was collected without ethics approval – Go to Q 2.1b ','I am using human tissues/fluids/stem cells – please complete Form HT - Application for Ethical Approval for the use of Human Tissue or Human Stem Cells ');
        }
}