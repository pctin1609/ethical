<?php

/**
 * This is the model class for table "radiation".
 *
 * The followings are the available columns in table 'radiation':
 * @property integer $form_id
 * @property integer $application_id
 * @property integer $section1_id
 * @property integer $section2_id
 * @property integer $section3_id
 * @property integer $section4_id
 * @property integer $section5_id
 * @property integer $section6_id
 * @property integer $section7_id
 * @property integer $section8_id
 * @property integer $section9_id
 * @property integer $section10_id
 * @property integer $section11_id
 * @property integer $section12_id
 */
class Radiation extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Radiation the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'radiation';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('application_id', 'required'),
            array('application_id, section1_id, section2_id, section3_id, section4_id, section5_id, section6_id, section7_id, section8_id, section9_id, section10_id, section11_id, section12_id', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('form_id, application_id, section1_id, section2_id, section3_id, section4_id, section5_id, section6_id, section7_id, section8_id, section9_id, section10_id, section11_id, section12_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'form_id' => 'Form',
            'application_id' => 'Application',
            'section1_id' => 'Section1',
            'section2_id' => 'Section2',
            'section3_id' => 'Section3',
            'section4_id' => 'Section4',
            'section5_id' => 'Section5',
            'section6_id' => 'Section6',
            'section7_id' => 'Section7',
            'section8_id' => 'Section8',
            'section9_id' => 'Section9',
            'section10_id' => 'Section10',
            'section11_id' => 'Section11',
            'section12_id' => 'Section12',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('form_id', $this->form_id);
        $criteria->compare('application_id', $this->application_id);
        $criteria->compare('section1_id', $this->section1_id);
        $criteria->compare('section2_id', $this->section2_id);
        $criteria->compare('section3_id', $this->section3_id);
        $criteria->compare('section4_id', $this->section4_id);
        $criteria->compare('section5_id', $this->section5_id);
        $criteria->compare('section6_id', $this->section6_id);
        $criteria->compare('section7_id', $this->section7_id);
        $criteria->compare('section8_id', $this->section8_id);
        $criteria->compare('section9_id', $this->section9_id);
        $criteria->compare('section10_id', $this->section10_id);
        $criteria->compare('section11_id', $this->section11_id);
        $criteria->compare('section12_id', $this->section12_id);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

}