<?php

/**
 * This is the model class for table "research_project".
 *
 * The followings are the available columns in table 'research_project':
 * @property integer $section_id
 * @property string $reimbursement
 * @property string $financial_involvement
 * @property string $conflict_of_interest
 * @property string $use_of_NCIS
 */
class ResearchProject extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ResearchProject the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'research_project';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('reimbursement, financial_involvement, conflict_of_interest, use_of_NCIS', 'required'),
			array('reimbursement, financial_involvement, conflict_of_interest, use_of_NCIS', 'length', 'max'=>250),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('section_id, reimbursement, financial_involvement, conflict_of_interest, use_of_NCIS', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'section_id' => 'Section',
			'reimbursement' => ' 3.1 Reimbursement or incentives to any participants of more than AUD$75',
			'financial_involvement' => '3.2 At least one of the researchers has a financial or other involvement in the research (apart from their research role) or may receive a reward, pecuniary, or otherwise',
			'conflict_of_interest' => '3.3 Any other potential conflict of interest (not mentioned above) for any of the researchers',
			'use_of_NCIS' => '3.4 The research project involves the use of the National Coronial Information System (NCIS)',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('section_id',$this->section_id);
		$criteria->compare('reimbursement',$this->reimbursement,true);
		$criteria->compare('financial_involvement',$this->financial_involvement,true);
		$criteria->compare('conflict_of_interest',$this->conflict_of_interest,true);
		$criteria->compare('use_of_NCIS',$this->use_of_NCIS,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function getValues()
        {
            return array('Yes','No');
        }
}