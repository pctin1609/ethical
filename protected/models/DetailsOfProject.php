<?php

/**
 * This is the model class for table "details_of_project".
 *
 * The followings are the available columns in table 'details_of_project':
 * @property integer $section_id
 * @property string $aim_research_project
 * @property string $details_of_procedures
 * @property string $research_ethics_committee
 * @property string $attach_letter_of_approval
 * @property string $primary_HREC
 * @property string $if_no_primary
 * @property string $HREC1
 * @property string $HREC1_yes
 * @property string $HREC1_no
 * @property string $HREC1_pending
 * @property string $HREC2
 * @property string $HREC2_yes
 * @property string $HREC2_no
 * @property string $HREC2_pending
 * @property string $HREC3
 * @property string $HREC3_yes
 * @property string $HREC3_no
 * @property string $HREC3_pending
 * @property string $HREC4
 * @property string $HREC4_yes
 * @property string $HREC4_no
 * @property string $HREC4_pending
 */
class DetailsOfProject extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DetailsOfProject the static model class
	 */
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'details_of_project';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('aim_research_project, details_of_procedures, research_ethics_committee, primary_HREC', 'required'),
			array('aim_research_project, attach_letter_of_approval, primary_HREC, HREC1, HREC1_yes, HREC1_no, HREC1_pending, HREC2, HREC2_yes, HREC2_no, HREC2_pending, HREC3, HREC3_yes, HREC3_no, HREC3_pending, HREC4, HREC4_yes, HREC4_no, HREC4_pending', 'length', 'max'=>250),
			
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('section_id, aim_research_project, details_of_procedures, research_ethics_committee, attach_letter_of_approval, primary_HREC,  HREC1, HREC1_yes, HREC1_no, HREC1_pending, HREC2, HREC2_yes, HREC2_no, HREC2_pending, HREC3, HREC3_yes, HREC3_no, HREC3_pending, HREC4, HREC4_yes, HREC4_no, HREC4_pending', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'section_id' => 'Section',
			'aim_research_project' => '1 Clearly state the aims and/or hypotheses of the research project',
			'details_of_procedures' => '1.1 Give details of procedures involving participants:',
			'research_ethics_committee' => '2 Please identify which human research ethics committee accepts primary responsibility for the scientific and technical assessment of the project and its ethical review.',
			'attach_letter_of_approval' => 'Please attach the Letter of Approval from the primary HREC',
			'primary_HREC' => '2.1 Please confirm that all researchers named in this form have been approved by the primary HREC:',
			
			'HREC1' => '3.1 Name of HREC1:',
			'HREC1_yes' => ' Yes',
			'HREC1_no' => 'No',
			'HREC1_pending' => 'Pending',
			'HREC2' => '3.2 Name of Hrec2',
			'HREC2_yes' => 'Yes',
			'HREC2_no' => 'No',
			'HREC2_pending' => 'Pending',
			'HREC3' => '3.3 Name of Hrec3',
			'HREC3_yes' => 'Yes',
			'HREC3_no' => 'No',
			'HREC3_pending' => 'Pending',
			'HREC4' => '3.4 Name of Hrec4',
			'HREC4_yes' => 'Yes',
			'HREC4_no' => 'No',
			'HREC4_pending' => 'Pending',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('section_id',$this->section_id);
		$criteria->compare('aim_research_project',$this->aim_research_project,true);
		$criteria->compare('details_of_procedures',$this->details_of_procedures,true);
		$criteria->compare('research_ethics_committee',$this->research_ethics_committee,true);
		$criteria->compare('attach_letter_of_approval',$this->attach_letter_of_approval,true);
		$criteria->compare('primary_HREC',$this->primary_HREC,true);
		
		$criteria->compare('HREC1',$this->HREC1,true);
		$criteria->compare('HREC1_yes',$this->HREC1_yes,true);
		$criteria->compare('HREC1_no',$this->HREC1_no,true);
		$criteria->compare('HREC1_pending',$this->HREC1_pending,true);
		$criteria->compare('HREC2',$this->HREC2,true);
		$criteria->compare('HREC2_yes',$this->HREC2_yes,true);
		$criteria->compare('HREC2_no',$this->HREC2_no,true);
		$criteria->compare('HREC2_pending',$this->HREC2_pending,true);
		$criteria->compare('HREC3',$this->HREC3,true);
		$criteria->compare('HREC3_yes',$this->HREC3_yes,true);
		$criteria->compare('HREC3_no',$this->HREC3_no,true);
		$criteria->compare('HREC3_pending',$this->HREC3_pending,true);
		$criteria->compare('HREC4',$this->HREC4,true);
		$criteria->compare('HREC4_yes',$this->HREC4_yes,true);
		$criteria->compare('HREC4_no',$this->HREC4_no,true);
		$criteria->compare('HREC4_pending',$this->HREC4_pending,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
  public function getValues()
        {
            return array('Yes','No');
        }
	}