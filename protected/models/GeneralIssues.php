<?php

/**
 * This is the model class for table "fm1_section6_d".
 *
 * The followings are the available columns in table 'fm1_section6_d':
 * @property integer $section_id
 * @property string $number_of_records
 * @property string $type_of_records
 * @property string $adoption_unique_identifiers
 * @property string $details_of_unique_identifiers
 * @property string $involve_transborder
 * @property string $details_of_transborder
 * @property string $period_of_time
 * @property string $security_arrangements
 * @property string $privacy_of_individuals
 * @property string $procedures_in_place
 * @property string $ethical_issues
 */
class GeneralIssues extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Fm1_section6d the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'general_issues';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('number_of_records, type_of_records, adoption_unique_identifiers,  involve_transborder,  period_of_time, security_arrangements, privacy_of_individuals, procedures_in_place, ethical_issues', 'required'),
			array('number_of_records, type_of_records, adoption_unique_identifiers, details_of_unique_identifiers, involve_transborder, details_of_transborder, period_of_time, security_arrangements, privacy_of_individuals, procedures_in_place, ethical_issues', 'length', 'max'=>100),
			array('details_of_unique_identifiers,details_of_transborder ', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('section_id, number_of_records, type_of_records, adoption_unique_identifiers, details_of_unique_identifiers, involve_transborder, details_of_transborder, period_of_time, security_arrangements, privacy_of_individuals, procedures_in_place, ethical_issues', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'section_id' => 'Section',
			'number_of_records' => 'Number Of Records',
			'type_of_records' => 'Type Of Records',
			'adoption_unique_identifiers' => '6.11b Does the project involve the adoption of unique identifiers assigned to individuals by other agencies or organisations? ',
			'details_of_unique_identifiers' => 'If Yes, give details of how this will be carried out in accordance with relevant Privacy Principles (e.g. HPP 7, VIPP 7 or NPP 7).',
			'involve_transborder' => '6.11c Does the project involve trans-border (i.e. interstate or overseas) data flow? Commonwealth ',
			'details_of_transborder' => 'If Yes, give details of how this will be carried out in accordance with relevant Privacy Principles (e.g. HPP 9, VIPP 9 or NPP 9).',
			'period_of_time' => '6.11d For what period of time will the information be retained? How will the information be disposed of at the end of this period?',
			'security_arrangements' => '6.11e Describe the security arrangements for storage of the information. Where will the information be stored? Who will have access to the information?',
			'privacy_of_individuals' => '6.11f How will the privacy of individuals be respected in any publication arising from this project?',
			'procedures_in_place' => '6.11g Are procedures in place to manage, monitor and report adverse and/or unforeseen events relating to the collection, use or disclosure of information?',
			'ethical_issues' => '6.11h Discuss any other ethical issues relevant to the collection, use or disclosure of information proposed in this project. Explain how these issues have been addressed.',
		
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('section_id',$this->section_id);
		$criteria->compare('number_of_records',$this->number_of_records,true);
		$criteria->compare('type_of_records',$this->type_of_records,true);
		$criteria->compare('adoption_unique_identifiers',$this->adoption_unique_identifiers,true);
		$criteria->compare('details_of_unique_identifiers',$this->details_of_unique_identifiers,true);
		$criteria->compare('involve_transborder',$this->involve_transborder,true);
		$criteria->compare('details_of_transborder',$this->details_of_transborder,true);
		$criteria->compare('period_of_time',$this->period_of_time,true);
		$criteria->compare('security_arrangements',$this->security_arrangements,true);
		$criteria->compare('privacy_of_individuals',$this->privacy_of_individuals,true);
		$criteria->compare('procedures_in_place',$this->procedures_in_place,true);
		$criteria->compare('ethical_issues',$this->ethical_issues,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
public function getValues()
        {
            return array('Yes','No');
        }
	}