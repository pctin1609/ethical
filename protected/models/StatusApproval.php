<?php

/**
 * This is the model class for table "status_approval".
 *
 * The followings are the available columns in table 'status_approval':
 * @property integer $section_id
 * @property string $approval_from
 * @property string $attach_letter
 * @property string $seek_approval
 * @property string $rac_approval
 * @property string $other
 * @property string $if_yes_specify
 */
class StatusApproval extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return StatusApproval the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'status_approval';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('approval_from, seek_approval, rac_approval, other', 'required'),
			array('approval_from, seek_approval, rac_approval, other', 'length', 'max'=>20),
			array('attach_letter, if_yes_specify', 'length', 'max'=>250),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('section_id, approval_from, attach_letter, seek_approval, rac_approval, other, if_yes_specify', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'section_id' => 'Section',
			'approval_from' => 'I have approval from DHS RAC',
			'attach_letter' => 'Please attach DHS RAC letter of approval and any other correspondence with DHS RAC related to this project',
			'seek_approval' => 'I am seeking approval from DHS RAC as part of this application',
			'rac_approval' => 'RAC approval is not required',
			'other' => 'Other',
			'if_yes_specify' => 'Please specify',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('section_id',$this->section_id);
		$criteria->compare('approval_from',$this->approval_from,true);
		$criteria->compare('attach_letter',$this->attach_letter,true);
		$criteria->compare('seek_approval',$this->seek_approval,true);
		$criteria->compare('rac_approval',$this->rac_approval,true);
		$criteria->compare('other',$this->other,true);
		$criteria->compare('if_yes_specify',$this->if_yes_specify,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
		 public function getValues()
        {
            return array('Yes','No');
        }
	
}