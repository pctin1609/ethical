<?php

/**
 * This is the model class for table "datacollection".
 *
 * The followings are the available columns in table 'datacollection':
 * @property integer $section_id
 * @property string $location_other_than_Australia
 * @property string $if_yes_location_explain
 * @property string $name_of_organization_agency
 * @property string $award
 * @property string $name_of_institution
 * @property string $role_of_monash
 * @property string $monash_staff
 * @property integer $how_many_people
 * @property string $groups_involved
 * @property string $age_range
 * @property string $recruitment_of_participants
 * @property string $reimbursement_to_participants
 * @property string $if_yes_how_much
 * @property string $dependent_or_unequal_relationship
 * @property string $if_yes_relationship
 * @property string $consent_form_attached
 * @property string $if_yes_please_attach
 * @property string $involve_the_collection
 * @property string $arrangements_in_place
 */
class DataLocation extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DataLocation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'datacollection';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('location_other_than_Australia, name_of_organization_agency, award, name_of_institution, role_of_monash, monash_staff, how_many_people, groups_involved, age_range, recruitment_of_participants, reimbursement_to_participants, dependent_or_unequal_relationship, consent_form_attached,  involve_the_collection, arrangements_in_place', 'required'),
			array('how_many_people', 'numerical', 'integerOnly'=>true),
			array('location_other_than_Australia, name_of_organization_agency, award, name_of_institution, role_of_monash, monash_staff, groups_involved, age_range, recruitment_of_participants, reimbursement_to_participants, dependent_or_unequal_relationship, consent_form_attached, if_yes_please_attach, involve_the_collection, arrangements_in_place', 'length', 'max'=>250),
			array('if_yes_location_explain,if_yes_please_attach, if_yes_how_much, if_yes_relationship', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('section_id, location_other_than_Australia, if_yes_location_explain, name_of_organization_agency, award, name_of_institution, role_of_monash, monash_staff, how_many_people, groups_involved, age_range, recruitment_of_participants, reimbursement_to_participants, if_yes_how_much, dependent_or_unequal_relationship, if_yes_relationship, consent_form_attached, if_yes_please_attach, involve_the_collection, arrangements_in_place', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'section_id' => 'Section',
			'location_other_than_Australia' => '5 Will the data be collected in a location other than Australia?',
			'if_yes_location_explain' => 'If YES, please specify the location and, if relevant, please explain whether there are any cultural issues related to the conduct of the research which would assist the committee with the review of your research project?
     

',
			'name_of_organization_agency' => '6.1 Name of organisation / funding agency',
			'award' => '6.2 Award (e.g. NHMRC, project grant)',
			'name_of_institution' => '6.3 Name of Institution administering the funds',
			'role_of_monash' => '7 What is the role of Monash University and Monash University staff in this research project?',
			'monash_staff' => '8 Will Monash University staff be directly in contact with participants in this study?',
			'how_many_people' => ' 11.1 How Many People',
			'groups_involved' => '11.2 Group of people Involved',
			'age_range' => '9.3 Age Range',
			'recruitment_of_participants' => '10 Will Monash University staff be involved in the recruitment of participants in this study? If Yes, please provide a step-by-step description of how this will happen and how potential participants will be invited to take part in the study? (i.e. via advertisement? through email?)',
			'reimbursement_to_participants' => '11 Will you be offering any reimbursement to participants?',
			'if_yes_how_much' => 'If Yes: how much and what form will the reimbursement take?',
			'dependent_or_unequal_relationship' => '12 Will any dependent or unequal relationship exist between anyone involved in the recruitment and the participants?',
			'if_yes_relationship' => 'If YES - describe the nature of the relationship, and explain what special precautions will preserve the rights of such people to decline to participate or to withdraw from participation once the research has begun.',
			'consent_form_attached' => '13 Is a consent form attached?',
			'if_yes_please_attach' => 'Yes - (please attach the consent form to this application)',
			'involve_the_collection' => '14 Does the project involve the collection, use or disclosure of health information, personal information or sensitive information?  ',
			'arrangements_in_place' => '15 What arrangements are in place if participants experience distress or there is an emergency (i.e. free counselling services? A nurse on hand? etc)?',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('section_id',$this->section_id);
		$criteria->compare('location_other_than_Australia',$this->location_other_than_Australia,true);
		$criteria->compare('if_yes_location_explain',$this->if_yes_location_explain,true);
		$criteria->compare('name_of_organization_agency',$this->name_of_organization_agency,true);
		$criteria->compare('award',$this->award,true);
		$criteria->compare('name_of_institution',$this->name_of_institution,true);
		$criteria->compare('role_of_monash',$this->role_of_monash,true);
		$criteria->compare('monash_staff',$this->monash_staff,true);
		$criteria->compare('how_many_people',$this->how_many_people);
		$criteria->compare('groups_involved',$this->groups_involved,true);
		$criteria->compare('age_range',$this->age_range,true);
		$criteria->compare('recruitment_of_participants',$this->recruitment_of_participants,true);
		$criteria->compare('reimbursement_to_participants',$this->reimbursement_to_participants,true);
		$criteria->compare('if_yes_how_much',$this->if_yes_how_much,true);
		$criteria->compare('dependent_or_unequal_relationship',$this->dependent_or_unequal_relationship,true);
		$criteria->compare('if_yes_relationship',$this->if_yes_relationship,true);
		$criteria->compare('consent_form_attached',$this->consent_form_attached,true);
		$criteria->compare('if_yes_please_attach',$this->if_yes_please_attach,true);
		$criteria->compare('involve_the_collection',$this->involve_the_collection,true);
		$criteria->compare('arrangements_in_place',$this->arrangements_in_place,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
 public function getValues()
        {
            return array('Yes','No');
        }
	public function getInfor()
        {
            return array('1 - 10'=>'1 - 10','11 - 20'=>'11 - 20', '21 - 30'=>'21 - 30', '31 - 40'=>'31 - 40', '41 - 50'=>'41 - 50','51 - 60'=>'51 - 60','61 - 70'=>'61 - 70','71 - 80'=>'71 - 80', '80+'=>'80+');
        }
	}