<?php

/**
 * This is the model class for table "configurations".
 *
 * The followings are the available columns in table 'configurations':
 * @property  string   $id
 * @property  string   $name
 * @property  string   $value
 */
class Configuration extends CActiveRecord
{

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Configuration the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'configurations';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('name, value', 'required'),
            array('name', 'length', 'max' => 128),
            array('name', 'unique'),
            array('value', 'boolean', 'on' => 'bool'),
            array('value', 'compare', 'compareValue' => 0, 'operator' => '>', 'on' => 'integer'),
            array('value', 'email', 'on' => 'email'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name => label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Name',
            'value' => 'Value'
        );
    }

    /**
     * @static
     * @param $name
     * @param string $default if empty value, return default value
     * @return string Configuration value
     */
    public static function get($name, $default = "")
    {
        $config = self::model()->findByAttributes(array('name' => $name));
        if ($config)
            return $config->value;
        elseif ($default)
            return $default;
        else
            return "";
    }

    /**
     * @static
     * @param $name
     * @param $value
     * @return boolean save result
     */
    public static function set($name, $value)
    {
        $config = self::model()->findByAttributes(array('name' => $name));
        if ($config)
            $config->value = $value;
        else {
            $config = new self;
            $config->name = $name;
            $config->value = $value;
        }
        return $config->save();
    }

    /**
     * @static data boolean yes no
     * @return array
     */
    public static function listDataBool()
    {
        return array(
            1 => 'Yes',
            0 => 'No',
        );
    }

}