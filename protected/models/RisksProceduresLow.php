<?php

/**
 * This is the model class for table "risks_procedures_low".
 *
 * The followings are the available columns in table 'risks_procedures_low':
 * @property integer $section_id
 * @property string $participation_risk_involved
 * @property string $participation_risk_description
 * @property string $risk_described_in_consent_form
 * @property string $risk_not_outlined_reason
 * @property string $risk_mitigation_plans
 * @property string $emergency_procedures
 * @property string $researcher_risk_mitigation_plans
 * @property string $report_discoveries
 */
class RisksProceduresLow extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return RisksProceduresLow the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'risks_procedures_low';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('participation_risk_involved, risk_described_in_consent_form, risk_mitigation_plans, emergency_procedures, researcher_risk_mitigation_plans, report_discoveries', 'required','message'=>'this field can not be left blank'),
			array('risk_described_in_consent_form', 'length', 'max'=>60),
			array('participation_risk_description, risk_not_outlined_reason', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('section_id, participation_risk_involved, participation_risk_description, risk_described_in_consent_form, risk_not_outlined_reason, risk_mitigation_plans, emergency_procedures, researcher_risk_mitigation_plans, report_discoveries', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'section_id' => 'Section',
			'participation_risk_involved' => '6.1 Are there any physical/psychological/social/economic or legal risks greater than inconvenience or discomfort, in either the short or long term, from participation in the project?  ',
			'participation_risk_description' => 'Define the risks of participation in the project',
			'risk_described_in_consent_form' => '6.2 Are all of these risks outlined on the explanatory statement and, where relevant, on the consent form?',
			'risk_not_outlined_reason' => 'If No, please explain why not?  ',
			'risk_mitigation_plans' => '6.3 Outline the arrangements planned to minimise the risks involved in these procedures.',
			'emergency_procedures' => '6.4 Should serious events or emergencies occur during the conduct of the research what will you do?  What facilities are available to deal with such incidents?  Is an appropriate list of counselling services available with the Explanatory Statement?  ',
			'researcher_risk_mitigation_plans' => '6.5 Are there any risks for the researchers?  Please outline the strategies you have in place to reduce this risk.',
			'report_discoveries' => '6.6 Some researchers are mandated by law to report certain findings � Is any person involved with the research project required by law to report? Please explain.',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('section_id',$this->section_id);
		$criteria->compare('participation_risk_involved',$this->participation_risk_involved,true);
		$criteria->compare('participation_risk_description',$this->participation_risk_description,true);
		$criteria->compare('risk_described_in_consent_form',$this->risk_described_in_consent_form,true);
		$criteria->compare('risk_not_outlined_reason',$this->risk_not_outlined_reason,true);
		$criteria->compare('risk_mitigation_plans',$this->risk_mitigation_plans,true);
		$criteria->compare('emergency_procedures',$this->emergency_procedures,true);
		$criteria->compare('researcher_risk_mitigation_plans',$this->researcher_risk_mitigation_plans,true);
		$criteria->compare('report_discoveries',$this->report_discoveries,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
         public function getValues()
        {
            return array('Yes','No');
        }
}