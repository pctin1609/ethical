<?php

/**
 * This is the model class for table "types_of_activities".
 *
 * The followings are the available columns in table 'types_of_activities':
 * @property integer $section_id
 * @property string $toxins
 * @property string $use_without_consent
 * @property string $causes_discomfort
 * @property string $covert_observation
 * @property string $sensitive_issues
 * @property string $disclosure_of_info
 * @property string $intrusive_techniques
 * @property string $illegal_activities
 * @property string $genetic_research
 * @property string $xenotransplantation
 * @property string $clinical_trials
 * @property string $random_trials
 * @property string $psychology_inventories
 * @property string $radioactive_substances
 * @property string $reproductive_technology
 * @property string $innovative_therapy
 * @property string $human_tissue
 * @property string $human_stem_cells
 * @property string $recombitant_DNA
 */
class TypeOfActivities extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TypeOfActivities the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'types_of_activities';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('toxins, use_without_consent, causes_discomfort, covert_observation, sensitive_issues, disclosure_of_info, intrusive_techniques, illegal_activities, genetic_research, xenotransplantation, clinical_trials, random_trials, psychology_inventories, radioactive_substances, reproductive_technology, innovative_therapy, human_tissue, human_stem_cells, recombitant_DNA', 'required'),
			array('toxins, use_without_consent, causes_discomfort, covert_observation, sensitive_issues, disclosure_of_info, intrusive_techniques, illegal_activities, genetic_research, xenotransplantation, clinical_trials, random_trials, psychology_inventories, radioactive_substances, reproductive_technology, innovative_therapy, human_tissue, human_stem_cells, recombitant_DNA', 'length', 'max'=>250),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('section_id, toxins, use_without_consent, causes_discomfort, covert_observation, sensitive_issues, disclosure_of_info, intrusive_techniques, illegal_activities, genetic_research, xenotransplantation, clinical_trials, random_trials, psychology_inventories, radioactive_substances, reproductive_technology, innovative_therapy, human_tissue, human_stem_cells, recombitant_DNA', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'section_id' => 'Section',
			'toxins' => ' 2.1 Toxins / Mutagens / Teratogens / Carcinogens',
			'use_without_consent' => '2.2 Collection, use or disclosure of information WITHOUT the consent of the individual whose information it is',
			'causes_discomfort' => '2.3 Causing discomfort to participants beyond normal levels of inconvenience',
			'covert_observation' => '2.4 Deception of participants, concealment or covert observation',
			'sensitive_issues' => '2.5 Examining potentially sensitive or contentious issues',
			'disclosure_of_info' => '2.6 Seeking disclosure of information which may be prejudicial to participants',
			'intrusive_techniques' => '2.7 Using intrusive techniques',
			'illegal_activities' => '2.8 Study of or participation in illegal activities',
			'genetic_research' => '2.9 Human genetic research or gene technology',
			'xenotransplantation' => '2.10 Xenotransplantation',
			'clinical_trials' => '2.11 Clinical trials, using drugs, diagnostic material, therapeutic devices or techniques',
			'random_trials' => '2.12 Randomised trials in other fields (ie other than those in the line above) including, for example, psychology and education. ',
			'psychology_inventories' => '2.13 Psychology Inventories / scales / tests
Exception:  The use of non-diagnostic psychology inventories which are judged to have minimal risks to participants.  Please refer to the inventory list for low risk research on our website for further information.  
',
			'radioactive_substances' => '2.14 Radioactive substances / ionising radiation e.g. X-rays, DEXA',
			'reproductive_technology' => '2.15 Assisted reproductive technology',
			'innovative_therapy' => '2.16 Innovative therapy or intervention',
			'human_tissue' => '2.17 Use of human tissue samples',
			'human_stem_cells' => '2.18 Derivation or use of human stem cells ',
			'recombitant_DNA' => '2.19 Recombinant DNA techniques',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('section_id',$this->section_id);
		$criteria->compare('toxins',$this->toxins,true);
		$criteria->compare('use_without_consent',$this->use_without_consent,true);
		$criteria->compare('causes_discomfort',$this->causes_discomfort,true);
		$criteria->compare('covert_observation',$this->covert_observation,true);
		$criteria->compare('sensitive_issues',$this->sensitive_issues,true);
		$criteria->compare('disclosure_of_info',$this->disclosure_of_info,true);
		$criteria->compare('intrusive_techniques',$this->intrusive_techniques,true);
		$criteria->compare('illegal_activities',$this->illegal_activities,true);
		$criteria->compare('genetic_research',$this->genetic_research,true);
		$criteria->compare('xenotransplantation',$this->xenotransplantation,true);
		$criteria->compare('clinical_trials',$this->clinical_trials,true);
		$criteria->compare('random_trials',$this->random_trials,true);
		$criteria->compare('psychology_inventories',$this->psychology_inventories,true);
		$criteria->compare('radioactive_substances',$this->radioactive_substances,true);
		$criteria->compare('reproductive_technology',$this->reproductive_technology,true);
		$criteria->compare('innovative_therapy',$this->innovative_therapy,true);
		$criteria->compare('human_tissue',$this->human_tissue,true);
		$criteria->compare('human_stem_cells',$this->human_stem_cells,true);
		$criteria->compare('recombitant_DNA',$this->recombitant_DNA,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function getValues()
        {
            return array('Yes','No');
        }
}