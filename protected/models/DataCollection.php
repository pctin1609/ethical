<?php

/**
 * This is the model class for table "data_collection".
 *
 * The followings are the available columns in table 'fm1_section5a':
 * @property integer $section_id
 * @property string $questionnaire
 * @property string $interviews
 * @property string $interview_audio_recording
 * @property string $interview_video_recording
 * @property string $focus_groups
 * @property string $observations
 * @property string $observations_with_kp
 * @property string $observations_without_kp
 * @property string $photo_video_graph_with_kp
 * @property string $photo_video_graph_without_kp
 * @property string $photo_video_graph_video
 * @property string $collection_of_HT
 * @property string $collection_of_details
 * @property string $response_to_tss
 * @property string $response_to_tss_details
 * @property string $epidemiological_research
 * @property string $epidemiological_research_details
 * @property string $psychology_inventories
 * @property string $psychology_inventories_details
 * @property string $online_survey
 */
class DataCollection extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Fm1_Section5a the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'data_collection';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('questionnaire, interviews, interview_audio_recording, interview_video_recording, focus_groups, observations, observations_with_kp, observations_without_kp, photo_video_graph_with_kp, photo_video_graph_without_kp, photo_video_graph_video, collection_of_HT,  response_to_tss, epidemiological_research, psychology_inventories,  online_survey', 'required','message'=>'this field can not be left blank'),
			array('questionnaire, interviews, interview_audio_recording, interview_video_recording, focus_groups, observations, observations_with_kp, observations_without_kp, photo_video_graph_with_kp, photo_video_graph_without_kp, photo_video_graph_video, collection_of_HT, response_to_tss, epidemiological_research, psychology_inventories, online_survey', 'length', 'max'=>35),
			array('collection_of_details,response_to_tss_details, epidemiological_research_details,psychology_inventories_details', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('section_id, questionnaire, interviews, interview_audio_recording, interview_video_recording, focus_groups, observations, observations_with_kp, observations_without_kp, photo_video_graph_with_kp, photo_video_graph_without_kp, photo_video_graph_video, collection_of_HT, collection_of_details, response_to_tss, response_to_tss_details, epidemiological_research, epidemiological_research_details, psychology_inventories, psychology_inventories_details, online_survey', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'questionnaire' => '5.1 How, where and by whom are the data to be collected? Researchers should briefly outline all research procedures to be used with each category of participants. How will the data be collected?<br> <br> How will the data be collected? <br> Questionnaire',
			'interviews' => 'Interviews',
			'interview_audio_recording' => 'Interview Audio Recording',
			'interview_video_recording' => 'Interview Video Recording',
			'focus_groups' => 'Focus Groups',
			'observations' => 'Observations',
			'observations_with_kp' => 'Observations with the knowledge of participants',
			'observations_without_kp' => 'Observations without the knowledge of participants',
			'photo_video_graph_with_kp' => 'Photography or videography with the knowledge of participants',
			'photo_video_graph_without_kp' => 'Photography or videography without the knowledge of participants',
			'photo_video_graph_video' => 'Photography or videography',
			'collection_of_HT' => 'Collection of human tissue / fluid',
			'collection_of_details' => 'Collection of human tissue / fluid details',
			'response_to_tss' => 'Responses to tasks or stimuli or simulations',
			'response_to_tss_details' => 'Responses to tasks or stimuli or simulations details',
			'epidemiological_research' => 'Epidemiological research',
			'epidemiological_research_details' => 'Epidemiological Research details',
			'psychology_inventories' => 'Psychology Inventories',
			'psychology_inventories_details' => 'Psychology Inventories details',
			'online_survey' => 'Online Survey',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('section_id',$this->section_id);
		$criteria->compare('questionnaire',$this->questionnaire,true);
		$criteria->compare('interviews',$this->interviews,true);
		$criteria->compare('interview_audio_recording',$this->interview_audio_recording,true);
		$criteria->compare('interview_video_recording',$this->interview_video_recording,true);
		$criteria->compare('focus_groups',$this->focus_groups,true);
		$criteria->compare('observations',$this->observations,true);
		$criteria->compare('observations_with_kp',$this->observations_with_kp,true);
		$criteria->compare('observations_without_kp',$this->observations_without_kp,true);
		$criteria->compare('photo_video_graph_with_kp',$this->photo_video_graph_with_kp,true);
		$criteria->compare('photo_video_graph_without_kp',$this->photo_video_graph_without_kp,true);
		$criteria->compare('photo_video_graph_video',$this->photo_video_graph_video,true);
		$criteria->compare('collection_of_HT',$this->collection_of_HT,true);
		$criteria->compare('collection_of_details',$this->collection_of_details,true);
		$criteria->compare('response_to_tss',$this->response_to_tss,true);
		$criteria->compare('response_to_tss_details',$this->response_to_tss_details,true);
		$criteria->compare('epidemiological_research',$this->epidemiological_research,true);
		$criteria->compare('epidemiological_research_details',$this->epidemiological_research_details,true);
		$criteria->compare('psychology_inventories',$this->psychology_inventories,true);
		$criteria->compare('psychology_inventories_details',$this->psychology_inventories_details,true);
		$criteria->compare('online_survey',$this->online_survey,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	
		    public function getQuestionnaireTypes()
        {
                return array(
                'Fully identifiable (name on it)'=>'Fully identifiable (name on it)',
                'Potentially identifiable (coded)'=>'Potentially identifiable (coded)',
                'Anonymous (never can be identified)'=>'Anonymous (never can be identified)',
                );
        }
		
		public function getInterviewTypes()
		{
		  return array (
                      'In_depth'=>'In_depth',
                      'Semi-structured'=>'Semi-structured',
                      'Unstructured'=>'Unstructured'
                      );
		}
		
		public function getValues()
        {
            return array('Yes','No');
        }
public function getInfor()
        {
        return array(
            'Audio recording'=>'Audio recording',
            'Video recording'=>'Video recording' 
            );
        }

		}