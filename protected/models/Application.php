<?php

/**
 * This is the model class for table "application".
 *
 * The followings are the available columns in table 'application':
 * @property integer $stage
 * @property integer $project_id
 * @property integer $user_id
 * @property string $status
 * @property string $create_time
 * @property integer $form_id
 * @property string $application_type
 *
 * The followings are the available model relations:
 * @property User $user
 * @property Lol[] $lols
 * @property Organisation[] $organisations
 * @property User[] $users
 */
class Application extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Application the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'application';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('stage, user_id, create_time, application_type', 'required'),
            array('stage, user_id, form_id', 'numerical', 'integerOnly' => true),
            array('status, application_type', 'length', 'max' => 64),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('stage, project_id, user_id, status, create_time, form_id, application_type', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
            'project' => array(self::HAS_MANY, 'Project', 'project_id'),
            'organisations' => array(self::HAS_MANY, 'Organisation', 'project_id'),
            'users' => array(self::HAS_MANY, 'User', 'application_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'stage' => 'Stage',
            'project_id' => 'Project',
            'user_id' => 'User',
            'status' => 'Status',
            'create_time' => 'Create Time',
            'form_id' => 'Form',
            'application_type' => 'Application Type',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('stage', $this->stage);
        $criteria->compare('project_id', $this->project_id);
            $criteria->compare('user_id', $this->user_id);
       
        $criteria->compare('status', $this->status, true);
        $criteria->compare('create_time', $this->create_time, true);
        $criteria->compare('form_id', $this->form_id);
        $criteria->compare('application_type', $this->application_type, true);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

    public function getUserApplications($id, $type = 'form1') {
        $criteria = new CDbCriteria;

        $criteria->compare('user_id', $id);
        $criteria->compare('application_type', $type);
        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }
    
    public function getSuperviseApplication($id) {
        $criteria = new CDbCriteria;
        
        $criteria->compare('project_id', $id);
        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

}

