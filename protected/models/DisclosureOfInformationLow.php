<?php

/**
 * This is the model class for table "disclosure_of_information_low".
 *
 * The followings are the available columns in table 'disclosure_of_information_low':
 * @property integer $id
 * @property string $information_type
 * @property string $sensi_information
 * @property string $heat_information
 * @property string $organisation_involved_health
 * @property string $private_sector
 * @property string $health_public_sector
 * @property string $other_orgar
 * @property string $organisation_involved_personal
 * @property string $victoria_private_sector
 * @property string $victoria_commonwealth
 * @property string $other_public_sector
 * @property string $organisation_involved_sensitive
 * @property string $vi_private_sector
 * @property string $vi_commonwealth
 * @property string $other_vi_public_sector
 * @property string $principle_health
 * @property string $hpp2_health
 * @property string $ipp11_health
 * @property string $npp2_other
 * @property string $principle_personal
 * @property string $npp2_personal
 * @property string $ipp11_personal
 * @property string $np2_personal
 * @property string $principle_sensitive
 * @property string $npp_sensitive
 * @property string $ipp_sensitive
 * @property string $npp2_sensitive
 * @property string $specific_purpose
 * @property string $secondary_purpose
 * @property string $give_details
 * @property string $not_used_in_deidentified_form
 * @property string $public_interest
 * @property string $potentially_identifiable
 * @property string $info
 * @property string $sen_infor
 * @property string $hea_infor
 * @property string $organisation_health
 * @property string $pri_sector
 * @property string $pu_sector
 * @property string $other_victorian
 * @property string $organisation_personal
 * @property string $vate_sector
 * @property string $vate_commonwealth
 * @property string $other_vate
 * @property string $organisation_sensitive
 * @property string $prite_sector
 * @property string $commu_public_sector
 * @property string $other_wealth
 * @property string $privacy_health
 * @property string $h_health
 * @property string $ip_health
 * @property string $np_health
 * @property string $privacy_personal
 * @property string $npp_personal1
 * @property string $ipp_personal1
 * @property string $n_personal1
 * @property string $privacy_sensitive
 * @property string $nppp_sensitive1
 * @property string $ippp_sensitive1
 * @property string $nppp_senstive1
 * @property string $organisations_list
 * @property string $disclosed_by_researcher
 * @property string $information
 * @property string $se_information
 * @property string $he_information
 * @property string $orga_involved_health
 * @property string $victorian_private_sec
 * @property string $commonwealth_pub_sector
 * @property string $other_involved_health
 * @property string $orga_involved_personal
 * @property string $victorian_pvt
 * @property string $commonwealth_pvt
 * @property string $other_victorian_pvt
 * @property string $orga_involved_sensitive
 * @property string $victorian_pvt_sensitive
 * @property string $commonwealth_pvt_sensitive
 * @property string $other_victorian_sensitive_pvt
 * @property string $principles_health
 * @property string $pvt_hpp2_health
 * @property string $pvt_ipp11_health
 * @property string $pvt_npp2_health
 * @property string $principles_personal
 * @property string $pvt_hpp2_personal
 * @property string $pvt_ipp11_personal
 * @property string $pvt_npp2_personal
 * @property string $principles_sensitive
 * @property string $pvt_hpp2_sensitive
 * @property string $pvt_ipp11_sensitive
 * @property string $pvt_npp2_sensitive
 * @property string $info_disclosed
 * @property string $info_not_used
 * @property string $explain_consent
 */
class DisclosureOfInformationLow extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DisclosureOfInformationLow the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'disclosure_of_information_low';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('specific_purpose, secondary_purpose,  not_used_in_deidentified_form, public_interest, potentially_identifiable,   organisations_list, disclosed_by_researcher,  info_disclosed, info_not_used, explain_consent', 'required'),
			array('information_type, sensi_information, heat_information, organisation_involved_health, private_sector, health_public_sector, other_orgar, organisation_involved_personal, victoria_private_sector, victoria_commonwealth, other_public_sector, organisation_involved_sensitive, vi_private_sector, vi_commonwealth, other_vi_public_sector, principle_health, hpp2_health, ipp11_health, npp2_other, principle_personal, npp2_personal, ipp11_personal, np2_personal, principle_sensitive, npp_sensitive, ipp_sensitive, npp2_sensitive, specific_purpose, give_details, not_used_in_deidentified_form, public_interest, potentially_identifiable, info, sen_infor, hea_infor, organisation_health, pri_sector, pu_sector, other_victorian, organisation_personal, vate_sector, vate_commonwealth, other_vate, organisation_sensitive, prite_sector, commu_public_sector, other_wealth, privacy_health, h_health, ip_health, np_health, privacy_personal, npp_personal1, ipp_personal1, n_personal1, privacy_sensitive, nppp_sensitive1, ippp_sensitive1, nppp_senstive1, organisations_list, disclosed_by_researcher, information, se_information, he_information, orga_involved_health, victorian_private_sec, commonwealth_pub_sector, other_involved_health, orga_involved_personal, victorian_pvt, commonwealth_pvt, other_victorian_pvt, orga_involved_sensitive, victorian_pvt_sensitive, commonwealth_pvt_sensitive, other_victorian_sensitive_pvt, principles_health, pvt_hpp2_health, pvt_ipp11_health, pvt_npp2_health, principles_personal, pvt_hpp2_personal, pvt_ipp11_personal, pvt_npp2_personal, principles_sensitive, pvt_hpp2_sensitive, pvt_ipp11_sensitive, pvt_npp2_sensitive, info_disclosed, info_not_used, explain_consent', 'length', 'max'=>100),
                array('give_details,information_type, sensi_information, heat_information,organisation_involved_health, private_sector, health_public_sector, other_orgar,organisation_involved_personal, victoria_private_sector, victoria_commonwealth, other_public_sector,organisation_involved_sensitive, vi_private_sector, vi_commonwealth, other_vi_public_sector,principle_health, hpp2_health, ipp11_health, npp2_other, principle_personal, npp2_personal, ipp11_personal, np2_personal, principle_sensitive, npp_sensitive, ipp_sensitive, npp2_sensitive, info, sen_infor, hea_infor, organisation_health, pri_sector, pu_sector, other_victorian, organisation_personal, vate_sector, vate_commonwealth,other_vate, organisation_sensitive, prite_sector, commu_public_sector, other_wealth, privacy_health,h_health, ip_health, np_health, privacy_personal, npp_personal1, ipp_personal1, n_personal1, privacy_sensitive, nppp_sensitive1, ippp_sensitive1, nppp_senstive1,information, se_information, he_information, orga_involved_health, victorian_private_sec, commonwealth_pub_sector, other_involved_health, orga_involved_personal, victorian_pvt, commonwealth_pvt, other_victorian_pvt, orga_involved_sensitive, victorian_pvt_sensitive, commonwealth_pvt_sensitive, other_victorian_sensitive_pvt, principles_health, pvt_hpp2_health, pvt_ipp11_health, pvt_npp2_health, principles_personal, pvt_hpp2_personal, pvt_ipp11_personal, pvt_npp2_personal, principles_sensitive, pvt_hpp2_sensitive, pvt_ipp11_sensitive, pvt_npp2_sensitive', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, information_type, sensi_information, heat_information, organisation_involved_health, private_sector, health_public_sector, other_orgar, organisation_involved_personal, victoria_private_sector, victoria_commonwealth, other_public_sector, organisation_involved_sensitive, vi_private_sector, vi_commonwealth, other_vi_public_sector, principle_health, hpp2_health, ipp11_health, npp2_other, principle_personal, npp2_personal, ipp11_personal, np2_personal, principle_sensitive, npp_sensitive, ipp_sensitive, npp2_sensitive, specific_purpose, secondary_purpose, give_details, not_used_in_deidentified_form, public_interest, potentially_identifiable, info, sen_infor, hea_infor, organisation_health, pri_sector, pu_sector, other_victorian, organisation_personal, vate_sector, vate_commonwealth, other_vate, organisation_sensitive, prite_sector, commu_public_sector, other_wealth, privacy_health, h_health, ip_health, np_health, privacy_personal, npp_personal1, ipp_personal1, n_personal1, privacy_sensitive, nppp_sensitive1, ippp_sensitive1, nppp_senstive1, organisations_list, disclosed_by_researcher, information, se_information, he_information, orga_involved_health, victorian_private_sec, commonwealth_pub_sector, other_involved_health, orga_involved_personal, victorian_pvt, commonwealth_pvt, other_victorian_pvt, orga_involved_sensitive, victorian_pvt_sensitive, commonwealth_pvt_sensitive, other_victorian_sensitive_pvt, principles_health, pvt_hpp2_health, pvt_ipp11_health, pvt_npp2_health, principles_personal, pvt_hpp2_personal, pvt_ipp11_personal, pvt_npp2_personal, principles_sensitive, pvt_hpp2_sensitive, pvt_ipp11_sensitive, pvt_npp2_sensitive, info_disclosed, info_not_used, explain_consent', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'information_type' => 'Health Information',
			'sensi_information' => 'Personal information (other than health information)',
			'heat_information' => 'Sensitive Information',
			'organisation_involved_health' => 'Victorian public sector',
			'private_sector' => 'Victorian private sector',
			'health_public_sector' => 'Commonwealth public sector',
			'other_orgar' => 'Other',
			'organisation_involved_personal' => 'Victorian public sector',
			'victoria_private_sector' => 'Victorian private Sector',
			'victoria_commonwealth' => 'Commonwealth public sector',
			'other_public_sector' => 'Other',
			'organisation_involved_sensitive' => 'Victorian public sector',
			'vi_private_sector' => 'Victorian private Sector',
			'vi_commonwealth' => 'Commonwealth public sector',
			'other_vi_public_sector' => 'Other',
			'principle_health' => 'HPP 1',
			'hpp2_health' => 'HPP 1, NPP 1, NPP 10',
			'ipp11_health' => 'IPP 11',
			'npp2_other' => 'NPP 1, NPP 10',
			'principle_personal' => 'VIPP 1',
			'npp2_personal' => 'NPP 1',
			'ipp11_personal' => 'IPP 11',
			'np2_personal' => 'NPP 1',
			'principle_sensitive' => 'VIPP 10',
			'npp_sensitive' => 'NPP 10',
			'ipp_sensitive' => 'IPP 11',
			'npp2_sensitive' => 'NPP 10',
			'specific_purpose' => '9.9b What are the specific purposes for which the information will be used?',
			'secondary_purpose' => '9.9c Is the purpose for which the information will be used (the secondary purpose) related to the purpose for which the information was originally collected (the primary purpose)?',
			'give_details' => 'If Yes: Give details ',
			'not_used_in_deidentified_form' => '9.9d Please explain why information will not be used in a de-identified form.',
			'public_interest' => '9.9e  Please explain why the proposed use of information is in the public interest. Note that the public interest in the proposed research must substantially outweigh the public interest in respecting individual privacy.',
			'potentially_identifiable' => '9.10a Will identified (or potentially identifiable) information be disclosed by an organisation to the researcher?',
			'info' => 'Health Information',
			'sen_infor' => 'Personal information (other than health information)',
			'hea_infor' => 'Sensitive Information',
			'organisation_health' => 'Victorian public sector',
			'pri_sector' => 'Victorian private sector',
			'pu_sector' => 'Commonwealth public sector',
			'other_victorian' => 'Other',
			'organisation_personal' => 'Victorian public sector',
			'vate_sector' => 'Victorian private sector',
			'vate_commonwealth' => 'Commonwealth public sector',
			'other_vate' => 'Other',
			'organisation_sensitive' => 'Victorian public sector',
			'prite_sector' => 'Victorian private sector',
			'commu_public_sector' => 'Commonwealth public sector',
			'other_wealth' => 'Other',
			'privacy_health' => 'HPP 2',
			'h_health' => 'HPP 2, NPP 2',
			'ip_health' => 'IPP 11',
			'np_health' => 'NPP 2',
			'privacy_personal' => 'VIPP2',
			'npp_personal1' => 'NPP2',
			'ipp_personal1' => 'IPP 11',
			'n_personal1' => 'NPP 2',
			'privacy_sensitive' => 'VIPP2',
			'nppp_sensitive1' => 'NPP2',
			'ippp_sensitive1' => 'IPP 11',
			'nppp_senstive1' => 'NPP 2',
			'organisations_list' => 'List the organisations that will disclose information to the researcher. If more than one organisation is involved, indicate clearly what information or records will be disclosed by each organisation to the researcher.',
			'disclosed_by_researcher' => '9.10b Will identified (or potentially identifiable) information be disclosed by the researcher to other organisations?',
			'information' => ' Health Information',
			'se_information' => 'Personal information (other than health information)',
			'he_information' => 'Sensitive Information',
			'orga_involved_health' => 'Victorian public sector',
			'victorian_private_sec' => 'Victorian private sector',
			'commonwealth_pub_sector' => 'Commonwealth public sector',
			'other_involved_health' => 'Other',
			'orga_involved_personal' => 'Victorian public sector',
			'victorian_pvt' => 'Victorian private sector',
			'commonwealth_pvt' => 'Commonwealth public sector',
			'other_victorian_pvt' => 'Other',
			'orga_involved_sensitive' => 'Victorian public sector',
			'victorian_pvt_sensitive' => 'Victorian private sector',
			'commonwealth_pvt_sensitive' => 'Commonwealth public sector',
			'other_victorian_sensitive_pvt' => 'Other',
			'principles_health' => 'HPP 2',
			'pvt_hpp2_health' => 'HPP 2, NPP 2',
			'pvt_ipp11_health' => 'IPP 11',
			'pvt_npp2_health' => 'NPP 2',
			'principles_personal' => 'VIPP2',
			'pvt_hpp2_personal' => 'NPP2',
			'pvt_ipp11_personal' => 'IPP 11',
			'pvt_npp2_personal' => 'NPP 2',
			'principles_sensitive' => 'VIPP2',
			'pvt_hpp2_sensitive' => 'NPP2',
			'pvt_ipp11_sensitive' => 'IPP 11',
			'pvt_npp2_sensitive' => 'NPP 2',
			'info_disclosed' => 'List the organisations to which information will be disclosed. If information will be disclosed to more than one organisation, indicate clearly what information or records will be disclosed in each case.',
			'info_not_used' => '9.10c Please explain why information will not be used in a de-identified form',
			'explain_consent' => '9.10d Please explain why consent will not be obtained from the individual(s) whose information will be used?',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('information_type',$this->information_type,true);
		$criteria->compare('sensi_information',$this->sensi_information,true);
		$criteria->compare('heat_information',$this->heat_information,true);
		$criteria->compare('organisation_involved_health',$this->organisation_involved_health,true);
		$criteria->compare('private_sector',$this->private_sector,true);
		$criteria->compare('health_public_sector',$this->health_public_sector,true);
		$criteria->compare('other_orgar',$this->other_orgar,true);
		$criteria->compare('organisation_involved_personal',$this->organisation_involved_personal,true);
		$criteria->compare('victoria_private_sector',$this->victoria_private_sector,true);
		$criteria->compare('victoria_commonwealth',$this->victoria_commonwealth,true);
		$criteria->compare('other_public_sector',$this->other_public_sector,true);
		$criteria->compare('organisation_involved_sensitive',$this->organisation_involved_sensitive,true);
		$criteria->compare('vi_private_sector',$this->vi_private_sector,true);
		$criteria->compare('vi_commonwealth',$this->vi_commonwealth,true);
		$criteria->compare('other_vi_public_sector',$this->other_vi_public_sector,true);
		$criteria->compare('principle_health',$this->principle_health,true);
		$criteria->compare('hpp2_health',$this->hpp2_health,true);
		$criteria->compare('ipp11_health',$this->ipp11_health,true);
		$criteria->compare('npp2_other',$this->npp2_other,true);
		$criteria->compare('principle_personal',$this->principle_personal,true);
		$criteria->compare('npp2_personal',$this->npp2_personal,true);
		$criteria->compare('ipp11_personal',$this->ipp11_personal,true);
		$criteria->compare('np2_personal',$this->np2_personal,true);
		$criteria->compare('principle_sensitive',$this->principle_sensitive,true);
		$criteria->compare('npp_sensitive',$this->npp_sensitive,true);
		$criteria->compare('ipp_sensitive',$this->ipp_sensitive,true);
		$criteria->compare('npp2_sensitive',$this->npp2_sensitive,true);
		$criteria->compare('specific_purpose',$this->specific_purpose,true);
		$criteria->compare('secondary_purpose',$this->secondary_purpose,true);
		$criteria->compare('give_details',$this->give_details,true);
		$criteria->compare('not_used_in_deidentified_form',$this->not_used_in_deidentified_form,true);
		$criteria->compare('public_interest',$this->public_interest,true);
		$criteria->compare('potentially_identifiable',$this->potentially_identifiable,true);
		$criteria->compare('info',$this->info,true);
		$criteria->compare('sen_infor',$this->sen_infor,true);
		$criteria->compare('hea_infor',$this->hea_infor,true);
		$criteria->compare('organisation_health',$this->organisation_health,true);
		$criteria->compare('pri_sector',$this->pri_sector,true);
		$criteria->compare('pu_sector',$this->pu_sector,true);
		$criteria->compare('other_victorian',$this->other_victorian,true);
		$criteria->compare('organisation_personal',$this->organisation_personal,true);
		$criteria->compare('vate_sector',$this->vate_sector,true);
		$criteria->compare('vate_commonwealth',$this->vate_commonwealth,true);
		$criteria->compare('other_vate',$this->other_vate,true);
		$criteria->compare('organisation_sensitive',$this->organisation_sensitive,true);
		$criteria->compare('prite_sector',$this->prite_sector,true);
		$criteria->compare('commu_public_sector',$this->commu_public_sector,true);
		$criteria->compare('other_wealth',$this->other_wealth,true);
		$criteria->compare('privacy_health',$this->privacy_health,true);
		$criteria->compare('h_health',$this->h_health,true);
		$criteria->compare('ip_health',$this->ip_health,true);
		$criteria->compare('np_health',$this->np_health,true);
		$criteria->compare('privacy_personal',$this->privacy_personal,true);
		$criteria->compare('npp_personal1',$this->npp_personal1,true);
		$criteria->compare('ipp_personal1',$this->ipp_personal1,true);
		$criteria->compare('n_personal1',$this->n_personal1,true);
		$criteria->compare('privacy_sensitive',$this->privacy_sensitive,true);
		$criteria->compare('nppp_sensitive1',$this->nppp_sensitive1,true);
		$criteria->compare('ippp_sensitive1',$this->ippp_sensitive1,true);
		$criteria->compare('nppp_senstive1',$this->nppp_senstive1,true);
		$criteria->compare('organisations_list',$this->organisations_list,true);
		$criteria->compare('disclosed_by_researcher',$this->disclosed_by_researcher,true);
		$criteria->compare('information',$this->information,true);
		$criteria->compare('se_information',$this->se_information,true);
		$criteria->compare('he_information',$this->he_information,true);
		$criteria->compare('orga_involved_health',$this->orga_involved_health,true);
		$criteria->compare('victorian_private_sec',$this->victorian_private_sec,true);
		$criteria->compare('commonwealth_pub_sector',$this->commonwealth_pub_sector,true);
		$criteria->compare('other_involved_health',$this->other_involved_health,true);
		$criteria->compare('orga_involved_personal',$this->orga_involved_personal,true);
		$criteria->compare('victorian_pvt',$this->victorian_pvt,true);
		$criteria->compare('commonwealth_pvt',$this->commonwealth_pvt,true);
		$criteria->compare('other_victorian_pvt',$this->other_victorian_pvt,true);
		$criteria->compare('orga_involved_sensitive',$this->orga_involved_sensitive,true);
		$criteria->compare('victorian_pvt_sensitive',$this->victorian_pvt_sensitive,true);
		$criteria->compare('commonwealth_pvt_sensitive',$this->commonwealth_pvt_sensitive,true);
		$criteria->compare('other_victorian_sensitive_pvt',$this->other_victorian_sensitive_pvt,true);
		$criteria->compare('principles_health',$this->principles_health,true);
		$criteria->compare('pvt_hpp2_health',$this->pvt_hpp2_health,true);
		$criteria->compare('pvt_ipp11_health',$this->pvt_ipp11_health,true);
		$criteria->compare('pvt_npp2_health',$this->pvt_npp2_health,true);
		$criteria->compare('principles_personal',$this->principles_personal,true);
		$criteria->compare('pvt_hpp2_personal',$this->pvt_hpp2_personal,true);
		$criteria->compare('pvt_ipp11_personal',$this->pvt_ipp11_personal,true);
		$criteria->compare('pvt_npp2_personal',$this->pvt_npp2_personal,true);
		$criteria->compare('principles_sensitive',$this->principles_sensitive,true);
		$criteria->compare('pvt_hpp2_sensitive',$this->pvt_hpp2_sensitive,true);
		$criteria->compare('pvt_ipp11_sensitive',$this->pvt_ipp11_sensitive,true);
		$criteria->compare('pvt_npp2_sensitive',$this->pvt_npp2_sensitive,true);
		$criteria->compare('info_disclosed',$this->info_disclosed,true);
		$criteria->compare('info_not_used',$this->info_not_used,true);
		$criteria->compare('explain_consent',$this->explain_consent,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
public function getValues()
        {
            return array('Yes','No');
        }
	public function getTypeOfOrganisation()
        {
            return array('Victorian public sector','Victorian private sector', 'Commonwealth public sector','Other');
        }
	public function getPrincipleHealth()
        {
            return array('HPP 2','HPP 2, NPP 2','IPP 11','NPP 2');
        }
	public function getPrinciplePersonal()
        {
            return array('VIPP 2','NPP 2','IPP 11','NPP 2');
        }
	public function getPrincipleSensitive()
        {
            return array('VIPP 2','NPP 2','IPP 11','NPP 2');
        }
	 public function getPrivacyPrincipleHealth()
        {
            return array('HPP 1','HPP 1, NPP 1, NPP 10', 'IPP 11','NPP 1, NPP 10');
        }
	public function getPrivacyPrinciplePersonal()
        {
            return array('VIPP 1','NPP 1','IPP 11','NPP 1');
        }
	public function getPrivacyPrincipleSensitive()
        {
            return array('VIPP 10', 'NPP 10','IPP 11','NPP 10');
        }
	public function getTypeOfInfo()
        {
            return array('Health information','Personal information (other than health information)','Sensitive information' );
        }
	}