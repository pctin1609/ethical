<?php

/**
 * This is the model class for table "group_identification".
 *
 * The followings are the available columns in table 'group_identification':
 * @property integer $section_id
 * @property string $children
 * @property string $adult
 * @property string $preg_women
 * @property string $breastfd_women
 * @property string $individuals
 * @property string $justification
 */
class GroupIdentification extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return GroupIdentification the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'group_identification';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('children, adult, preg_women, breastfd_women, individuals', 'required'),
			array('children, adult, preg_women, breastfd_women, individuals', 'length', 'max'=>20),
			array('justification', 'length', 'max'=>250),
			array('children, adult, preg_women, breastfd_women, individuals', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('section_id, children, adult, preg_women, breastfd_women, individuals, justification', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
	     	'section_id' => 'Section',
			'children' => 'Children or young people under the age of 18',
			'adult' => 'Adults under the age of 50',
			'preg_women' => 'Pregnant women',
			'breastfd_women' => 'Women who are breastfeeding',
			'individuals' => 'Individuals who have previously been exposed to radiation',
			'justification' => 'Please justify the use of this group if you ticked any of the above ',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('section_id',$this->section_id);
		$criteria->compare('children',$this->children,true);
		$criteria->compare('adult',$this->adult,true);
		$criteria->compare('preg_women',$this->preg_women,true);
		$criteria->compare('breastfd_women',$this->breastfd_women,true);
		$criteria->compare('individuals',$this->individuals,true);
		$criteria->compare('justification',$this->justification,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
		 public function getValues()
        {
            return array('Yes','No');
        }
}