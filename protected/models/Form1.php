<?php

/**
 * This is the model class for table "form1".
 *
 * The followings are the available columns in table 'form1':
 * @property integer $form_id
 * @property integer $application_id
 * @property integer $section1_id
 * @property integer $section2_id
 * @property integer $section3_id
 * @property integer $section4A_id
 * @property integer $section4B_id
 * @property integer $section5A_id
 * @property integer $section5B_id
 * @property integer $section6A_id
 * @property integer $section6B_id
 * @property integer $section6C_id
 * @property integer $section6D_id
 * @property integer $section7_id

 * @property integer $ethical_issues
 *
 * The followings are the available model relations:
 * @property OrganisationInvolvedInfo $section6B
 * @property Application $application
 * @property ProjectDetails $section1
 * @property ResearchGroup $section2
 * @property RisksProcedures $section3
 * @property GainingInformedConsent $section4A
 * @property DataMaterialsProcedures $section4B
 * @property DataCollection $section5A
 * @property DataCollectionCont $section5B
 * @property DataCollectionCont $section6A
 */
class Form1 extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Form1 the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'form1';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('application_id', 'required'),
			array('application_id, section1_id, section2_id, section3_id, section4A_id, section4B_id, section5A_id, section5B_id, section6A_id, section6B_id, section6C_id, section6D_id, section7_id, ethical_issues', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('form_id, application_id, section1_id, section2_id, section3_id, section4A_id, section4B_id, section5A_id, section5B_id, section6A_id, section6B_id, section6C_id, section6D_id, section7_id, ethical_issues', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'section6B' => array(self::BELONGS_TO, 'OrganisationInvolvedInfo', 'section6B_id'),
			'application' => array(self::BELONGS_TO, 'Application', 'application_id'),
			'section1' => array(self::BELONGS_TO, 'ProjectDetails', 'section1_id'),
			'section2' => array(self::BELONGS_TO, 'ResearchGroup', 'section2_id'),
			'section3' => array(self::BELONGS_TO, 'RisksProcedures', 'section3_id'),
			'section4A' => array(self::BELONGS_TO, 'GainingInformedConsent', 'section4A_id'),
			'section4B' => array(self::BELONGS_TO, 'DataMaterialsProcedures', 'section4B_id'),
			'section5A' => array(self::BELONGS_TO, 'DataCollection', 'section5A_id'),
			'section5B' => array(self::BELONGS_TO, 'DataCollectionCont', 'section5B_id'),
			'section6A' => array(self::BELONGS_TO, 'DataCollectionCont', 'section6A_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'form_id' => 'Form',
			'application_id' => 'Application',
			'section1_id' => 'Section1',
			'section2_id' => 'Section2',
			'section3_id' => 'Section3',
			'section4A_id' => 'Section4 A',
			'section4B_id' => 'Section4 B',
			'section5A_id' => 'Section5 A',
			'section5B_id' => 'Section5 B',
			'section6A_id' => 'Section6 A',
			'section6B_id' => 'Section6 B',
			'section6C_id' => 'Section6 C',
			'section6D_id' => 'Section6 D',
			'section7_id' => 'Section7',
			'section8_id' => 'Section8',
			'section9_id' => 'Section9',
			'ethical_issues' => 'Ethical Issues',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('form_id',$this->form_id);
		$criteria->compare('application_id',$this->application_id);
		$criteria->compare('section1_id',$this->section1_id);
		$criteria->compare('section2_id',$this->section2_id);
		$criteria->compare('section3_id',$this->section3_id);
		$criteria->compare('section4A_id',$this->section4A_id);
		$criteria->compare('section4B_id',$this->section4B_id);
		$criteria->compare('section5A_id',$this->section5A_id);
		$criteria->compare('section5B_id',$this->section5B_id);
		$criteria->compare('section6A_id',$this->section6A_id);
		$criteria->compare('section6B_id',$this->section6B_id);
		$criteria->compare('section6C_id',$this->section6C_id);
		$criteria->compare('section6D_id',$this->section6D_id);
		$criteria->compare('section7_id',$this->section7_id);
		$criteria->compare('section8_id',$this->section8_id);
		$criteria->compare('section9_id',$this->section9_id);
		$criteria->compare('ethical_issues',$this->ethical_issues);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}