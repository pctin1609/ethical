<?php

class ChangePasswordForm extends CFormModel
{
    public $currentPassword;
    public $newPassword;
    public $repeatPassword;

    public function rules()
    {
        return array(
            array('currentPassword, newPassword, repeatPassword,', 'required'),
            array('newPassword', 'length', 'min' => 6),
            array('currentPassword', 'required', 'on' => 'changePassword'),
            array('repeatPassword', 'compare', 'compareAttribute' => 'newPassword')
        );
    }

    public function attributeLabels()
    {
        return array(
            'repeatPassword' => 'Repeat New Password'
        );
    }
}