<?php

/**
 * This is the model class for table "low_risk_research".
 *
 * The followings are the available columns in table 'low_risk_research':
 * @property integer $form_id
 * @property integer $application_id
 * @property integer $section1_id
 * @property integer $section2_id
 * @property integer $section3_id
 * @property integer $section4_id
 * @property integer $section5_id
 * @property integer $section6_id
 * @property integer $section7A_id
 * @property integer $section7B_id
 * @property integer $section8A_id
 * @property integer $section8B_id
 * @property integer $section9A_id
 * @property integer $section9B_id
 * @property integer $section9C_id
 * @property integer $section9D_id
 * @property integer $section10_id
 * @property integer $section11_id
 * @property integer $section12_id
 */
class LowRiskResearch extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LowRiskResearch the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'low_risk_research';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('application_id', 'required'),
			array('application_id, section1_id, section2_id, section3_id, section4_id, section5_id, section6_id, section7A_id, section7B_id, section8A_id, section8B_id, section9A_id, section9B_id, section9C_id, section9D_id, section10_id, section11_id, section12_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('form_id, application_id, section1_id, section2_id, section3_id, section4_id, section5_id, section6_id, section7A_id, section7B_id, section8A_id, section8B_id, section9A_id, section9B_id, section9C_id, section9D_id, section10_id, section11_id, section12_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'form_id' => 'Form',
			'application_id' => 'Application',
			'section1_id' => 'Section1',
			'section2_id' => 'Section2',
			'section3_id' => 'Section3',
			'section4_id' => 'Section4',
			'section5_id' => 'Section5',
            'section6_id' => 'Section5',
			'section9A_id' => 'Section9 A',
			'section9B_id' => 'Section9 B',
			'section9C_id' => 'Section9 C',
			'section9D_id' => 'Section9 D',
			'section7A_id' => 'Section7 A',
            'section7B_id' => 'Section7 B',
			'section8A_id' => 'Section8 A',
            'section8B_id' => 'Section8 A',
			'section10_id' => 'Section10',
			'section11_id' => 'Section11',
			'section12_id' => 'Section12',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('form_id',$this->form_id);
		$criteria->compare('application_id',$this->application_id);
		$criteria->compare('section1_id',$this->section1_id);
		$criteria->compare('section2_id',$this->section2_id);
		$criteria->compare('section3_id',$this->section3_id);
		$criteria->compare('section4_id',$this->section4_id);
		$criteria->compare('section5_id',$this->section5_id);
		$criteria->compare('section6_id',$this->section6_id);
		$criteria->compare('section7A_id',$this->section7A_id);
		$criteria->compare('section7B_id',$this->section7B_id);
		$criteria->compare('section8A_id',$this->section8A_id);
		$criteria->compare('section8B_id',$this->section8B_id);
		$criteria->compare('section9A_id',$this->section9A_id);
		$criteria->compare('section9B_id',$this->section9B_id);
		$criteria->compare('section9C_id',$this->section9C_id);
		$criteria->compare('section9D_id',$this->section9D_id);
		$criteria->compare('section10_id',$this->section10_id);
		$criteria->compare('section11_id',$this->section11_id);
		$criteria->compare('section12_id',$this->section12_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}