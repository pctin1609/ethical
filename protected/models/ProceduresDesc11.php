<?php

/**
 * This is the model class for table "procedures_desc".
 *
 * The followings are the available columns in table 'procedures_desc':
 * @property integer $section_id
 * @property string $grp_1
 * @property string $grp_2
 * @property string $grp_3
 * @property string $grp_4
 */
class ProceduresDesc11 extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ProceduresDesc11 the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'procedures_desc';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('grp_1, grp_2, grp_3, grp_4', 'required'),
			array('grp_1, grp_2, grp_3, grp_4', 'length', 'max'=>400),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('section_id, grp_1, grp_2, grp_3, grp_4', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'section_id' => 'Section',
			'grp_1' => 'Group 1',
			'grp_2' => 'Group 2',
			'grp_3' => 'Group 3',
			'grp_4' => 'Group 4',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('section_id',$this->section_id);
		$criteria->compare('grp_1',$this->grp_1,true);
		$criteria->compare('grp_2',$this->grp_2,true);
		$criteria->compare('grp_3',$this->grp_3,true);
		$criteria->compare('grp_4',$this->grp_4,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}