<?php

/**
 * This is the model class for table "multi_centre_research".
 *
 * The followings are the available columns in table 'multi_centre_research':
 * @property integer $form_id
 * @property integer $section1_id
 * @property integer $section2_id
 * @property integer $section3_id
 */
class MultiCentreResearch extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return MultiCentreResearch the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'multi_centre_research';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('application_id', 'required'),
            array('section1_id, section2_id, section3_id', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('application_id,form_id, section1_id, section2_id, section3_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'application' => array(self::BELONGS_TO, 'Application', 'application_id'),
            'section1' => array(self::BELONGS_TO, 'DetailsOfProject', 'section1_id'),
            'section2' => array(self::BELONGS_TO, 'PotentiallyVulnerableParticipants', 'section2_id'),
            'section3' => array(self::BELONGS_TO, 'DataLocation', 'section3_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'application_id' => 'Application',
            'form_id' => 'Form',
            'section1_id' => 'Section1',
            'section2_id' => 'Section2',
            'section3_id' => 'Section3',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
        $criteria->compare('application_id',$this->application_id);
        $criteria->compare('form_id', $this->form_id);
        $criteria->compare('section1_id', $this->section1_id);
        $criteria->compare('section2_id', $this->section2_id);
        $criteria->compare('section3_id', $this->section3_id);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

}