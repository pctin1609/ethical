<?php

/**
 * This is the model class for table "profile".
 *
 * The followings are the available columns in table 'profile':
 * @property string $user_name
 * @property string $first_name
 * @property string $title
 * @property string $surname
 * @property string $gender
 * @property integer $contact_number
 * @property string $email
 * @property String $designation
 * @property String $profile_picture 
 * The followings are the available model relations:
 * @property User $user
 */
class Profile extends CActiveRecord
{

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Profile the static model class
     */
            public $verifyCode, $password, $password_repeat, $email_repeat, $valid, $create;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'profile';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {

        return array(
            array('user_name, email', 'unique'),
            array('user_name, first_name, title, surname, gender, contact_number, email,designation', 'required'),
            array('contact_number', 'numerical', 'integerOnly' => true),
            array('user_name, first_name, surname', 'length', 'max' => 20),
            array('user_name, first_name, surname', 'length', 'min' => 2),
            //array('first_name,surname', 'filter', 'filter'=>'strtoupper'),
            array('title', 'length', 'max' => 8),
            array('gender', 'length', 'max' => 6),
            array('profile_picture', 'file', 'types' => 'gif,jpg,png', 'allowEmpty' => true),
            array('email', 'length', 'max' => 65),
            array('email', 'email'),
            array('password,password_repeat,email_repeat,verifyCode', 'safe'),
            array('password', 'length', 'max' => 64, 'min' => 6),
            array('surname', 'match', 'pattern' => '/^[A-Za-z_]+$/u'),
            array('first_name', 'match', 'pattern' => '/^[A-Za-z_]+$/u', 'message' => 'First Name has word form A-Z and a-z'),
            array('password', 'required', 'on' => 'register'),
            array('verifyCode', 'captcha', 'on' => 'register'),
            array('email,password', 'compare', 'on' => 'register'),
//array('password_repeat', 'match', 'pattern'=>'/^[a-z0-9_\-]{5,}/i', 'message'=>'Passwords do not match.'),

            array('user_name, first_name, title, surname, gender, contact_number, email', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'users' => array(self::HAS_ONE, 'User', 'user_name'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'user_name' => 'Username',
            'first_name' => 'First Name',
            'title' => 'Title',
            'surname' => 'Surname',
            'gender' => 'Gender',
            'profile_picture' => 'Profile Picture(Optional)',
            'designation' => 'Role',
            'contact_number' => 'Contact Number',
            'email' => 'Email',
            'password' => 'Password ',
            'password_repeat' => 'Confirm your Password',
            'email_repeat' => 'Confirm your Email',
            'verifyCode' => 'Verification Code',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('user_name', $this->user_name, true);
        $criteria->compare('first_name', $this->first_name, true);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('surname', $this->surname, true);
        $criteria->compare('gender', $this->gender, true);
        $criteria->compare('contact_number', $this->contact_number);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('designation', $this->designation, true);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

    public function searchStaff()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        $criteria = new CDbCriteria;

        if ($this->user_name != null) {
            $criteria->addCondition("user_name LIKE CONCAT('%', :user_name , '%') OR surname LIKE CONCAT('%', :user_name , '%')OR email LIKE CONCAT('%', :user_name , '%')");
            $criteria->params = array(':user_name' => $this->user_name);
        }
        $criteria->compare('designation', 'staff');


        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function searchStudent()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        $criteria = new CDbCriteria;

        if ($this->user_name != null) {
            $criteria->addCondition("user_name LIKE CONCAT('%', :user_name , '%') OR surname LIKE CONCAT('%', :user_name , '%')OR email LIKE CONCAT('%', :user_name , '%')");
            $criteria->params = array(':user_name' => $this->user_name);
        }
        $criteria->compare('designation', 'student');


        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * @return the types of titles which can be selected
     */
    public function getTitle()
    {
        return array('Mr' => 'Mr', 'Miss' => 'Miss', 'Ms' => 'Ms', 'Mrs' => 'Mrs', 'Prof' => 'Prof', 'Dr' => 'Dr');
    }

    /**
     * @return array the gender which can be selected 
     */
    public function getSex()
    {
        return array('Male' => 'Male', 'Female' => 'Female');
    }

    public function getDesignation()
    {
        return array('' => '--Select one--', 'Staff' => 'Staff', 'Student' => 'Student');
    }

//        public function beforeSave() {
//           if($image=  CUploadedFile::getInstanceByName($this,'profile_picture')){
//               if($image->size>0){
//                   $this->profile_picture=  file_get_contents($image->tempName);
//                   //$this->profile_picture_image_type
//                   
//                   return parent::beforeSave();
//               }
//           }
//        }
}