<?php

/**
 * This is the model class for table "position_indv".
 *
 * The followings are the available columns in table 'position_indv':
 * @property integer $section_id
 * @property string $indv_name
 * @property string $indv_pos
 * @property string $org
 */
class PositionIndv extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PositionIndv the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'position_indv';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('indv_name, indv_pos, org', 'required'),
			array('indv_name, indv_pos, org', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('section_id, indv_name, indv_pos, org', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'section_id' => 'Section',
			'indv_name' => 'Name of individual ',
			'indv_pos' => 'Position of the individual',
			'org' => 'Organisation',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('section_id',$this->section_id);
		$criteria->compare('indv_name',$this->indv_name,true);
		$criteria->compare('indv_pos',$this->indv_pos,true);
		$criteria->compare('org',$this->org,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}