<?php

/**
 * This is the model class for table "compliance_privacy_legislation_low".
 *
 * The followings are the available columns in table 'compliance_privacy_legislation_low':
 * @property integer $section_id
 * @property string $collecting
 * @property string $answer
 * @property string $other_explain
 * @property string $researchers_access_data
 * @property string $data_retained
 * @property string $victorian_privacy
 * @property string $information_handled
 * @property string $descibe_procedures
 * @property string $collection_of_information
 * @property string $information_collected
 * @property string $identity_of_organisation
 * @property string $purpose
 * @property string $periods_for_records
 * @property string $steps_taken
 * @property string $types
 * @property string $privacy_protected
 * @property string $access_information
 * @property string $law_required
 * @property string $consequences
 * @property string $sensi_infor
 * @property string $health_infor
 */
class CompliancePrivacyLegislationLow extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CompliancePrivacyLegislationLow the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'compliance_privacy_legislation_low';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('collecting, answer, researchers_access_data, data_retained, victorian_privacy, information_handled, descibe_procedures, collection_of_information, identity_of_organisation, purpose, periods_for_records, steps_taken, types, privacy_protected, access_information, law_required, consequences', 'required'),
			array('collecting, answer, other_explain, researchers_access_data, data_retained, victorian_privacy, information_handled, descibe_procedures, collection_of_information, information_collected, sensi_infor, health_infor, identity_of_organisation, purpose, periods_for_records, steps_taken, types, privacy_protected, access_information, law_required, consequences', 'length', 'max'=>100),
			array('health_infor, other_explain, information_collected, sensi_infor', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('section_id, collecting, answer, other_explain, researchers_access_data, data_retained, victorian_privacy, information_handled, descibe_procedures, collection_of_information, information_collected, sensi_infor, health_infor, identity_of_organisation, purpose, periods_for_records, steps_taken, types, privacy_protected, access_information, law_required, consequences', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'form1s' => array(self::HAS_MANY, 'Form1', 'section6A_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'section_id' => 'Section',
			'collecting' => '9.1 Are you collecting, using or disclosing personal information, health information or sensitive information:',
			'answer' => 'Why is your answer no?',
			'other_explain' => 'lf you clicked on no and you chose other Please Explain',
			'researchers_access_data' => '9.2 University regulations require the following procedures concerning storage of data.  You should indicate your compliance with these regulations by ticking the following three boxes.  Do you agree to comply with each of the following: <br> Only the researchers will have access to the original data.',
			'data_retained' => 'Data will be retained in the Department for at least five years, longer for clinical trials.  If the data are to be retained other than within a department or academic unit, a record of their location must be filed with the Head of the unit and a copy with the secretary',
			'victorian_privacy' => 'Victorian privacy laws require the University to take reasonable steps to destroy or permanently de-identify personal information if it is no longer needed for any purpose (IPP 4.2, Information Privacy Act 2000 (Vic.);',
			'information_handled' => '9.3 If the above regulations (in Q 6.2) are not being adhered to, how will information be handled to safeguard confidentiality?',
			'descibe_procedures' => '9.4 Describe the procedures you will use to protect participants from any distress, embarrassment or other harm that might be caused when the data is reported.',
			'collection_of_information' => '<br> 9.5 Collection of Data <br>9.5a Does the project involve collection of information directly from individuals about themselves?',
			'information_collected' => 'Personal Information',
			'sensi_infor' => 'Sensitive Information',
			'health_infor' => 'Health Information',
			'identity_of_organisation' => '9.5c Does the Participant Information and Consent Form explain the following: <br> The identity of the organisation collecting the information and how to contact it?',
			'purpose' => 'The purposes for which the information is being collected?',
			'periods_for_records' => 'The period for which the records relating to the participant will be kept?',
			'steps_taken' => 'The steps taken to ensure confidentiality and secure storage of data?',
			'types' => 'The types of individuals or organisations to which your organisation usually discloses information of this kind?',
			'privacy_protected' => 'How privacy will be protected in any publication of the information?',
			'access_information' => 'The fact that the individual may access that information?',
			'law_required' => 'Any law that requires the particular information to be collected?',
			'consequences' => 'The consequences (if any) for the individual if all or part of the information is not provided',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('section_id',$this->section_id);
		$criteria->compare('collecting',$this->collecting,true);
		$criteria->compare('answer',$this->answer,true);
		$criteria->compare('other_explain',$this->other_explain,true);
		$criteria->compare('researchers_access_data',$this->researchers_access_data,true);
		$criteria->compare('data_retained',$this->data_retained,true);
		$criteria->compare('victorian_privacy',$this->victorian_privacy,true);
		$criteria->compare('information_handled',$this->information_handled,true);
		$criteria->compare('descibe_procedures',$this->descibe_procedures,true);
		$criteria->compare('collection_of_information',$this->collection_of_information,true);
		$criteria->compare('information_collected',$this->information_collected,true);
		$criteria->compare('sensi_infor',$this->sensi_infor,true);
		$criteria->compare('health_infor',$this->health_infor,true);
		$criteria->compare('identity_of_organisation',$this->identity_of_organisation,true);
		$criteria->compare('purpose',$this->purpose,true);
		$criteria->compare('periods_for_records',$this->periods_for_records,true);
		$criteria->compare('steps_taken',$this->steps_taken,true);
		$criteria->compare('types',$this->types,true);
		$criteria->compare('privacy_protected',$this->privacy_protected,true);
		$criteria->compare('access_information',$this->access_information,true);
		$criteria->compare('law_required',$this->law_required,true);
		$criteria->compare('consequences',$this->consequences,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
public function getReason()
        {
        return array('I am obtaining participants names from a public domain source','I am not obtaining participants names at any point during the research','I am using fully de-identified data from a database.','Other');
        }
	public function getValues()
        {
            return array('Yes','No');
        }
	public function getInformation()
        {
            return array('personal information','sensitive information', 'health information');
        }
	public function getAnswer()
        {
            return array('Yes if there is a law that requires the particular information to be collected','N/A');
        }
	public function getConsequences()
        {
            return array('Yes if the consequences (if any) for the individual if all or part of the information is not provided','N/A');
        }
	}