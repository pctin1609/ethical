<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class ContactForm extends CFormModel
{
	public $name;
	public $email;
	public $subject;
	public $body;
	public $verifyCode;
        public $valid;


	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
           $valid=!Yii::app()->user->getId()==null||!CCaptcha::checkRequirements();
           if(!Yii::app()->user->getId()==null)
           {
               $required=array('subject, body', 'required');

           }  else {
               
           
           $required=array('name, email, subject, body', 'required');

           }
		return array(
			// name, email, subject and body are required
                        $required,                    
			// email has to be a valid email address
		         array('email', 'email'),
			// verifyCode needs to be entered correctly
			array('verifyCode', 'captcha', 'allowEmpty'=>$valid),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			'verifyCode'=>'Verification Code',
                        'email'=>'Your E-mail'
		);
	}
        public function getIssues()
        {
            return(array('General issues'=>'General issues','Security issues'=>'Security issues',));
        }
}