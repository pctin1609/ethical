<?php

/**
 * This is the model class for table "researcher".
 *
 * The followings are the available columns in table 'researcher':
 * @property integer $researcher_id
 * @property string $degree_id
 * @property integer $student_number
 */
class Researcher extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Researcher the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'researcher';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('degree_id, student_number', 'required'),
            array('student_number, degree_id', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('researcher_id, degree_id, student_number', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'degree' => array(self::BELONGS_TO, 'Degree', 'degree_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'researcher_id' => 'Researcher',
            'degree_id' => 'Degree',
            'current_qualification' => 'Current study',
            'student_number' => 'Student Number',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('researcher_id', $this->researcher_id);
        $criteria->compare('degree_id', $this->degree_id, true);
        $criteria->compare('student_number', $this->student_number);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }
}