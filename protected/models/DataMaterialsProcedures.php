<?php

/**
 * This is the model class for table "fm1_section4b".
 *
 * The followings are the available columns in table 'fm1_section4b':
 * @property integer $section_id
 * @property string $data_set
 * @property string $data_collected
 * @property string $ethics_approval
 * @property string $primary_purpose
 * @property string $information_given
 * @property string $statement_used
 * @property string $organisation_informed
 * @property string $procedure_description
 * @property string $method_used
 * @property string $participants_consented
 * @property string $access_NCIS
 * @property string $access_level
 * @property string $usually_access
 * @property string $describe_role
 * @property string $coronial_records
 * @property string $access_aproved
 */
class DataMaterialsProcedures  extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Fm1_Section4b the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'data_materials_procedures';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('data_set, data_collected, ethics_approval, primary_purpose, information_given, organisation_informed,  method_used, participants_consented, access_NCIS, access_level, usually_access,  coronial_records, access_aproved', 'required','message'=>'this field can not be left blank'),
                   
                    //array('statement_used','file','types'=>'doc,docx,txt,pdf','allowEmpty'=>false),

			array('ethics_approval, information_given, procedure_description, access_level, describe_role', 'length', 'max'=>100),
			array('organisation_informed, method_used, participants_consented, access_NCIS, usually_access, coronial_records, access_aproved', 'length', 'max'=>100),
			array('procedure_description,describe_role', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('section_id, data_set, data_collected, ethics_approval, primary_purpose, information_given, organisation_informed, procedure_description, method_used, participants_consented, access_NCIS, access_level, usually_access, describe_role, coronial_records, access_aproved', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'section_id' => 'Section',
			'data_set' => '4b.1 Please describe the form of the data set',
			'data_collected' => '4b.2 How was the data originally collected?',
			'ethics_approval' => '4b.3 Did you obtain Ethics approval at the time of data collection? Please provide approval number',
			'primary_purpose' => '4b.4 What was the primary purpose for original collection of data?',
			'information_given' => '4b.5 Please explain what information the participants were given at the time the data was collected from them and how that information was given to them (eg Explanatory Statement, verbal explanation).  Your explanation will be determined by how the data was originally collected.',
			
			'organisation_informed' => '4b.6 Will the organisation who owns the dataset be fully informed about the true nature of the research?',
			'procedure_description' => 'If NO, please describe the procedure and explain why the real purpose needs to be concealed',
			'method_used' => '4b.7 Please explain the method used for obtaining consent from the original participants for the original collection and use of the data.',
			'participants_consented' => '4b.8 Have all the participants consented to the use of the information for the purposes of this research project? ',
			'access_NCIS' => '4b.9a Does your project involve access to the National Coronial Information System (NCIS)? ',
			'access_level' => '4b.9b What level of access will you be seeking access to?',
			'usually_access' => '4b.9c Do you usually have access to this information in any other role that you have',
			'describe_role' => 'If YES, please describe that role and how you will manage your dual role in the context of this research project ',
			'coronial_records' => '4b.10 Does your project involve access to coronial records from the Coroners Court of Victoria?',
			'access_aproved' => 'If YES, have you had your request for access approved by the State Coroner via an application to the Coroners Court of Victoria : Research Committee?',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('section_id',$this->section_id);
		$criteria->compare('data_set',$this->data_set,true);
		$criteria->compare('data_collected',$this->data_collected,true);
		$criteria->compare('ethics_approval',$this->ethics_approval,true);
		$criteria->compare('primary_purpose',$this->primary_purpose,true);
		$criteria->compare('information_given',$this->information_given,true);
		
		$criteria->compare('organisation_informed',$this->organisation_informed,true);
		$criteria->compare('procedure_description',$this->procedure_description,true);
		$criteria->compare('method_used',$this->method_used,true);
		$criteria->compare('participants_consented',$this->participants_consented,true);
		$criteria->compare('access_NCIS',$this->access_NCIS,true);
		$criteria->compare('access_level',$this->access_level,true);
		$criteria->compare('usually_access',$this->usually_access,true);
		$criteria->compare('describe_role',$this->describe_role,true);
		$criteria->compare('coronial_records',$this->coronial_records,true);
		$criteria->compare('access_aproved',$this->access_aproved,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
                
	}
        public function getInformedConsentTypes()
        {
        return array(
            'Implied consent'=>'Implied consent',
            'Consent form'=>'Consent form',
            'Consent not obtained'=>'Consent not obtained',
            'Other'=>'Other');
        }
	
	public function getAccessLevel()
        {
            return array(
                'Level 1'=>'Level 1',
                'Level 2'=>'Level 2');
        }
	public function getAccessApproved()
        {
            return array('Pending'=>'Pending',
                'Request for access has been approved by the State Coroner'=>'Request for access has been approved by the State Coroner');
        }
	public function getValues()
        {
            return array('Yes','No');
        }
}