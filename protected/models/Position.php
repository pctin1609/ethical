<?php

/**
 * This is the model class for table "position".
 *
 * The followings are the available columns in table 'position':
 * @property integer $position_id
 * @property string $position_name
 * @property integer $position_level
 * @property string $department_name
 *
 * The followings are the available model relations:
 * @property Staff[] $staffs
 */
class Position extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Position the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'position';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('position_name, position_level, department_name', 'required'),
			array('position_id, position_level', 'numerical', 'integerOnly'=>true),
			array('position_name, department_name', 'length', 'max'=>35),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('position_id, position_name, position_level, department_name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'staffs' => array(self::MANY_MANY, 'Staff', 'occupy(position_id, staff_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'position_id' => 'Position',
			'position_name' => 'Position Name',
			'position_level' => 'Position Level',
			'department_name' => 'Department',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('position_id',$this->position_id);
		$criteria->compare('position_name',$this->position_name,true);
		$criteria->compare('position_level',$this->position_level);
		$criteria->compare('department_name',$this->department_name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        public function getDepartment()
        {
            return array('Adminstration'=>'Adminstration','Research'=>'Research','Finance'=>'Finance','Ademic'=>'Academic','Information Technology'=>'Information Technology');
        }
        public function isPositionExisting($position)
        {
           $positions = Position::model()->findAll();
           foreach ($positions as $oldPosition) {
               if($oldPosition->position_name==$position->position_name&&$oldPosition->position_level==$position->position_level&&$oldPosition->department_name==$position->department_name)
                   {
                   return true; 
                   }   
           }
            return false;
        }
}