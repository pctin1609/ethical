<?php

/**
 * This is the model class for table "potentially_vulnerable_participants".
 *
 * The followings are the available columns in table 'potentially_vulnerable_participants':
 * @property integer $section_id
 * @property string $women_pregnant
 * @property string $children_young
 * @property string $intellectual_disability
 * @property string $forensic_patient
 * @property string $impaired_capacity
 * @property string $prisoners_parole
 * @property string $children_wards_of_the_state
 * @property string $dependent_on_medical_care
 * @property string $examining_potentially
 * @property string $seeking_disclosure
 * @property string $participation_in_illegal_activities
 * @property string $other_vulnerable_participants
 * @property string $if_yes_other_specify
 */
class PotentiallyVulnerableParticipants extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PotentiallyVulnerableParticipants the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'potentially_vulnerable_participants';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('women_pregnant, children_young, intellectual_disability, forensic_patient, impaired_capacity, prisoners_parole, children_wards_of_the_state, dependent_on_medical_care, examining_potentially, seeking_disclosure, participation_in_illegal_activities, other_vulnerable_participants', 'required'),
			array('women_pregnant, children_young, intellectual_disability', 'length', 'max'=>250),
			array('forensic_patient, impaired_capacity, prisoners_parole, children_wards_of_the_state, dependent_on_medical_care, examining_potentially, seeking_disclosure, participation_in_illegal_activities, other_vulnerable_participants', 'length', 'max'=>25),
			array('if_yes_other_specify', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('section_id, women_pregnant, children_young, intellectual_disability, forensic_patient, impaired_capacity, prisoners_parole, children_wards_of_the_state, dependent_on_medical_care, examining_potentially, seeking_disclosure, participation_in_illegal_activities, other_vulnerable_participants, if_yes_other_specify', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'section_id' => 'Section',
			'women_pregnant' => ' Women who are pregnant and the human foetus',
			'children_young' => 'Children or young people under the age of 18',
			'intellectual_disability' => 'Children or young people aged 16 or 17 whose circumstances indicate that they are capable of giving informed consent, e.g. University students aged 17.',
			'forensic_patient' => 'Persons with an intellectual disability or mental impairment of any kind, includes: intellectual or mental impairment,mental disorder, brain injury,dementia',
            'impaired_capacity' => 'Persons considered to be:a forensic patient; or,an involuntary patient; or,a security patient;',
			'prisoners_parole' => 'Prisoners or people on parole',
			'children_wards_of_the_state' => 'Children who are Wards of the State',
			'dependent_on_medical_care' => 'Persons highly dependent on medical care including a person who is unconscious',
			'examining_potentially' => 'Examining potentially sensitive or contentious issues',
			'seeking_disclosure' => 'Seeking disclosure of information which may be prejudicial to participants',
			'participation_in_illegal_activities' => 'Study of or participation in illegal activities',
			'other_vulnerable_participants' => 'Other:',
			'if_yes_other_specify' => 'If Yes Other.  Please specify',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('section_id',$this->section_id);
		$criteria->compare('women_pregnant',$this->women_pregnant,true);
		$criteria->compare('children_young',$this->children_young,true);
		$criteria->compare('intellectual_disability',$this->intellectual_disability,true);
		$criteria->compare('forensic_patient',$this->forensic_patient,true);
		$criteria->compare('impaired_capacity',$this->impaired_capacity,true);
		$criteria->compare('prisoners_parole',$this->prisoners_parole,true);
		$criteria->compare('children_wards_of_the_state',$this->children_wards_of_the_state,true);
		$criteria->compare('dependent_on_medical_care',$this->dependent_on_medical_care,true);
		$criteria->compare('examining_potentially',$this->examining_potentially,true);
		$criteria->compare('seeking_disclosure',$this->seeking_disclosure,true);
		$criteria->compare('participation_in_illegal_activities',$this->participation_in_illegal_activities,true);
		$criteria->compare('other_vulnerable_participants',$this->other_vulnerable_participants,true);
		$criteria->compare('if_yes_other_specify',$this->if_yes_other_specify,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
 public function getValues()
        {
            return array('Yes','No');
        }
	}