<?php

/**
 * This is the model class for table "project_details_low".
 *
 * The followings are the available columns in table 'project_details_low':
 * @property integer $section_id
 * @property string $description
 * @property string $aim
 * @property string $type1_research
 * @property string $type2_research
 * @property string $researcher_finance_involvement
 * @property string $research_funding
 */
class ProjectDetailsLow extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ProjectDetailsLow the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'project_details_low';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('description, aim, type1_research, type2_research, researcher_finance_involvement', 'required','message'=>'this field can not be left blank'),
			array('type1_research, type2_research, researcher_finance_involvement', 'length', 'max'=>250),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('section_id, description, aim, type1_research, type2_research, researcher_finance_involvement', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'section_id' => 'Section',
			'description' => '4a In plain language, give a succinct description of the background and potential significance of the research project',
			'aim' => '4b Clearly state the aims and/or hypotheses of the research project',
			'type1_research' => '4c Type of research ',
			'type2_research' => '4d Type of research - 2',
			'researcher_finance_involvement' => '4f Do any of the researchers have any financial or other involvement in the research (apart from their research role) or will they receive any reward, pecuniary or otherwise?',
                        'research_funding'=>'4e Funding of your research project',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('section_id',$this->section_id);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('aim',$this->aim,true);
		$criteria->compare('type1_research',$this->type1_research,true);
		$criteria->compare('type2_research',$this->type2_research,true);
		$criteria->compare('researcher_finance_involvement',$this->researcher_finance_involvement,true);
                $criteria->compare('research_funding',$this->research_funding,true);


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        public function getResearchTypes()
        {
            return array('Action research'=>'Action research',
                'Case study'=>'Case study',
                'Clinical research'=>'Clinical research',
                'Clinical trial / use of drug or therapeutic device'=>'Clinical trial / use of drug or therapeutic device',
                'Epidemiological'=>'Epidemiological',
                'Oral history/Biographical '=>'Oral history/Biographical ',
                'Medical research'=>'Medical research',
                'Mental health'=>'Mental health',
                'Qualitative'=>'Qualitative',
                'Quantitative '=>'Quantitative ',
                'Other');
        }
        public function getFundingTypes()
        {
        return array('Funding will not be sought'=>'Funding will not be sought','Funding will be sought in the future'=>'Funding will be sought in the future','Funding has been sought'=>'Funding has been sought');
        }
        public function getForm()
        {
            return section1;
        }
                
        
        
}