<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $user_id
 * @property string $user_name
 * @property string $password
 * @property string $last_login
 * @property integer $application_id
 *
 * The followings are the available model relations:
 * @property Administrator $administrator
 * @property Application[] $applications
 * @property Researcher $researcher
 * @property Staff $staff
 * @property Application $application
 * @property Profile $userName
 */
class User extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_name', 'required'),
			array('application_id', 'numerical', 'integerOnly'=>true),
			array('user_name', 'length', 'max'=>20),
			array('password', 'length', 'max'=>256),
			array('last_login', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('user_id, user_name, password, last_login, application_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'administrator' => array(self::HAS_ONE, 'Administrator', 'admin_id'),
			'applications' => array(self::HAS_MANY, 'Application', 'researcher'),
			'researcher' => array(self::HAS_ONE, 'Researcher', 'researcher_id'),
			'staff' => array(self::HAS_ONE, 'Staff', 'staff_id'),
			'userName' => array(self::BELONGS_TO, 'Profile', 'user_name'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_id' => 'User',
			'user_name' => 'User Name',
			'password' => 'Password',
			'last_login' => 'Last Login',
			'application_id' => 'Application',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('user_name',$this->user_name,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('last_login',$this->last_login,true);
		$criteria->compare('application_id',$this->application_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	/**
     * perform one-way encryption on the password before we store it in 
      the database
     */
    protected function afterValidate() {
        parent::afterValidate();
        $this->password = $this->encrypt($this->password);
    }

    public function encrypt($value) {
        return md5($value);
        
    }
    public function getFullName() {
        return $this->user_name;
    }

//    public function getId(){
//        return $this->user_id;
//    }

    public function getSuggest($q) {
        $c = new CDbCriteria();
        $c->addSearchCondition('user_name', $q, true, 'OR');
        $c->addSearchCondition('user_id', $q, true, 'OR');
        return $this->findAll($c);
    }
    public static function getUserApplications($id)
    {
       $applications=  Application::model()->with(
                array(
                    'user'=>array(
                        'select'=>false,
                        'joinType'=>'INNER JOIN',
                        'condition'=>"user.user_id=$id",
                    )
                    )
            )->findAll();
        
    return new CArrayDataProvider($rawData);  
    }
    
      public static function getSuperviseApplication($id){
        $applications = Application::model()->with(
                array(
                    'user'=>array(
                        'select'=>false,
                        'joinType'=>'INNER JOIN',
                        'condition'=>"user.user_id=$id",
                        )
                )
                )->findAll();
         return new CArrayDataProvider($rawData);  
    }

}