<?php

/**
 * This is the model class for table "participants_desc".
 *
 * The followings are the available columns in table 'participants_desc':
 * @property integer $section_id
 * @property integer $pple_num
 * @property string $pple_involved
 * @property string $age_range
 */
class ParticipantsDesc extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ParticipantsDesc the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'participants_desc';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pple_num, pple_involved, age_range', 'required'),
			array('pple_num', 'numerical', 'integerOnly'=>true),
			array('pple_involved', 'length', 'max'=>40),
			array('age_range', 'length', 'max'=>60),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('section_id, pple_num, pple_involved, age_range', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'section_id' => 'Section',
			'pple_num' => 'How many people',
			'pple_involved' => 'Group of people involved',
			'age_range' => 'Age range',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('section_id',$this->section_id);
		$criteria->compare('pple_num',$this->pple_num);
		$criteria->compare('pple_involved',$this->pple_involved,true);
		$criteria->compare('age_range',$this->age_range,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
		public function getInfor()
        {
            return array('1 - 10'=>'1 - 10','11 - 20'=>'11 - 20', '21 - 30'=>'21 - 30', '31 - 40'=>'31 - 40', '41 - 50'=>'41 - 50','51 - 60'=>'51 - 60','61 - 70'=>'61 - 70','71 - 80'=>'71 - 80', '80+'=>'80+');
        }
}