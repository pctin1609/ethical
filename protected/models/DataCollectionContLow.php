<?php

/**
 * This is the model class for table "data_collection_cont_low".
 *
 * The followings are the available columns in table 'data_collection_cont_low':
 * @property integer $id
 * @property string $admin_of_substance
 * @property string $admin_of_substance_details
 * @property string $other
 * @property string $other_description
 * @property string $data_collection_location
 * @property string $data_collection_location_sug
 * @property string $person_to_collect_data
 * @property string $translator_required
 * @property string $translator_required_desc
 * @property string $research_involve_children
 * @property string $research_involve_children_ys
 * @property string $research_involve_children_no
 * @property string $dependent_or_unequal_rlshp
 * @property string $dependent_or_unequal_rlshp_details
 * @property string $admin_involvement
 * @property string $admin_involvement_details
 * @property string $procedures_use_diagonistic_or_indicative
 * @property string $criteria_to_assess_results
 * @property string $duty_of_care_to_participants
 * @property string $acquired_necessary_competence
 * @property string $indicate_procedure_proposed_to_PP
 */
class DataCollectionContLow extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DataCollectionContLow the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'data_collection_cont_low';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('admin_of_substance,  other,  data_collection_location, data_collection_location_sug, person_to_collect_data, translator_required,  research_involve_children, research_involve_children_ys,  dependent_or_unequal_rlshp,  admin_involvement,  procedures_use_diagonistic_or_indicative,  duty_of_care_to_participants, acquired_necessary_competence, indicate_procedure_proposed_to_PP', 'required','message'=>'this field can not be left blank'),
			array('admin_of_substance, other, person_to_collect_data, translator_required, research_involve_children, research_involve_children_ys, dependent_or_unequal_rlshp, admin_involvement, procedures_use_diagonistic_or_indicative, criteria_to_assess_results, duty_of_care_to_participants, acquired_necessary_competence, indicate_procedure_proposed_to_PP', 'length', 'max'=>35),
			array('admin_of_substance_details,other_description,translator_required_desc,research_involve_children_no,dependent_or_unequal_rlshp_details,admin_involvement_details,criteria_to_assess_results ', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('section_id, admin_of_substance, admin_of_substance_details, other, other_description, data_collection_location, data_collection_location_sug, person_to_collect_data, translator_required, translator_required_desc, research_involve_children, research_involve_children_ys, research_involve_children_no, dependent_or_unequal_rlshp, dependent_or_unequal_rlshp_details, admin_involvement, admin_involvement_details, procedures_use_diagonistic_or_indicative, criteria_to_assess_results, duty_of_care_to_participants, acquired_necessary_competence, indicate_procedure_proposed_to_PP', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
		    'id' => 'ID',
			'admin_of_substance' => 'Administration of a substance',
			'admin_of_substance_details' => 'Administration of a substance details',
			'other' => 'Other means of collecting besides the ones listed',
			'other_description' => 'Other Description',
			'data_collection_location' => 'Open text for further details you would like to provide (for example description of experimental protocol, description of complicated data collection method, description of new data collection method).  Attach any additional documents you think would assist the committee. ',
			'data_collection_location_sug' => ' 8.2 Where will the data be collected? <br>If not known, please provide suggested locations.',
			'person_to_collect_data' => '8.3 By whom will the data be collected?',
			'translator_required' => '8.4 Will you require the use of a translator or will you use documentation translated into a language other than English?',
			'translator_required_desc' => 'Please describe how the translator will be utilised',
			'research_involve_children' => '8.5 Does this research involve interactions with children or other vulnerable individuals who are not supervised by a parent/guardian/teacher/carer?  ',
			'research_involve_children_ys' => '8.6 Does the supervisor have a recent written certificate from the police for each researcher certifying that there are no relevant convictions recorded against him/her?',
			'research_involve_children_no' => 'Please explain',
			'dependent_or_unequal_rlshp' => '8.7 Is there a dependent or unequal relationship between any person collecting the data and the participant?',
			'dependent_or_unequal_rlshp_details' => 'If YES, please give details and explain the measures implemented to manage this situation',
			'admin_involvement' => '8.8 Does the research involve the administration of any tests or other procedures that can only be used by people with particular qualifications? ',
			'admin_involvement_details' => 'If YES, give details of the test or procedure',
			'procedures_use_diagonistic_or_indicative' => '8.9a Are any of the measures or procedures you propose using diagnostic or indicative of any medical or clinical condition',
			'criteria_to_assess_results' => 'If YES, please complete the following sections <br> 8.9b Describe the criteria you will use to assess when participants in your research have results indicating that they or others are at risk',
			'duty_of_care_to_participants' => '8.9c How will you deal with your duty of care to the participants in your research identified as at risk?',
			'acquired_necessary_competence' => '8.9d Have you acquired the necessary competence to administer, score and interpret the proposed measures and procedures, with the type of participants being used in this research?',
			'indicate_procedure_proposed_to_PP' => '8.9e Will you indicate the procedure proposed above to potential participants in your explanatory statement?',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('section_id',$this->section_id);
		$criteria->compare('admin_of_substance',$this->admin_of_substance,true);
		$criteria->compare('admin_of_substance_details',$this->admin_of_substance_details,true);
		$criteria->compare('other',$this->other,true);
		$criteria->compare('other_description',$this->other_description,true);
		$criteria->compare('data_collection_location',$this->data_collection_location,true);
		$criteria->compare('data_collection_location_sug',$this->data_collection_location_sug,true);
		$criteria->compare('person_to_collect_data',$this->person_to_collect_data,true);
		$criteria->compare('translator_required',$this->translator_required,true);
		$criteria->compare('translator_required_desc',$this->translator_required_desc,true);
		$criteria->compare('research_involve_children',$this->research_involve_children,true);
		$criteria->compare('research_involve_children_ys',$this->research_involve_children_ys,true);
		$criteria->compare('research_involve_children_no',$this->research_involve_children_no,true);
		$criteria->compare('dependent_or_unequal_rlshp',$this->dependent_or_unequal_rlshp,true);
		$criteria->compare('dependent_or_unequal_rlshp_details',$this->dependent_or_unequal_rlshp_details,true);
		$criteria->compare('admin_involvement',$this->admin_involvement,true);
		$criteria->compare('admin_involvement_details',$this->admin_involvement_details,true);
		$criteria->compare('procedures_use_diagonistic_or_indicative',$this->procedures_use_diagonistic_or_indicative,true);
		$criteria->compare('criteria_to_assess_results',$this->criteria_to_assess_results,true);
		$criteria->compare('duty_of_care_to_participants',$this->duty_of_care_to_participants,true);
		$criteria->compare('acquired_necessary_competence',$this->acquired_necessary_competence,true);
		$criteria->compare('indicate_procedure_proposed_to_PP',$this->indicate_procedure_proposed_to_PP,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function getValues()
        {
            return array('Yes','No');
        }
		
}