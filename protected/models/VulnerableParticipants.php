<?php
/**
 * This is the model class for table "vulnerable_participants".
 *
 * The followings are the available columns in table 'vulnerable_participants':
 * @property integer $section_id
 * @property string $women_pregnant
 * @property string $children_young
 * @property string $intellectual_disability
 * @property string $forensic_patient
 * @property string $impaired_capacity
 * @property string $prisoners_parole
 * @property string $children_wards_of_the_state
 * @property string $dependent_on_medical_care
 * @property string $examining_potentially
 * @property string $seeking_disclosure
 * @property string $participation_in_illegal_activities
 * @property string $other_vulnerable_participants
 * @property string $if_yes_other_specify
 * @property string $working_in_formalized_institutions
 * @property string $collectives
 * @property string $aboriginal
 *
 * The followings are the available model relations:
 * @property LowRiskResearch[] $lowRiskResearches
 */
class VulnerableParticipants extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return VulnerableParticipants the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'vulnerable_participants';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('women_pregnant, children_young, intellectual_disability, forensic_patient, impaired_capacity, prisoners_parole, children_wards_of_the_state, dependent_on_medical_care, examining_potentially, seeking_disclosure, other_vulnerable_participants, working_in_formalized_institutions, collectives, aboriginal', 'required'),
            array('women_pregnant, children_young, intellectual_disability', 'length', 'max' => 250),
            array('forensic_patient, impaired_capacity, prisoners_parole, children_wards_of_the_state, dependent_on_medical_care, examining_potentially, seeking_disclosure, participation_in_illegal_activities, other_vulnerable_participants, working_in_formalized_institutions, collectives, aboriginal', 'length', 'max' => 25),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('section_id, women_pregnant, children_young, intellectual_disability, forensic_patient, impaired_capacity, prisoners_parole, children_wards_of_the_state, dependent_on_medical_care, examining_potentially, seeking_disclosure, participation_in_illegal_activities, other_vulnerable_participants, if_yes_other_specify, working_in_formalized_institutions, collectives, aboriginal', 'safe', 'on' => 'search'),
            array('participation_in_illegal_activities, if_yes_other_specify', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'lowRiskResearches' => array(self::HAS_MANY, 'LowRiskResearch', 'section11_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'section_id' => 'Section',
            'women_pregnant' => '1.13 Women who are pregnant and the human foetus',
            'children_young' => '1.1 Children or young people under the age of 18 ',
            'intellectual_disability' => '1.14 Persons with an intellectual disability or mental impairment of any kind',
            'forensic_patient' => '1.3 Persons considered to be a forensic patient; or an involuntary patient; or a security patient;
',
            'impaired_capacity' => '1.2 Persons with an intellectual disability or mental impairment of any kind',
            'prisoners_parole' => '1.4 Prisoners or people on parole',
            'children_wards_of_the_state' => '1.7 Children who are Wards of the State',
            'dependent_on_medical_care' => '1.8 Persons highly dependent on medical care including a person who is unconscious',
            'examining_potentially' => '1.5 Examining potentially sensitive or contentious issues',
            'seeking_disclosure' => '1.6 Seeking disclosure of information which may be prejudicial to participants',
            'participation_in_illegal_activities' => '1.11 Participation In Illegal Activities',
            'other_vulnerable_participants' => '1.10 Persons not usually considered to be vulnerable but would be considered vulnerable in the context of this research project',
            'if_yes_other_specify' => '1.13 Children or young people aged 16 or 17 whose circumstances indicate that they are capable of giving informed consent, e.g. University students aged 17.',
            'working_in_formalized_institutions' => '1.9 People working in highly formalised institutions (e.g. Defence Force, Police Force)',
            'collectives' => '1.11 Collectivities / communities ',
            'aboriginal' => '1.12 Aboriginal and / or Torres Strait Islanders',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('section_id', $this->section_id);
        $criteria->compare('women_pregnant', $this->women_pregnant, true);
        $criteria->compare('children_young', $this->children_young, true);
        $criteria->compare('intellectual_disability', $this->intellectual_disability, true);
        $criteria->compare('forensic_patient', $this->forensic_patient, true);
        $criteria->compare('impaired_capacity', $this->impaired_capacity, true);
        $criteria->compare('prisoners_parole', $this->prisoners_parole, true);
        $criteria->compare('children_wards_of_the_state', $this->children_wards_of_the_state, true);
        $criteria->compare('dependent_on_medical_care', $this->dependent_on_medical_care, true);
        $criteria->compare('examining_potentially', $this->examining_potentially, true);
        $criteria->compare('seeking_disclosure', $this->seeking_disclosure, true);
        $criteria->compare('participation_in_illegal_activities', $this->participation_in_illegal_activities, true);
        $criteria->compare('other_vulnerable_participants', $this->other_vulnerable_participants, true);
        $criteria->compare('if_yes_other_specify', $this->if_yes_other_specify, true);
        $criteria->compare('working_in_formalized_institutions', $this->working_in_formalized_institutions, true);
        $criteria->compare('collectives', $this->collectives, true);
        $criteria->compare('aboriginal', $this->aboriginal, true);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

    public function getValues()
    {
        return array('Yes', 'No');
    }

}