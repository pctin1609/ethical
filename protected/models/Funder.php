<?php

/**
 * This is the model class for table "funder".
 *
 * The followings are the available columns in table 'funder':
 * @property integer $id
 * @property string $agency
 * @property integer $funding_approved
 * @property string $summary_attached
 * @property integer $fm1_section1
 * @property integer $application_id
 *
 * The followings are the available model relations:
 * @property Fm1Section1 $fm1Section1
 */
class Funder extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Funder the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'funder';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('agency, funding_approved, summary_attached,', 'required'),
			array('funding_approved, fm1_section1, application_id', 'numerical', 'integerOnly'=>true),
			array('agency, summary_attached', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, agency, funding_approved, summary_attached, fm1_section1, application_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'fm1Section1' => array(self::BELONGS_TO, 'Fm1Section1', 'fm1_section1'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'agency' => 'Agency',
			'funding_approved' => 'Funding Approved',
			'summary_attached' => 'Summary Attached',
			'fm1_section1' => 'Fm1 Section1',
			'application_id' => 'Application',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('agency',$this->agency,true);
		$criteria->compare('funding_approved',$this->funding_approved);
		$criteria->compare('summary_attached',$this->summary_attached,true);
		$criteria->compare('fm1_section1',$this->fm1_section1);
		$criteria->compare('application_id',$this->application_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}