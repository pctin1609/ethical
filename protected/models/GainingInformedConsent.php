<?php

/**
 * This is the model class for table "fm1_section4a".
 *
 * The followings are the available columns in table 'fm1_section4a':
 * @property integer $section_id
 * @property string $explanatory_statement
 * @property string $exp_2_participants
 * @property string $research_nature
 * @property string $describe_procedure
 * @property string $obtain_consent
 * @property string $participants_informed
 * @property string $other_specify
 * @property string $obtain_informed_consent
 * @property string $witness_necessity
 * @property string $independent_witness_necessity
 */
class GainingInformedConsent extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return fm1_section4a the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gaining_informed_consent';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('explanatory_statement,research_nature, obtain_consent,  obtain_informed_consent, witness_necessity', 'required','message'=>'this field can not be left blank'),
			array('explanatory_statement, exp_2_participants, research_nature, describe_procedure, obtain_consent, participants_informed, other_specify, obtain_informed_consent, witness_necessity, independent_witness_necessity', 'length', 'max'=>100),
			array('participants_informed, other_specify', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('section_id, explanatory_statement, exp_2_participants, research_nature, describe_procedure, obtain_consent, participants_informed, other_specify, obtain_informed_consent, witness_necessity, independent_witness_necessity', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'section_id' => 'Section',
			'explanatory_statement' => '4a.1 Will you use a written Explanatory Statement to inform each participant about the research project? ',
			'exp_2_participants' => 'If no, describe how and by whom the explanation will be given to participants.',
			'research_nature' => '4a.2 Will all participants, including organisations, be fully informed about the true nature of the research?',
			'describe_procedure' => 'If NO, please describe the procedure and explain why the real purpose needs to be concealed',
			'obtain_consent' => '4a.3 Please explain how you will obtain informed consent from your participants.  If you are not using a consent form, explain why one is unnecessary or inappropriate.',
			'participants_informed' => 'Consent form: Please explain the process by which the participants will give consent and how the consent form will be returned to the researcher',
			'other_specify' => 'Other, please specify',
			'obtain_informed_consent' => '4a.4 If the participants in your study are unable to consent for themselves, explain how you intend to obtain informed consent.  How will adequate information be provided to those who will give consent on their behalf?',
			'witness_necessity' => '4a.5 Is an independent witness to the participants consent necessary? ',
			'independent_witness_necessity' => 'If YES, please explain why',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('section_id',$this->section_id);
		$criteria->compare('explanatory_statement',$this->explanatory_statement,true);
		$criteria->compare('exp_2_participants',$this->exp_2_participants,true);
		$criteria->compare('research_nature',$this->research_nature,true);
		$criteria->compare('describe_procedure',$this->describe_procedure,true);
		$criteria->compare('obtain_consent',$this->obtain_consent,true);
		$criteria->compare('participants_informed',$this->participants_informed,true);
		$criteria->compare('other_specify',$this->other_specify,true);
		$criteria->compare('obtain_informed_consent',$this->obtain_informed_consent,true);
		$criteria->compare('witness_necessity',$this->witness_necessity,true);
		$criteria->compare('independent_witness_necessity',$this->independent_witness_necessity,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        public function getInformedConsentTypes()
        {
        return array(
            'Implied consent'=>'Implied consent',
            'Consent form'=>'Consent form', 
            'Other'=>'Other');
        }

	public function getValues()
        {
            return array('Yes','No');
        }
	
	
	
}