<?php

/**
 * This is the model class for table "staff".
 *
 * The followings are the available columns in table 'staff':
 * @property integer $staff_id
 * @property integer $staff_id_number
 * @property integer $validated
 * @property integer $available
 * @property string $unavailable_start_time
 * @property string $unavailable_end_time
 *
 * The followings are the available model relations:
 * @property Application[] $applications
 * @property Application[] $applications1
 * @property Position[] $positions
 * @property User $staff
 */
class Staff extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Staff the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'staff';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('staff_id, staff_id_number, validated, available', 'required'),
			array('staff_id, staff_id_number, validated, available', 'numerical', 'integerOnly'=>true),
			array('unavailable_start_time, unavailable_end_time', 'safe'),
       // array('unavailable_end_time','compare','compareAttribute'=>'unavailable_start_time','operator'=>'>', 'allowEmpty'=>false , 'message'=>'please note that start time cant be after end time.'
//),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('staff_id, staff_id_number, validated, available, unavailable_start_time, unavailable_end_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'applications' => array(self::MANY_MANY, 'Application', 'administor(staff_id, project_id)'),
			'applications1' => array(self::HAS_MANY, 'Application', 'supervisor'),
			'positions' => array(self::MANY_MANY, 'Position', 'occupy(staff_id, position_id)'),
			'staff' => array(self::BELONGS_TO, 'User', 'staff_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'staff_id' => 'Staff',
			'staff_id_number' => 'Staff Id Number',
			'validated' => 'Validated',
			'available' => 'Available',
			'unavailable_start_time' => 'Set unavailable start date',
			'unavailable_end_time' => 'Set unavailable end date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('staff_id',$this->staff_id);
		$criteria->compare('staff_id_number',$this->staff_id_number);
		$criteria->compare('validated',$this->validated);
		$criteria->compare('available',$this->available);
		$criteria->compare('unavailable_start_time',$this->unavailable_start_time,true);
		$criteria->compare('unavailable_end_time',$this->unavailable_end_time,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
          
}