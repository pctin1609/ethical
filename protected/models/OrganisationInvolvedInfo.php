<?php

/**
 * This is the model class for table "organisation_involved_info".
 *
 * The followings are the available columns in table 'organisation_involved_info':
 * @property integer $section_id
 * @property string $involve_collection
 * @property string $collection_without_consent
 * @property string $collection_identifiable
 * @property string $identifiable_information
 * @property string $disclosure_of_identifiable_information
 * @property string $collect_info_from_third_party
 * @property string $victorian_private
 * @property string $organisation_other
 * @property string $a_data_set
 * @property string $victorian_govt_depart
 * @property string $victorian_source
 * @property string $commonwealth_agency
 * @property string $agency_from_another_state
 * @property string $organization_defined
 * @property string $individual_carer
 * @property string $other_infor
 * @property string $category
 * @property string $information_of_records
 * @property string $agree_to_provide_information
 * @property string $explain_if_no
 * @property string $evidence
 * @property string $seperate_HREC_approval
 * @property string $routinely_access_info
 * @property string $type_of_info
 * @property string $sensitive_information
 * @property string $health_information
 * @property string $type_of_organisation_health
 * @property string $victorian_private_sector
 * @property string $commonwealth
 * @property string $other_organisation
 * @property string $type_of_organisation_personal
 * @property string $victor_private_sector
 * @property string $common_public_sector
 * @property string $other_personal
 * @property string $type_of_organisation_sensitive
 * @property string $vic_private_sector
 * @property string $com_public_sector
 * @property string $other_senstive
 * @property string $privacy_principle_health
 * @property string $HPP1_health
 * @property string $IPP11_health
 * @property string $NPP1_health
 * @property string $privacy_principle_personal
 * @property string $npp1_personal
 * @property string $ipp11_personal
 * @property string $npp10_personal
 * @property string $privacy_principle_sensitive
 * @property string $npp0_sensitive
 * @property string $ipp11_senstive
 * @property string $npp10_sensitive
 * @property string $de_intefied_form
 * @property string $not_obtain_consent
 * @property string $reasons
 *
 * The followings are the available model relations:
 * @property Form1[] $form1s
 */
class OrganisationInvolvedInfo extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return OrganisationInvolvedInfo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'organisation_involved_info';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('involve_collection, collection_without_consent, collection_identifiable, category, information_of_records, agree_to_provide_information, seperate_HREC_approval, routinely_access_info, de_intefied_form, not_obtain_consent, reasons', 'required'),
			array('involve_collection, collection_without_consent, collection_identifiable, identifiable_information, disclosure_of_identifiable_information, collect_info_from_third_party, victorian_private, organisation_other, a_data_set, victorian_govt_depart, victorian_source, commonwealth_agency, agency_from_another_state, organization_defined, individual_carer, other_infor, category, information_of_records, agree_to_provide_information, explain_if_no, evidence, seperate_HREC_approval, routinely_access_info, type_of_info, sensitive_information, health_information, type_of_organisation_health, victorian_private_sector, commonwealth, other_organisation, type_of_organisation_personal, victor_private_sector, common_public_sector, other_personal, type_of_organisation_sensitive, vic_private_sector, com_public_sector, other_senstive, privacy_principle_health, HPP1_health, IPP11_health, NPP1_health, privacy_principle_personal, npp1_personal, ipp11_personal, npp10_personal, privacy_principle_sensitive, npp0_sensitive, ipp11_senstive, npp10_sensitive, de_intefied_form, not_obtain_consent, reasons', 'length', 'max'=>100),
			array('npp0_sensitive, ipp11_senstive, npp10_sensitive, npp10_personal, ipp11_personal, npp1_personal, privacy_principle_personal, IPP11_health, NPP1_health, HPP1_health, privacy_principle_health, other_senstive, com_public_sector, vic_private_sector, commonwealth, other_organisation, other_personal, common_public_sector, victor_private_sector, type_of_organisation_personal, victorian_private_sector, type_of_organisation_health, health_information, sensitive_information, type_of_info, explain_if_no, evidence, collect_info_from_third_party, victorian_private, organisation_other, a_data_set, victorian_govt_depart, victorian_source, commonwealth_agency, agency_from_another_state, organization_defined, individual_carer, other_infor', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('section_id, involve_collection, collection_without_consent, collection_identifiable, identifiable_information, disclosure_of_identifiable_information, collect_info_from_third_party, victorian_private, organisation_other, a_data_set, victorian_govt_depart, victorian_source, commonwealth_agency, agency_from_another_state, organization_defined, individual_carer, other_infor, category, information_of_records, agree_to_provide_information, explain_if_no, evidence, seperate_HREC_approval, routinely_access_info, type_of_info, sensitive_information, health_information, type_of_organisation_health, victorian_private_sector, commonwealth, other_organisation, type_of_organisation_personal, victor_private_sector, common_public_sector, other_personal, type_of_organisation_sensitive, vic_private_sector, com_public_sector, other_senstive, privacy_principle_health, HPP1_health, IPP11_health, NPP1_health, privacy_principle_personal, npp1_personal, ipp11_personal, npp10_personal, privacy_principle_sensitive, npp0_sensitive, ipp11_senstive, npp10_sensitive, de_intefied_form, not_obtain_consent, reasons', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'form1s' => array(self::HAS_MANY, 'Form1', 'section6B_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'section_id' => 'Section',
			'involve_collection' => '6.6a Does the project involve the collection, use or disclosure of identified or potentially identifiable information from sources other than the individual whose information it is?',
			'collection_without_consent' => '6.6b Does the project involve the collection, use or disclosure of information without the consent of the individual whose information it is (or their legal guardian)?',
			'collection_identifiable' => '6.7 Are you seeking approval from this HREC for the: <br>Collection of identifiable information from a third party?',
			'identifiable_information' => 'Use of identifiable information?',
			'disclosure_of_identifiable_information' => 'Disclosure of identifiable information?',
			'collect_info_from_third_party' => 'A Victorian public health service ',
			'victorian_private' => 'A Victorian private health service provider',
			'organisation_other' => 'An organisation other than a health service provider',
			'a_data_set' => ' A data set under the auspices of the Victorian DHS',
			'victorian_govt_depart' => 'A data set under the auspices of another Victorian government department',
			'victorian_source' => 'A data set from another Victorian source',
			'commonwealth_agency' => 'A Commonwealth agency',
			'agency_from_another_state' => 'An agency from another state',
			'organization_defined' => 'An "organisation" as defined in s95A of the  Privacy Act',
			'individual_carer' => 'An individual (such as a carer)',
			'other_infor' => ' Other',
			'category' => '6.8b List the categories of individuals or organisations from which information will be collected. If information will be collected from more than one category, indicate clearly what information or records will be collected from each category.
			Category e.g. carers; hospitals',
			'information_of_records' => 'Type of information or records to be collected (e.g. contact information; complete medical history)',
			'agree_to_provide_information' => '6.8c Have all organisations from which the information is to be collected agreed to provide the information or to allow access to the information?',
			'explain_if_no' => 'Explain how and when the agreement of the disclosing organisation will be obtained.',
			'evidence' => 'If yes, provide evidence of this agreement. Provide details of any conditions imposed by the organisation(s) concerning the release of the information.',
			'seperate_HREC_approval' => '6.8d Is any organisation from which the information will be collected seeking separate HREC approval for disclosure of the information? The organisation(s) disclosing the information is not required by law to obtain separate HREC approval to disclose the information. However, some institutions may wish to obtain separate approval for disclosure for their own purposes.)',
			'routinely_access_info' => '6.8e Does the person who is collecting the information routinely have access to that information?',
			'type_of_info' => 'Health information',
			'sensitive_information' => 'Personal information (other than health information)',
			'health_information' => 'Sensitive Information',
			'type_of_organisation_health' => 'Victorian public sector',
			'victorian_private_sector' => 'Victorian private sector',
			'commonwealth' => 'Commonwealth public sector',
			'other_organisation' => 'Other',
			'type_of_organisation_personal' => 'Victorian public sector',
			'victor_private_sector' => 'Victorian private Sector',
			'common_public_sector' => 'Commonwealth Public Sector',
			'other_personal' => 'Other',
			'type_of_organisation_sensitive' => 'Victorian public sector',
			'vic_private_sector' => 'Victorian private Sector',
			'com_public_sector' => 'Commonwealth Public Sector',
			'other_senstive' => 'Other',
			'privacy_principle_health' => 'HPP 1',
			'HPP1_health' => 'HPP 1, NPP 1, NPP 10',
			'IPP11_health' => 'IPP 11',
			'NPP1_health' => 'NPP 1, NPP 10',
			'privacy_principle_personal' => 'VIPP 1',
			'npp1_personal' => 'NPP 1',
			'ipp11_personal' => 'IPP 11',
			'npp10_personal' => 'NPP 1',
			'privacy_principle_sensitive' => 'VIPP 10',
			'npp0_sensitive' => 'NPP 10',
			'ipp11_senstive' => 'IPP 11',
			'npp10_sensitive' => 'NPP 10',
			'de_intefied_form' => '6.8g Please explain why information will not be collected in a de-identified form.',
			'not_obtain_consent' => '6.8h Please explain why you will not obtain consent from the individual(s) whose information will be collected?',
			'reasons' => '6.8i Give reasons why the proposed collection of information is in the public interest. Note that the public interest in the proposed research must substantially outweigh the public interest in respecting individual privacy.',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('section_id',$this->section_id);
		$criteria->compare('involve_collection',$this->involve_collection,true);
		$criteria->compare('collection_without_consent',$this->collection_without_consent,true);
		$criteria->compare('collection_identifiable',$this->collection_identifiable,true);
		$criteria->compare('identifiable_information',$this->identifiable_information,true);
		$criteria->compare('disclosure_of_identifiable_information',$this->disclosure_of_identifiable_information,true);
		$criteria->compare('collect_info_from_third_party',$this->collect_info_from_third_party,true);
		$criteria->compare('victorian_private',$this->victorian_private,true);
		$criteria->compare('organisation_other',$this->organisation_other,true);
		$criteria->compare('a_data_set',$this->a_data_set,true);
		$criteria->compare('victorian_govt_depart',$this->victorian_govt_depart,true);
		$criteria->compare('victorian_source',$this->victorian_source,true);
		$criteria->compare('commonwealth_agency',$this->commonwealth_agency,true);
		$criteria->compare('agency_from_another_state',$this->agency_from_another_state,true);
		$criteria->compare('organization_defined',$this->organization_defined,true);
		$criteria->compare('individual_carer',$this->individual_carer,true);
		$criteria->compare('other_infor',$this->other_infor,true);
		$criteria->compare('category',$this->category,true);
		$criteria->compare('information_of_records',$this->information_of_records,true);
		$criteria->compare('agree_to_provide_information',$this->agree_to_provide_information,true);
		$criteria->compare('explain_if_no',$this->explain_if_no,true);
		$criteria->compare('evidence',$this->evidence,true);
		$criteria->compare('seperate_HREC_approval',$this->seperate_HREC_approval,true);
		$criteria->compare('routinely_access_info',$this->routinely_access_info,true);
		$criteria->compare('type_of_info',$this->type_of_info,true);
		$criteria->compare('sensitive_information',$this->sensitive_information,true);
		$criteria->compare('health_information',$this->health_information,true);
		$criteria->compare('type_of_organisation_health',$this->type_of_organisation_health,true);
		$criteria->compare('victorian_private_sector',$this->victorian_private_sector,true);
		$criteria->compare('commonwealth',$this->commonwealth,true);
		$criteria->compare('other_organisation',$this->other_organisation,true);
		$criteria->compare('type_of_organisation_personal',$this->type_of_organisation_personal,true);
		$criteria->compare('victor_private_sector',$this->victor_private_sector,true);
		$criteria->compare('common_public_sector',$this->common_public_sector,true);
		$criteria->compare('other_personal',$this->other_personal,true);
		$criteria->compare('type_of_organisation_sensitive',$this->type_of_organisation_sensitive,true);
		$criteria->compare('vic_private_sector',$this->vic_private_sector,true);
		$criteria->compare('com_public_sector',$this->com_public_sector,true);
		$criteria->compare('other_senstive',$this->other_senstive,true);
		$criteria->compare('privacy_principle_health',$this->privacy_principle_health,true);
		$criteria->compare('HPP1_health',$this->HPP1_health,true);
		$criteria->compare('IPP11_health',$this->IPP11_health,true);
		$criteria->compare('NPP1_health',$this->NPP1_health,true);
		$criteria->compare('privacy_principle_personal',$this->privacy_principle_personal,true);
		$criteria->compare('npp1_personal',$this->npp1_personal,true);
		$criteria->compare('ipp11_personal',$this->ipp11_personal,true);
		$criteria->compare('npp10_personal',$this->npp10_personal,true);
		$criteria->compare('privacy_principle_sensitive',$this->privacy_principle_sensitive,true);
		$criteria->compare('npp0_sensitive',$this->npp0_sensitive,true);
		$criteria->compare('ipp11_senstive',$this->ipp11_senstive,true);
		$criteria->compare('npp10_sensitive',$this->npp10_sensitive,true);
		$criteria->compare('de_intefied_form',$this->de_intefied_form,true);
		$criteria->compare('not_obtain_consent',$this->not_obtain_consent,true);
		$criteria->compare('reasons',$this->reasons,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
public function getValues()
        {
            return array('Yes','No');
        }
	public function getInformationCollected()
        {
            return array('A Victorian public health service '=>'A Victorian public health service ',
                'A Victorian private health service provider'=>'A Victorian private health service provider', 
                'An organisation other than a health service provider'=>'An organisation other than a health service provider',
                'A data set under the auspices of the Victorian DHS'=>'A data set under the auspices of the Victorian DHS',
                'A data set under the auspices of another Victorian government department'=>'A data set under the auspices of another Victorian government department',
                'A data set from another Victorian source','A Commonwealth agency'=>'A data set from another Victorian source','A Commonwealth agency',
                'An agency from another state'=> 'An agency from another state',

                'An organisation as defined in s95A of the  Privacy Act'=>'An organisation as defined in s95A of the  Privacy Act',
                'Other'=>'Other'
                
                );
        }
	public function getTypeOfInfo()
        {
            return array(
                'Health information'=>'Health information',
                'Personal information (other than health information)'=>'Personal information (other than health information)',
                'Sensitive information'=>'Sensitive information' );
        }
	 public function getTypeOfOrganisation()
        {
            return array(
                'Victorian public sector'=>'Victorian public sector',
                'Victorian private sector'=>'Victorian private sector', 
                'Commonwealth public sector'=>'Commonwealth public sector',
                'Other'=>'Other');
        }
	 public function getPrivacyPrincipleHealth()
        {
            return array(
                'HPP 1'=>'HPP 1',
                'HPP 1, NPP 1, NPP 10'=>'HPP 1, NPP 1, NPP 10',
                'IPP 11'=>'IPP 11',
                'NPP 1, NPP 10');
        }
	public function getPrivacyPrinciplePersonal()
        {
            return array(
                'VIPP 1'=>'VIPP 1',
                'NPP 1'=>'NPP 1',
                'IPP 11'=>'IPP 11',
                'NPP 1'=>'NPP 1');
        }
	public function getPrivacyPrincipleSensitive()
        {
            return array(
                'VIPP 10'=>'VIPP 10',
                'NPP 10'=>'NPP 10',
                'IPP 11'=>'IPP 11',
                'NPP 10'=>'NPP 10');
        }
	
	}