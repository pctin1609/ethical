<?php

/**
 * This is the model class for table "exclusion_criteria".
 *
 * The followings are the available columns in table 'exclusion_criteria':
 * @property integer $section_id
 * @property string $children
 * @property string $adult
 * @property string $preg_women
 * @property string $breastfd_women
 * @property string $individuals
 * @property string $other
 * @property string $if_yes_specify
 * @property string $justification
 */
class ExclusionCriteria extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ExclusionCriteria the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'exclusion_criteria';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('children, adult, preg_women, breastfd_women, individuals, other', 'required'),
			array('children, adult, preg_women, breastfd_women, individuals, other', 'length', 'max'=>20),
			array('if_yes_specify, justification', 'length', 'max'=>250),
			array('children, adult, preg_women, breastfd_women, individuals, other', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('section_id, children, adult, preg_women, breastfd_women, individuals, other, if_yes_specify, justification', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'section_id' => 'Section',
			'children' => 'Children or young people under the age of 18',
			'adult' => 'Adults under the age of 50',
			'preg_women' => 'Pregnant women',
			'breastfd_women' => 'Women who are breastfeeding',
			'individuals' => 'Individuals who have previously been exposed to radiation',
			'other' => 'Other.',
			'if_yes_specify' => 'Please specify other exclusion criteria ',
			'justification' => 'If you did not answer YES to any of the above, please justify why the group has not been excluded ',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('section_id',$this->section_id);
		$criteria->compare('children',$this->children,true);
		$criteria->compare('adult',$this->adult,true);
		$criteria->compare('preg_women',$this->preg_women,true);
		$criteria->compare('breastfd_women',$this->breastfd_women,true);
		$criteria->compare('individuals',$this->individuals,true);
		$criteria->compare('other',$this->other,true);
		$criteria->compare('if_yes_specify',$this->if_yes_specify,true);
		$criteria->compare('justification',$this->justification,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	 public function getValues()
        {
            return array('Yes','No');
        }
}