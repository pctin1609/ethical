<?php

/**
 * This is the model class for table "fm1_section7".
 *
 * The followings are the available columns in table 'fm1_section7':
 * @property integer $section_id
 * @property string $publish_form
 * @property string $communication_form
 * @property string $participants_results
 * @property string $results_provided
 * @property string $results_provided_others
 * @property string $debriefing_form_required
 * @property string $results_infor_communication
 * @property string $collectivity_community_research
 */
class DebriefingProcedures extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Fm1_Section7 the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'debriefing_procedures';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('publish_form, communication_form, participants_results, results_provided, results_provided_others, debriefing_form_required, results_infor_communication, collectivity_community_research', 'required','message'=>'this field can not be left blank'),
			array('publish_form, communication_form, participants_results, results_provided_others', 'length', 'max'=>250),
			array('results_provided', 'length', 'max'=>25),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('section_id, publish_form, communication_form, participants_results, results_provided, results_provided_others, debriefing_form_required, results_infor_communication, collectivity_community_research', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'publish_form' => '7.1a In what form will you publish this research?',
			'communication_form' => '7.1b In what form will information about results of the project be communicated to participants and / or parents and guardians?',
			'participants_results' => '7.1c How will participants be provided with the results?',
			'results_provided' => '7.1d Will any other persons or organisation be provided with the results?',
			'results_provided_others' => '7.1e How will others be provided with the results?',
			'debriefing_form_required' => '7.2 Is a form of debriefing required because deception has been employed or because the research has aroused emotional feelings?  How will this be arranged?  ',
			'results_infor_communication' => '7.3 How will information about results of any tests be communicated to participants and / or parents and guardians?  What arrangements will be in place to deal with participants’ distress in the case of adverse test results?',
			'collectivity_community_research' => '7.4 If your research involves a collectivity and / or community, will you and if so, how will you provide the information to the community?',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('section_id',$this->section_id);
		$criteria->compare('publish_form',$this->publish_form,true);
		$criteria->compare('communication_form',$this->communication_form,true);
		$criteria->compare('participants_results',$this->participants_results,true);
		$criteria->compare('results_provided',$this->results_provided,true);
		$criteria->compare('results_provided_others',$this->results_provided_others,true);
		$criteria->compare('debriefing_form_required',$this->debriefing_form_required,true);
		$criteria->compare('results_infor_communication',$this->results_infor_communication,true);
		$criteria->compare('collectivity_community_research',$this->collectivity_community_research,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
		
	public function getFormTypes()
	{
	  return array (
              'Thesis'=>'Thesis',
              'Journal article / book / chapter'=>'Journal article / book / chapter',
              'Conference presentation'=>'Conference presentation',
	       'Report to organisation'=>'Report to organisation',
              'On-line web based'=>'On-line web based',
              'Oral presentation'=>'Oral presentation',
              'Other, please specify'=>'Other, please specify');
	}
	
	public function getFormResults()
	{
	  return array (
              'Copy of journal article / book / chapter'=>'Copy of journal article / book / chapter',
              'Summary'=>'Summary',
              'Report to organisation'=>'Report to organisation',
              'On-line web based'=>'On-line web based',
	      'Oral presentation'=>'Oral presentation',
              'Results will not be communicated to participants and / or public'=>'Results will not be communicated to participants and / or public',
              'Other'=>'Other'
              );
	}
	
	public function getParticipantsResults()
	{
	  return array (
              'Participants will be provided with the researchers’ contact details in the Explanatory Statement to request the results'=>'Participants will be provided with the researchers’ contact details in the Explanatory Statement to request the results',
	  'Participants will be advised of the website on which the results will be available'=>'Participants will be advised of the website on which the results will be available',
            'Other, please specify'=>'Other, please specify'
              );
	}
	
	public function getOthersResults()
	{
	  return array (
          'In totally deidentified summary form in which no individual can be identified'=>'In totally deidentified summary form in which no individual can be identified',
	  'In deidentified summary form, but in a manner which may allow individuals to be identified'=>'In deidentified summary form, but in a manner which may allow individuals to be identified',
	  'In identified form, or in a manner which may allow participants to be identified '=>'In identified form, or in a manner which may allow participants to be identified ');
	}
	
	public function getValues()
	{
	  return array('Yes','No');
	 
	}
	
	public function getArea()
	{ 
	  return array ('rows'=>5, 
	         'cols'=>100,
			 'size'=>'100%',
			 'maxlength'=>250, 
			 'style'=>'width: 98%');
	}
}