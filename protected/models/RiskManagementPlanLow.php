<?php

/**
 * This is the model class for table "risk_management_plan_low".
 *
 * The followings are the available columns in table 'risk_management_plan_low':
 * @property integer $section_id
 * @property string $assessment
 */
class RiskManagementPlanLow extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return RiskManagementPlanLow the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'risk_management_plan_low';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('assessment', 'required'),
			array('assessment', 'length', 'max'=>25),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('section_id, assessment', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'section_id' => 'Section',
			'assessment' => '12. It is the responsibility of the Chief Investigator to conduct a Risk Assessment and Risk Management Plan where appropriate for any Monash student researcher or co-investigator who may be involved in the data collection process.<br> Has an appropriate Risk Assessment taken place for this project?',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('section_id',$this->section_id);
		$criteria->compare('assessment',$this->assessment,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
public function getRiskAssessment()
        {
            return array('Yes It is'=>'Yes It is','No'=>'No','Not Yet'=>'Not Yet','Not Required'=>'Not Required');
        }
	}