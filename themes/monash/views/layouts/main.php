<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />
        
        <?php cs()->registerCoreScript('jquery') ?>
        <!-- blueprint CSS framework -->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/screen.css" media="screen, projection" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print.css" media="print" />
        <!--[if lt IE 8]>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/ie.css" media="screen, projection" />
        <![endif]-->

        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/main.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/form.css" />

        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    </head>

    <body>

        <div class="container" id="page">
            <?php
            foreach (array_slice(Yii::app()->user->getFlashes(), 0, -1) as $class => $message) {
                echo CHtml::tag('div', array('class' => "flash-$class"), $message);
            }
            ?>
            <div id="topMenu">
                <?php
                //if no more items for top menu are going to be used use a link instead of ul li a,(Cmenu) ensur you include a condintion if he is loged in and modify css

                if (!Yii::app()->user->isGuest) {
                    echo CHtml::link('Logout (' . Yii::app()->user->name . ')', array('/site/logout'));
                }
                ?>
            </div>
            <div id="header">
                <img src="<?php echo app()->getBaseUrl(); ?>/themes/monash/img/banner.jpg" usemap="#planetmap"/>
                <map name="planetmap">
                    <area shape="rect" coords="50,0,160,126" alt="Sun" href="<?php echo (!user()->isGuest && user()->isAdmin ? Yii::app()->createAbsoluteUrl('/admin/profile') : Yii::app()->createAbsoluteUrl('/site/index')) ?>">
                </map>
            </div><!-- header -->


            <div id="mainmenu">
                <?php
                $this->widget('zii.widgets.CMenu', array(
                    'activeCssClass' => 'active',
                    'activateParents' => true,
                    'items' => array(
                        array('label' => 'Home', 'url' => array('/site/index'), 'visible' => Yii::app()->user->isGuest),
                        array('label' => 'Home', 'url' => array('/site/home'), 'visible' => !Yii::app()->user->isGuest && !Yii::app()->user->isAdmin),
						//array('label' => 'Home', 'url' => array('/site/home'), 'visible' => !Yii::app()->user->isGuest&& Yii::app()->user->!Admin),
                        array('label' => 'Home', 'url' => array('/admin/profile/index'), 'visible' => !Yii::app()->user->isGuest && Yii::app()->user->isAdmin),
						array('label' => 'Profile', 'url' => array('/user/index'), 'visible' => !Yii::app()->user->isGuest),
                        array('label' => 'Downloads', 'url' => array('/application/downloads'), 'visible' => !Yii::app()->user->isGuest, array('class' => 'lol')),
                        array(
                            'label' => 'Forms',
                            'visible' => !Yii::app()->user->isGuest,
                            'active' => $this->id == 'applicationForm1' || $this->id == 'applicationLowRisk' || $this->id == 'applicationMultiCentre' || $this->id == 'applicationRadiation',
                            'url' => array(''),
                            'items' => array(
                                array('label' => 'Form 1', 'url' => array('/applicationForm1/index')),
                                array('label' => 'Low Risk Research', 'url' => array('/applicationLowRisk/index')),
                                array('label' => 'MultiCentre Research ', 'url' => array('/applicationMultiCentre/index')),
                                array('label' => 'Research Involving Radiation', 'url' => array('/applicationRadiation/index')),
                            ),
                        ),
                        /* array('label' => 'Application', 'url' => array('/application/index'), 'visible' => !Yii::app()->user->isGuest), */

                        array('label' => 'Login', 'url' => array('/site/login'), 'visible' => Yii::app()->user->isGuest),
                          array('label' => 'Contact Us', 'url' => array('/site/contact')),
						array('label' => 'About Us', 'url' => array('/site/page', 'view' => 'about')),
                      array('label' => 'Messages', 'url' => array('/message/compose'), 'visible' => !Yii::app()->user->isGuest,
					  'active' => $this->id=='compose'|| $this->id=='inbox'||$this->id=='sent'),
                        array('label' => 'Register', 'url' => array('/site/register'), 'visible' => Yii::app()->user->isGuest),
                        array('label' => 'Admin Email', 'url' => array('/admin/configuration/adminEmail'), 'visible' => !Yii::app()->user->isGuest && Yii::app()->user->isAdmin),
                        array('label' => 'Degrees', 'url' => array('/admin/degree/index'), 'visible' => !Yii::app()->user->isGuest && Yii::app()->user->isAdmin),
                    ),
                ));
                ?>
            </div><!-- mainmenu -->


                <?php echo $content; ?>

            <div class="clear"></div>



            <div id="footer">
                <ul>
                    <li class="category">  <?php echo CHtml::link('Development', array('site/development')); ?> </li>
                    <li> <?php echo CHtml::link('Report General Issue', array('site/development')); ?></li>
                    <li> <?php echo CHtml::link('Report Security Issue', array('site/development')); ?></li>
                    <li> <?php echo CHtml::link('Latest Updates', array('site/development')); ?></li>

                </ul>

                <ul>
                    <li class="category"><?php echo CHtml::link('Contact us', array('site/contact')); ?> </li>
                    <li><?php echo CHtml::link('Online Query', array('site/contact')); ?></li>
                    <li><?php echo CHtml::link('Postal Address', array('site/contact')); ?></li>
                    <li><?php echo CHtml::link('Street Address', array('site/contact')); ?></li>
                </ul>

                <ul>
                    <li class="category"> <?php echo CHtml::link('About us', array('site/page&view=about')); ?></li>  
                    <li><?php echo CHtml::link('Ethical Clearance', array('site/page&view=about')); ?> </li>
                    <li><?php echo CHtml::link('Eagle South Africa', array('site/page&view=about')); ?></li>
                </ul>
                <ul>
                    <li class="category"> <?php echo CHtml::link('Help', array('site/page&view=help')); ?> </li> 
                    <li><?php echo CHtml::link('How to Register', array('site/page&view=help')); ?> </li>
                    <li><?php echo CHtml::link('What Forms to Fill', array('site/page&view=help')); ?> </li>
                    <li><?php echo CHtml::link('FAQ', array('site/page&view=help')); ?> </li>
                </ul>
            </div><!-- footer -->
        </div><!-- page -->


    </body>
</html>